# Wp2Leads Instant transfer Modules

## Update checker

For checking modules update we are using https://github.com/YahnisElsts/plugin-update-checker

### Update Gravity Forms Plugin
- Download file `wp-content/uploads/modules-update/wp2leads_itm_gf.json`
- Edit content:

```
    {
      "name": "Wp2Leads Instant transfer module for Gravity Form Plugin",
      "version": "{version}", # set current plugin version 0.0.1
      "download_url": "https://saleswonder.biz/wp2leads-itm/wp2leads_itm_gf.zip", # set current plugin version download URL
    
      "homepage": "https://saleswonder.biz/", # set last updated date
      "requires": "5.0",
      "tested": "5.4",
      "last_updated": "2020-04-26 16:00:00",
    
      "author": "Tobias Conrad",
      "author_homepage": "https://saleswonder.biz/"
    }
```
- Download last plugin version from `wp-content/plugins/wp2leads_itm_gf`
- Compress it and set a name `wp2leads_itm_gf.zip`
- Structure should be like this

```
    wp2leads_itm_gf.zip
    |_wp2leads_itm_gf # main plugin folder
      |_ wp2leads_itm_gf.php
      |_ functions.php
      |_ ... other files and folders ...

```
- F.e. new plugin version 0.2.0

```
    {
      ...
      "version": "0.2.0",
      "download_url": "https://saleswonder.biz/wp2leads-itm/wp2leads_itm_gf.zip", # set current plugin version download URL
      ...
    }
```
- Zip file name `wp2leads_itm_gf.zip`

### Update Amelia Plugin
- Download file `wp-content/uploads/modules-update/wp2leads_itm_amelia.json`
- Edit content:

```
    {
      "name": "Wp2Leads Instant transfer module for Amelia Booking Plugin",
      "version": "{version}", # set current plugin version like 0.0.1
      "download_url": "https://saleswonder.biz/wp2leads-itm/wp2leads_itm_amelia-{version}.zip", # set current plugin version download URL
    
      "homepage": "https://saleswonder.biz/",
      "requires": "5.0",
      "tested": "5.4",
      "last_updated": "2020-04-26 16:00:00", # set last updated date
    
      "author": "Tobias Conrad",
      "author_homepage": "https://saleswonder.biz/"
    }
```
- Download last plugin version from `wp-content/plugins/wp2leads_itm_amelia`
- Compress it and set a name `wp2leads_itm_amelia.zip`
- Structure should be like this

```
    wp2leads_itm_amelia.zip
    |wp2leads_itm_amelia # main plugin folder
      |_ wp2leads_itm_amelia.php
      |_ functions.php
      |_ ... other files and folders ...

```
- F.e. new plugin version 0.2.0

```
    {
      ...
      "version": "0.2.0",
      "download_url": "https://saleswonder.biz/wp2leads-itm/wp2leads_itm_amelia.zip", # set current plugin version download URL
      ...
    }
```
- Zip file name `wp2leads_itm_amelia.zip`