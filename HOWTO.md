# How to create new Instant transfer module

## 1. Instant transfer modules list
To get all available transfer modules list we are using method `get_transfer_modules_class_names` of `Wp2leads_Transfer_Modules` class

**wp-content/plugins/wp2leads/includes/class-wp2leads-transfer-modules.php:36**
```php
<?php
class Wp2leads_Transfer_Modules {
  // code before
  public static function get_transfer_modules_class_names() {
      $transfer_modules = array();

      return apply_filters( 'wp2leads_transfer_modules', $transfer_modules );
  }
  // Code after
}
?>
```

## 2. Add new instant transfer module to list
To add new module to existed modules list we need to hook into `'wp2leads_transfer_modules'` filter
```php
<?php
add_filter('wp2leads_transfer_modules', 'wp2leads_transfer_module_example_add');

function wp2leads_transfer_module_example_add($transfer_modules) {
    $transfer_modules['wp2leads_transfer_module_example_key'] = 'Wp2leads_Transfer_Module_Example_Class';

    return $transfer_modules;
}
?>
```

## 3. Before creating new instant transfer module for plugin
We need to identify an event on which user data should be transfered to KT.
It should be some hook, that we can hook in our start transfer action.
F.e. for woocommerce we are using `'woocommerce_checkout_order_processed'` (new order created) and `'woocommerce_order_status_changed'` (order status changed) actions.
We need to check which data is available in this hook. In woocommerce it is `$order_id` which will be saved to DB as `posts.ID` column. So `posts.ID` must be in DB Entries of the map we are creating transfer module for.

## 4. Create new instant transfer module
New module is a new class. I'm using separate file for each module, but it's not required.

**class-wp2leads-transfer-module-class.php**
```php
<?php
class Wp2leads_Transfer_Module_Example_Class {
    // this parameter is required just set it to some unique value 
    // just unique identifier of module (required)
    private static $key = 'wp2leads_transfer_module_example_key'; 
    
    // this parameter is required just set it to some unique value 
    // required table column that must be in map DB Entries
    // for woocommerce it would be posts.ID that refer to order number
    private static $required_column = 'table.column';

    // this method is required just change and return some unique value 
    public static function get_label() {
        return __('Plugin name: Module name', 'slug');
    }

    // this method is required just change and return some unique value 
    public static function get_description() {
        return __('Some short description for what is module do and when run', 'slug');
    }

    // this method is required and return $required_column from public request
    public static function get_required_column() {
        return self::$required_column;
    }

    // this method is required and return more details on what modul do, when run
    // and some requirements
    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('Some description.', 'slug') ?></p>
        <p><?php _e('Some more description.', 'slug') ?></p>
        <p><?php _e('Requirement: <strong>table.column</strong> column withing selected data.', 'slug') ?></p>
        <?php

        return ob_get_clean();
    }

    // here we are hooks into some events and maybe start data transferring
    // 
    public static function transfer_init() {
        // some add_action
        add_action('some_third_party_plugin_hook_one', 'Wp2leads_Transfer_Module_Example_Class::module_example_method_one', 10, 2);
        add_action('some_third_party_plugin_hook_two', 'Wp2leads_Transfer_Module_Example_Class::module_example_method_two', 10, 2);
        // maybe add more if needed
    }

    // this method is required
    // it will check if any map with enabled module exists
    // and run transferring in background 
    public static function transfer($id) {
        // here we check if Wp2Leads activated and all classes we need exist
        // if transfer module is separate plugin it's better to check this in main plugin file
        // and exit if Wp2Leads not installed
        if (!class_exists('Wp2leads_Transfer_Modules') || !class_exists('Wp2leads_Background_Module_Transfer')) {
            return;
        }
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column, // set here $required_column value
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }

    // custom method hooked into first action
    // need to be created for each plugin according to conditions
    public static function module_example_method_one($parameter_one, $parameter_two) {
        // maybe add some logic here to check if translation needed
        // for example if post_status is the one that we need or other
        $some_condition = true;

        if ($some_condition) {
            self::transfer($parameter_one);
        }
    }

    // custom method hooked into second action
    // need to be created for each plugin according to conditions
    public static function module_example_method_two($parameter_one, $parameter_two) {
        // maybe add some logic here to check if translation needed
        // for example if post_status is the one that we need or other
        $some_condition = true;

        if ($some_condition) {
            self::transfer($parameter_one);
        }
    }
}

// Add our module to available module lists
add_filter('wp2leads_transfer_modules', 'wp2leads_transfer_module_example_add');

function wp2leads_transfer_module_example_add($transfer_modules) {
    $transfer_modules['wp2leads_transfer_module_example_key'] = 'Wp2leads_Transfer_Module_Example_Class';

    return $transfer_modules;
}
?>
```

## 5. Example for Woocommerce
```php
<?php
class Wp2leads_Transfer_Woo_Order_Status_Changed {
    private static $key = 'woo_order_status_changed';

    // refers to order number
    private static $required_column = 'posts.ID';

    public static function transfer_init() {
        // Order created on checkout
        add_action( 'woocommerce_checkout_order_processed', 'Wp2leads_Transfer_Woo_Order_Status_Changed::checkout_order_processed', 10, 3 );

        // Order status changed by admin
        add_action('woocommerce_order_status_changed', 'Wp2leads_Transfer_Woo_Order_Status_Changed::order_status_changed', 50, 4);
    }

    public static function get_label() {
        return __('Woocommerce: Order status changed', 'wp2leads');
    }

    public static function get_description() {
        return __('This module will transfer user data once order will be created or order\'s status will be changed');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Woocommerce orders maps.', 'wp2leads') ?></p>
        <p><?php _e('Once new order created or existed order\'s status changed user data will be transfered to KT account.', 'wp2leads') ?></p>
        <p><?php _e('Requirement: <strong>posts.ID</strong> column withing selected data.', 'wp2leads') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function checkout_order_processed($order_id, $order_data, $order) {
        $id = $order_id;

        self::transfer($id);
    }

    public static function order_status_changed($order_id, $from, $to, $order_data) {
        $id = $order_id;

        self::transfer($id);
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => 'posts.ID',
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}


add_filter('wp2leads_transfer_modules', 'wp2leads_transfer_woo_order_status_changed_module');
function wp2leads_transfer_woo_order_status_changed_module($transfer_modules) {
    $transfer_modules['woo_order_status_changed'] = 'Wp2leads_Transfer_Woo_Order_Status_Changed';

    return $transfer_modules;
}
```
