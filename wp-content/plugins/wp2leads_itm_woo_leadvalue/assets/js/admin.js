(function( $ ) {
    $(document.body).on('click', '#wp2leads_itm_woo_leadvalue_initial_calculation', function() {
        var data = {
            action: 'wp2leads_itm_woo_leadvalue_initial_calculation',
            calculation: 'leadvalue',
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#wp2leads_itm_woo_leadvalue_total_orders_initial_calculation', function() {
        var data = {
            action: 'wp2leads_itm_woo_leadvalue_initial_calculation',
            calculation: 'total_orders',
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#wp2leads_itm_woo_leadvalue_terminate_calculation', function() {
        var data = {
            action: 'wp2leads_itm_woo_leadvalue_terminate_calculation',
            calculation: 'leadvalue',
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#wp2leads_itm_woo_leadvalue_total_orders_terminate_calculation', function() {
        var data = {
            action: 'wp2leads_itm_woo_leadvalue_terminate_calculation',
            calculation: 'total_orders',
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#wp2leads_itm_webinarignition_delete_apikey', function() {
        var btn = $(this);
        var confirm_message = btn.data('confirm');
        var confirmed = confirm(confirm_message);

        if (confirmed) {
            var data = {
                action: 'wp2leads_itm_webinarignition_delete_apikey'
            };

            ajaxRequest(
                data,
                function() {},
                function() {}
            );
        }

    });

    $(document).ready(function () {
        var wp2leads_itm_woo_calculation_progress = $('#wp2leads_itm_woo_calculation_progress');
        var wp2leads_itm_woo_calculation_total_orders_progress = $('#wp2leads_itm_woo_calculation_total_orders_progress');

        if (wp2leads_itm_woo_calculation_progress.length || wp2leads_itm_woo_calculation_total_orders_progress.length) {
            setTimeout(periodical_worker, 5000);
        }
    });

    function periodical_worker() {
        let wp2leads_itm_woo_calculation_progress = $('#wp2leads_itm_woo_calculation_progress');
        let wp2leads_itm_woo_calculation_total_orders_progress = $('#wp2leads_itm_woo_calculation_total_orders_progress');
        let calculation = 'leadvalue';

        if (wp2leads_itm_woo_calculation_progress.length) {
            calculation = 'leadvalue';
        } else if (wp2leads_itm_woo_calculation_total_orders_progress.length) {
            calculation = 'total_orders';
        }
        var data = {
            action: 'wp2leads_itm_woo_leadvalue_calculation_progress',
            calculation,
        };

        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function(response) {
                var decoded;

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        if (wp2leads_itm_woo_calculation_progress.length) {
                            $('#wp2leads_itm_woo_calculation_progress').text(decoded.progress_message);
                        } else if (wp2leads_itm_woo_calculation_total_orders_progress.length) {
                            $('#wp2leads_itm_woo_calculation_total_orders_progress').text(decoded.progress_message);
                        }

                        $('.wptl-progress-bar span').css('width', decoded.progress_percent + '%')

                        if (decoded.progress_reload) {
                            setTimeout(function() {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                } else {

                }
            },
            complete: function() {
                // Schedule the next request when the current one's complete
                setTimeout(periodical_worker, 500);
            }
        });
    }

    function ajaxRequest(data, cb, cbError) {
        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function (response) {
                var decoded;

                console.log(response);

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (decoded.url) {
                            window.location.replace(decoded.url);
                        } else if (decoded.reload) {
                            window.location.reload();
                        }

                        if (typeof cb === 'function') {
                            cb();
                        }
                    } else {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (typeof cbError === 'function') {
                            cbError();
                        }
                    }
                } else {
                    alert('Something went wrong');
                }
            }
        });
    }
})( jQuery );
