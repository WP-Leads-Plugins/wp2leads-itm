<?php

function wp2leads_itm_woo_leadvalue_load_plugin_textdomain() {
    add_filter( 'plugin_locale', 'wp2leads_itm_woo_leadvalue_check_de_locale');

    load_plugin_textdomain(
        'wp2leads_itm_woo_leadvalue',
        false,
        dirname( plugin_basename( __FILE__ ) ) . '/languages/'
    );

    remove_filter( 'plugin_locale', 'wp2leads_itm_woo_leadvalue_check_de_locale');
}

function wp2leads_itm_woo_leadvalue_check_de_locale($domain) {
    $site_lang = get_user_locale();
    $de_lang_list = array(
        'de_CH_informal',
        'de_DE_formal',
        'de_AT',
        'de_CH',
        'de_DE'
    );

    if (in_array($site_lang, $de_lang_list)) return 'de_DE';
    return $domain;
}

function wp2leads_itm_woo_leadvalue_requirement() {
    $wp2leads_installed = function_exists('run_wp2leads') && class_exists('Wp2leads_Background_Module_Transfer');
    if (!$wp2leads_installed) return false;
    $woocommerce = wp2leads_itm_woo_leadvalue_is_plugin_activated( 'woocommerce', 'woocommerce.php' );
    if (!$woocommerce) return false;

    return true;
}

function wp2leads_itm_woo_leadvalue_is_plugin_activated( $plugin_folder, $plugin_file ) {
    if ( wp2leads_itm_woo_leadvalue_is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
    else return wp2leads_itm_woo_leadvalue_is_plugin_active_by_file( $plugin_file );
}

function wp2leads_itm_woo_leadvalue_is_plugin_active_simple( $plugin ) {
    return (
        in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
        ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
    );
}

function wp2leads_itm_woo_leadvalue_is_plugin_active_by_file( $plugin_file ) {
    foreach ( wp2leads_itm_woo_leadvalue_get_active_plugins() as $active_plugin ) {
        $active_plugin = explode( '/', $active_plugin );
        if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
    }

    return false;
}

function wp2leads_itm_woo_leadvalue_get_active_plugins() {
    $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
    if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

    return $active_plugins;
}

function wp2leads_itm_woo_leadvalue_init() {
    $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
    $is_cron = defined( 'DOING_CRON' ) && DOING_CRON;
    $is_favicon = strpos($_SERVER['REQUEST_URI'], 'favicon.ico') !== false;

    if ($is_ajax || $is_favicon || $is_cron) return;

    wp2leads_itm_woo_leadvalue_check_version();
}

function wp2leads_itm_woo_leadvalue_check_version() {
    $version = get_option( 'wp2leads_itm_woo_leadvalue_version' );
    $dbversion = get_option( 'wp2leads_itm_woo_leadvalue_db_version' );

    if (
        empty($version) || version_compare( $version, WP2LITM_WOO_LEADVALUE_VERSION, '<' ) ||
        empty($dbversion) || version_compare( $dbversion, WP2LITM_WOO_LEADVALUE_DB_VERSION, '<' )
    ) {
        wp2leads_itm_woo_leadvalue_install();
        do_action( 'wp2leads_itm_woo_leadvalue_updated' );
    }
}

function wp2leads_itm_woo_leadvalue_install() {
    Wp2leadsItmWooLeadvalueModel::create_table('woo_leadvalue_customers');
    Wp2leadsItmWooLeadvalueModel::create_table('woo_leadvalue_total_orders');
    wp2leads_itm_woo_leadvalue_update_version();
}

function wp2leads_itm_woo_leadvalue_update_version() {
    delete_option( 'wp2leads_itm_woo_leadvalue_version' );
    add_option( 'wp2leads_itm_woo_leadvalue_version', WP2LITM_WOO_LEADVALUE_VERSION );

    delete_option( 'wp2leads_itm_woo_leadvalue_db_version' );
    add_option( 'wp2leads_itm_woo_leadvalue_db_version', WP2LITM_WOO_LEADVALUE_DB_VERSION );
}

function wp2leads_itm_woo_leadvalue_user_transfered($map_id, $email, $data, $tags, $detach_tags) {
    $map = MapsModel::get($map_id);

    if (empty($map)) {
        return false;
    }

    $map_mapping = unserialize($map->mapping);
    $is_map_leadvalue = false;

    foreach ($map_mapping['selects_only'] as $map_column) {
        if (
            'wp2leads_itm_woo_leadvalue_customers.email' === trim($map_column) ||
            'wp2leads_itm_woo_leadvalue_customers.total_amount_to_transfer' === trim($map_column)
        ) {
            $is_map_leadvalue = true;
            break;
        }
    }

    if (!$is_map_leadvalue) {
        return false;
    }

    $customer = Wp2leadsItmWooLeadvalueModel::get_customer($email);

    if (!$customer || empty($customer['total_amount_to_transfer'])) {
        return false;
    }

    $data = array(
        'ID' => $customer['ID'],
        'total_amount_last_transfer' => $customer['total_amount_to_transfer'],
        'total_amount_to_transfer' => '0'
    );

    return Wp2leadsItmWooLeadvalueModel::create_customer($data);
}

function wp2leads_itm_woo_leadvalue_modules_init() {
    include_once 'transfer-modules/Wp2leads_Woo_Leadvalue_Transfer.php';
    include_once 'transfer-modules/Wp2leads_Woo_Leadvalue_Total_Orders_Transfer.php';
}

function wp2leads_itm_woo_order_status_changed ($order_id, $status_from, $status_to, $order) {
    if ($status_from !== 'completed' && $status_to !== 'completed') return;
    $email = $order->get_billing_email();
    Wp2leadsItmWooLeadvalueTotalOrdersBackgroundRecalculate::bg_process([$email], true);
}
