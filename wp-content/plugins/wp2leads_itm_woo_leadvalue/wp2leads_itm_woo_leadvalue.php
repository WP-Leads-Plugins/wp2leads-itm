<?php
/**
 * Plugin Name:     WP2LEADS ITM for WooCommerce LeadValue
 * Description:		Wp2Leads Instant transfer module for WooCommerce summarize your Klick-Tipp LeadValue field with your customer orders amount
 * Version:         1.1.6
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_woo_leadvalue
 *
 * Requires at least: 5.0
 * Tested up to: 5.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_WOO_LEADVALUE_VERSION', '1.1.6' );
define( 'WP2LITM_WOO_LEADVALUE_DB_VERSION', '1.1.5' );

if ( ! defined( 'WP2LITM_WOO_LEADVALUE_PLUGIN_FILE' ) ) {
    define( 'WP2LITM_WOO_LEADVALUE_PLUGIN_FILE', __FILE__ );
}

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_woo_leadvalue.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_woo_leadvalue'
);

require_once('functions.php');

add_action( 'plugins_loaded', 'wp2leads_itm_woo_leadvalue_load_plugin_textdomain' );

if (!wp2leads_itm_woo_leadvalue_requirement()) return;

require_once 'includes/Wp2leadsItmWooLeadvalueModel.php';
require_once 'includes/Wp2leadsItmWooLeadvalueAjax.php';

add_action( 'init', 'wp2leads_itm_woo_leadvalue_init' );

require_once 'includes/Wp2leadsItmWooLeadvalueBackgroundRecalculate.php';
require_once 'includes/Wp2leadsItmWooLeadvalueTotalOrdersBackgroundRecalculate.php';

if (is_admin()) {
    require_once 'includes/Wp2leadsItmWooLeadvalueAdmin.php';
}

add_action('wp2leads_transfer_user_created', 'wp2leads_itm_woo_leadvalue_user_transfered', 15, 5);
add_action('wp2leads_transfer_user_updated', 'wp2leads_itm_woo_leadvalue_user_transfered', 15, 5);

// Woocommerce hooks
add_action('woocommerce_order_status_changed', 'wp2leads_itm_woo_order_status_changed', 50, 4);

wp2leads_itm_woo_leadvalue_modules_init();
