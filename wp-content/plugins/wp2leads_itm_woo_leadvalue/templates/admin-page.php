<?php
$map_name = !empty($_GET['map_name']) ? str_replace('_', ' ', sanitize_text_field($_GET['map_name'])) : '';

if (!empty($map_name)) {
    global $wpdb;
    $table = $wpdb->prefix . 'wp2l_maps';
    $sql = "SELECT * FROM {$table} WHERE name LIKE '%{$map_name}%' ORDER BY id DESC";
    $result = $wpdb->get_results($sql, ARRAY_A);

    if (!empty($result)) {
        $map_id = $result[0]['id'];
    } else {
        $map_id = get_option('wp2leads_itm_woo_leadvalue_last_map');
    }
} else {
    $map_id = get_option('wp2leads_itm_woo_leadvalue_last_map');
}

if (!empty($map_id)) {
    $map = MapsModel::get($map_id);

    if (empty($map)) {
        $map_id = '';
    } else {
        delete_option('wp2leads_itm_woo_leadvalue_last_map');
        add_option('wp2leads_itm_woo_leadvalue_last_map', $map_id);
    }
}
?>
<div class="wrap">
    <h1><?php _e('Woocommerce order total to Klick-Tipp LeadValue', 'wp2leads_itm_woo_leadvalue'); ?></h1>
    <?php settings_errors(); ?>

    <?php
    if (Wp2leadsItmWooLeadvalueModel::is_calculation_in_progress()) {
        $progress_percent = Wp2leadsItmWooLeadvalueModel::get_calculation_progress_percent();
        ?>
        <div class="wptl-settings-group">
            <div class="wptl-settings-group-header">
                <h3><?php _e("Calculation in progress" , 'wp2leads_itm_woo_leadvalue') ?></h3>
            </div>

            <div class="wptl-settings-group-body">
                <div class="wptl-row">
                    <div class="wptl-col-xs-12">
                        <div class="wptl-row">
                            <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                                <p>
                                    <?php _e('We calculating your user data.', 'wp2leads_itm_woo_leadvalue'); ?>

                                    <strong id="wp2leads_itm_woo_calculation_progress"><?php echo Wp2leadsItmWooLeadvalueModel::get_calculation_in_progress_message(); ?></strong>. <?php _e('You can close or reload a page. Calculation will continue in background.', 'wp2leads_itm_woo_leadvalue'); ?> <?php _e('If you have more than one customer done you can start working with a map.', 'wp2leads_itm_woo_leadvalue'); ?>
                                </p>
                            </div>

                            <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                                <?php
                                if (!empty($map_id)) {
                                    ?>
                                    <a target="_blank" class="button button-primary" href="?page=wp2l-admin&tab=map_to_api&active_mapping=<?php echo $map_id ?>">
                                        <?php echo __('Edit map', 'wp2leads_itm_woo_leadvalue'); ?>
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    <a target="_blank" class="button button-primary" href="?page=wp2l-admin">
                                        <?php echo __('Install map', 'wp2leads_itm_woo_leadvalue'); ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="wptl-col-xs-12">
                        <div class="wptl-row">
                            <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                                <div class="wptl-progress-bar">
                                    <span style="width: <?php echo $progress_percent ?>%"></span>
                                </div>

                            </div>

                            <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                                <p>
                                    <button
                                            id="wp2leads_itm_woo_leadvalue_terminate_calculation"
                                            type="button"
                                            class="button button-primary"
                                    >
                                        <?php echo __('Terminate calculation', 'wp2leads_itm_woo_leadvalue'); ?>
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="wptl-col-xs-12">
                        <p>
                            <?php _e('If calculation progress will not updating for a few minutes it means that background process failed for some reason. Please click "Terminate calculation" button and start recalculation again.', 'wp2leads_itm_woo_leadvalue'); ?>
                            <strong><small><?php _e('In some cases after first 10 customers progress can stop and you need to wait.', 'wp2leads_itm_woo_leadvalue'); ?></small></strong>
                        </p>

                        <p>
                            <strong><?php _e('Important!', 'wp2leads_itm_woo_leadvalue') ?></strong>
                            <?php _e('On map to api tab please click "Transfer current" button only once per contact!', 'wp2leads_itm_woo_leadvalue') ?>
                            <br>
                            <?php _e('If you click and transfer again you will transfer the last amount again and the Leadvalue will be inaccurate.', 'wp2leads_itm_woo_leadvalue') ?>
                            <br>
                            <?php _e('Reason behind: We are caching the map to api tab for more speed and the cache is only renewed on reloading the browser tab.', 'wp2leads_itm_woo_leadvalue') ?>
                            <strong><?php _e('Transfer same contact again without reloading the tab will add the last amount to transfer again.', 'wp2leads_itm_woo_leadvalue') ?></strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } elseif (Wp2leadsItmWooLeadvalueModel::is_initial_calculation_done()) {
        ?>
        <div class="wptl-settings-group">
            <div class="wptl-settings-group-header">
                <h3><?php _e("You ready to go." , 'wp2leads_itm_webinarignition') ?></h3>
            </div>

            <div class="wptl-settings-group-body">
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-4">
                        <h4>
                            <?php _e('Your customers total orders amount ready for transfer.', 'wp2leads_itm_woo_leadvalue') ?>
                        </h4>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <?php _e('Your customer\'s orders total amount already calculated and you can use it for transfering to Klick-Tipp.', 'wp2leads_itm_woo_leadvalue') ?>
                        </p>

                        <p>
                            <strong><?php _e('Important!', 'wp2leads_itm_woo_leadvalue') ?></strong>
                            <?php _e('On map to api tab please click "Transfer current" button only once per contact!', 'wp2leads_itm_woo_leadvalue') ?>
                            <br>
                            <?php _e('If you click and transfer again you will transfer the last amount again and the Leadvalue will be inaccurate.', 'wp2leads_itm_woo_leadvalue') ?>
                            <br>
                            <?php _e('Reason behind: We are caching the map to api tab for more speed and the cache is only renewed on reloading the browser tab.', 'wp2leads_itm_woo_leadvalue') ?>
                            <strong><?php _e('Transfer same contact again without reloading the tab will add the last amount to transfer again.', 'wp2leads_itm_woo_leadvalue') ?></strong>
                        </p>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-2 wptl-col-lg-2">
                        <p>
                            <button
                                    id="wp2leads_itm_woo_leadvalue_initial_calculation"
                                    type="button"
                                    class="button button-primary"
                            >
                                <?php echo __('Recalculate again', 'wp2leads_itm_woo_leadvalue'); ?>
                            </button>

                            <?php
                            if (!empty($map_id)) {
                                ?>
                                <a target="_blank" href="?page=wp2l-admin&tab=map_to_api&active_mapping=<?php echo $map_id ?>" class="button button-primary">
                                    <?php echo __('Edit map', 'wp2leads_itm_woo_leadvalue'); ?>
                                </a>
                                <?php
                            } else {
                                ?>
                                <a target="_blank" class="button button-primary" href="?page=wp2l-admin">
                                    <?php echo __('Install map', 'wp2leads_itm_woo_leadvalue'); ?>
                                </a>
                                <?php
                            }
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="wptl-settings-group">
            <div class="wptl-settings-group-header">
                <h3><?php _e("Welcome. Let's start working" , 'wp2leads_itm_woo_leadvalue') ?></h3>
            </div>

            <div class="wptl-settings-group-body">
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-4">
                        <h4>
                            <?php _e('Before going any further let\'s make one simple step.', 'wp2leads_itm_woo_leadvalue') ?>
                        </h4>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <?php _e('We recomend you to calculate your customers total orders amount. It will allow you to start manual transfering with correct data, so after setting up your map, you can run initial transfer for all your users. Please click button <strong>"Start calculation"</strong>', 'wp2leads_itm_woo_leadvalue') ?>
                        </p>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-2 wptl-col-lg-2">
                        <p>
                            <button
                                    id="wp2leads_itm_woo_leadvalue_initial_calculation"
                                    type="button"
                                    class="button button-primary"
                            >
                                <?php echo __('Start calculation', 'wp2leads_itm_woo_leadvalue'); ?>
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
