<?php
$map_name = !empty($_GET['map_name']) ? str_replace('_', ' ', sanitize_text_field($_GET['map_name'])) : '';

if (!empty($map_name)) {
    global $wpdb;
    $table = $wpdb->prefix . 'wp2l_maps';
    $sql = "SELECT * FROM {$table} WHERE name LIKE '%{$map_name}%' ORDER BY id DESC";
    $result = $wpdb->get_results($sql, ARRAY_A);

    if (!empty($result)) {
        $map_id = $result[0]['id'];
    } else {
        $map_id = get_option('wp2leads_itm_woo_total_orders_last_map');
    }
} else {
    $map_id = get_option('wp2leads_itm_woo_total_orders_last_map');
}

if (!empty($map_id)) {
    $map = MapsModel::get($map_id);

    if (empty($map)) {
        $map_id = '';
    } else {
        delete_option('wp2leads_itm_woo_total_orders_last_map');
        add_option('wp2leads_itm_woo_total_orders_last_map', $map_id);
    }
}
?>

<div class="wrap">
    <h1><?php _e('Woocommerce to Klick-Tipp Total Customer Orders', 'wp2leads_itm_woo_leadvalue'); ?></h1>
    <?php settings_errors(); ?>

    <div class="wptl-settings-group">
    <?php
    if (Wp2leadsItmWooLeadvalueModel::is_calculation_in_progress('total_orders')) {
        $progress_percent = Wp2leadsItmWooLeadvalueModel::get_calculation_progress_percent('total_orders');
        ?>
        <div class="wptl-settings-group-header">
            <h3><?php _e("Calculation in progress" , 'wp2leads_itm_woo_leadvalue') ?></h3>
        </div>

        <div class="wptl-settings-group-body">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                            <p>
                                <?php _e('We calculating your user data.', 'wp2leads_itm_woo_leadvalue'); ?>

                                <strong id="wp2leads_itm_woo_calculation_total_orders_progress"><?php echo Wp2leadsItmWooLeadvalueModel::get_calculation_in_progress_message('total_orders'); ?></strong>. <?php _e('You can close or reload a page. Calculation will continue in background.', 'wp2leads_itm_woo_leadvalue'); ?> <?php _e('If you have more than one customer done you can start working with a map.', 'wp2leads_itm_woo_leadvalue'); ?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="wptl-col-xs-12">
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                            <div class="wptl-progress-bar">
                                <span style="width: <?php echo $progress_percent ?>%"></span>
                            </div>

                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                            <p>
                                <button
                                    id="wp2leads_itm_woo_leadvalue_total_orders_terminate_calculation"
                                    type="button"
                                    class="button button-primary"
                                >
                                    <?php echo __('Terminate calculation', 'wp2leads_itm_woo_leadvalue'); ?>
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } elseif (Wp2leadsItmWooLeadvalueModel::is_initial_calculation_done('woo_leadvalue_total_orders')) {
        ?>
        <div class="wptl-settings-group-header">
            <h3><?php _e("You ready to go." , 'wp2leads_itm_webinarignition') ?></h3>
        </div>

        <div class="wptl-settings-group-body">
            <div class="wptl-row">
                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-4">
                    <h4>
                        <?php _e('Your customers total orders amount ready for transfer.', 'wp2leads_itm_woo_leadvalue') ?>
                    </h4>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                    <p>
                        <?php _e('Your customer\'s orders total amount already calculated and you can use it for transfering to Klick-Tipp.', 'wp2leads_itm_woo_leadvalue') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-2 wptl-col-lg-2">
                    <p>
                        <button
                            id="wp2leads_itm_woo_leadvalue_total_orders_initial_calculation"
                            type="button"
                            class="button button-primary"
                        >
                            <?php echo __('Recalculate again', 'wp2leads_itm_woo_leadvalue'); ?>
                        </button>

                        <?php
                        if (!empty($map_id)) {
                            ?>
                            <a target="_blank" href="?page=wp2l-admin&tab=map_to_api&active_mapping=<?php echo $map_id ?>" class="button button-primary">
                                <?php echo __('Edit map', 'wp2leads_itm_woo_leadvalue'); ?>
                            </a>
                            <?php
                        } else {
                            ?>
                            <a target="_blank" class="button button-primary" href="?page=wp2l-admin">
                                <?php echo __('Install map', 'wp2leads_itm_woo_leadvalue'); ?>
                            </a>
                            <?php
                        }
                        ?>
                    </p>
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="wptl-settings-group-header">
            <h3><?php _e("Welcome. Let's start working" , 'wp2leads_itm_woo_leadvalue') ?></h3>
        </div>

        <div class="wptl-settings-group-body">
            <div class="wptl-row">
                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-4">
                    <h4>
                        <?php _e('Before going any further let\'s make one simple step.', 'wp2leads_itm_woo_leadvalue') ?>
                    </h4>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                    <p>
                        <?php _e('We recomend you to calculate numbers of completed orders for each customer. It will allow you to start initial transfer to Klick-Tipp. Please click button <strong>"Calculate existed orders"</strong>', 'wp2leads_itm_woo_leadvalue') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-2 wptl-col-lg-2">
                    <p>
                        <button
                            id="wp2leads_itm_woo_leadvalue_total_orders_initial_calculation"
                            type="button"
                            class="button button-primary"
                        >
                            <?php echo __('Calculate existed orders', 'wp2leads_itm_woo_leadvalue'); ?>
                        </button>
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    </div>
</div>
