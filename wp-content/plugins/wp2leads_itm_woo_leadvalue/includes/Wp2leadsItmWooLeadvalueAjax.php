<?php
class Wp2leadsItmWooLeadvalueAjax {
    public static function initial_calculation() {
        $calculation = 'leadvalue';
        if (!empty($_POST["calculation"]) && in_array(sanitize_text_field($_POST["calculation"]), ['leadvalue', 'total_orders'])) {
            $calculation = sanitize_text_field($_POST["calculation"]);
        }
        $emails = Wp2leadsItmWooLeadvalueModel::get_all_customers($calculation);

        if (empty($emails)) {
            Wp2leadsItmWooLeadvalueAjax::success_response([
                'message' => __('Warning', 'wp2leads_itm_woo_leadvalue') . ': ' . __('No orders to calculate.', 'wp2leads_itm_woo_leadvalue'),
            ]);
        }

        if ($calculation === 'leadvalue') {
            $result = Wp2leadsItmWooLeadvalueBackgroundRecalculate::bg_process($emails);
        } else {
            $result = Wp2leadsItmWooLeadvalueTotalOrdersBackgroundRecalculate::bg_process($emails);
        }

        Wp2leadsItmWooLeadvalueAjax::success_response([
            'message' => __('Success', 'wp2leads_itm_woo_leadvalue') . ': ' . $result . ' ' . __('customers started recalculation.', 'wp2leads_itm_woo_leadvalue'),
            'reload' => 1,
        ]);
    }

    public static function calculation_progress() {
        $calculation = 'leadvalue';
        if (!empty($_POST["calculation"]) && in_array(sanitize_text_field($_POST["calculation"]), ['leadvalue', 'total_orders'])) {
            $calculation = sanitize_text_field($_POST["calculation"]);
        }

        $progress_message = Wp2leadsItmWooLeadvalueModel::get_calculation_in_progress_message($calculation);
        $progress_percent = Wp2leadsItmWooLeadvalueModel::get_calculation_progress_percent($calculation);

        if ($progress_message) {
            $params = array(
                'progress_message' => $progress_message,
                'progress_percent' => $progress_percent,
            );
        } else {
            $params = array(
                'progress_message' => __('Calculation finished. We will reload a page now.', 'wp2leads_itm_woo_leadvalue'),
                'progress_percent' => $progress_percent,
                'progress_reload' => 1
            );
        }



        Wp2leadsItmWooLeadvalueAjax::success_response($params);
    }

    public static function terminate_calculation() {
        $calculation = 'leadvalue';
        if (!empty($_POST["calculation"]) && in_array(sanitize_text_field($_POST["calculation"]), ['leadvalue', 'total_orders'])) {
            $calculation = sanitize_text_field($_POST["calculation"]);
        }
        global $wpdb;

        if ($calculation === 'leadvalue') {
            $sql = ("DELETE FROM $wpdb->options WHERE option_name LIKE '%wp_leadvalue_background_recalculate%'");
        } else {
            $sql = ("DELETE FROM $wpdb->options WHERE option_name LIKE '%wp_leadvalue_total_orders_background_recalculate%'");
        }

        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );
        $wpdb->query( $sql );

        if ($calculation === 'leadvalue') {
            delete_option('wp2leads_itm_woo_leadvalue_rcalculation_progress');
        } else {
            delete_option('wp2leads_itm_woo_total_orders_rcalculation_progress');
        }

        $params = array(
            'message' => __('Calculation terminated', 'wp2leads_itm_woo_leadvalue'),
            'reload' => 1,
        );

        Wp2leadsItmWooLeadvalueAjax::success_response($params);
    }

    public static function error_response($params = array()) {
        $response = array('success' => 0, 'error' => 1);

        if (!empty($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }

    public static function success_response($params = array()) {
        $response = array('success' => 1, 'error' => 0);

        if (!empty($params) && is_array($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }
}

add_action('wp_ajax_nopriv_wp2leads_itm_woo_leadvalue_initial_calculation', array('Wp2leadsItmWooLeadvalueAjax', 'initial_calculation'));
add_action('wp_ajax_wp2leads_itm_woo_leadvalue_initial_calculation', array('Wp2leadsItmWooLeadvalueAjax', 'initial_calculation'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_leadvalue_calculation_progress', array('Wp2leadsItmWooLeadvalueAjax', 'calculation_progress'));
add_action('wp_ajax_wp2leads_itm_woo_leadvalue_calculation_progress', array('Wp2leadsItmWooLeadvalueAjax', 'calculation_progress'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_leadvalue_terminate_calculation', array('Wp2leadsItmWooLeadvalueAjax', 'terminate_calculation'));
add_action('wp_ajax_wp2leads_itm_woo_leadvalue_terminate_calculation', array('Wp2leadsItmWooLeadvalueAjax', 'terminate_calculation'));
