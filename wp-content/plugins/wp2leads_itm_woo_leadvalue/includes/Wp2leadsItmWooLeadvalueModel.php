<?php
class Wp2leadsItmWooLeadvalueModel {
    private static $table_woo_leadvalue_name = 'wp2leads_itm_woo_leadvalue_customers';
    private static $table_woo_total_orders_name = 'wp2leads_itm_woo_leadvalue_total_orders';

    private static function get_table_name($table) {
        global $wpdb;

        if ('woo_leadvalue_customers' === $table) {
            return $wpdb->prefix . self::$table_woo_leadvalue_name;
        }

        if ('woo_leadvalue_total_orders' === $table) {
            return $wpdb->prefix . self::$table_woo_total_orders_name;
        }

        return false;
    }

    private static function get_schema($table) {
        global $wpdb;

        $table_name = self::get_table_name($table);

        if (!$table_name) {
            return false;
        }

        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';

        if ('woo_leadvalue_customers' === $table) {
            return "CREATE TABLE {$table_name} (
  ID BIGINT UNSIGNED NOT NULL auto_increment,
  email varchar(200) NOT NULL,
  total_order_amount varchar(100) NOT NULL,
  total_order_refund varchar(100) NOT NULL,
  total_amount varchar(100) NOT NULL,
  total_amount_last_transfer varchar(100) NOT NULL,
  total_amount_to_transfer varchar(100) NOT NULL,
  PRIMARY KEY  (ID)
) $collate;";
        }

        if ('woo_leadvalue_total_orders' === $table) {
            return "CREATE TABLE {$table_name} (
  ID BIGINT UNSIGNED NOT NULL auto_increment,
  email varchar(200) NOT NULL,
  completed_orders_amount varchar(100) NOT NULL,
  PRIMARY KEY  (ID)
) $collate;";
        }

        return false;
    }

    public static function create_table($table) {
        global $wpdb;
        $table_name = self::get_table_name($table);
        $query = $wpdb->prepare('SHOW TABLES LIKE %s', $table_name);
        $result = $wpdb->get_var($query);

        if ($wpdb->get_var($query) != $table_name) {
            $wpdb->hide_errors();
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            $sql = self::get_schema($table);
            if ($sql) dbDelta( self::get_schema($table) );
        }
    }

    public static function get_all_customers($calculation = 'leadvalue') {
        if (!in_array($calculation, ['leadvalue', 'total_orders'])) return false;

        if ($calculation === 'leadvalue') {
            global $wpdb;
            $sql = "SELECT * FROM {$wpdb->postmeta} WHERE meta_key = '_billing_email'";
            $result = $wpdb->get_results($sql, ARRAY_A);

            if (!$result) return false;
            $customers = array();

            foreach ($result as $order_email) {
                $customers[] = $order_email['meta_value'];
            }

            return array_unique($customers);
        } else {
            global $wpdb;
            $sql = "
                SELECT DISTINCT pm.meta_value AS email
                FROM {$wpdb->postmeta} pm
                INNER JOIN {$wpdb->posts} p ON pm.post_id = p.ID
                WHERE p.post_type = 'shop_order'
                AND p.post_status = 'wc-completed'
                AND pm.meta_key = '_billing_email'
                ORDER BY p.post_modified ASC
            ";

            $result = $wpdb->get_col($sql);
            return array_unique($result);
        }
    }

    /**
     * @param string $email
     * @param string $table Allowed values: 'woo_leadvalue_customers', 'woo_leadvalue_total_orders'.
     * @return false|mixed|stdClass
     */
    public static function get_customer($email, $table = 'woo_leadvalue_customers') {
        global $wpdb;
        $table_name = self::get_table_name($table);
        $sql = "SELECT * FROM {$table_name} WHERE email = '{$email}'";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!$result) return false;
        return $result[0];
    }

    /**
     * @param string $table Allowed values: 'woo_leadvalue_customers', 'woo_leadvalue_total_orders'.
     * @return bool
     */
    public static function is_initial_calculation_done($table = 'woo_leadvalue_customers') {
        global $wpdb;
        $table_name = self::get_table_name($table);
        $sql = "SELECT * FROM {$table_name}";
        $result = $wpdb->get_results($sql, ARRAY_A);

        return !empty($result);
    }

    /**
     * @param string $calculation Allowed values: 'leadvalue', 'total_orders'.
     * @return bool
     */
    public static function is_calculation_in_progress($calculation = 'leadvalue') {
        $calculation_in_progress = get_option("wp2leads_itm_woo_{$calculation}_rcalculation_progress", array());

        if (empty($calculation_in_progress)) return false;
        return true;
    }

    /**
     * @param string $calculation Allowed values: 'leadvalue', 'total_orders'.
     * @return false|string
     */
    public static function get_calculation_in_progress_message($calculation = 'leadvalue') {
        $calculation_in_progress = get_option("wp2leads_itm_woo_{$calculation}_rcalculation_progress", array());

        if (empty($calculation_in_progress)) return false;

        $percent = self::get_calculation_progress_percent($calculation);

        $message = floor($percent) . __('% done', 'wp2leads_itm_woo_leadvalue') . ' (' . $calculation_in_progress['done'] . ' ' . __('customers out of', 'wp2leads_itm_woo_leadvalue') . ' ' . $calculation_in_progress['total'] . ')';

        return $message;
    }

    /**
     * @param string $calculation Allowed values: 'leadvalue', 'total_orders'.
     * @return false|float
     */
    public static function get_calculation_progress_percent($calculation = 'leadvalue') {
        $calculation_in_progress = get_option("wp2leads_itm_woo_{$calculation}_rcalculation_progress", array());
        if (empty($calculation_in_progress)) return false;
        $percent = ($calculation_in_progress['done'] / $calculation_in_progress['total']) * 100;

        return floor($percent);
    }

    /**
     * @param $data
     * @param string $table Allowed values: 'woo_leadvalue_customers', 'woo_leadvalue_total_orders'.
     * @return int|mixed
     */
    public static function create_customer($data, $table = 'woo_leadvalue_customers') {
        global $wpdb;

        $table_name = self::get_table_name($table);

        if (!empty($data['ID'])) {
            $id = $data['ID'];
            unset($data['ID']);
            $wpdb->update( $table_name, $data, array( 'ID' => $id ) );
        } else {
            $wpdb->insert( $table_name, $data );
            $id = $wpdb->insert_id;
        }

        return $id;
    }

    public static function delete_customer($email, $table = 'woo_leadvalue_customers') {
        global $wpdb;
        $table_name = self::get_table_name($table);
        $customer = self::get_customer($email, $table);

        if ($customer) {
            return $wpdb->delete( $table_name, [ 'email' => $email ] );
        }

        return false;
    }

    public static function recalculate_total_orders_amount($email = null, $instant = false) {
        global $wpdb;

        if ($email) {
            $sql = "
                SELECT pm.meta_value AS email, COUNT(*) as completed_orders_amount
                FROM {$wpdb->postmeta} pm
                INNER JOIN {$wpdb->posts} p ON pm.post_id = p.ID
                WHERE p.post_type = 'shop_order'
                AND p.post_status = 'wc-completed'
                AND pm.meta_key = '_billing_email'
                AND pm.meta_value = '{$email}'
                GROUP BY pm.meta_value
                ORDER BY completed_orders_amount DESC, email ASC;
            ";
        } else {
            $sql = "
                SELECT pm.meta_value AS email, COUNT(*) as completed_orders_amount
                FROM {$wpdb->postmeta} pm
                INNER JOIN {$wpdb->posts} p ON pm.post_id = p.ID
                WHERE p.post_type = 'shop_order'
                AND p.post_status = 'wc-completed'
                AND pm.meta_key = '_billing_email'
                GROUP BY pm.meta_value
                ORDER BY completed_orders_amount DESC, email ASC;
            ";
        }

        $result = $wpdb->get_results($sql, ARRAY_A);

        if (empty($result) && $email && Wp2leadsItmWooLeadvalueModel::get_customer($email, 'woo_leadvalue_total_orders')) {
            $result = array ( ['email' => $email, 'completed_orders_amount' => 0] );
        }

        if (empty($result)) return false;

        foreach ($result as $order_email) {
            error_log("in recalculate_total_orders_amount :: instant :: {$instant} ::  {$order_email['email']} - {$order_email['completed_orders_amount']}");
            $customer = Wp2leadsItmWooLeadvalueModel::get_customer($email, 'woo_leadvalue_total_orders');

            if (!empty($customer['ID']) && $instant) {
                $delete_result = Wp2leadsItmWooLeadvalueModel::delete_customer($email, 'woo_leadvalue_total_orders');

                if ($delete_result) {
                    $customer = null;
                }
            }

            if (!$customer) {
                $customer_data = array(
                    'email' => $email,
                    'completed_orders_amount' => $order_email['completed_orders_amount'],
                );
            } else {
                error_log("in recalculate_total_orders_amount :: {$order_email['email']} email exists");
                $customer_data = array(
                    'ID' => $customer['ID'],
                    'completed_orders_amount' => $order_email['completed_orders_amount'],
                );
            }

            $customer_ID = Wp2leadsItmWooLeadvalueModel::create_customer($customer_data, 'woo_leadvalue_total_orders');

            if (!empty($customer_ID)) {
                if ($instant) {
                    do_action( 'wp2leads_itm_woo_total_orders_rcalculation_instant', $customer_ID );
                } else {
                    do_action( 'wp2leads_itm_woo_total_orders_rcalculation_initial', $customer_ID );
                }
            }

            error_log("in recalculate_total_orders_amount :: {$customer_ID}");
        }

        return $result;
    }

    public static function recalculate_total_amount($email = null) {
        global $wpdb;

        if ($email) {
            $sql = "SELECT * FROM {$wpdb->postmeta} WHERE meta_key = '_billing_email' AND meta_value = '{$email}'";
        } else {
            $sql = "SELECT * FROM {$wpdb->postmeta} WHERE meta_key = '_billing_email'";
        }

        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!$result) {
            return false;
        }

        $customers = array();

        foreach ($result as $order_email) {
            $order_id = $order_email['post_id'];
            $order = wc_get_order($order_id);
            if ($order) {
                $order_status = $order->get_status();
                $user_order_total = !empty($customers[$order_email['meta_value']]['total_order_amount']) ? (float) $customers[$order_email['meta_value']]['total_order_amount'] : 0;
                $user_total_refunded = !empty($customers[$order_email['meta_value']]['total_order_refund']) ? (float) $customers[$order_email['meta_value']]['total_order_refund'] : 0;
                $user_total_amount = !empty($customers[$order_email['meta_value']]['total_amount']) ? (float) $customers[$order_email['meta_value']]['total_amount'] : 0;

                $order_total     = (float) $order->get_total();
                $total_refunded  = (float) $order->get_total_refunded();
                $total_amount = $order_total - $total_refunded;

                if ('completed' === $order_status || $total_refunded > 0) {
                    $customers[$order_email['meta_value']]['orders'][$order_id] = array(
                        'order_total' => $order_total,
                        'total_refunded' => $total_refunded,
                        'total_amount' => $order_total - $total_refunded,
                        'order_status' => $order_status,
                    );

                    $customers[$order_email['meta_value']]['total_order_amount'] = (float) $user_order_total + (float) $order_total;
                    $customers[$order_email['meta_value']]['total_order_refund'] = (float) $user_total_refunded + (float) $total_refunded;
                    $customers[$order_email['meta_value']]['total_amount'] = (float) $user_total_amount + (float) $total_amount;
                } else {
                    $customers[$order_email['meta_value']]['orders'][$order_id] = array(
                        'order_total' => $order_total,
                        'total_refunded' => $total_refunded,
                        'total_amount' => $order_total - $total_refunded,
                        'order_status' => $order_status,
                    );

                    $customers[$order_email['meta_value']]['total_order_amount'] = (float) $user_order_total + 0;
                    $customers[$order_email['meta_value']]['total_order_refund'] = (float) $user_total_refunded + 0;
                    $customers[$order_email['meta_value']]['total_amount'] = (float) $user_total_amount + 0;
                }
            }
        }

        foreach ($customers as $email => $data) {
            $customer = Wp2leadsItmWooLeadvalueModel::get_customer($email);

            if (!$customer) {
                $customer_data = array(
                    'email' => $email,
                    'total_order_amount' => $data['total_order_amount'],
                    'total_order_refund' => $data['total_order_refund'],
                    'total_amount' => $data['total_amount'],
                    'total_amount_last_transfer' => '0',
                    'total_amount_to_transfer' => $data['total_amount'],
                );
            } else {
                $total_amount_last_transfer = $customer['total_amount_last_transfer'];
                $last_total_amount = $customer['total_amount'];
                $current_total_amount = $data['total_amount'];

                if (!empty($total_amount_last_transfer)) {
                    $total_amount_to_transfer = $current_total_amount - $last_total_amount;
                } else {
                    $total_amount_to_transfer = $current_total_amount;
                }

                $customer_data = array(
                    'ID' => $customer['ID'],
                    'total_order_amount' => $data['total_order_amount'],
                    'total_order_refund' => $data['total_order_refund'],
                    'total_amount' => $data['total_amount'],
                );

                $last_transfered = $customer['total_amount_last_transfer'];
                // $amount_to_transfer = (float) $data['total_amount'] - (float)$last_transfered;
                $amount_to_transfer = $total_amount_to_transfer;

                $customer['total_amount_last_transfer'] = '0';
                $customer_data['total_amount_to_transfer'] = $amount_to_transfer;
            }

            $customer_created = Wp2leadsItmWooLeadvalueModel::create_customer($customer_data);
        }

        return $customers;
    }
}
