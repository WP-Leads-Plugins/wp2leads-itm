<?php
class Wp2leadsItmWooLeadvalueAdmin {
    public static function add_submenu_page() {
        add_submenu_page(
            'wp2l-admin',
            __("Klick-Tipp LeadValue", 'wp2leads_itm_woo_leadvalue'),
            __("Klick-Tipp LeadValue", 'wp2leads_itm_woo_leadvalue'),
            'manage_options',
            'wp2leads_itm_woo_leadvalue',
            'Wp2leadsItmWooLeadvalueAdmin::display_submenu_page',
            10
        );
        add_submenu_page(
            'wp2l-admin',
            __("Klick-Tipp Total Orders", 'wp2leads_itm_woo_leadvalue'),
            __("Klick-Tipp Total Orders", 'wp2leads_itm_woo_leadvalue'),
            'manage_options',
            'wp2leads_itm_woo_total_orders',
            'Wp2leadsItmWooLeadvalueAdmin::display_total_orders_submenu_page',
            11
        );
    }

    public static function display_submenu_page() {
        include_once plugin_dir_path( WP2LITM_WOO_LEADVALUE_PLUGIN_FILE ) . 'templates/admin-page.php';
    }

    public static function display_total_orders_submenu_page() {
        include_once plugin_dir_path( WP2LITM_WOO_LEADVALUE_PLUGIN_FILE ) . 'templates/admin-page_total_orders.php';
    }

    public static function enqueue_scripts() {
        if (!empty($_GET['page']) && ($_GET['page'] === 'wp2leads_itm_woo_leadvalue' || $_GET['page'] === 'wp2leads_itm_woo_total_orders')) {
            wp_enqueue_style( 'wp2leads-itm-woo-leadvalue', plugin_dir_url( WP2LITM_WOO_LEADVALUE_PLUGIN_FILE ) . 'assets/css/admin.css', array(), WP2LITM_WOO_LEADVALUE_VERSION . time(), 'all' );
            wp_enqueue_script( 'wp2leads-itm-woo-leadvalue', plugin_dir_url( WP2LITM_WOO_LEADVALUE_PLUGIN_FILE ) . 'assets/js/admin.js', array( 'jquery' ), WP2LITM_WOO_LEADVALUE_PLUGIN_FILE . time(), true );
        }
    }
}

add_action('admin_menu', array ('Wp2leadsItmWooLeadvalueAdmin', 'add_submenu_page'));
add_action( 'admin_enqueue_scripts', array( 'Wp2leadsItmWooLeadvalueAdmin', 'enqueue_scripts' ) );
