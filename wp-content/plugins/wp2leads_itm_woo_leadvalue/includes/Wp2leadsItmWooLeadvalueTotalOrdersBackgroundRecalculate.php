<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leadsItmWooLeadvalueTotalOrdersBackgroundRecalculate {
    protected static $bg_process;

    public static function init() {
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/class-total_orders-logger.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/abstract-class-total_orders-background.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/class-total_orders-background-recalculate-process.php';

        self::$bg_process = new Leadvalue_Background_Total_Orders_Recalculate_Process();
    }

    public static function bg_process($emails, $instant = false) {
        $i = 0;

        foreach ($emails as $email) {
            self::$bg_process->push_to_queue( ['email' => $email, 'instant' => $instant] );

            $i++;
        }

        delete_option('wp2leads_itm_woo_total_orders_rcalculation_progress');
        add_option('wp2leads_itm_woo_total_orders_rcalculation_progress', array(
            'total' => $i,
            'done' => 0
        ));

        self::$bg_process->save()->dispatch();

        return $i;
    }
}

add_action( 'init', array( 'Wp2leadsItmWooLeadvalueTotalOrdersBackgroundRecalculate', 'init' ) );
