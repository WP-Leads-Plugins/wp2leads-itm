<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leadsItmWooLeadvalueBackgroundRecalculate {
    protected static $bg_process;

    public static function init() {
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/class-leadvalue-logger.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/abstract-class-leadvalue-background.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/class-leadvalue-background-recalculate-process.php';

        self::$bg_process = new Leadvalue_Background_Recalculate_Process();
    }

    public static function bg_process($emails) {
        $i = 0;

        foreach ($emails as $email) {
            self::$bg_process->push_to_queue( $email );

            $i++;
        }

        delete_option('wp2leads_itm_woo_leadvalue_rcalculation_progress');
        add_option('wp2leads_itm_woo_leadvalue_rcalculation_progress', array(
            'total' => $i,
            'done' => 0
        ));

        self::$bg_process->save()->dispatch();

        return $i;
    }
}

add_action( 'init', array( 'Wp2leadsItmWooLeadvalueBackgroundRecalculate', 'init' ) );