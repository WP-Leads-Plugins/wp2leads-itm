<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_Woo_Leadvalue_Total_Orders_Transfer
{
    private static $key = 'wp2leads_woo_leadvalue_total_orders';
    private static $required_column = 'wp2leads_itm_woo_leadvalue_total_orders.ID';

    public static function get_label() {
        return __('Woocommerce Customer Completed Orders Number', 'wp2leads_itm_woo_leadvalue');
    }

    public static function get_description() {
        return __('This module will transfer customer total completed order number', 'wp2leads_itm_woo_leadvalue');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Woocommerce Customer Completed Orders Number maps.', 'wp2leads_itm_woo_leadvalue') ?></p>
        <p><?php _e('Once order status will be changed from or to Completed, current customer completed order number will be transfered to Klick-Tipp.', 'wp2leads_itm_woo_leadvalue') ?></p>
        <p><?php _e('Requirement: <strong>wp2leads_itm_woo_leadvalue_total_orders.ID</strong> column withing selected data.', 'wp2leads_itm_woo_leadvalue') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function transfer_init() {
        add_action('wp2leads_itm_woo_total_orders_rcalculation_instant', 'Wp2leads_Woo_Leadvalue_Total_Orders_Transfer::transfer');
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_woo_leadvalue_total_orders_init($transfer_modules) {
    $transfer_modules['wp2leads_woo_leadvalue_total_orders'] = 'Wp2leads_Woo_Leadvalue_Total_Orders_Transfer';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_woo_leadvalue_total_orders_init');
