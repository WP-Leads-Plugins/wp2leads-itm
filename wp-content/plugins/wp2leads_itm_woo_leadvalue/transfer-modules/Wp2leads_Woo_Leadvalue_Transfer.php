<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_Woo_Leadvalue_Transfer {
    private static $key = 'wp2leads_woo_leadvalue';
    private static $required_column = 'wp2leads_itm_woo_leadvalue_customers.ID';

    public static function transfer_init() {
        // add_action( 'woocommerce_checkout_order_processed', 'Wp2leads_Woo_Leadvalue_Transfer::checkout_order_processed', 10, 3 );
        add_action('woocommerce_order_status_changed', 'Wp2leads_Woo_Leadvalue_Transfer::order_status_changed', 50, 4);
        // add_action('woocommerce_process_shop_order_meta', 'Wp2leads_Woo_Leadvalue_Transfer::process_shop_order_meta', 70, 2);
        add_action('woocommerce_order_refunded', 'Wp2leads_Woo_Leadvalue_Transfer::woocommerce_order_refunded', 70, 2);
    }

    public static function get_label() {
        return __('Woocommerce LeadValue to Klick-Tipp', 'wp2leads_itm_woo_leadvalue');
    }

    public static function get_description() {
        return __('This module will transfer user orders total amount and update LeadValue', 'wp2leads_itm_woo_leadvalue');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Woocommerce LeadValue maps.', 'wp2leads_itm_woo_leadvalue') ?></p>
        <p><?php _e('Once order will be created or order\'s data changed user orders total amount will be transfered to Klick-Tipp account and update LeadValue.', 'wp2leads_itm_woo_leadvalue') ?></p>
        <p><?php _e('Requirement: <strong>wp2leads_itm_woo_leadvalue_customers.ID</strong> column withing selected data.', 'wp2leads_itm_woo_leadvalue') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function checkout_order_processed($order_id, $order_data, $order) {
        self::check_if_transfer($order_id);
    }

    public static function woocommerce_order_refunded($order_id, $refund_id) {
        self::check_if_transfer($order_id);
    }

    public static function order_status_changed($order_id, $from, $to, $order_data) {
        if ('completed' === $to) {
            self::check_if_transfer($order_id);
        }
    }

    public static function process_shop_order_meta($order_id, $post) {
        self::check_if_transfer($order_id);
    }

    private static function check_if_transfer($order_id) {
        $order = wc_get_order($order_id);

        if (!$order) {
            return false;
        }

        $email = $order->get_billing_email();

        if (!$email) {
            return false;
        }

        Wp2leadsItmWooLeadvalueModel::recalculate_total_amount($email);

        $customer = Wp2leadsItmWooLeadvalueModel::get_customer($email);

        if (empty($customer['total_amount_to_transfer'])) {
            return false;
        }

        self::transfer($customer['ID']);
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => 'wp2leads_itm_woo_leadvalue_customers.ID',
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_woo_leadvalue($transfer_modules) {
    $transfer_modules['wp2leads_woo_leadvalue'] = 'Wp2leads_Woo_Leadvalue_Transfer';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_woo_leadvalue');