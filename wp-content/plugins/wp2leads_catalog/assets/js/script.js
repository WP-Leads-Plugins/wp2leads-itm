(function( $ ) {
    $(document.body).on('click', '.wptl-catalog-item .wptl-item-tag-old', function() {
        var tag = $(this).data('tag');
        var tag_item = $('.wptl-catalog-tags .wptl-catalog-tag.tag-' + tag);

        if (tag_item.length) {
            tag_item.click();
        }
    });

    $(document.body).on('click', '.wptl-catalog-tags .wptl-catalog-tag', function() {
        var tag = $(this);
        var container = tag.parents('.wptl-catalog-container');

        if (tag.hasClass('all')) {
            if (!tag.hasClass('active')) {
                container.find('.wptl-catalog-tags .active').removeClass('active');
                tag.addClass('active');
            }
        } else {
            if (tag.hasClass('active')) {
                tag.removeClass('active');

                if (container.find('.wptl-catalog-tags .active').length < 1) {
                    container.find('.wptl-catalog-tags .all').addClass('active');
                }
            } else {
                tag.addClass('active');
                container.find('.wptl-catalog-tags .all').removeClass('active');
            }
        }

        updateCatalogItems(container);
    });

    $(window).on("load",function(){
        $(".wptl-catalog-item-content").niceScroll({autohidemode:false});
    });

    function updateCatalogItems(container) {
        // get data
        var activeTags = container.find('.wptl-catalog-tags .active');
        var catalogItems = container.find('.wptl-catalog-items');

        if (activeTags.length === 1 && activeTags.eq(0).hasClass('all')) {
            catalogItems.find('.wptl-catalog-item').data('show', 1);
        } else {
            catalogItems.find('.wptl-catalog-item').data('show', 0);

            activeTags.each(function(){
                var tag = $(this).data('tag');

                if (tag) {
                    catalogItems.find('.wptl-catalog-item').each(function(){

                        var tags = $(this).data('tags').split('|');

                        if ( tags.indexOf( tag + '' ) > -1 ) {
                            $(this).data('show', 1);
                        }

                    });
                }
            });
        }

        catalogItems.find('.wptl-catalog-item').each(function(){
            if ($(this).data('show')) {
                $(this).show('fast');
            } else {
                $(this).hide('fast');
            }
        });
    }
})( jQuery );
