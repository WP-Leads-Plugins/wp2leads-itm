<?php
/**
 * Class WP2L_Catalog_Public
 */

class WP2L_Catalog_Public {
    public static function enqueue_scripts() {
        wp_enqueue_style( 'wp2l-catalog-css', plugin_dir_url( WP2L_CATALOG_PLUGIN_FILE ) . 'assets/css/style.css', array(), WP2L_CATALOG_VERSION . time(), 'all' );
        wp_enqueue_script( 'scrollbar-js', plugin_dir_url( WP2L_CATALOG_PLUGIN_FILE ) . 'assets/js/scrollbar.js', array( 'jquery' ), '3.1.13', true );
        wp_enqueue_script( 'match-height-js', plugin_dir_url( WP2L_CATALOG_PLUGIN_FILE ) . 'assets/js/match-height.js', array( 'jquery' ), '0.7.2', true );
        wp_enqueue_script( 'wp2l-catalog-js', plugin_dir_url( WP2L_CATALOG_PLUGIN_FILE ) . 'assets/js/script.js', array( 'jquery' ), WP2L_CATALOG_VERSION . time(), true );
        wp_localize_script( 'wp2l-catalog-js', 'wp2lAjaxObj', array( 'ajaxurl'       => admin_url( 'admin-ajax.php' ) ) );
    }
}

add_action('wp_enqueue_scripts', array('WP2L_Catalog_Public', 'enqueue_scripts'), 999);
