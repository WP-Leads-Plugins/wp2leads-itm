<?php
/**
 * Class WP2L_Catalog_Shortcode
 */

class WP2L_Catalog_Shortcode {
    public static function shortcode($atts = array()) {
        $params = shortcode_atts( array(
            'order_by' => 'updated',
            'order_direction' => 'desc',
            'cache' => 0,
            'col_md' => 2,
            'col_lg' => 3,
            'tags' => 'all',
            'tag_cloud' => 1,
            'example_link' => 1,
            'link_url' => 'https://wp2leads-for-klick-tipp.com/woo-wp4free/pop-verbinde-woo-wp-fuer-lau/',
            'link_text' => __('Get WP2LEADS', 'wp2leads_catalog'),
            'color' => '#ff8b38',
        ), $atts );

        $params['order_by'] = !in_array($params['order_by'], array('updated', 'created', 'title')) ? 'updated' : $params['order_by'];
        $params['order_derection'] = !in_array($params['order_direction'], array('desc', 'asc')) ? 'desc' : $params['order_direction'];
        $params['tag_cloud'] = !in_array((int)$params['tag_cloud'], array(1, 0)) ? 1 : (int)$params['tag_cloud'];
        $params['example_link'] = !in_array((int)$params['example_link'], array(1, 0)) ? 1 : (int)$params['example_link'];
        $params['col_md'] = !in_array((int)$params['col_md'], array(1, 2, 3)) ? 2 : (int)$params['col_md'];
        $params['col_lg'] = !in_array((int)$params['col_lg'], array(1, 2, 3, 4)) ? 3 : (int)$params['col_lg'];

        extract( $params );

        $catalog_data = WP2L_Catalog_Model::get($params['order_by'], $params['order_derection']);
        // $items = WP2L_Catalog_Model::get_all();
        $items = !empty($catalog_data['items']) ? $catalog_data['items'] : array();

        if (!empty($items) && !empty($params['tags']) && $params['tags'] !== 'all') {
            $filter_tags = explode('|', $params['tags']);

            if (!empty($filter_tags)) {
                foreach ($items as $i => $item) {
                    if (empty($item['tags'])) {
                        unset($items[$i]);
                    } else {
                        $show = false;
                        $item_tags = unserialize($item['tags']);

                        foreach ($filter_tags as $filter_tag) {
                            if (in_array($filter_tag, $item_tags)) {
                                $show = true;
                                break;
                            }
                        }

                        if (!$show) unset($items[$i]);
                    }
                }

            }
        }

        // $tags = WP2L_Catalog_Model::get_all_tags();
        $tags = !empty($catalog_data['tags']) ? $catalog_data['tags'] : array();

        if (!empty($tags) && !empty($params['tags']) && $params['tags'] !== 'all') {
            $filter_tags = explode('|', $params['tags']);

            if (!empty($filter_tags)) {
                foreach ($tags as $i => $tag) {
                    if (!in_array($tag['id'], $filter_tags)) unset($tags[$i]);
                }
            }
        }

        $tags_by_items = array();

        foreach ($items as $item) {
            $item_tags = unserialize($item['tags']);

            foreach ($item_tags as $item_tag) {
                if (!in_array($item_tag, $tags_by_items)) {
                    $tags_by_items[] = $item_tag;
                }
            }
        }

        $tags_by_id = array();

        foreach ($tags as $tag) {
            if (in_array($tag['id'], $tags_by_items)) {
                $tags_by_id[$tag['id']] = $tag['name'];
            }
        }

        $catalog = array(
            'tags' => $tags_by_id,
            'items' => $items,
        );

        return WP2L_Catalog_Functions::get_template('catalog.php', array('catalog' => $catalog, 'params' => $params));
    }
}

add_shortcode( 'wp2l_catalog', array('WP2L_Catalog_Shortcode', 'shortcode') );
