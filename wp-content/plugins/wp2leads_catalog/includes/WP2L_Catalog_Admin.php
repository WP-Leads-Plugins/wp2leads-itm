<?php
/**
 * Class WP2L_Catalog_Admin
 *
 * @since 1.0.1
 */

class WP2L_Catalog_Admin
{
    public static function add_submenu_page() {
        if (WP2L_Catalog_Functions::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) {
            add_submenu_page(
                'wp2l-admin',
                __("Catalog Shortcode", 'wp2leads_catalog'),
                __("Catalog Shortcode", 'wp2leads_catalog'),
                'manage_options',
                'wp2l-catalog',
                'WP2L_Catalog_Admin::display_submenu_page'
            );
        } else {
            add_menu_page(
                __("Catalog Shortcode", 'wp2leads_catalog'),
                __("Catalog Shortcode", 'wp2leads_catalog'),
                'manage_options',
                'wp2l-catalog',
                'WP2L_Catalog_Admin::display_submenu_page'
            );
        }
    }

    public static function display_submenu_page() {
        include_once plugin_dir_path( WP2L_CATALOG_PLUGIN_FILE ) . 'templates/admin/admin-page.php';
    }
}

add_action('admin_menu', array ('WP2L_Catalog_Admin', 'add_submenu_page'));
