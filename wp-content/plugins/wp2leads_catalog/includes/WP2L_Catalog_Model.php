<?php
/**
 * Class WP2L_Catalog_Model
 */

class WP2L_Catalog_Model {
    public static function get($order_by = 'updated', $order_direction = 'desc') {
        $order_column = 'mc.added';

        if ($order_by === 'created') {
            $order_column = 'ci.id';
        } elseif ($order_by === 'title') {
            $order_column = 'ci.title';
        }

        $parameters = array (
            'order_by' => $order_column,
            'order_direction' => strtoupper($order_direction),
            'per_page' => 0,
            'event' => 'get_catalog_public'
        );

        $request = wp_remote_post(
            base64_decode(self::get_server()),
            array(
                'body'    => $parameters,
            )
        );

        $response = json_decode(wp_remote_retrieve_body( $request ), true);

        if (200 !== $response['code']) return array();
        if (!is_array($response['data'])) return array();
        return $response['data'];
    }

    public static function get_all() {
        $parameters = array (
            'per_page' => 0,
            'event' => 'get_catalog_items'
        );

        $request = wp_remote_post(
            base64_decode(self::get_server()),
            array(
                'body'    => $parameters,
            )
        );

        $response = json_decode(wp_remote_retrieve_body( $request ), true);

        if (200 !== $response['code']) return array();
        if (!is_array($response['data'])) return array();
        return $response['data'];
    }

    public static function get_all_tags() {
        $parameters = array ( 'event' => 'get_all_tags' );

        $request = wp_remote_post(
            base64_decode(self::get_server()),
            array(
                'body'    => $parameters,
            )
        );

        $response = json_decode(wp_remote_retrieve_body( $request ), true);

        if (200 !== $response['code']) return array();

        return isset($response['tags']) ? $response['tags'] : array();
    }

    private static function get_server() {
        if (defined( 'WP2LEADS_MAPS_SANDBOX' ) && WP2LEADS_MAPS_SANDBOX) {
            return 'aHR0cDovL21hcHMuc2FudGVncmEtaW50ZXJuYXRpb25hbC5jb20vc2VydmVyL21hcHMucGhw';
        } else {
            return 'aHR0cHM6Ly9tYXBzLndwMmxlYWRzLmNvbS9zZXJ2ZXIvbWFwcy5waHA=';
        }
    }
}