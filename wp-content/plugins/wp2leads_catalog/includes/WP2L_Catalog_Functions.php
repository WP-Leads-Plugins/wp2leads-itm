<?php
/**
 * Class WP2L_Catalog_Functions
 */

class WP2L_Catalog_Functions {
    public static function load_plugin_textdomain() {
        add_filter( 'plugin_locale', 'WP2L_Catalog_Functions::check_de_locale');
        load_plugin_textdomain( 'wp2leads_catalog', false, WP2L_CATALOG_PLUGIN_REL_FILE . '/languages/' );
        remove_filter( 'plugin_locale', 'WP2L_Catalog_Functions::check_de_locale');
    }

    public static function check_de_locale($domain) {
        $site_lang = get_user_locale();
        $de_lang_list = array( 'de_CH_informal', 'de_DE_formal', 'de_AT', 'de_CH', 'de_DE' );
        return in_array($site_lang, $de_lang_list) ? 'de_DE' : $domain;
    }

    public static function show_template( $template_name, $args = array(), $template_path = '', $default_path = '' ) {
        $template = self::locate_template( $template_name, $template_path, $default_path );
        if (!empty($args) && is_array($args)) extract($args);
        include $template;
    }

    public static function get_template( $template_name, $args = array(), $template_path = '', $default_path = '' ) {
        ob_start();
        self::show_template( $template_name, $args, $template_path, $default_path );
        return ob_get_clean();
    }

    public static function locate_template( $template_name, $template_path = '', $default_path = '' ) {
        if ( ! $template_path ) $template_path = 'wp2leads_catalog';
        if ( ! $default_path ) $default_path = untrailingslashit( plugin_dir_path( WP2L_CATALOG_PLUGIN_FILE ) ) . '/templates/';
        $template = locate_template( array( trailingslashit( $template_path ) . $template_name, $template_name) );
        if ( ! $template ) $template = $default_path . $template_name;
        return $template;
    }

    public static function is_plugin_activated( $plugin_folder, $plugin_file ) {
        if ( self::is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
        else return self::is_plugin_active_by_file( $plugin_file );
    }

    public static function is_plugin_active_simple( $plugin ) {
        return (
            in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
            ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
        );
    }

    public static function is_plugin_active_by_file( $plugin_file ) {
        foreach ( self::get_active_plugins() as $active_plugin ) {
            $active_plugin = explode( '/', $active_plugin );
            if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
        }

        return false;
    }

    public static function get_active_plugins() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
        if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

        return $active_plugins;
    }

    public static function hex2rgb($htmlCode) {
        if($htmlCode[0] == '#') $htmlCode = substr($htmlCode, 1);

        if (strlen($htmlCode) == 3) {
            $htmlCode = $htmlCode[0] . $htmlCode[0] . $htmlCode[1] . $htmlCode[1] . $htmlCode[2] . $htmlCode[2];
        }

        $r = hexdec($htmlCode[0] . $htmlCode[1]);
        $g = hexdec($htmlCode[2] . $htmlCode[3]);
        $b = hexdec($htmlCode[4] . $htmlCode[5]);

        return $b + ($g << 0x8) + ($r << 0x10);
    }

    public static function rgb2hsl($RGB) {
        $r = 0xFF & ($RGB >> 0x10);
        $g = 0xFF & ($RGB >> 0x8);
        $b = 0xFF & $RGB;

        $r = ((float)$r) / 255.0;
        $g = ((float)$g) / 255.0;
        $b = ((float)$b) / 255.0;

        $maxC = max($r, $g, $b);
        $minC = min($r, $g, $b);

        $l = ($maxC + $minC) / 2.0;

        if($maxC == $minC) {
            $s = 0;
            $h = 0;
        } else {
            if($l < .5) {
                $s = ($maxC - $minC) / ($maxC + $minC);
            } else {
                $s = ($maxC - $minC) / (2.0 - $maxC - $minC);
            }
            if($r == $maxC) $h = ($g - $b) / ($maxC - $minC);
            if($g == $maxC) $h = 2.0 + ($b - $r) / ($maxC - $minC);
            if($b == $maxC) $h = 4.0 + ($r - $g) / ($maxC - $minC);

            $h = $h / 6.0;
        }

        $h = (int)round(255.0 * $h);
        $s = (int)round(255.0 * $s);
        $l = (int)round(255.0 * $l);

        return (object) Array('hue' => $h, 'saturation' => $s, 'lightness' => $l);
    }

    public static function adjustBrightness($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ($color_parts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }
}
