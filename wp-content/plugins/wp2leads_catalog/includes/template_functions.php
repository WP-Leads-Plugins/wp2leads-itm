<?php
/**
 *
 */
if (!function_exists('wp2leads_add_catalog_style')) {
    function wp2leads_add_catalog_style($catalog, $params) {
        $color = $params['color'];
        $color_rgb = WP2L_Catalog_Functions::hex2rgb($color);
        $color_hex = WP2L_Catalog_Functions::rgb2hsl($color_rgb);
        $color_darker = WP2L_Catalog_Functions::adjustBrightness($color, -10);
//        var_dump($color);
//        var_dump($color_darker);
//        var_dump($color_rgb);
//        var_dump($color_hex);
        $txt_color = $color_hex->lightness < 200 ? '#fefefe' : '#43454b';
        $color_hollow = $color_hex->lightness < 200 ? $color : WP2L_Catalog_Functions::adjustBrightness($color, -50);
        $color_hollow_darker = WP2L_Catalog_Functions::adjustBrightness($color_hollow, -10);



        $unique_id = wp_unique_id( $prefix = 'wptl-catalog-' );
        ?>
        <style>

#<?php echo $unique_id; ?> .wptl-catalog-item-inner:hover {
    border-color: <?php echo $color_hollow ?>;
}
#<?php echo $unique_id; ?> .wptl-btn {
    border-color: <?php echo $color ?>;
    background-color: <?php echo $color ?>;
    color: <?php echo $txt_color ?>; }
#<?php echo $unique_id; ?> .wptl-btn:hover {
    border-color: <?php echo $color_darker ?>;
    background-color: <?php echo $color_darker ?>;
    color: <?php echo $txt_color ?>; }
#<?php echo $unique_id; ?> .wptl-btn.hvr-invert:hover {
    border-color: <?php echo $color ?>;
    background-color: transparent;
    color: <?php echo $color ?>; }
#<?php echo $unique_id; ?> .wptl-btn.type-hollow {
    border-color: <?php echo $color_hollow ?>;
    background-color: transparent;
    color: <?php echo $color_hollow ?>; }
#<?php echo $unique_id; ?> .wptl-btn.type-hollow:hover {
    border-color: <?php echo $color_hollow_darker ?>;
    background-color: transparent;
    color: <?php echo $color_hollow_darker ?>; }
#<?php echo $unique_id; ?> .wptl-btn.type-hollow.hvr-invert:hover {
    border-color: <?php echo $color ?>;
    background-color: <?php echo $color ?>;
    color: <?php echo $txt_color ?>; }
#<?php echo $unique_id; ?> .wptl-item-tag, #<?php echo $unique_id; ?> .wptl-item-tag:hover {
      border-color: <?php echo $color ?>;
      background-color: <?php echo $color ?>;
      color: <?php echo $txt_color ?>;
}
#<?php echo $unique_id; ?> .wptl-catalog-tag {
    border-color: <?php echo $color_hollow ?>;
    color: <?php echo $color_hollow ?>;
}
#<?php echo $unique_id; ?> .wptl-catalog-tag:hover {
   border-color: <?php echo $color ?>;
   background-color: <?php echo $color ?>;
   color: <?php echo $txt_color ?>;
}
#<?php echo $unique_id; ?> .wptl-catalog-tag.active {
    border-color: <?php echo $color ?>;
    background-color: <?php echo $color ?>;
    color: <?php echo $txt_color ?>;
}
        </style>
        <div id="<?php echo $unique_id; ?>" class="wptl-catalog-container"><?php
    }
}

add_action('wp2leads_catalog_before_wrapper', 'wp2leads_add_catalog_style', 15, 2);
/**
 *
 */
if (!function_exists('wp2leads_add_catalog_style_after')) {
    function wp2leads_add_catalog_style_after($catalog, $params) {
        ?></div><?php
    }
}

add_action('wp2leads_catalog_after_wrapper', 'wp2leads_add_catalog_style_after', 15, 2);
/**
 *
 */
if (!function_exists('wp2leads_catalog_tags_cloud')) {
    function wp2leads_catalog_tags_cloud($catalog, $params) {
        WP2L_Catalog_Functions::show_template('catalog-tags.php', array( 'tags' => $catalog['tags'],'params' => $params ));
    }
}

add_action('wp2leads_catalog_before_wrapper', 'wp2leads_catalog_tags_cloud', 15, 2);
/**
 *
 */
if (!function_exists('wp2leads_catalog_wrap_before')) {
    function wp2leads_catalog_wrap_before($catalog) {
        ob_start();
        ?><div class="wptl-row"><?php
        echo ob_get_clean();
    }
}
add_action('wp2leads_catalog_before_main_content', 'wp2leads_catalog_wrap_before', 30);

/**
 *
 */
if (!function_exists('wp2leads_catalog_wrap_after')) {
    function wp2leads_catalog_wrap_after($catalog) {
        ob_start();
        ?></div><?php
        echo ob_get_clean();
    }
}
add_action('wp2leads_catalog_after_main_content', 'wp2leads_catalog_wrap_after', 30);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_wrap_before')) {
    function wp2leads_catalog_item_wrap_before($catalog_item, $params) {
        $classes = apply_filters('wp2leads_catalog_item_wrap_class', array(), $params);
        $tags = !empty($catalog_item['tags']) ? unserialize($catalog_item['tags']) : array();
        $tags_str = ' ';

        foreach ($tags as $tag) {
            $tags_str .= $tag . '|';
        }
        ob_start();
        ?>
        <div class="wptl-catalog-item <?php echo implode(' ', $classes) ?>" data-tags="<?php echo trim($tags_str); ?>">
        <div class="wptl-catalog-item-inner">
        <?php
        echo ob_get_clean();
    }
}
add_action('wp2leads_catalog_before_catalog_item', 'wp2leads_catalog_item_wrap_before', 30, 2);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_wrap_after')) {
    function wp2leads_catalog_item_wrap_after($catalog_item, $params) {
        ob_start();
        ?>
        </div></div>
        <?php
        echo ob_get_clean();
    }
}
add_action('wp2leads_catalog_after_catalog_item', 'wp2leads_catalog_item_wrap_after', 30, 2);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_img')) {
    function wp2leads_catalog_item_img($catalog_item, $params) {
        WP2L_Catalog_Functions::show_template('catalog-item-img.php', array(
                'alt' => $catalog_item['title'],
                'src' => $catalog_item['image'],
                'id' => $catalog_item['id'],
        ));
    }
}
add_action('wp2leads_catalog_catalog_item_title', 'wp2leads_catalog_item_img', 30, 2);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_header_open')) {
    function wp2leads_catalog_item_header_open($catalog_item, $params) {
        ?><div id="wptl-catalog-item-tags-<?php echo $catalog_item['id'] ?>" class="wptl-catalog-item-header" data-mh="wptl-catalog-item-header"><?php
    }
}
add_action('wp2leads_catalog_catalog_item_title', 'wp2leads_catalog_item_header_open', 20, 2);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_title')) {
    function wp2leads_catalog_item_title($catalog_item, $params) {
        WP2L_Catalog_Functions::show_template('catalog-item-title.php', array(
                'title' => $catalog_item['title'],'id' => $catalog_item['id']
        ));
    }
}
add_action('wp2leads_catalog_catalog_item_title', 'wp2leads_catalog_item_title', 30, 2);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_header_close')) {
    function wp2leads_catalog_item_header_close($catalog_item, $params) {
        ?></div><?php
    }
}
add_action('wp2leads_catalog_catalog_item_title', 'wp2leads_catalog_item_header_close', 50, 2);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_tags')) {
    function wp2leads_catalog_item_tags($catalog_item, $params, $catalog) {
        if (!empty($catalog_item['tags'])) {
            $item_tags = unserialize($catalog_item['tags']);

            WP2L_Catalog_Functions::show_template('catalog-item-tags.php', array(
                'item_tags' => $item_tags,'tags' => $catalog['tags'],'id' => $catalog_item['id']
            ));
        }
    }
}
add_action('wp2leads_catalog_catalog_item_title', 'wp2leads_catalog_item_tags', 40, 3);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_content')) {
    function wp2leads_catalog_item_content($catalog_item, $params) {
        WP2L_Catalog_Functions::show_template('catalog-item-content.php', array(
            'content' => $catalog_item['short_description'],'id' => $catalog_item['id']
        ));
    }
}
add_action('wp2leads_catalog_catalog_item_content', 'wp2leads_catalog_item_content', 30, 2);

/**
 *
 */
if (!function_exists('wp2leads_catalog_item_buttons')) {
    function wp2leads_catalog_item_buttons($catalog_item, $params) {
        $example_link = false;

        if (!empty($params['example_link'])) {
            $example_link = !empty($catalog_item['example_link']) ? esc_url($catalog_item['example_link']) : null;
        }

        WP2L_Catalog_Functions::show_template('catalog-item-buttons.php', array(
            'example_link' => $example_link,'id' => $catalog_item['id'],'params' => $params
        ));
    }
}
add_action('wp2leads_catalog_catalog_item_content', 'wp2leads_catalog_item_buttons', 30, 2);

if (!function_exists('wp2leads_catalog_item_wrap_class')) {
    function wp2leads_catalog_item_wrap_class($classes, $params) {
        $classes[] = 'wptl-col-xs-12';

        switch ($params['col_md']) {
            case 3:
                $classes[] = 'wptl-col-md-4';
                break;
            case 1:
                $classes[] = 'wptl-col-md-12';
                break;
            default:
                $classes[] = 'wptl-col-md-6';
        }

        switch ($params['col_lg']) {
            case 4:
                $classes[] = 'wptl-col-lg-3';
                break;
            case 2:
                $classes[] = 'wptl-col-lg-6';
                break;
            case 1:
                $classes[] = 'wptl-col-lg-12';
                break;
            default:
                $classes[] = 'wptl-col-lg-4';
        }

        return $classes;
    }
}
add_filter('wp2leads_catalog_item_wrap_class', 'wp2leads_catalog_item_wrap_class', 10, 2);
