<?php
/**
 * Plugin Name:     WP2LEADS connection Catalog
 * Description:     Use [wp2l_catalog] shortcode to show all items. Click author URL to see all shortcode settings with examples
 * Version:         1.0.5
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/wp2leads-catalog-on-your-website/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_catalog
 *
 * Requires at least: 5.0
 * Tested up to: 6.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2L_CATALOG_VERSION', '1.0.5' );
define( 'WP2L_CATALOG_PLUGIN_FILE', __FILE__ );
define( 'WP2L_CATALOG_PLUGIN_REL_FILE', dirname( plugin_basename( __FILE__ ) ) );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_catalog.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_catalog'
);

require_once('includes/WP2L_Catalog_Functions.php');
add_action( 'plugins_loaded', 'WP2L_Catalog_Functions::load_plugin_textdomain' );
require_once('includes/WP2L_Catalog_Shortcode.php');
require_once('includes/WP2L_Catalog_Model.php');
require_once('includes/WP2L_Catalog_Public.php');

if (is_admin()) {
    require_once('includes/WP2L_Catalog_Admin.php');
}

require_once('includes/template_functions.php');
