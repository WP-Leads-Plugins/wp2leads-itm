<?php
/**
 * @var $catalog
 * @var $params
 */

defined( 'ABSPATH' ) || exit;

//var_dump($catalog['tags']);

if (empty($catalog)) return '';

do_action( 'wp2leads_catalog_before_wrapper', $catalog, $params );
?>
<div class="wptl-catalog-items">
<?php
do_action( 'wp2leads_catalog_before_main_content', $catalog, $params );

foreach ($catalog['items'] as $catalog_item) {
    WP2L_Catalog_Functions::show_template('catalog-item.php', array(
        'catalog_item' => $catalog_item,'params' => $params,'catalog' => $catalog
    ));
}

do_action( 'wp2leads_catalog_after_main_content', $catalog, $params );
?>
</div>
<?php
do_action( 'wp2leads_catalog_after_wrapper', $catalog, $params );

