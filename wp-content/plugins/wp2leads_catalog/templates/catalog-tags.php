<?php
/**
 * @var $tags
 * @var $params
 */

defined( 'ABSPATH' ) || exit;
if (empty($tags)) return '';
?>
<div class="wptl-catalog-tags">
    <div class="wptl-row">
        <div class="wptl-col-xs-12">
            <?php
            if (1 === count($tags)) {
                foreach ($tags as $id => $tag) {
                    ?><button class="wptl-catalog-tag all active" data-tag="all_tags"><?php echo $tag; ?></button><?php
                }
            } else {
                ?><button class="wptl-catalog-tag all active" data-tag="all_tags"><?php echo __('All tags', 'wp2leads_catalog'); ?></button><?php
                foreach ($tags as $id => $tag) {
                    ?><button class="wptl-catalog-tag tag-<?php echo $id; ?>" data-tag="<?php echo $id; ?>"><?php echo $tag; ?></button><?php
                }
                ?>
                <?php
            }
            ?>
        </div>
    </div>
</div>