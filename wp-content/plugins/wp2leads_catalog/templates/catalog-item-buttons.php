<?php
/**
 * @var $example_link
 * @var $params
 * @var $id
 */
?>
<div class="wptl-catalog-item-action">
    <?php
    if (!empty($example_link)) {
        ?>
        <a href="<?php echo $example_link ?>" target="_blank" class="wptl-btn size-extended type-hollow">
            <?php echo __('View Example', 'wp2leads_catalog'); ?>
        </a>
        <?php
    }

    if (!empty($params['link_url']) && !empty($params['link_text'])) {
        ?><a target="_blank" href="<?php echo $params['link_url']; ?>" class="wptl-btn size-extended"><?php echo $params['link_text']; ?></a><?php
    }
    ?>
</div>