<?php
/**
 * @var $id
 * @var $content
 */
?>

<div id="wptl-catalog-item-content-<?php echo $id ?>" class="wptl-catalog-item-content content">
    <?php echo $content; ?>
</div>
