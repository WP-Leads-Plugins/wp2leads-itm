<?php
/**
 * @var $id
 * @var $src
 * @var $alt
 */

defined( 'ABSPATH' ) || exit;
if (empty($src)) return '';
?>

<div id="wptl-catalog-item-img-<?php echo $id ?>" class="wptl-catalog-item-img">
    <img src="<?php echo $src ?>" alt="<?php echo $alt ?>">
</div>
