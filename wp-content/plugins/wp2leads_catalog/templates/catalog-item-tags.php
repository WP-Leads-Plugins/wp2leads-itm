<?php
/**
 * @var $item_tags
 * @var $tags
 * @var $id
 */
?>
<div id="wptl-catalog-item-tags-<?php echo $id ?>" class="wptl-catalog-item-tags">
    <?php
    foreach ($item_tags as $item_tag) {
        if (!empty($tags[$item_tag])) {
            ?><button class="wptl-item-tag" data-tag="<?php echo $item_tag; ?>"><?php echo $tags[$item_tag] ?></button><?php
        }
    }
    ?>
</div>
