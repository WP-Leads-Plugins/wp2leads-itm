<?php
/**
 * @var $catalog_item
 * @var $catalog
 * @var $params
 */

defined( 'ABSPATH' ) || exit;
if (empty($catalog_item)) return '';

do_action( 'wp2leads_catalog_before_catalog_item', $catalog_item, $params, $catalog );
do_action( 'wp2leads_catalog_catalog_item_title', $catalog_item, $params, $catalog );

// var_dump($catalog_item);
// var_dump(unserialize($catalog_item['tags']));
do_action( 'wp2leads_catalog_catalog_item_content', $catalog_item, $params, $catalog );
do_action( 'wp2leads_catalog_after_catalog_item', $catalog_item, $params, $catalog );
