<?php
$catalog_data = WP2L_Catalog_Model::get();
$items = !empty($catalog_data['items']) ? $catalog_data['items'] : array();
$tags = !empty($catalog_data['tags']) ? $catalog_data['tags'] : array();
$tags_items = array();

foreach ($items as $item) {
    $item_tags = unserialize($item['tags']);

    foreach ($item_tags as $item_tag) {
        $tags_items[$item_tag][$item['id']] = $item['title'];
    }

    // var_dump($item_tags);
}

// var_dump($tags_items);
?>

<div class="wrap">
    <h1><?php _e("WP2LEADS Catalog Shortcode", 'wp2leads_catalog'); ?></h1>

    <?php settings_errors(); ?>

    <div class="wptl-settings-group">
        <div class="wptl-settings-group-header">
            <h3><?php _e('Tags List', 'wp2leads_catalog') ?></h3>
        </div>

        <div class="wptl-settings-group-body">
            <?php
            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    ?>
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                            <p>
                                <small>
                                    <?php _e("ID", 'wp2leads_catalog'); ?>
                                </small>: <strong><?php echo $tag['id'] ?> </strong> -
                                <strong><?php echo $tag['name'] ?></strong>
                            </p>
                        </div>
                        <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                            <?php
                            if (!empty($tags_items[$tag['id']])) {
                                ?>
                                <p>
                                    <?php _e("items for this tag", 'wp2leads_catalog'); ?>:
                                </p>
                                <ul>
                                    <?php
                                    foreach ($tags_items[$tag['id']] as $cat_id => $cat_item) {
                                        ?>
                                        <li>
                                            <small>
                                                <?php _e("ID", 'wp2leads_catalog'); ?>
                                            </small>: <strong><?php echo $cat_id?></strong> - <strong><?php echo $cat_item ?></strong>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                                // var_dump($tags_items[$tag['id']]);
                            } else {
                                ?>
                                <p>
                                    <?php _e("no items for this tag", 'wp2leads_catalog'); ?>
                                </p>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
            }
            // var_dump($tags);
            ?>
        </div>
    </div>
</div>
