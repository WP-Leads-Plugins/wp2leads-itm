<?php


class WP2LITM_WOOFD_Instant_Transfer {
    private static $key = 'wp2leads_itm_woo_free_downloads';
    private static $required_column = 'posts.ID';

    public static function get_label() {
        return __('Free Downloads WooCommerce Pro Transfer', 'wp2leads_itm_woo_free_downloads');
    }

    public static function get_description() {
        return __('This module will transfer user data to KlickTipp once Downloads WooCommerce Transfer log item created created', 'wp2leads_itm_woo_free_downloads');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Downloads WooCommerce Transfer Pro plugin map.', 'wp2leads_itm_woo_free_downloads') ?></p>
        <p><?php _e('Once new Downloads WooCommerce Transfer log item created user data will be transfered to KlickTipp account.', 'wp2leads_itm_woo_free_downloads') ?></p>
        <p><?php _e('Requirement: <strong>posts.ID</strong> column within selected data.', 'wp2leads_itm_woo_free_downloads') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function transfer_init() {
        add_action('somdn_count_download_post_success', 'WP2LITM_WOOFD_Instant_Transfer::transfer', 30, 2);
    }

    public static function transfer($post_id, $post_information) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $post_id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }

}

function wp2leads_itm_woo_free_downloads_itm($transfer_modules) {
    $transfer_modules['wp2leads_itm_woo_free_downloads'] = 'WP2LITM_WOOFD_Instant_Transfer';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_itm_woo_free_downloads_itm');