<?php


class WP2LITM_WOOFD_Notices {
    private static $notices = array(
        'requirements_failed' => array(
            'type' => 'error',
            'position' => ''
        ),
    );

    public static function show_message($notice_id) {
        add_action('admin_notices', 'WP2LITM_WOOFD_Notices::' . $notice_id);
    }

    public static function requirements_failed() {
        $message = sprintf( __( 'Please, install all required plugins.', 'wp2leads_itm_woo_free_downloads' ) );

        if (!WP2LITM_WOOFD_Functions::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) {
            $message .= ' <br>' . sprintf(
                    __( 'WP2LEADS plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_woo_free_downloads' ),
                    '<a href="/wp-admin/plugin-install.php?s=WP2LEADS&tab=search&type=term">',
                    '</a>'
                );
        }

        if (!WP2LITM_WOOFD_Functions::is_plugin_activated( 'woocommerce', 'woocommerce.php' )) {
            $message .= ' <br>' . sprintf(
                    __( 'Woocommerce plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_woo_free_downloads' ),
                    '<a href="/wp-admin/plugin-install.php?s=Woocommerce&tab=search&type=term">',
                    '</a>'
                );
        }

        if (!WP2LITM_WOOFD_Functions::is_plugin_activated( 'som-free-downloads-woocommerce-pro', 'free-downloads-woocommerce-pro.php' )) {
            $message .= ' <br>' . sprintf(
                    __( 'Free Downloads WooCommerce Pro plugin is required.', 'wp2leads_itm_woo_free_downloads' )
                );
        }
        ?>
        <div class="notice notice-error">
            <p>
                <strong><?php echo WP2LITM_WOOFD_Functions::get_plugin_name(); ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }
}