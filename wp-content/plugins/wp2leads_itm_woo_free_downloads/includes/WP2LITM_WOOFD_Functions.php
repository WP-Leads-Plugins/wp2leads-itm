<?php


class WP2LITM_WOOFD_Functions {
    public static function load_plugin_textdomain() {
        add_filter( 'plugin_locale', 'WP2LITM_WOOFD_Functions::check_de_locale');

        load_plugin_textdomain(
            'wp2leads_itm_woo_free_downloads',
            false,
            WP2LITM_NEXFORMS_PLUGIN_REL_FILE . '/languages/'
        );

        remove_filter( 'plugin_locale', 'WP2LITM_WOOFD_Functions::check_de_locale');
    }

    public static function check_de_locale($domain) {
        $site_lang = get_user_locale();
        $de_lang_list = array(
            'de_CH_informal',
            'de_DE_formal',
            'de_AT',
            'de_CH',
            'de_DE'
        );

        if (in_array($site_lang, $de_lang_list)) return 'de_DE';
        return $domain;
    }

    public static function requirement() {
        if (!self::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) return false;
        if (!self::is_plugin_activated( 'woocommerce', 'woocommerce.php' )) return false;
        if (!self::is_plugin_activated( 'som-free-downloads-woocommerce-pro', 'free-downloads-woocommerce-pro.php' )) return false;

        return true;
    }

    public static function is_plugin_activated( $plugin_folder, $plugin_file ) {
        if ( self::is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
        else return self::is_plugin_active_by_file( $plugin_file );
    }

    public static function is_plugin_active_simple( $plugin ) {
        return (
            in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
            ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
        );
    }

    public static function is_plugin_active_by_file( $plugin_file ) {
        foreach ( self::get_active_plugins() as $active_plugin ) {
            $active_plugin = explode( '/', $active_plugin );
            if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
        }

        return false;
    }

    public static function get_active_plugins() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
        if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

        return $active_plugins;
    }

    public static function get_plugin_name() {
        $data = get_plugin_data( WP2LITM_WOOFD_PLUGIN_FILE );

        return $data['Name'];
    }
}