<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for Free Downloads WooCommerce plugin
 * Description:
 * Version:         1.0.0
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_woo_free_downloads
 *
 * Requires at least: 5.0
 * Tested up to: 5.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_WOOFD_VERSION', '1.0.0' );
define( 'WP2LITM_WOOFD_DB_VERSION', '1.0.0' );
define( 'WP2LITM_WOOFD_PLUGIN_FILE', __FILE__ );
define( 'WP2LITM_WOOFD_PLUGIN_REL_FILE', dirname( plugin_basename( __FILE__ ) ) );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_woo_free_downloads.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_woo_free_downloads'
);

require_once('includes/WP2LITM_WOOFD_Functions.php');
require_once('includes/WP2LITM_WOOFD_Notices.php');

if (!WP2LITM_WOOFD_Functions::requirement()) {
    WP2LITM_WOOFD_Notices::show_message('requirements_failed');

    return;
}

require_once('transfer-modules/WP2LITM_WOOFD_Instant_Transfer.php');