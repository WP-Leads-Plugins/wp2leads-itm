<?php


class WP2LITMMID_NFIntegration {
    public static function get_subscriber_mandanten_fields($email_address) {
        if (!WP2LITMMID_Settings::is_settings_done()) return false;
        $subscriber = WP2LITMMID_KlickTippHelper::get_kt_subscriber($email_address);
        if (false === $subscriber) return WP2LITMMID_NFIntegration::get_subscriber_mandanten_fields_db($email_address);
        $settings = WP2LITMMID_Settings::get_settings();
        $subscriber_data = array();

        foreach ($settings['fields'] as $field_type => $field_id) {
            if (isset($subscriber[$field_id])) {
                $subscriber_data[$field_type] = $subscriber[$field_id];
            } else {
                $subscriber_data[$field_type] = '';
            }
        }

        return $subscriber_data;
    }

    public static function get_subscriber_mandanten_fields_db($email_address) {
        global $wpdb;
        $sql = "SELECT nfe.Id, nfe.nex_forms_Id, nfe.date_time, wptlnf.field_name_single, wptlnf.field_value_single
FROM {$wpdb->prefix}wap_nex_forms_entries AS nfe 
LEFT JOIN {$wpdb->prefix}wp2leads_nexforms_fields AS wptlnf
ON nfe.Id = wptlnf.entry_id
WHERE 
nfe.saved_user_email_address = '{$email_address}'
AND wptlnf.field_name_single IN (
'mandantenid_1', 
'mandantenid_2', 
'mandantenid_3', 
'mandantenid_4', 
'mandantenid_5', 
'mandantenpdf_1', 
'mandantenpdf_2', 
'mandantenpdf_3', 
'mandantenpdf_4', 
'mandantenpdf_5', 
'mandantenpdftitle_1', 
'mandantenpdftitle_2', 
'mandantenpdftitle_3', 
'mandantenpdftitle_4', 
'mandantenpdftitle_5', 
'mandantenhtml_1', 
'mandantenhtml_2', 
'mandantenhtml_3', 
'mandantenhtml_4', 
'mandantenhtml_5'
)
ORDER BY nfe.Id DESC 
";
        $default_settings = WP2LITMMID_Settings::get_default_settings();
        $subscriber_data = $default_settings['fields'];
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!empty($result)) {
            foreach ($result as $data) {
                if (
                    isset($subscriber_data[$data['field_name_single']]) &&
                    !empty($data['field_value_single']) &&
                    empty($subscriber_data[$data['field_name_single']])
                ) {
                    $subscriber_data[$data['field_name_single']] = $data['field_value_single'];
                }
            }
        }

        return $subscriber_data;
    }

    public static function forms_to_map($forms_to_map, $form_id) {
        $settings = WP2LITMMID_Settings::get_settings();

        if (empty($settings['map']) || empty($settings['forms']) || !in_array($form_id, array_keys($settings['forms']))) {
            return $forms_to_map;
        }

        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2l_maps';
        $map_sql = "SELECT * FROM {$table_name} WHERE id = '{$settings['map']}'";
        $map_result = $wpdb->get_row($map_sql, ARRAY_A);

        $forms_to_map[$form_id] = $map_result;

        return $forms_to_map;
    }

    /**
     * Set global variable with KT user data
     *
     * @param $entry_id
     */
    public static function after_insert_post_data($entry_id) {
        if (!WP2LITMMID_Settings::is_settings_done()) return;

        $nex_forms_id = isset($_REQUEST['nex_forms_Id']) ? filter_var($_POST['nex_forms_Id'],FILTER_SANITIZE_NUMBER_INT) : '';
        $settings = WP2LITMMID_Settings::get_settings();

        if (empty($settings['forms'][$nex_forms_id])) return;

        global $wpdb;
        global $wp2leads_itm_nexforms_entry;

        $get_form = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms WHERE Id = %d',$nex_forms_id);
        $form_attr = $wpdb->get_row($get_form);
        $user_email = $_REQUEST[$form_attr->user_email_field];
        $kt_fields = self::get_subscriber_mandanten_fields($user_email);

        if (false === $kt_fields) return;

        $get_entry = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms_entries WHERE Id = %d',$entry_id);
        $form_entry = $wpdb->get_row($get_entry, ARRAY_A);
        $form_data_global = array();

        if (!empty($form_entry['form_data'])) {
            $form_data = json_decode( $form_entry['form_data'], true );

            if (!empty($form_data)) {
                foreach ($form_data as $form_data_item) {
                    $field_name = rtrim ($form_data_item['field_name'], ':');
                    $field_value = $form_data_item['field_value'];
                    $form_data_global[$field_name] = $field_value;
                }
            }
        }

        $wp2leads_itm_nexforms_entry = array(
            'now' => current_time( 'timestamp' ),
            'email' => $user_email,
            'kt_fields' => self::get_subscriber_mandanten_fields($user_email),
            'form_title' => $form_attr->title,
            'form_data' => $form_data_global,
        );
    }

    public static function entry_field_by_pdf($pdf, $pdf_id) {
        if (!WP2LITMMID_Settings::is_settings_done()) return $pdf;
        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2leads_nexforms_fields';
        $sql = "SELECT * FROM {$table_name} WHERE field_name_single LIKE ('mandantenpdf_%') AND field_value_single LIKE ('%{$pdf_id}%') ORDER BY ID ASC";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (empty($result)) return $pdf;
        $last_entry = $result[0];
        $entry_id = $last_entry['entry_id'];

        global $wpdb;
        $get_entry = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms_entries WHERE Id = %d',$entry_id);
        $form_entry = $wpdb->get_row($get_entry, ARRAY_A);
        $nex_forms_id = $form_entry['nex_forms_Id'];
        $settings = WP2LITMMID_Settings::get_settings();
        if (empty($settings['forms'][$nex_forms_id])) return $pdf;

        $pdf_title_array = explode('_', $pdf_id);
        $id = $pdf_title_array[0] . '_';
        $pdf_title = ltrim($pdf_id, $id);
        $sql = "SELECT * FROM {$table_name} WHERE field_name_single LIKE ('mandantenpdftitle_%') AND field_value_single LIKE ('{$pdf_title}.pdf') ORDER BY ID DESC";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (empty($result)) return $pdf;
        global $wp2leads_itm_nexforms_entry;

        $wp2leads_itm_nexforms_entry = array('pdf_name' => $pdf_title );

        return $last_entry;
    }

    public static function before_delete_pdf($pdf_id) {
        if (!WP2LITMMID_Settings::is_settings_done()) return;

        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2leads_nexforms_fields';
        $sql = "SELECT * FROM {$table_name} WHERE field_name_single LIKE ('mandantenpdf_%') AND field_value_single LIKE ('%{$pdf_id}%') ORDER BY ID DESC";
        $result = $wpdb->get_results($sql, ARRAY_A);
        if (empty($result)) return;

        $pdf_title_array = explode('_', $pdf_id);
        $id = $pdf_title_array[0] . '_';
        $pdf_title = ltrim($pdf_id, $id);

        foreach ($result as $item_to_delete) {
            $delete_sql = "DELETE FROM {$table_name} WHERE ID = '{$item_to_delete['ID']}'";
            $wpdb->query($delete_sql);

            $index_array = explode('_', $item_to_delete['field_name_single']);
            $sql = "SELECT * FROM {$table_name} WHERE field_name_single = 'mandantenpdftitle_{$index_array[1]}' AND entry_id = '{$item_to_delete['entry_id']}' AND field_value_single LIKE ('{$pdf_title}.pdf') ORDER BY ID DESC";
            $result_title = $wpdb->get_row($sql, ARRAY_A);

            if (!empty($result_title)) {
                $delete_sql = "DELETE FROM {$table_name} WHERE ID = '{$result['ID']}'";
                $wpdb->query($delete_sql);
            }
        }
    }

    public static function pdf_name($name, $entry_ID) {
        if (!WP2LITMMID_Settings::is_settings_done()) return $name;
        global $wp2leads_itm_nexforms_entry;
        if (empty($wp2leads_itm_nexforms_entry)) return $name;

        if (!empty($wp2leads_itm_nexforms_entry['pdf_name'])) return $wp2leads_itm_nexforms_entry['pdf_name'];

        $settings = WP2LITMMID_Settings::get_settings();
        if (empty($settings['pdf_title'])) return $name;

        $name = $settings['pdf_title'];
        $form_title = sanitize_title($wp2leads_itm_nexforms_entry['form_title']);
        $form_title_array = explode('-', $form_title);

        foreach ($form_title_array as $i => $part) {
            $form_title_array[$i] = ucfirst($part);
        }

        $form_title = implode('_', $form_title_array);

        if (!empty($wp2leads_itm_nexforms_entry['form_data'])) {
            foreach ($wp2leads_itm_nexforms_entry['form_data'] as $fn => $fv) {
                $name = str_replace('{{'.trim($fn).'}}', str_replace(' ', '_', sanitize_text_field($fv)), $name);
            }
        }

        $name = str_replace('{{YYYY}}', date('Y', $wp2leads_itm_nexforms_entry['now']), $name);
        $name = str_replace('{{MM}}', date('m', $wp2leads_itm_nexforms_entry['now']), $name);
        $name = str_replace('{{DD}}', date('d', $wp2leads_itm_nexforms_entry['now']), $name);
        $name = str_replace('{{entry_id}}', $entry_ID, $name);
        $name = str_replace('{{Form_Title}}', $form_title, $name);
        $name = str_replace('{{', '', $name);
        $name = str_replace('}}', '', $name);
        $name = sanitize_text_field($name);

        return $name;
    }

    public static function data_for_kt($data_for_kt, $entry_id) {
        if (!WP2LITMMID_Settings::is_settings_done()) return $data_for_kt;
        global $wp2leads_itm_nexforms_entry;
        if (empty($wp2leads_itm_nexforms_entry)) return $data_for_kt;

        $mandantenid = '';
        $mandantenpdf = '';
        $mandantenpdftitle = '';
        $mandantenhtml = '';

        foreach ($data_for_kt as $i => $data_for_kt_item) {
            if ($data_for_kt_item['field_name'] === 'mandantenid') {
                $mandantenid = $data_for_kt_item['field_value'];
            }

            if ($data_for_kt_item['field_name_single'] === 'user_pdf_link') {
                $mandantenpdf = $data_for_kt_item['field_value_single'];
                unset($data_for_kt[$i]);
            }

            if ($data_for_kt_item['field_name_single'] === 'user_pdf_title') {
                $mandantenpdftitle = $data_for_kt_item['field_value_single'];
                unset($data_for_kt[$i]);
            }

            if ($data_for_kt_item['field_name_single'] === 'user_email_html') {
                $mandantenhtml = $data_for_kt_item['field_value_single'];
                unset($data_for_kt[$i]);
            }
        }

        if (empty($mandantenid)) return $data_for_kt;

        $kt_fields = $wp2leads_itm_nexforms_entry['kt_fields'];

        if (empty($kt_fields['mandantenid_1']) || $kt_fields['mandantenid_1'] == $mandantenid ) {
            $index = 1;
        } elseif (empty($kt_fields['mandantenid_2']) || $kt_fields['mandantenid_2'] == $mandantenid ) {
            $index = 2;
        } elseif (empty($kt_fields['mandantenid_3']) || $kt_fields['mandantenid_3'] == $mandantenid ) {
            $index = 3;
        } elseif (empty($kt_fields['mandantenid_4']) || $kt_fields['mandantenid_4'] == $mandantenid ) {
            $index = 4;
        } elseif (empty($kt_fields['mandantenid_5']) || $kt_fields['mandantenid_5'] == $mandantenid ) {
            $index = 5;
        }

        $kt_fields['mandantenid_' . $index] = $mandantenid;
        $kt_fields['mandantenpdf_' . $index] = $mandantenpdf;
        $kt_fields['mandantenpdftitle_' . $index] = $mandantenpdftitle;
        $kt_fields['mandantenhtml_' . $index] = $mandantenhtml;

        if (!empty($wp2leads_itm_nexforms_entry)) {
            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenid_1',
                'field_value_single' => $kt_fields['mandantenid_1'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenid_2',
                'field_value_single' => $kt_fields['mandantenid_2'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenid_3',
                'field_value_single' => $kt_fields['mandantenid_3'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenid_4',
                'field_value_single' => $kt_fields['mandantenid_4'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenid_5',
                'field_value_single' => $kt_fields['mandantenid_5'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdf_1',
                'field_value_single' => $kt_fields['mandantenpdf_1'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdf_2',
                'field_value_single' => $kt_fields['mandantenpdf_2'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdf_3',
                'field_value_single' => $kt_fields['mandantenpdf_3'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdf_4',
                'field_value_single' => $kt_fields['mandantenpdf_4'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdf_5',
                'field_value_single' => $kt_fields['mandantenpdf_5'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdftitle_1',
                'field_value_single' => $kt_fields['mandantenpdftitle_1'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdftitle_2',
                'field_value_single' => $kt_fields['mandantenpdftitle_2'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdftitle_3',
                'field_value_single' => $kt_fields['mandantenpdftitle_3'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdftitle_4',
                'field_value_single' => $kt_fields['mandantenpdftitle_4'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenpdftitle_5',
                'field_value_single' => $kt_fields['mandantenpdftitle_5'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenhtml_1',
                'field_value_single' => $kt_fields['mandantenhtml_1'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenhtml_2',
                'field_value_single' => $kt_fields['mandantenhtml_2'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenhtml_3',
                'field_value_single' => $kt_fields['mandantenhtml_3'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenhtml_4',
                'field_value_single' => $kt_fields['mandantenhtml_4'],
            );

            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'mandantenhtml_5',
                'field_value_single' => $kt_fields['mandantenhtml_5'],
            );
        }

        return $data_for_kt;
    }

    public static function pdf_after_created_new($pdf) {
        $pdf->SetLeftMargin(25);
        $pdf->SetRightMargin(15);

        return $pdf;
    }
}

add_action( 'wp2leads_itm_nexforms_after_insert_post_data', 'WP2LITMMID_NFIntegration::after_insert_post_data' );
add_action( 'wp2leads_itm_nexforms_before_delete_pdf', 'WP2LITMMID_NFIntegration::before_delete_pdf' );

add_filter( 'wp2leads_itm_nexforms_entry_field_by_pdf', 'WP2LITMMID_NFIntegration::entry_field_by_pdf', 15, 2 );
add_filter( 'wp2leads_itm_nexforms_pdf_name', 'WP2LITMMID_NFIntegration::pdf_name', 15, 2 );
add_filter( 'wp2leads_itm_nexforms_data_for_kt', 'WP2LITMMID_NFIntegration::data_for_kt', 15, 2 );
add_filter( 'wp2leads_itm_nexforms_forms_to_map', 'WP2LITMMID_NFIntegration::forms_to_map', 15, 2 );

add_filter( 'wp2leads_itm_nexforms_pdf_after_created_new', 'WP2LITMMID_NFIntegration::pdf_after_created_new' );