<?php


class WP2LITMMID_KlickTippHelper {
    public static function get_kt_connector() {
        $connector = new WP2LITMMID_KlickTippConnector();

        $logged_in = $connector->login(get_option('wp2l_klicktipp_username'), get_option('wp2l_klicktipp_password'));
        if(!$logged_in) return false;

        return $connector;
    }

    public static function get_kt_fields($connector) {
        if (empty($connector)) {
            $connector = self::get_kt_connector();
        }

        if (empty($connector)) return false;
        $fields = $connector->field_index();

        foreach (self::get_default_kt_fields() as $field_id) {
            if (!empty($fields[$field_id])) {
                unset($fields[$field_id]);
            }
        }

        return $fields;
    }

    public static function get_kt_subscriber($email_address, $connector = null) {
        if (empty($connector)) {
            $connector = self::get_kt_connector();
        }

        if (empty($connector)) return false;
        $id = $connector->subscriber_search($email_address);
        if (empty($id)) return array('new_user', true);
        $subscriber = json_decode(json_encode($connector->subscriber_get($id)), true);
        return $subscriber;
    }

    public static function get_default_kt_fields() {
        return array(
            'fieldFirstName',
            'fieldLastName',
            'fieldCompanyName',
            'fieldStreet1',
            'fieldStreet2',
            'fieldCity',
            'fieldState',
            'fieldZip',
            'fieldCountry',
            'fieldPrivatePhone',
            'fieldMobilePhone',
            'fieldPhone',
            'fieldFax',
            'fieldWebsite',
            'fieldBirthday',
            'fieldLeadValue',
        );
    }
}