<?php


class WP2LITMMID_Settings {
    public static function get_settings() {
        $settings = get_option('wp2leads_itm_mid_settings', array());

        return array_merge(self::get_default_settings(), $settings);
    }

    public static function get_default_settings() {
        return array(
            'fields' => array(
                'mandantenid_1' => '',
                'mandantenid_2' => '',
                'mandantenid_3' => '',
                'mandantenid_4' => '',
                'mandantenid_5' => '',
                'mandantenpdf_1' => '',
                'mandantenpdf_2' => '',
                'mandantenpdf_3' => '',
                'mandantenpdf_4' => '',
                'mandantenpdf_5' => '',
                'mandantenpdftitle_1' => '',
                'mandantenpdftitle_2' => '',
                'mandantenpdftitle_3' => '',
                'mandantenpdftitle_4' => '',
                'mandantenpdftitle_5' => '',
                'mandantenhtml_1' => '',
                'mandantenhtml_2' => '',
                'mandantenhtml_3' => '',
                'mandantenhtml_4' => '',
                'mandantenhtml_5' => '',
            ),
            'forms' => array(),
            'map' => '',
            'pdf_title' => '',
        );
    }

    public static function is_settings_done() {
        $settings = self::get_settings();
        $required_fields = self::get_default_settings();
        $required_fields = $required_fields['fields'];

        if (empty($settings['forms']) || empty($settings['map'])) return false;

        foreach ($required_fields as $fi => $fv) {
            if (empty($settings['fields'][$fi])) return false;
        }

        return true;
    }
}