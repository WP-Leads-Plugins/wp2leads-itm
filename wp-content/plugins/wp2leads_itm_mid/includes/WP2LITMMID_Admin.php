<?php


class WP2LITMMID_Admin {
    public static function add_submenu_page() {
        add_submenu_page(
            'wp2l-admin',
            __("MID to KlickTipp", 'wp2leads_itm_mid'),
            __("MID to KlickTipp", 'wp2leads_itm_mid'),
            'manage_options',
            'wp2leads_itm_mid',
            'WP2LITMMID_Admin::display_submenu_page'
        );
    }

    public static function display_submenu_page() {
        include_once plugin_dir_path( WP2LITM_MID_PLUGIN_FILE ) . 'templates/admin-page.php';
    }

    public static function enqueue_scripts() {
        if (!empty($_GET['page']) && $_GET['page'] === 'wp2leads_itm_mid') {
            wp_enqueue_style( 'wp2leads-itm-mid', plugin_dir_url( WP2LITM_MID_PLUGIN_FILE ) . 'assets/css/admin.css', array(), WP2LITM_NEXFORMS_VERSION . time(), 'all' );
            wp_enqueue_script( 'wp2leads-itm-mid', plugin_dir_url( WP2LITM_MID_PLUGIN_FILE ) . 'assets/js/admin.js', array( 'jquery' ), WP2LITM_NEXFORMS_VERSION . time(), true );
        }
    }
}

add_action('admin_menu', array ('WP2LITMMID_Admin', 'add_submenu_page'));
add_action( 'admin_enqueue_scripts', array( 'WP2LITMMID_Admin', 'enqueue_scripts' ) );