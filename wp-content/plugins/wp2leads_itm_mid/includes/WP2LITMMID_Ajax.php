<?php


class WP2LITMMID_Ajax {
    public static function save_settings() {
        $form_data = $_POST['formData'];

        if (empty($form_data)) {
            $params = array(
                'message' => __('Error', 'wp2leads_itm_mid') . ': ' . __('Cheating, huh!!!', 'wp2leads_itm_mid'),
                'reload' => 0,
            );

            self::error_response($params);
        }

        $data = array();

        foreach ($_POST['formData'] as $form_data) {
            $form_data_name = sanitize_text_field($form_data['name']);
            $form_data_name_array = explode('[', $form_data_name);

            if (1 === count($form_data_name_array)) {
                $data[$form_data_name] = $form_data['value'];
            } else {
                $first_level = $form_data_name_array[0];
                $second_level = rtrim($form_data_name_array[1], ']');

                $data[$first_level][$second_level] = $form_data['value'];
            }

        }

        if (!empty($data)) {
            $settings =  WP2LITMMID_Settings::get_settings();
            if (empty($data['forms'])) {
                $settings['forms'] = array();
            }
            $settings = array_merge($settings, $data);
            update_option('wp2leads_itm_mid_settings', $settings);
        }

        $params = array(
            'message' => __('Success', 'wp2leads_itm_mid') . ': ' . __('Settings saved.', 'wp2leads_itm_mid'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function error_response($params = array()) {
        $response = array('success' => 0, 'error' => 1);

        if (!empty($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }

    public static function success_response($params = array()) {
        $response = array('success' => 1, 'error' => 0);

        if (!empty($params) && is_array($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }
}

add_action('wp_ajax_nopriv_wp2leads_itm_mid_settings_save', array('WP2LITMMID_Ajax', 'save_settings'));
add_action('wp_ajax_wp2leads_itm_mid_settings_save', array('WP2LITMMID_Ajax', 'save_settings'));