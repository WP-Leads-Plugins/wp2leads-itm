<?php
/**
 * Plugin Name:     Wp2Leads ITM for NEX-Forms Addon (Mandanten ID)
 * Description:
 * Version:         1.0.10
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_mid
 *
 * Requires at least: 5.0
 * Tested up to: 5.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_MID_VERSION', '1.0.10' );
define( 'WP2LITM_MID_DB_VERSION', '1.0.10' );
define( 'WP2LITM_MID_PLUGIN_FILE', __FILE__ );
define( 'WP2LITM_MID_PLUGIN_REL_FILE', dirname( plugin_basename( __FILE__ ) ) );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_mid.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_mid'
);

require_once('includes/WP2LITMMID_Functions.php');

add_action( 'plugins_loaded', 'WP2LITMMID_Functions::load_plugin_textdomain' );

if (!WP2LITMMID_Functions::requirement()) {
    return;
}

require_once('includes/WP2LITMMID_Settings.php');
require_once('includes/WP2LITMMID_Ajax.php');
require_once('includes/WP2LITMMID_KlickTippConnector.php');
require_once('includes/WP2LITMMID_KlickTippHelper.php');
require_once('includes/WP2LITMMID_NFIntegration.php');

if (is_admin()) {
    require_once('includes/WP2LITMMID_Admin.php');
}