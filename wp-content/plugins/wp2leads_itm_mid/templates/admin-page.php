<?php
// phpinfo();
$connector = WP2LITMMID_KlickTippHelper::get_kt_connector();
global $wpdb;
$forms_sql = "SELECT * FROM {$wpdb->prefix}wap_nex_forms WHERE is_form=1 ORDER BY Id DESC";
$forms_result = $wpdb->get_results($forms_sql, ARRAY_A);

$maps_sql = "SELECT * FROM {$wpdb->prefix}wp2l_maps ORDER BY id DESC";
$maps_result = $wpdb->get_results($maps_sql, ARRAY_A);

// var_dump($forms_result);
if ($connector) {
    $kt_fields = WP2LITMMID_KlickTippHelper::get_kt_fields($connector);
}

$mid_settings = WP2LITMMID_Settings::get_settings();

// var_dump(WP2LITMMID_NFIntegration::get_subscriber_mandanten_fields_db('vlad-mid123@wp2leads.com'));
// WP2LITMMID_NFIntegration::before_delete_pdf('4a4717174b37c5f4fd9a3cd067918ff4709d0a6f_Freigabeerklaerung_152395-2019-12-12-2020');
?>

<div class="wrap">
    <h1><?php _e("Mandanten ID Set up", 'wp2leads_itm_mid'); ?></h1>
    <?php settings_errors(); ?>

    <div class="wptl-settings-group">
        <div class="wptl-settings-group-header">
            <h3><?php _e('Make initial settings', 'wp2leads_itm_mid') ?></h3>
        </div>

        <?php
        if (!empty($forms_result) && !empty($kt_fields)) {
            ?>
            <form id="wp2leads_itm_mid_settings_form">
                <div class="wptl-settings-group-body">
                    <div class="wptl-row" style="padding-bottom:0;">
                        <div class="wptl-col-xs-12">
                            <h4 style="margin: 5px 0"><?php _e('Connect your fields', 'wp2leads_itm_mid') ?></h4>
                        </div>
                    </div>
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                            <?php
                            for ($i = 1; $i <= 5; $i++) {
                                ?>
                                <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                    <h4 style="margin-top: 0;">
                                        <?php _e('Mandanten ID', 'wp2leads_itm_mid') ?> <?php echo $i ?>
                                    </h4>
                                    <p style="margin-bottom: 0;">
                                        <select name="fields[mandantenid_<?php echo $i ?>]" id="mandantenid_<?php echo $i ?>" style="max-width: 98%">
                                            <option value=""><?php _e('--- select KT field ---', 'wp2leads_itm_mid') ?></option>
                                            <?php
                                            foreach ($kt_fields as $id => $field_title) {
                                                $selected = $mid_settings['fields']['mandantenid_' . $i] === $id;
                                                ?>
                                                <option value="<?php echo $id ?>"<?php echo $selected ? ' selected' : ''; ?>>
                                                    <?php echo $field_title ?> (<?php echo $id ?>)
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </p>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                            <?php
                            for ($i = 1; $i <= 5; $i++) {
                                ?>
                                <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                    <h4 style="margin-top: 0;">
                                        <?php _e('Mandanten PDF URL', 'wp2leads_itm_mid') ?> <?php echo $i ?>
                                    </h4>
                                    <p style="margin-bottom: 0;">
                                        <select name="fields[mandantenpdf_<?php echo $i ?>]" id="mandantenpdf_<?php echo $i ?>" style="max-width: 98%">
                                            <option value=""><?php _e('--- select KT field ---', 'wp2leads_itm_mid') ?></option>
                                            <?php
                                            foreach ($kt_fields as $id => $field_title) {
                                                $selected = $mid_settings['fields']['mandantenpdf_' . $i] === $id;
                                                ?>
                                                <option value="<?php echo $id ?>"<?php echo $selected ? ' selected' : ''; ?>>
                                                    <?php echo $field_title ?> (<?php echo $id ?>)
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </p>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                            <?php
                            for ($i = 1; $i <= 5; $i++) {
                                ?>
                                <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                    <h4 style="margin-top: 0;">
                                        <?php _e('Mandanten PDF Title', 'wp2leads_itm_mid') ?> <?php echo $i ?>
                                    </h4>
                                    <p style="margin-bottom: 0;">
                                        <select name="fields[mandantenpdftitle_<?php echo $i ?>]" id="mandantenpdftitle_<?php echo $i ?>" style="max-width: 98%">
                                            <option value=""><?php _e('--- select KT field ---', 'wp2leads_itm_mid') ?></option>
                                            <?php
                                            foreach ($kt_fields as $id => $field_title) {
                                                $selected = $mid_settings['fields']['mandantenpdftitle_' . $i] === $id;
                                                ?>
                                                <option value="<?php echo $id ?>"<?php echo $selected ? ' selected' : ''; ?>>
                                                    <?php echo $field_title ?> (<?php echo $id ?>)
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </p>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                            <?php
                            for ($i = 1; $i <= 5; $i++) {
                                ?>
                                <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                    <h4 style="margin-top: 0;">
                                        <?php _e('Mandanten HTML', 'wp2leads_itm_mid') ?> <?php echo $i ?>
                                    </h4>
                                    <p style="margin-bottom: 0;">
                                        <select name="fields[mandantenhtml_<?php echo $i ?>]" id="mandantenhtml_<?php echo $i ?>" style="max-width: 98%">
                                            <option value=""><?php _e('--- select KT field ---', 'wp2leads_itm_mid') ?></option>
                                            <?php
                                            foreach ($kt_fields as $id => $field_title) {
                                                $selected = $mid_settings['fields']['mandantenhtml_' . $i] === $id;
                                                ?>
                                                <option value="<?php echo $id ?>"<?php echo $selected ? ' selected' : ''; ?>>
                                                    <?php echo $field_title ?> (<?php echo $id ?>)
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </p>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <div class="wptl-row" style="padding-bottom:0;">
                        <div class="wptl-col-xs-12">
                            <h4 style="margin: 5px 0"><?php _e('Select forms and map', 'wp2leads_itm_mid') ?></h4>
                        </div>
                    </div>

                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-6">
                            <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                <h4 style="margin-top: 0;">
                                    <?php _e('Select forms', 'wp2leads_itm_mid') ?>
                                </h4>

                                <p style="margin-bottom: 0;">
                                    <?php
                                    $is_forms_selected = false;
                                    foreach ($forms_result as $form) {
                                    $selected = !empty($mid_settings['forms'][$form['Id']]);

                                    if ($selected) {
                                        $is_forms_selected = true;
                                    }
                                    ?>
                                <fieldset>
                                    <input
                                            id="form_<?php echo $form['Id'] ?>"
                                            type="checkbox"
                                            name="forms[<?php echo $form['Id'] ?>]"
                                            value="<?php echo $form['title'] ?>"
                                        <?php echo $selected ? ' checked' : ''; ?>
                                    >
                                    <label for="form_<?php echo $form['Id'] ?>"><?php echo $form['title'] ?> (<?php _e('ID', 'wp2leads_itm_mid') ?>: <?php echo $form['Id'] ?>)</label>
                                </fieldset>
                                <?php
                                }
                                ?>
                                </p>
                            </div>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-6">
                            <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                <h4 style="margin-top: 0;">
                                    <?php _e('Select map', 'wp2leads_itm_mid') ?>
                                </h4>

                                <p style="margin-bottom: 0;">
                                    <select name="map" id="map">
                                        <option value=""><?php _e('--- select map ---', 'wp2leads_itm_mid') ?></option>
                                        <?php
                                        foreach ($maps_result as $map) {
                                            $selected = $mid_settings['map'] === $map['id'];
                                            ?>
                                            <option value="<?php echo $map['id'] ?>"<?php echo $selected ? ' selected' : ''; ?>>
                                                <?php echo $map['name'] ?> (<?php _e('ID', 'wp2leads_itm_mid') ?>: <?php echo $map['id'] ?>)
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <?php
                                    //                            foreach ($maps_result as $map) {
                                    //                                var_dump($map);
                                    //                            }
                                    ?>
                                </p>
                            </div>

                            <?php
                            if (!empty($mid_settings['map']) && $is_forms_selected) {
                                ?>
                                <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                    <h4 style="margin-top: 0;">
                                        <?php _e('Set redirection on forms', 'wp2leads_itm_mid') ?>
                                    </h4>

                                    <?php
                                    foreach ($forms_result as $form) {
                                        if (empty($mid_settings['forms'][$form['Id']])) {
                                            continue;
                                        }
                                        ?>
                                        <div class="wptl-row">
                                            <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-8">
                                                <p><strong><?php echo $form['title'] ?></strong></p>
                                                <p>
                                                    <?php echo __('Redirection URL', 'wp2leads_itm_mid'); ?>:
                                                    <strong><?php echo get_site_url(null, '/'); ?>wp2leads_itm_nexforms/<?php echo $form['Id'] ?>/<?php echo $mid_settings['map'] ?></strong>
                                                </p>
                                            </div>
                                            <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-4">
                                                <p>
                                                    <a href="<?php echo get_admin_url( null, 'admin.php?page=nex-forms-builder&open_form='.$form['Id'] ) ?>" target="_blank" class="button button-primary button-small"><?php _e('Edit form', 'wp2leads_itm_mid') ?></a>
                                                </p>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <div class="wptl-row" style="padding-bottom:0;">
                        <div class="wptl-col-xs-12">
                            <h4 style="margin: 5px 0"><?php _e('Other settings', 'wp2leads_itm_mid') ?></h4>
                        </div>
                    </div>

                    <div class="wptl-row">
                        <div class="wptl-col-xs-12">
                            <div class="item" style="padding:8px; border: 1px solid #ddd;margin-bottom:15px;">
                                <h4 style="margin-top: 0;">
                                    <?php _e('PDF Title pattern', 'wp2leads_itm_mid') ?>
                                </h4>

                                <p style="margin-bottom: 0;">
                                    <input style="width: 90%;" name="pdf_title" id="pdf_title" type="text" value="<?php echo $mid_settings['pdf_title'] ?>">
                                </p>

                                <p style="margin-bottom: 0;">
                                    <?php _e('You can use', 'wp2leads_itm_mid') ?>:
                                </p>

                                <p style="margin-bottom: 0;">
                                    {{Form_Title}} - <?php _e('Form Title without spaces', 'wp2leads_itm_mid') ?><br>
                                    {{YYYY}} - <?php _e('Year f.e. 2020', 'wp2leads_itm_mid') ?><br>
                                    {{MM}} - <?php _e('Month f.e. 09', 'wp2leads_itm_mid') ?><br>
                                    {{DD}} - <?php _e('Day f.e. 25', 'wp2leads_itm_mid') ?><br>
                                    {{entry_id}} - <?php _e('ID of current form entry', 'wp2leads_itm_mid') ?><br>
                                    {{form_field_name}} - <?php _e('You can use any of form field by name f.e. {{mandantenid}}', 'wp2leads_itm_mid') ?>
                                    <?php _e('Be careful, ensure that no special characters are used.', 'wp2leads_itm_mid') ?><br>
                                    <strong>Important</strong>: <?php _e('No spaces or special symbols allowed allowed', 'wp2leads_itm_mid') ?><br>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="wptl-row">
                        <div class="wptl-col-xs-12">
                            <button id="wp2leads_itm_mid_settings_submit" class="button button-primary" type="button">
                                <?php _e('Save settings', 'wp2leads_itm_mid') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <?php
        } else {
            ?>
            <div class="wptl-settings-group-body">
                <?php
                if (empty($forms_result)) {
                    ?>
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12">
                            <h4 style="margin: 5px 0"><?php _e('No forms', 'wp2leads_itm_mid') ?></h4>

                            <p>
                                <?php _e('To start working create at least one form, and visit this page again or reload it.', 'wp2leads_itm_mid') ?>
                                <a href="<?php echo get_admin_url( null, 'admin.php?page=nex-forms-dashboard' ) ?>" class="button button-primary button-small" target="_blank">
                                    <?php _e('Create form', 'wp2leads_itm_mid') ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php
                } elseif (empty($kt_fields)) {
                    ?>
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12">
                            <h4 style="margin: 5px 0"><?php _e('No KlickTipp Fields', 'wp2leads_itm_mid') ?></h4>

                            <p>
                                <?php _e('We can not load fields list from KlickTipp. Please visit Wp2Leads settings page and check KlickTipp credentials, or create all required fields on KlickTipp', 'wp2leads_itm_mid') ?>
                                <a href="<?php echo get_admin_url( null, 'admin.php?page=wp2l-admin&tab=settings' ) ?>" class="button button-primary button-small" target="_blank">
                                    <?php _e('KlickTipp credentials', 'wp2leads_itm_mid') ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>

    </div>

    <div class="wptl-settings-group">
        <div class="wptl-settings-group-header">
            <h3><?php _e('Documentation', 'wp2leads_itm_mid') ?></h3>
        </div>

        <div class="wptl-settings-group-body">
            <div class="wptl-row">
                <div class="wptl-col-xs-12 wptl-col-md-6">
                    <h4><?php _e('Step', 'wp2leads_itm_mid') ?> 1: <?php _e('Create Forms', 'wp2leads_itm_mid') ?></h4>
                    <ol>
                        <li><?php _e('Create as many forms as needed', 'wp2leads_itm_mid') ?></li>
                        <li><strong><?php _e('Very important', 'wp2leads_itm_mid') ?></strong>: <?php _e('Required field in each form is <strong>mandantenid</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('You can add language for form in Hidden field under Extra Options section', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('In section <strong>"Select forms and map"</strong> -> <strong>"Select forms"</strong> select all created forms', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Click <strong>"Save settings"</strong> button', 'wp2leads_itm_mid') ?></li>
                    </ol>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-6">
                    <h4><?php _e('Step', 'wp2leads_itm_mid') ?> 2: <?php _e('Create KlickTipp Fields and Connect them', 'wp2leads_itm_mid') ?></h4>
                    <p><?php _e('Create four types of fields', 'wp2leads_itm_mid') ?>:</p>
                    <ol>
                        <li><?php _e('5 fields for <strong>Mandanten ID</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('5 fields for <strong>PDF URL</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('5 fields for <strong>PDF Title</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('5 fields for <strong>HTML</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('In section "Connect your fields" select from dropdown each type of fields you created on KT', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Click <strong>"Save settings"</strong> button', 'wp2leads_itm_mid') ?></li>
                    </ol>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-6">
                    <h4><?php _e('Step', 'wp2leads_itm_mid') ?> 3: <?php _e('Create a map', 'wp2leads_itm_mid') ?></h4>
                    <ol>
                        <li><a href="<?php echo get_admin_url( null, 'admin.php?page=wp2l-admin&tab=map_port') ?>" target="_blank"><?php _e('Visit Maps Page', 'wp2leads_itm_mid') ?></a></li>
                        <li><?php _e('Find in Maps on Server and import "NEX Forms Entries Master v1.x" template', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('You will be redirected on Map Runner page', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Open map in <strong>"Map builder"</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('If you see a message <strong>"This Map is maintained by another user. If you want fully customize it, you need to duplicate or make it your own."</strong> click <strong>"Make it my own map"</strong> button', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('In "Add Comparison" section add items for each form <strong>"wap_nex_forms_entries.nex_forms_Id"</strong> => <strong>"is like"</strong> => <strong>"{form_id}"</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Rename a map if you wish and click <strong>"Save and Exit"</strong> button.', 'wp2leads_itm_mid') ?></li>
                    </ol>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-6">
                    <h4><?php _e('Step', 'wp2leads_itm_mid') ?> 4: <?php _e('Connect map and set redirections', 'wp2leads_itm_mid') ?></h4>
                    <ol>
                        <li><?php _e('In section <strong>"Select forms and map"</strong> -> <strong>"Select map"</strong> select map from dropdown.', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Click <strong>"Save settings"</strong> button', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('After page reloaded you will see a list with form titles and redirection URL for each form in section <strong>"Select forms and map"</strong> -> <strong>"Set redirection on forms"</strong>', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Copy and paste redirection URL for each form', 'wp2leads_itm_mid') ?></li>
                    </ol>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-6">
                    <h4><?php _e('Step', 'wp2leads_itm_mid') ?> 5: <?php _e('Set PDF Title pattern (optional)', 'wp2leads_itm_mid') ?></h4>
                    <ol>
                        <li><?php _e('In section <strong>"Other settings"</strong> -> <strong>"PDF Title pattern"</strong> set pattern for PDF title using explanation.', 'wp2leads_itm_mid') ?></li>
                    </ol>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-6">
                    <h4><?php _e('Step', 'wp2leads_itm_mid') ?> 6: <?php _e('Setup Map to API settings', 'wp2leads_itm_mid') ?></h4>
                    <ol>
                        <li><?php _e('Before setting up Map to API you need to create dummy data.', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Visit any page with any form and submit it at least once.', 'wp2leads_itm_mid') ?></li>
                        <li>
                            <?php _e('After this you can start Map to API connection.', 'wp2leads_itm_mid') ?>
                            <a href="<?php echo get_admin_url( null, 'admin.php?page=wp2l-admin&tab=map_to_api') ?>" target="_blank"><?php _e('Visit Map to API Page', 'wp2leads_itm_mid') ?></a>
                        </li>
                        <li><?php _e('Make initial settings and Setup API fields', 'wp2leads_itm_mid') ?>:</li>
                        <li><strong>v.wp2leads_nexforms_fields-mandantenid_{index from 1 to 5}</strong> -> <strong><?php _e('Mandanten ID', 'wp2leads_itm_mid') ?></strong> <?php _e('from 1 to 5', 'wp2leads_itm_mid') ?></li>
                        <li><strong>v.wp2leads_nexforms_fields-mandantenpdf_{index from 1 to 5}</strong> -> <strong><?php _e('Mandanten PDF URL', 'wp2leads_itm_mid') ?></strong> <?php _e('from 1 to 5', 'wp2leads_itm_mid') ?></li>
                        <li><strong>v.wp2leads_nexforms_fields-mandantenpdftitle_{index from 1 to 5}</strong> -> <strong><?php _e('Mandanten PDF Title', 'wp2leads_itm_mid') ?></strong> <?php _e('from 1 to 5', 'wp2leads_itm_mid') ?></li>
                        <li><strong>v.wp2leads_nexforms_fields-mandantenhtml_{index from 1 to 5}</strong> -> <strong><?php _e('Mandanten HTML', 'wp2leads_itm_mid') ?></strong> <?php _e('from 1 to 5', 'wp2leads_itm_mid') ?></li>
                        <li><?php _e('Enable instant transfer module and save map', 'wp2leads_itm_mid') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
