<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_Woo_Orders_v3l {
    private static $key = 'wp2leads_woo_orders_v3l';

    private static $required_column = 'posts.ID';

    public static function transfer_init() {
        add_action( 'woocommerce_checkout_order_processed', 'Wp2leads_Woo_Orders_v3l::order_transfer', 10 );
        add_action('woocommerce_order_status_changed', 'Wp2leads_Woo_Orders_v3l::order_transfer', 50);
        add_action('woocommerce_process_shop_order_meta', 'Wp2leads_Woo_Orders_v3l::process_shop_order_meta', 70, 2);
    }

    public static function get_label() {
        return __('Woocommerce v3: Legacy', 'wp2leads_itm_woo_orders');
    }

    public static function get_description() {
        return __('This module will transfer user data once order will be created or order\'s data will be changed', 'wp2leads_itm_woo_orders');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Woocommerce orders maps.', 'wp2leads_itm_woo_orders') ?></p>
        <p><?php _e("Once order will be created or order's data changed user data will be transfered to Klick-Tipp account.", 'wp2leads_itm_woo_orders') ?></p>
        <p>
            <?php
            printf(
                __( 'Requirement: <strong>%s</strong> column withing selected data.', 'wp2leads_itm_woo_orders' ),
                implode(' or ', explode('||', self::get_required_column()))
            );
            ?>
        </p>
        <?php

        return ob_get_clean();
    }

    public static function order_transfer($order_id) {
        self::transfer($order_id);
    }

    public static function process_shop_order_meta($order_id, $post) {
        if (empty($_POST['send_to_kt'])) return;

        self::transfer($order_id);
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::get_required_column(),
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_woo_orders_v3l($transfer_modules) {
    $transfer_modules['wp2leads_woo_orders_v3l'] = 'Wp2leads_Woo_Orders_v3l';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_woo_orders_v3l');
