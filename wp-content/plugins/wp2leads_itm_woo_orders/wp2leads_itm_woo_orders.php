<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for WooCommerce Orders
 * Description:
 * Version:         1.4.2
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_woo_orders
 *
 * Requires at least: 5.0
 * Tested up to: 6.3.1
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_WOO_ORDERS_VERSION', '1.4.2' );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_woo_orders.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_woo_orders'
);

require_once('functions.php');

add_action( 'plugins_loaded', 'wp2leads_itm_woo_orders_load_plugin_textdomain' );
add_action( 'wp2leads_member_kt_invite', 'wp2leads_itm_woo_orders_member_kt_invite', 10, 3 );
add_action( 'wp2leads_save_map_to_api_initial_settings', 'wp2leads_itm_woo_orders_save_map_to_api_initial_settings', 10, 2 );

add_action( 'edited_product_cat', 'wp2leads_itm_woo_orders_update_category', 99 );
add_action( 'save_post_product', 'wp2leads_itm_woo_orders_update_product', 99, 3 );
add_action( 'init', 'wp2leads_itm_woo_orders_get_multilanguage_countries' );

// If not met requirement do not run
if (!wp2leads_itm_woo_orders_requirement()) return;

add_action( 'woocommerce_order_actions_end', 'wp2leads_itm_woo_orders_send_to_kt_button' );
add_filter('wp2leads_get_map_by_id', 'wp2leads_itm_woo_orders_map_by_id', 15, 2);
add_filter('wp2leads_map_query_results', 'wp2leads_itm_woo_orders_query_results', 15, 2);
add_filter('wp2leads_available_options_map_columns', 'wp2leads_itm_woo_orders_available_options_map_columns', 15, 2);
add_filter('wp2leads_all_columns_for_map', 'wp2leads_itm_woo_orders_add_virtual_columns', 15, 2);

wp2leads_itm_woo_orders_modules_init();
