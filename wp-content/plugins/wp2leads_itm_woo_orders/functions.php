<?php

function wp2leads_itm_woo_orders_load_plugin_textdomain() {
    add_filter( 'plugin_locale', 'wp2leads_itm_woo_orders_check_de_locale');

    load_plugin_textdomain(
        'wp2leads_itm_woo_orders',
        false,
        dirname( plugin_basename( __FILE__ ) ) . '/languages/'
    );

    remove_filter( 'plugin_locale', 'wp2leads_itm_woo_orders_check_de_locale');
}

function wp2leads_itm_woo_orders_check_de_locale($domain) {
    $site_lang = get_user_locale();
    $de_lang_list = array(
        'de_CH_informal',
        'de_DE_formal',
        'de_AT',
        'de_CH',
        'de_DE'
    );

    if (in_array($site_lang, $de_lang_list)) return 'de_DE';
    return $domain;
}

function wp2leads_itm_woo_orders_requirement() {
    $wp2leads_installed = function_exists('run_wp2leads') && class_exists('Wp2leads_Background_Module_Transfer');
    if (!$wp2leads_installed) return false;
    $woocommerce = wp2leads_itm_woo_orders_is_plugin_activated( 'woocommerce', 'woocommerce.php' );
    if (!$woocommerce) return false;

    return true;
}

function wp2leads_itm_woo_orders_get_wp2leads_version() {
    $plugin_file = WP_PLUGIN_DIR . '/wp2leads/wp2leads.php';
    $data = get_plugin_data($plugin_file);

    if (isset($data['Version'])) {
        return $data['Version'];
    }

    return '0.0.1';
}

function wp2leads_itm_woo_orders_is_plugin_activated( $plugin_folder, $plugin_file ) {
    if ( wp2leads_itm_woo_orders_is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
    else return wp2leads_itm_woo_orders_is_plugin_active_by_file( $plugin_file );
}

function wp2leads_itm_woo_orders_is_plugin_active_simple( $plugin ) {
    return (
        in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
        ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
    );
}

function wp2leads_itm_woo_orders_is_plugin_active_by_file( $plugin_file ) {
    foreach ( wp2leads_itm_woo_orders_get_active_plugins() as $active_plugin ) {
        $active_plugin = explode( '/', $active_plugin );
        if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
    }

    return false;
}

function wp2leads_itm_woo_orders_get_active_plugins() {
    $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
    if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

    return $active_plugins;
}

function wp2leads_itm_woo_orders_modules_init() {
    include_once 'transfer-modules/Wp2leads_Woo_Orders.php';
    include_once 'transfer-modules/Wp2leads_Woo_Orders_New.php';
    include_once 'transfer-modules/Wp2leads_Woo_Orders_v3.php';
    include_once 'transfer-modules/Wp2leads_Woo_Orders_v3l.php';
}

function wp2leads_itm_woo_orders_send_to_kt_button($post_ID) {
    if (!class_exists('Wp2leads_Transfer_Modules')) {
        return;
    }
    $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

    if (empty($existed_modules_map['wp2leads_woo_orders'])) {
        return;
    }

    $post = get_post($post_ID);
    if (!$post || 'auto-draft' === $post->post_status) return;
    ?>
    <li class="wide" style="text-align: right;">
        <button id="send_to_kt_btn" type="button" class="button button-primary" value="<?php echo esc_attr__( 'Update & Send to Klick-Tipp', 'wp2leads_itm_woo_orders' ); ?>">
            <?php echo esc_html__( 'Update & Send to Klick-Tipp', 'wp2leads_itm_woo_orders' ); ?>
        </button>
        <input type="hidden" disabled="disabled" id="send_to_kt" name="send_to_kt" value="1">
    </li>

    <script type="text/javascript">
        jQuery(function() {
            jQuery( '#send_to_kt_btn' ).on( 'click', function() {
                const btn = jQuery(this);
                jQuery('#send_to_kt').attr('disabled', false);
                btn.closest('form').submit();
            } );
        });
    </script>
    <?php
}

function wp2leads_itm_woo_orders_member_kt_invite($user_id, $subscriber, $created) {
    $email = $subscriber->email;
    $fname = !empty($subscriber->fieldFirstName) ? $subscriber->fieldFirstName : '';
    $lname = !empty($subscriber->fieldLastName) ? $subscriber->fieldLastName : '';
    $company = !empty($subscriber->fieldCompanyName) ? $subscriber->fieldCompanyName : '';
    $address1 = !empty($subscriber->fieldStreet1) ? $subscriber->fieldStreet1 : '';
    $address2 = !empty($subscriber->fieldStreet2) ? $subscriber->fieldStreet2 : '';
    $city = !empty($subscriber->fieldCity) ? $subscriber->fieldCity : '';
    $state = !empty($subscriber->fieldState) ? $subscriber->fieldState : '';
    $zip = !empty($subscriber->fieldZip) ? $subscriber->fieldZip : '';
    $country = !empty($subscriber->fieldCountry) ? wp2leads_itm_woo_orders_get_country_code($subscriber->fieldCountry) : '';

    $phone = '';
    if (!empty($subscriber->fieldPrivatePhone)) $phone = $subscriber->fieldPrivatePhone;
    elseif (!empty($subscriber->fieldMobilePhone)) $phone = $subscriber->fieldMobilePhone;
    elseif (!empty($subscriber->fieldPhone)) $phone = $subscriber->fieldPhone;
    elseif (!empty($subscriber->fieldFax)) $phone = $subscriber->fieldFax;

    $user_meta_fields = [
        'billing_email' => $email,
        'shipping_email' => $email,
        'billing_first_name' => $fname,
        'shipping_first_name' => $fname,
        'billing_last_name' => $lname,
        'shipping_last_name' => $lname,
        'billing_phone' => $phone,
        'shipping_phone' => $phone,
        'billing_country' => $country,
        'shipping_country' => $country,
        'billing_postcode' => $zip,
        'shipping_postcode' => $zip,
        'billing_state' => $state,
        'shipping_state' => $state,
        'billing_city' => $city,
        'shipping_city' => $city,
        'billing_address_1' => $address1,
        'shipping_address_1' => $address1,
        'billing_address_2' => $address2,
        'shipping_address_2' => $address2,
        'billing_company' => $company,
        'shipping_company' => $company,
    ];

    if (empty($created)) {
        global $wpdb;
        $sql = "SELECT * FROM {$wpdb->usermeta} WHERE user_id = '{$user_id}'";
        $usermeta = $wpdb->get_results($sql, ARRAY_A);

        if (!empty($usermeta)) {
            foreach ($usermeta as $data) {
                $meta_key = $data['meta_key'];
                $meta_value = $data['meta_value'];

                if (!empty($user_meta_fields[$meta_key]) && empty($meta_value)) {
                    update_user_meta( $user_id, $meta_key, $user_meta_fields[$meta_key] );
                }

                if (!empty($user_meta_fields[$meta_key])) {
                    unset($user_meta_fields[$meta_key]);
                }
            }
        }
    }

    if (!empty($user_meta_fields)) {
        foreach ($user_meta_fields as $meta_key => $meta_value) {
            if (!empty($meta_value)) {
                update_user_meta( $user_id, $meta_key, $meta_value );
            }
        }
    }
}

function wp2leads_itm_woo_orders_update_category($category_id) {
    wp2leads_itm_woo_orders_maybe_update_categories_filter();
}

function wp2leads_itm_woo_orders_update_product($post_ID, $post, $update) {
    wp2leads_itm_woo_orders_maybe_update_categories_filter();
}

function wp2leads_itm_woo_orders_maybe_update_categories_filter() {
    $wp2leads_version = wp2leads_itm_woo_orders_get_wp2leads_version();

    if (version_compare($wp2leads_version, '3.0.22', '<')) return;

    global $wpdb;
    $table = MapsModel::get_table();
    $maps = $wpdb->get_results("SELECT * FROM {$table}");

    foreach ($maps as $map) {
        $map_id = $map->id;

        wp2leads_itm_woo_orders_update_categories_filter($map_id, $map);
    }
}

function wp2leads_itm_woo_orders_update_categories_filter($map_id, $map, $is_initial = false) {
    $api = unserialize($map->api);
    $mapping = unserialize($map->mapping);
    $info = unserialize($map->info);

    if (
        empty($mapping["transferModule"])
        || 'wp2leads_woo_orders' !== $mapping["transferModule"]
        || empty($info["possibleUsedTags"]["userInputTags"])
        || empty($api["connected_for_tags"]["separators"])
    ) {
        return;
    }

    $is_categories_separator = false;
    $categories_separator_i = null;

    $separators = $api["connected_for_tags"]["separators"];

    foreach ($separators as $i => $separator) {
        if (!empty($separator["option"])) {
            if (
                in_array('terms.name(concatenated)', $separator["option"])
                && ($is_initial || !empty($separator["filter_action"]))
            ) {
                $is_categories_separator = true;
                $categories_separator_i = $i;
                break;
            }
        }
    }

    if (empty($is_categories_separator)) return;

    $categories_filter = [];

    foreach ($info["possibleUsedTags"]["userInputTags"] as $userInputTag) {
        if (
            'terms' === $userInputTag['fromTable']
            && 'terms.name' === $userInputTag['tagColumn']
        ) {
            $categories_filter = $userInputTag;
        }
    }

    if (empty($categories_filter)) return;

    if (!class_exists('MapBuilderManager')) {
        if (!file_exists(WP_PLUGIN_DIR . 'wp2leads/includes/library/MapBuilderManager.php')) {
            return;
        }

        include_once WP_PLUGIN_DIR . 'wp2leads/includes/library/MapBuilderManager.php';
    }

    if (!method_exists('MapBuilderManager', 'get_recomended_tags')) return;

    $recomended_tags = MapBuilderManager::get_recomended_tags($categories_filter);

    if (empty($recomended_tags["tags"]) || !is_array($recomended_tags["tags"])) return;

    $recomended_tags_filter = implode('||', $recomended_tags["tags"]);

    $api["connected_for_tags"]["separators"][$categories_separator_i]['filter'] = $recomended_tags_filter;
    $api["connected_for_tags"]["separators"][$categories_separator_i]['filter_action'] = 1;

    global $wpdb;
    $table = MapsModel::get_table();

    $wpdb->update( $table, ['api' => serialize($api)], array('id' => $map_id) );
}

function wp2leads_itm_woo_orders_save_map_to_api_initial_settings($map_id, $map) {
    $wp2leads_version = wp2leads_itm_woo_orders_get_wp2leads_version();

    if (version_compare($wp2leads_version, '3.0.22', '<')) return;

    wp2leads_itm_woo_orders_update_categories_filter($map_id, $map, true);
}

function wp2leads_itm_woo_orders_load_texdomain($locale = null) {
    if (empty($locale)) {
        $locale = determine_locale();
        $locale = apply_filters( 'plugin_locale', $locale, 'woocommerce' );
    }

    unload_textdomain( 'woocommerce' );
    load_textdomain( 'woocommerce', WP_LANG_DIR . '/woocommerce/woocommerce-' . $locale . '.mo' );
    load_plugin_textdomain( 'woocommerce', false, plugin_basename( dirname( WC_PLUGIN_FILE ) ) . '/i18n/languages' );
}

function wp2leads_itm_woo_orders_get_multilanguage_countries() {
    if ( $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX ) return;

    if (!defined('WC_PLUGIN_FILE')) return;
    if(!function_exists('get_plugin_data')) require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    $wc_data = get_plugin_data(WC_PLUGIN_FILE);
    $wc_version = !empty($wc_data['Version']) ? $wc_data['Version'] : '';
    if (empty($wc_version)) return;
    $wc_countries_version = get_option('wp2leads_itm_woo_orders_wc_countries_version');

    if (
        !empty($wc_countries_version)
        && version_compare($wc_countries_version, $wc_version) >= 0
    ) {
        return;
    }

    $current_locale = determine_locale();
    $current_locale_temp = $current_locale;

    $wc_countries = WC()->countries->get_countries();
    $wc_countries_by_title = [];
    foreach ($wc_countries as $ccode => $ctitle) $wc_countries_by_title[$ctitle] = $ccode;
    $wc_countries_by_title_current = $wc_countries_by_title;

    if ($current_locale !== 'de_DE') {
        $switch_to = switch_to_locale( 'de_DE' );

        if ($switch_to) {
            wp2leads_itm_woo_orders_load_texdomain();
            $countries_obj   = new WC_Countries();
            $wc_countries = $countries_obj->__get('countries');
            $wc_countries_by_title = [];
            foreach ($wc_countries as $ccode => $ctitle) $wc_countries_by_title[$ctitle] = $ccode;
            update_option( 'wp2leads_itm_woo_orders_wc_countries_de_DE', $wc_countries_by_title );
        }
    } else {
        update_option( 'wp2leads_itm_woo_orders_wc_countries_de_DE', $wc_countries_by_title_current );
    }

    if ($current_locale !== 'en_US') {
        $switch_to = switch_to_locale( 'en_US' );

         if ($switch_to) {
             wp2leads_itm_woo_orders_load_texdomain();
             $countries_obj   = new WC_Countries();
            $wc_countries = $countries_obj->__get('countries');
            $wc_countries_by_title = [];
            foreach ($wc_countries as $ccode => $ctitle) $wc_countries_by_title[$ctitle] = $ccode;
            update_option( 'wp2leads_itm_woo_orders_wc_countries_en_US', $wc_countries_by_title );
         }
    } else {
        update_option( 'wp2leads_itm_woo_orders_wc_countries_en_US', $wc_countries_by_title_current );
    }

    switch_to_locale( $current_locale_temp );
    wp2leads_itm_woo_orders_load_texdomain();
    update_option( 'wp2leads_itm_woo_orders_wc_countries_version', $wc_version );
}

function wp2leads_itm_woo_orders_get_country_code($country) {
    $wc_countries = WC()->countries->get_countries();
    if (!empty($wc_countries[$country])) return $country;
    $wc_countries_by_title = [];

    foreach ($wc_countries as $ccode => $ctitle) {
        $wc_countries_by_title[$ctitle] = $ccode;
    }

    if (!empty($wc_countries_by_title[$country])) return $wc_countries_by_title[$country];

    $current_locale = determine_locale();

    if ($current_locale !== 'de_DE') {
        $wc_countries_by_title = get_option( 'wp2leads_itm_woo_orders_wc_countries_de_DE', [] );

        if (!empty($wc_countries_by_title[$country])) return $wc_countries_by_title[$country];
    }

    if ($current_locale !== 'en_US') {
        $wc_countries_by_title = get_option( 'wp2leads_itm_woo_orders_wc_countries_en_US', [] );

        if (!empty($wc_countries_by_title[$country])) return $wc_countries_by_title[$country];
    }

    return $country;
}

function wp2leads_itm_woo_orders_get_orders_ids($results) {
    $order_ids = array();

    foreach ($results as $result) {
        $order_id_column = !empty($result->{'posts.ID'}) ? 'posts.ID' : 'wc_orders.id';
        if (!empty($result->{$order_id_column})) {
            $order_ids[] = $result->{$order_id_column};
        }
    }

    return $order_ids;
}

function wp2leads_itm_woo_orders_get_orders_ids_in($order_ids) {
    if (empty($order_ids)) return '';
    return "'" . implode("', '", $order_ids) . "'";
}

function wp2leads_itm_woo_orders_get_products() {
    global $wpdb;
    // First, fetch all products
    $products_sql = "
        SELECT 
            p.ID AS product_id,
            p.post_title AS product_name
        FROM 
            $wpdb->posts p
        WHERE 
            p.post_type IN ('product', 'product_variation') AND
            p.post_status = 'publish';
    ";
    $products = $wpdb->get_results($products_sql, OBJECT_K);

// Then, fetch all categories for products
    $categories_sql = "
        SELECT 
            tr.object_id AS product_id,
            t.term_id AS category_id,
            t.name AS category_name
        FROM 
            $wpdb->term_relationships tr
        INNER JOIN 
            $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
        INNER JOIN 
            $wpdb->terms t ON tt.term_id = t.term_id
        WHERE 
            tt.taxonomy = 'product_cat';
    ";
    $categories = $wpdb->get_results($categories_sql);

// Combine categories into the products array
    foreach ($categories as $category) {
        if (isset($products[$category->product_id])) {
            $products[$category->product_id]->categories[] = $category->category_name;
        }
    }

    return $products;
}

function wp2leads_itm_woo_orders_get_orders_items($in_ids) {
    if (empty($in_ids)) return array();
    global $wpdb;

    $order_items_table = $wpdb->prefix . 'woocommerce_order_items';
    $order_itemmeta_table = $wpdb->prefix . 'woocommerce_order_itemmeta';
    $sql = "
        SELECT * FROM {$order_items_table} wi
            LEFT JOIN {$order_itemmeta_table} wim ON wim.order_item_id = wi.order_item_id
        WHERE wi.order_id IN ({$in_ids})
        -- AND wi.order_item_type IN ('shipping', 'coupon')
        AND wi.order_item_type IN ('line_item', 'shipping', 'coupon')
        AND wim.meta_key IN ('_product_id', '_variation_id', '_qty', 'method_id', 'coupon_info')
        -- AND wim.meta_key IN ('_product_id', '_variation_id', '_qty', '_line_total', 'method_id', 'coupon_info')
        ORDER BY wi.order_id DESC;
    ";

    $results = $wpdb->get_results($sql, ARRAY_A);

    if (empty($results)) {
        return array();
    }

    $products = wp2leads_itm_woo_orders_get_products();

    $order_items = array();
    $types = array();
    $meta_keys = array();

    foreach ($results as $result) {
        $order_id = $result['order_id'];
        $type = $result['order_item_type'] === 'line_item' ? 'line_items' : $result['order_item_type'];

        if (!in_array($type, $types)) {
            $types[] = $type;
        }
        if (!in_array($result['meta_key'], $meta_keys)) {
            $meta_keys[] = $result['meta_key'];
        }
        if (empty($order_items[$order_id])) {
            $order_items[$order_id] = array(
                'line_items' => array(),
                'products' => array(),
                'variations' => array(),
                'categories' => array(),
                'shipping' => array(),
                'coupon' => array(),
            );
        }

        if (empty($order_items[$order_id][$type])) {
            $order_items[$order_id][$type] = array();
        }

        if ($type === 'line_items') {
            if ($result["meta_key"] === '_product_id' || $result["meta_key"] === '_variation_id') {
                $_product_id = $result["meta_value"];

                if (!empty($products[$_product_id])) {
                    $_product = $products[$_product_id];

                    if (!empty($_product)) {
                        if (isset($_product->categories)) {
                            $_categories = array_values($_product->categories);
                        }
                        if (!empty($_categories)) {
                            foreach ($_categories as $category) {
                                if (!in_array($category, $order_items[$order_id]['categories'])) {
                                    $order_items[$order_id]['categories'][] = $category;
                                }
                            }
                        }
                        if ($result["meta_key"] === '_product_id' && !in_array($_product->product_name, $order_items[$order_id]['products'])) {
                            $order_items[$order_id]['products'][] = $_product->product_name;
                        }
                        if ($result["meta_key"] === '_variation_id' && !in_array($_product->product_name, $order_items[$order_id]['variations'])) {
                            $order_items[$order_id]['variations'][] = $_product->product_name;
                        }
                    }
                }
            }
        }

        if (!in_array($result["order_item_name"], $order_items[$order_id][$type])) {
            $order_items[$order_id][$type][] = $result["order_item_name"];
        }
    }

    return $order_items;
}

function wp2leads_itm_woo_orders_get_hpos_orders_data($in_ids, $excludes_filter) {
    if (empty($in_ids)) return array();
    global $wpdb;

    $order_table = $wpdb->prefix . 'wc_orders';
    $order_addresses_table = $wpdb->prefix . 'wc_order_addresses';
    $orders_meta_table = $wpdb->prefix . 'wc_orders_meta';
    $wc_order_operational_data_table = $wpdb->prefix . 'wc_order_operational_data';
    $exclude_filters_sql = '';

    if (!empty($excludes_filter)) {
        foreach ($excludes_filter as $filter) {
            $exclude_filters_sql .= " OR wcm.meta_key LIKE '%{$filter}%' ";
        }
    }

    $sql = "
            SELECT 
                wco.id as order_id,
                wco.billing_email as _billing_email,
                wco.payment_method as _payment_method,
                wco.payment_method_title as _payment_method_title,
                wco.currency as _order_currency,
                wco.total_amount as _order_total,
                wca.address_1 as _billing_address_1,
                wca.address_2 as _billing_address_2,
                wca.city as _billing_city,
                wca.company as _billing_company,
                wca.country as _billing_country,
                wca.state as _billing_state,
                wca.postcode as _billing_postcode,
                wca.first_name as _billing_first_name,
                wca.last_name as _billing_last_name,
                wca.phone as _billing_phone,
                wcod.created_via as _created_via,
                wcod.prices_include_tax as _prices_include_tax,
                wcm.meta_key,
                wcm.meta_value
                -- wca.*
            FROM {$order_table} wco
            LEFT JOIN {$order_addresses_table} wca 
            ON wca.order_id = wco.id
            LEFT JOIN {$orders_meta_table} wcm 
            ON wcm.order_id = wco.id
            LEFT JOIN {$wc_order_operational_data_table} wcod 
            ON wcod.order_id = wco.id
            WHERE wco.id IN ({$in_ids})
            AND wca.address_type = 'billing'
            AND (
                wcm.meta_key = '_completed_date'
                OR wcm.meta_key = '_paid_date'
                OR wcm.meta_key = '_billing_address_index'
                {$exclude_filters_sql}
            )
        ";

    $result = $wpdb->get_results($sql, ARRAY_A);

    if (empty($result)) return array();

    $orders_data = array();
    $order_data_columns = array();

    foreach ($result as $order) {
        $order_id = $order['order_id'];
        unset($order['order_id']);
        $meta_key = $order['meta_key'];
        $meta_value = $order['meta_value'];
        unset($order['meta_key']);
        unset($order['meta_value']);

        if (empty($orders_data[$order_id])) {
            $orders_data[$order_id] = array();
        }
        foreach ($order as $column => $value) {
            if (empty($orders_data[$order_id][$column])) {
                if ($column === '_order_total') {
                    $value = number_format($value, 2, '.', '');
                }
                if ($column === '_prices_include_tax') {
                    $value = $value === 0 | $value === '0' ? 'no' : 'yes';
                }
                if (!in_array($column, $order_data_columns)) {
                    $order_data_columns[] = $column;
                }
                $orders_data[$order_id][$column] = $value;
            }
        }

        if (empty($orders_data[$order_id][$meta_key])) {
            if (!in_array($meta_key, $order_data_columns)) {
                $order_data_columns[] = $meta_key;
            }
            $orders_data[$order_id][$meta_key] = trim($meta_value);
        }
    }

    return array(
        'orders_data' => $orders_data,
        'order_data_columns' => $order_data_columns,
    );
}

function wp2leads_itm_woo_orders_query_results($results, $map) {
    if (!wp2leads_itm_woo_orders_is_woo_order_map($map)) return $results;

    $order_ids = wp2leads_itm_woo_orders_get_orders_ids($results);
    $in_ids = wp2leads_itm_woo_orders_get_orders_ids_in($order_ids);
    $order_items = wp2leads_itm_woo_orders_get_orders_items($in_ids);
    $separator = !empty($map["groupConcatSeparator"]) ? $map["groupConcatSeparator"] : ',';

    $is_hpos_enabled = wp2leads_itm_woo_orders_is_hpos_enabled();

    if ($is_hpos_enabled) {
        $excludes_filter = !empty($map["excludesFilters"]) ? $map["excludesFilters"] : array();
        $hpos_orders_data = wp2leads_itm_woo_orders_get_hpos_orders_data($in_ids, $excludes_filter);
    }

    foreach ($results as $result) {
        $order_id = !empty($result->{'posts.ID'}) ? $result->{'posts.ID'} : $result->{'wc_orders.id'};

        if (!empty($order_items[$order_id])) {
            if ($map['transferModule'] !== 'wp2leads_woo_orders_v3' && $map['transferModule'] !== 'wp2leads_woo_orders_v3l') {
                $result->{'woocommerce_order_items.order_item_name(concatenated)'} = ApiHelper::filterBeforeOutput(ApiHelper::filterForbidenKTSymbols(implode($separator, $order_items[$order_id]['line_items'])));
                $result->{'terms.name(concatenated)'} = ApiHelper::filterBeforeOutput(ApiHelper::filterForbidenKTSymbols(implode($separator, $order_items[$order_id]['categories'])));
            }
            $result->{'v.order_line_items'} = '';
            $result->{'v.order_products'} = '';
            $result->{'v.order_variations'} = '';
            $result->{'v.order_categories'} = '';
            $result->{'v.order_shipping'} = '';
            $result->{'v.order_coupon'} = '';
            foreach ($order_items[$order_id] as $type => $value) {
                $column_name = "v.order_{$type}";
                $result->{$column_name} = ApiHelper::filterBeforeOutput(ApiHelper::filterForbidenKTSymbols(implode($separator, $value)));
            }
        }

        $result->{'v.order_date_created_gmt'} = '';
        if (!empty($result->{'wc_orders.date_created_gmt'})) {
            $result->{'v.order_date_created_gmt'} = $result->{'wc_orders.date_created_gmt'};
        } elseif (!empty($result->{'posts.post_date_gmt'})) {
            $result->{'v.order_date_created_gmt'} = $result->{'posts.post_date_gmt'};
        }
        $result->{'v.order_date_updated_gmt'} = '';
        if (!empty($result->{'wc_orders.date_updated_gmt'})) {
            $result->{'v.order_date_updated_gmt'} = $result->{'wc_orders.date_updated_gmt'};
        } elseif (!empty($result->{'posts.post_modified_gmt'})) {
            $result->{'v.order_date_updated_gmt'} = $result->{'posts.post_modified_gmt'};
        }

        $result->{'v.order_status'} = '';
        if (!empty($result->{'wc_orders.status'})) {
            $result->{'v.order_status'} = $result->{'wc_orders.status'};
        } elseif (!empty($result->{'posts.post_status'})) {
            $result->{'v.order_status'} = $result->{'posts.post_status'};
        }

        if (
            !empty($hpos_orders_data) &&
            !empty($hpos_orders_data['orders_data']) &&
            !empty($hpos_orders_data['orders_data'][$order_id])
        ) {
            foreach ($hpos_orders_data['order_data_columns'] as $column) {
                $result->{'v.postmeta-' . $column} = '';
            }
            foreach ($hpos_orders_data['orders_data'][$order_id] as $column => $value) {
                $result->{'v.postmeta-' . $column} = $value;
            }
        }
    }
    return $results;
}

function wp2leads_itm_woo_orders_available_options_map_columns($columns, $map) {
    if (!wp2leads_itm_woo_orders_is_woo_order_map($map)) return $columns;

    if (!in_array('v.order_line_items', $columns)) $columns[] = 'v.order_line_items';
    if (!in_array('v.order_products', $columns)) $columns[] = 'v.order_products';
    if (!in_array('v.order_variations', $columns)) $columns[] = 'v.order_variations';
    if (!in_array('v.order_categories', $columns)) $columns[] = 'v.order_categories';

    if (!in_array('v.order_shipping', $columns)) $columns[] = 'v.order_shipping';
    if (!in_array('v.order_coupon', $columns)) $columns[] = 'v.order_coupon';

    if ($map['transferModule'] !== 'wp2leads_woo_orders_v3' && $map['transferModule'] !== 'wp2leads_woo_orders_v3l') {
        if (!in_array('woocommerce_order_items.order_item_name', $columns)) $columns[] = 'woocommerce_order_items.order_item_name(concatenated)';
        if (!in_array('terms.name', $columns)) $columns[] = 'terms.name(concatenated)';
    }

    if ($map['transferModule'] === 'wp2leads_woo_orders_v3' || $map['transferModule'] === 'wp2leads_woo_orders_v3l') {
        if (!in_array('v.order_status', $columns)) $columns[] = 'v.order_status';
        if (!in_array('v.order_date_created_gmt', $columns)) $columns[] = 'v.order_date_created_gmt';
        if (!in_array('v.order_date_updated_gmt', $columns)) $columns[] = 'v.order_date_updated_gmt';

        foreach ($columns as $i => $column) {
            if (
                $column === 'wc_orders.type' ||
                $column === 'wc_orders.status' ||
                $column === 'wc_orders.date_created_gmt' ||
                $column === 'wc_orders.date_updated_gmt' ||
                $column === 'posts.post_type' ||
                $column === 'posts.post_status' ||
                $column === 'posts.post_date_gmt' ||
                $column === 'posts.post_modified_gmt'
            ) {
                unset($columns[$i]);
            }
        }
    }

    return $columns;
}

function wp2leads_itm_woo_orders_add_virtual_columns($columns, $map_id) {
    $mapDb = MapsModel::get($map_id);
    if (empty($mapDb)) return $columns;
    $map = unserialize($mapDb->mapping);
    if (empty($map['transferModule']) || ($map['transferModule'] !== 'wp2leads_woo_orders_v3' && $map['transferModule'] !== 'wp2leads_woo_orders_v3l')) return $columns;

    if (!in_array('v.order_date_created_gmt', $columns)) $columns[] = 'v.order_date_created_gmt';
    if (!in_array('v.order_date_updated_gmt', $columns)) $columns[] = 'v.order_date_updated_gmt';

    return $columns;
}

function wp2leads_itm_woo_orders_is_woo_order_map($map) {
    if (
        empty($map['transferModule']) ||
        (
            $map['transferModule'] !== 'wp2leads_woo_orders' &&
            $map['transferModule'] !== 'wp2leads_woo_orders_new' &&
            $map['transferModule'] !== 'wp2leads_woo_orders_v3' &&
            $map['transferModule'] !== 'wp2leads_woo_orders_v3l'
        )
    ) return false;

    return true;
}

function wp2leads_itm_woo_orders_is_hpos_enabled() {
    if (class_exists('WooCommerce') && function_exists('wc_get_container')) {
        return wc_get_container()
            ->get(Automattic\WooCommerce\Internal\DataStores\Orders\CustomOrdersTableController::class)
            ->custom_orders_table_usage_is_enabled();
    }

    return false;
}

function wp2leads_itm_woo_orders_get_replace_selects($is_hpos_enabled = false) {
    $type = $is_hpos_enabled ? 'hpos' : 'legacy';

    $data = array(
        'legacy' => array(
            "posts.ID",
            "posts.comment_count",
            "posts.comment_status",
            "posts.guid",
            "posts.menu_order",
            "posts.ping_status",
            "posts.pinged",
            "posts.post_author",
            "posts.post_content",
            "posts.post_content_filtered",
            "posts.post_date",
            "posts.post_date_gmt",
            "posts.post_excerpt",
            "posts.post_mime_type",
            "posts.post_modified",
            "posts.post_modified_gmt",
            "posts.post_name",
            "posts.post_parent",
            "posts.post_password",
            "posts.post_status",
            "posts.post_title",
            "posts.post_type",
            "posts.to_ping"
        ),
        'hpos' => array(
            "wc_orders.billing_email",
            "wc_orders.currency",
            "wc_orders.customer_id",
            "wc_orders.customer_note",
            "wc_orders.date_created_gmt",
            "wc_orders.date_updated_gmt",
            "wc_orders.id",
            "wc_orders.ip_address",
            "wc_orders.parent_order_id",
            "wc_orders.payment_method",
            "wc_orders.payment_method_title",
            "wc_orders.status",
            "wc_orders.tax_amount",
            "wc_orders.total_amount",
            "wc_orders.transaction_id",
            "wc_orders.type",
            "wc_orders.user_agent"
        ),
    );

    return $data[$type];
}

function wp2leads_itm_woo_orders_get_replace_selects_only($is_hpos_enabled = false) {
    $type = $is_hpos_enabled ? 'hpos' : 'legacy';

    $data = array(
        'legacy' => array(
            "posts.ID",
            "posts.post_status",
            "posts.post_type",
            "posts.post_date_gmt",
            "posts.post_modified_gmt"
        ),
        'hpos' => array(
            "wc_orders.id",
            "wc_orders.status",
            "wc_orders.type",
            "wc_orders.date_created_gmt",
            "wc_orders.date_updated_gmt"
        ),
    );

    return $data[$type];
}

function wp2leads_itm_woo_orders_get_replace_excludes($is_hpos_enabled = false) {
    $type = $is_hpos_enabled ? 'hpos' : 'legacy';

    $data = array(
        'legacy' => array(
            "posts.comment_count",
            "posts.comment_status",
            "posts.guid",
            "posts.menu_order",
            "posts.ping_status",
            "posts.pinged",
            "posts.post_author",
            "posts.post_content",
            "posts.post_content_filtered",
            "posts.post_date",
            "posts.post_excerpt",
            "posts.post_mime_type",
            "posts.post_modified",
            "posts.post_name",
            "posts.post_parent",
            "posts.post_password",
            "posts.post_title",
            "posts.to_ping"
        ),
        'hpos' => array(
            "wc_orders.billing_email",
            "wc_orders.currency",
            "wc_orders.customer_id",
            "wc_orders.customer_note",
            "wc_orders.ip_address",
            "wc_orders.parent_order_id",
            "wc_orders.payment_method",
            "wc_orders.payment_method_title",
            "wc_orders.tax_amount",
            "wc_orders.total_amount",
            "wc_orders.transaction_id",
            "wc_orders.user_agent"
        ),
    );

    return $data[$type];
}

function wp2leads_itm_woo_orders_map_by_id($map, $map_id) {
    if (!$map || empty($map->mapping)) return $map;
    $mapping = maybe_unserialize($map->mapping);
    asort($mapping['selects']);
    asort($mapping['selects_only']);
    asort($mapping['excludes']);

    $mapping['selects'] = array_values($mapping['selects']);
    $mapping['selects_only'] = array_values($mapping['selects_only']);
    $mapping['excludes'] = array_values($mapping['excludes']);

    if (
        empty($mapping) ||
        empty($mapping['transferModule']) ||
        ($mapping['transferModule'] !== 'wp2leads_woo_orders_v3' && $mapping['transferModule'] !== 'wp2leads_woo_orders_v3l')
    )  return $map;

    $is_hpos_enabled = wp2leads_itm_woo_orders_is_hpos_enabled();

    if ($is_hpos_enabled) {
        $mapping['comparisons'][0]['tableColumn'] = "wc_orders.type";
        $mapping['virtual_relationships'][0]['table_from'] = 'wc_orders';
        $mapping['virtual_relationships'][0]['column_from'] = 'id';
        $mapping['from'] = 'wc_orders';
        $mapping['keyBy'] = 'wc_orders.id';
        $mapping['transferModule'] = 'wp2leads_woo_orders_v3';
    } else {
        $mapping['comparisons'][0]['tableColumn'] = "posts.post_type";
        $mapping['virtual_relationships'][0]['table_from'] = 'posts';
        $mapping['virtual_relationships'][0]['column_from'] = 'ID';
        $mapping['from'] = 'posts';
        $mapping['keyBy'] = 'posts.ID';
        $mapping['transferModule'] = 'wp2leads_woo_orders_v3l';
    }

    $needle = $is_hpos_enabled ? 'posts.' : 'wc_orders.';
    $selects = wp2leads_itm_woo_orders_get_replace_selects($is_hpos_enabled);
    $selects_only = wp2leads_itm_woo_orders_get_replace_selects_only($is_hpos_enabled);
    $excludes = wp2leads_itm_woo_orders_get_replace_excludes($is_hpos_enabled);

    foreach ($mapping['selects'] as $i => $haystack) {
        if (wp2leads_itm_woo_orders_starts_with($haystack, $needle)) {
            unset($mapping['selects'][$i]);
        }
    }

    $mapping['selects'] = array_merge(array_values($selects), $mapping['selects']);

    foreach ($mapping['selects_only'] as $i => $haystack) {
        if (wp2leads_itm_woo_orders_starts_with($haystack, $needle)) {
            unset($mapping['selects_only'][$i]);
        }
    }

    $mapping['selects_only'] = array_merge(array_values($selects_only), $mapping['selects_only']);

    foreach ($mapping['excludes'] as $i => $haystack) {
        if (wp2leads_itm_woo_orders_starts_with($haystack, $needle)) {
            unset($mapping['excludes'][$i]);
        }
    }

    $mapping['excludes'] = array_merge(array_values($excludes), $mapping['excludes']);

    if (!in_array("v.order_date_created_gmt", $mapping['selects'])) {
        $mapping['selects'][] = "v.order_date_created_gmt";
    }
    if (!in_array("v.order_date_created_gmt", $mapping['selects_only'])) {
        $mapping['selects_only'][] = "v.order_date_created_gmt";
    }
    if (!in_array("v.order_date_created_gmt", $mapping['dateTime'])) {
        $mapping['dateTime'][] = "v.order_date_created_gmt";
    }

    if (!in_array("v.order_date_updated_gmt", $mapping['selects'])) {
        $mapping['selects'][] = "v.order_date_updated_gmt";
    }
    if (!in_array("v.order_date_updated_gmt", $mapping['selects_only'])) {
        $mapping['selects_only'][] = "v.order_date_updated_gmt";
    }
    if (!in_array("v.order_date_updated_gmt", $mapping['dateTime'])) {
        $mapping['dateTime'][] = "v.order_date_updated_gmt";
    }

    $map->mapping = serialize($mapping);
    return $map;
}

function wp2leads_itm_woo_orders_starts_with($haystack, $needle) {
    return substr($haystack, 0, strlen($needle)) === $needle;
}
