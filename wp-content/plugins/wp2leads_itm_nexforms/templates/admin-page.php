<?php
global $wpdb;
$forms_sql = "SELECT * FROM {$wpdb->prefix}wap_nex_forms WHERE is_form=1 ORDER BY Id DESC";
$forms_result = $wpdb->get_results($forms_sql, ARRAY_A);

?>

<div class="wrap">
    <h1><?php _e("NEX-Forms to KlickTipp", 'wp2leads_itm_nexforms'); ?></h1>
    <?php settings_errors(); ?>

    <div class="settings-group-header">
        <div class="wptl-settings-group-header">
            <h3><?php _e('Connect your forms to wp2leads maps', 'wp2leads_itm_nexforms') ?></h3>
        </div>

        <div class="wptl-settings-group-body">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <?php
                    if (!empty($forms_result)) {
                        ?>
                        <div id="redirection_url_content" style="display: none;">
                            <ol>
                                <li><?php _e('Copy redirection URL.', 'wp2leads_itm_nexforms'); ?></li>
                                <li><?php _e('Visit form editing page', 'wp2leads_itm_nexforms'); ?></li>
                                <li><?php _e('Open "OPTIONS" tab', 'wp2leads_itm_nexforms'); ?></li>
                                <li><?php _e('Select "Custom (For developers)" for SUBMISSION TYPE. We reccomend to use POST for POST METHOD', 'wp2leads_itm_nexforms'); ?></li>
                                <li><?php _e('Paste copied URL into "SUBMIT FORM TO" field', 'wp2leads_itm_nexforms'); ?></li>
                            </ol>
                        </div>
                        <?php
                        $forms_to_map = WP2LITMNF_Map::get_form_to_map_connection();

                        foreach ($forms_result as $form_item) {
                            ?>
                            <div id="wp2litmnf_form_<?php echo $form_item['Id'] ?>" class="wptl-row">
                                <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                                    <h4><small style="font-weight: lighter;"><?php echo __('Form', 'wp2leads_itm_nexforms'); ?>:</small> <?php echo $form_item['title']; ?></h4>
                                </div>

                                <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                                    <?php
                                    if (empty($forms_to_map[$form_item['Id']])) {
                                        ?>
                                        <p>
                                            <input
                                                    class="form-input"
                                                    type="text"
                                                    id="wp2litmnf_form_map_name_<?php echo $form_item['Id'] ?>"
                                                    placeholder="<?php echo __('Input Map Name', 'wp2leads_itm_nexforms'); ?>"
                                            >
                                        </p>
                                        <?php
                                    } else {
                                        ?>
                                        <h4><small style="font-weight: lighter;"><?php echo __('Map', 'wp2leads_itm_nexforms'); ?>:</small> <?php echo $forms_to_map[$form_item['Id']]['name'] ?></h4>
                                        <p>
                                            <?php echo __('Redirection URL', 'wp2leads_itm_nexforms'); ?>:
                                            <strong><?php echo get_site_url(null, '/'); ?>wp2leads_itm_nexforms/<?php echo $form_item['Id'] ?>/<?php echo $forms_to_map[$form_item['Id']]['id'] ?></strong>
                                            <span id="tippy_<?php echo $form_item['Id'] ?>" data-template="redirection_url_content" class="dashicons dashicons-editor-help tippy_button"></span>
                                        </p>
                                        <?php
                                    }
                                    ?>
                                </div>

                                <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                                    <p>
                                        <a
                                                href="<?php echo get_admin_url( null, 'admin.php?page=nex-forms-builder&open_form='.$form_item['Id'] ) ?>"
                                                class="button button-primary button-small "
                                                target="_blank"
                                        >
                                            <?php echo __('Edit Form', 'wp2leads_itm_nexforms'); ?>
                                        </a>
                                        <?php
                                        if (empty($forms_to_map[$form_item['Id']])) {
                                            ?>
                                            <button
                                                class="wp2litmnf_generate_map button button-primary button-small"
                                                data-form-id="<?php echo $form_item['Id'] ?>"
                                            >
                                                <?php _e('Generate new map', 'wp2leads_itm_nexforms') ?>
                                            </button>
                                            <?php
                                        } else {
                                            ?>
                                            <a
                                                    href="<?php echo get_admin_url( null, 'admin.php?page=wp2l-admin&tab=map_to_api&active_mapping='.$forms_to_map[$form_item['Id']]['id'] ) ?>"
                                                    class="button button-primary button-small "
                                                    target="_blank"
                                            >
                                                <?php echo __('Edit Map', 'wp2leads_itm_nexforms'); ?>
                                            </a>

                                            <button
                                                    class="wp2litmnf_delete_map button button-small"
                                                    data-form-id="<?php echo $form_item['Id'] ?>"
                                                    data-map-id="<?php echo $forms_to_map[$form_item['Id']]['id'] ?>"
                                            >
                                                <?php _e('Delete map', 'wp2leads_itm_nexforms') ?>
                                            </button>
                                            <?php
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php
                        }

                        ?>
                        <script>
                            // With the above scripts loaded, you can call `tippy()` with a CSS
                            // selector and a `content` prop:
                            tippy('.tippy_button', {
                                content(reference) {
                                    var id = reference.getAttribute('data-template');
                                    var template = document.getElementById(id);
                                    return template.innerHTML;
                                },
                                allowHTML: true,
                                theme: 'light-border',
                                placement: 'bottom-end',
                                interactive: true
                            });
                        </script>
                        <?php
                    } else {
                        ?>

                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wptl-settings-group open">
        <div class="wptl-settings-group-header wptl-settings-group-header-accordeon">
            <h3><?php _e('Documentation', 'wp2leads_itm_nexforms') ?></h3>

            <span class="dashicons dashicons-arrow-up-alt2"></span>
            <span class="dashicons dashicons-arrow-down-alt2"></span>
        </div>

        <div class="wptl-settings-group-body">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <h4>
                        <?php echo __('How to integrate NEX-Forms with Klick-Tipp', 'wp2leads_itm_nexforms') ?>
                    </h4>
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-6">
                            <ol>
                                <li>
                                    <strong><?php echo __('Connect your form with map:', 'wp2leads_itm_nexforms') ?></strong>

                                    <ol style="list-style-type: lower-latin">
                                        <li><?php _e('Click "Generate new map" button', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Copy "Redirect URL" and click "Edit Form" button', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('On form editing page open "OPTIONS" tab', 'wp2leads_itm_nexforms'); ?></li>
                                        <li><?php _e('Select "Custom (For developers)" for SUBMISSION TYPE. We reccomend to use POST for POST METHOD', 'wp2leads_itm_nexforms'); ?></li>
                                        <li><?php _e('Paste copied URL into "SUBMIT FORM TO" field', 'wp2leads_itm_nexforms'); ?></li>
                                        <li><?php _e('Click "Save" button', 'wp2leads_itm_nexforms'); ?></li>
                                    </ol>
                                </li>
                            </ol>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-6">
                            <ol start="2">
                                <li>
                                    <strong><?php echo __('Enable User Emailing:', 'wp2leads_itm_nexforms') ?></strong>

                                    <ol style="list-style-type: lower-latin">
                                        <li><?php _e('Open "EMAIL SETUP" tab in form editing page', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Click "User Email Auto Responder" in left submenu', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Change "Dont send confirmation mail to user" option in "RECIPIENTS (MAP EMAIL FIELD)" to proper Email field in your form', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Click "Save" button', 'wp2leads_itm_nexforms'); ?></li>
                                    </ol>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-6">
                            <ol start="3">
                                <li>
                                    <strong><?php echo __('Enable Admin Emailing:', 'wp2leads_itm_nexforms') ?></strong>

                                    <ol style="list-style-type: lower-latin">
                                        <li><?php _e('Open "EMAIL SETUP" tab in form editing page', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Click "YES" in Send Admin Email', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Open "INTEGRATION" tab in form editing page', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Click "Attach this PDF to Admin Notifications Emails" in PDF Creator section', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Click "Save" button', 'wp2leads_itm_nexforms'); ?></li>
                                    </ol>
                                </li>
                            </ol>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-6">
                            <ol start="4">
                                <li>
                                    <strong><?php echo __('Make map settings:', 'wp2leads_itm_nexforms') ?></strong>

                                    <ol style="list-style-type: lower-latin">
                                        <li><?php _e('Before making initial settings for map, we highly recommend to submit form at least once.', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Visit a page with a form published and submit it.', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Click "Edit map" button', 'wp2leads_itm_nexforms') ?></li>
                                        <li><?php _e('Please follow the green buttons to complete the initial settings.', 'wp2leads_itm_nexforms') ?></li>
                                    </ol>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <h4><?php echo __('How to remove Klick-Tipp integration for NEX-Forms', 'wp2leads_itm_nexforms') ?></h4>

                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-6">
                            <ol>
                                <li><?php _e('Click "Delete map" button', 'wp2leads_itm_nexforms') ?></li>
                                <li><?php _e('Click "Edit Form" button', 'wp2leads_itm_nexforms') ?></li>
                                <li><?php _e('On form editing page open "OPTIONS" tab', 'wp2leads_itm_nexforms') ?></li>
                                <li><?php _e('Select "AJAX (default)" for SUBMISSION TYPE', 'wp2leads_itm_nexforms'); ?></li>
                            </ol>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
