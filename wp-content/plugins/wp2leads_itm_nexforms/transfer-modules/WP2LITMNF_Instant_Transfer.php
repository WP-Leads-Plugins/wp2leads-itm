<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class WP2LITMNF_Instant_Transfer {
    private static $key = 'wp2leads_itm_nexforms';
    private static $required_column = 'wap_nex_forms_entries.Id';

    public static function get_label() {
        return __('Nexforms Entries Transfer', 'wp2leads_itm_nexforms');
    }

    public static function get_description() {
        return __('This module will transfer user data to KlickTipp once Nexforms Entry created', 'wp2leads_itm_nexforms');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for NEX-Forms plugin map.', 'wp2leads_itm_nexforms') ?></p>
        <p><?php _e('Once new NEX-Forms Entry created user data will be transfered to KlickTipp account.', 'wp2leads_itm_nexforms') ?></p>
        <p><?php _e('Requirement: <strong>wap_nex_forms_entries.Id</strong> column within selected data.', 'wp2leads_itm_nexforms') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function transfer_init() {
        add_action('wp2leads_itm_nexforms_created', 'WP2LITMNF_Instant_Transfer::transfer', 30, 2);
    }

    public static function transfer($entry_id, $form_id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $entry_id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_itm_nexforms_itm($transfer_modules) {
    $transfer_modules['wp2leads_itm_nexforms'] = 'WP2LITMNF_Instant_Transfer';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_itm_nexforms_itm');