(function( $ ) {
    $(document.body).on('click', '.wp2litmnf_generate_map', function() {
        var btn = $(this);
        var formId = btn.data('form-id');
        var mapName = $('#wp2litmnf_form_map_name_' + formId).val();

        var data = {
            action: 'wp2litmnf_generate_map',
            formId: formId,
            mapName: mapName
        };

        ajaxRequest(data, function() {}, function() {});
    });

    $(document.body).on('click', '.wp2litmnf_delete_map', function() {
        var btn = $(this);
        var formId = btn.data('form-id');
        var mapId = btn.data('map-id');

        var data = {
            action: 'wp2litmnf_delete_map',
            formId: formId,
            mapId: mapId
        };

        ajaxRequest(data, function() {}, function() {});
    });

    $(document.body).on('click', '.wptl-settings-group-header.wptl-settings-group-header-accordeon', function() {
        var container = $(this).parents('.wptl-settings-group');
        var body = container.find('.wptl-settings-group-body');

        if (container.hasClass('open')) {
            container.removeClass('open').addClass('closed');
            body.slideUp();
        } else {
            container.removeClass('closed').addClass('open');
            body.slideDown();
        }
    });

    function ajaxRequest(data, cb, cbError) {
        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function (response) {
                var decoded;

                console.log(response);

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (decoded.url) {
                            window.location.replace(decoded.url);
                        } else if (decoded.reload) {
                            window.location.reload();
                        }

                        if (typeof cb === 'function') {
                            cb();
                        }
                    } else {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (typeof cbError === 'function') {
                            cbError();
                        }
                    }
                } else {
                    alert('Something went wrong');
                }
            }
        });
    }
})( jQuery );