<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for NEX-Forms plugin
 * Description:
 * Version:         1.1.10
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_nexforms
 *
 * Requires at least: 5.0
 * Tested up to: 5.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_NEXFORMS_VERSION', '1.1.10' );
define( 'WP2LITM_NEXFORMS_DB_VERSION', '1.1.10' );
define( 'WP2LITM_NEXFORMS_PLUGIN_FILE', __FILE__ );
define( 'WP2LITM_NEXFORMS_PLUGIN_REL_FILE', dirname( plugin_basename( __FILE__ ) ) );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_nexforms.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_nexforms'
);
require_once('includes/WP2LITMNF_Functions.php');
require_once('includes/WP2LITMNF_Notices.php');

add_action( 'plugins_loaded', 'WP2LITMNF_Functions::load_plugin_textdomain' );

if (!WP2LITMNF_Functions::requirement()) {
    WP2LITMNF_Notices::show_message('requirements_failed');

    return;
}

require_once('includes/WP2LITMNF_Settings.php');
require_once('includes/WP2LITMNF_Model.php');
require_once('includes/WP2LITMNF_Ajax.php');
require_once('includes/WP2LITMNF_Map.php');
require_once('includes/WP2LITMNF_Submit.php');
require_once('includes/WP2LITMNF_Public.php');

if (is_admin()) {
    require_once 'includes/WP2LITMNF_Admin.php';
}

add_action( 'init', 'WP2LITMNF_Functions::init' );
require_once('transfer-modules/WP2LITMNF_Instant_Transfer.php');