<?php


class WP2LITMNF_Functions {
    public static function init() {
        $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

        if (!$is_ajax) {
            // WP2LITMNF_Model::import_data();
            self::check_version();
            self::check_updates();
        }
    }

    public static function check_version() {
        $version = get_option( 'wp2leads_itm_nexforms' );
        $dbversion = get_option( 'wp2leads_itm_nexforms' );

        if (
            empty($version) || version_compare( $version, WP2LITM_NEXFORMS_VERSION, '<' ) ||
            empty($dbversion) || version_compare( $dbversion, WP2LITM_NEXFORMS_DB_VERSION, '<' )
        ) {
            self::install();
            do_action( 'wp2leads_itm_nexforms_updated' );
        }

        update_option('wp2leads_itm_nexforms', WP2LITM_NEXFORMS_VERSION);
        update_option('wp2leads_itm_nexforms', WP2LITM_NEXFORMS_DB_VERSION);
    }

    public static function check_updates() {
        $update_1_1_4 = get_option('wp2leads_itm_nexforms_update_1_1_4');

        if (empty($update_1_1_4)) {
            require_once 'WP2LITMNF_Update.php';

            WP2LITMNF_Update::update_1_1_4();
        }
    }

    public static function install() {
        WP2LITMNF_Model::create_table();
    }
//    public static function enqueue_admin_scripts() {
//        wp_enqueue_script( 'limit-product-purchase', plugin_dir_url( LIMIT_PRODUCT_PURCHASE_FILE ) . 'assets/js/admin.js', array( 'jquery' ), time(), true );
//    }

    public static function load_plugin_textdomain() {
        add_filter( 'plugin_locale', 'WP2LITMNF_Functions::check_de_locale');

        load_plugin_textdomain(
            'wp2leads_itm_nexforms',
            false,
            WP2LITM_NEXFORMS_PLUGIN_REL_FILE . '/languages/'
        );

        remove_filter( 'plugin_locale', 'WP2LITMNF_Functions::check_de_locale');
    }

    public static function check_de_locale($domain) {
        $site_lang = get_user_locale();
        $de_lang_list = array(
            'de_CH_informal',
            'de_DE_formal',
            'de_AT',
            'de_CH',
            'de_DE'
        );

        if (in_array($site_lang, $de_lang_list)) return 'de_DE';
        return $domain;
    }

    public static function requirement() {
        if (!self::is_plugin_activated( 'nex-forms', 'main.php' )) return false;
        if (!self::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) return false;

        return true;
    }

    public static function is_plugin_activated( $plugin_folder, $plugin_file ) {
        if ( WP2LITMNF_Functions::is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
        else return WP2LITMNF_Functions::is_plugin_active_by_file( $plugin_file );
    }

    public static function is_plugin_active_simple( $plugin ) {
        return (
            in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
            ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
        );
    }

    public static function is_plugin_active_by_file( $plugin_file ) {
        foreach ( WP2LITMNF_Functions::get_active_plugins() as $active_plugin ) {
            $active_plugin = explode( '/', $active_plugin );
            if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
        }

        return false;
    }

    public static function get_active_plugins() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
        if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

        return $active_plugins;
    }

    public static function get_plugin_name() {
        $data = get_plugin_data( WP2LITM_NEXFORMS_PLUGIN_FILE );

        return $data['Name'];
    }
}