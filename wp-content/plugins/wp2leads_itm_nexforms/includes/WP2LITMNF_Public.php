<?php


class WP2LITMNF_Public {
    public static function add_rewrite_rules() {
        flush_rewrite_rules();

        add_rewrite_rule('^wp2leads_itm_nexforms/(.*)/([^/]*)/?',           'index.php?wp2leads_itm_nexforms=1&form_id=$matches[1]&map_id=$matches[2]', 'top');
        add_rewrite_rule('^wp2leads_itm_nexforms/(.*)/?',                   'index.php?wp2leads_itm_nexforms=1&form_id=$matches[1]', 'top');
        add_rewrite_tag( '%wp2leads_itm_nexforms%', '(.*)' );
        add_rewrite_tag( '%form_id%', '(.*)' );
        add_rewrite_tag( '%map_id%', '(.*)' );

        add_rewrite_rule('^nf_entry_pdf/(.*)/?', 'index.php?nf_entry_pdf=1&pdf_id=$matches[1]', 'top');
        add_rewrite_tag( '%nf_entry_pdf%', '(.*)' );
        add_rewrite_tag( '%pdf_id%', '(.*)' );
    }

    public static function template_redirect() {
        $wp2leads_itm_nexforms = get_query_var( 'wp2leads_itm_nexforms' );

        if (!empty($wp2leads_itm_nexforms)) {
            $form_id = !empty(get_query_var( 'form_id' )) ? get_query_var( 'form_id' ) : '';
            $map_id = !empty(get_query_var( 'map_id' )) ? get_query_var( 'map_id' ) : '';

            $submit = WP2LITMNF_Submit::submit_nex_form();
            $redirect_link = WP2LITMNF_Submit::get_optin_redirect_link($form_id, $submit['entry_id'], $submit['email']);

            ob_start();
            echo '=======================================';
            var_dump($submit);
            var_dump($redirect_link);
            echo '=======================================';
            error_log(ob_get_clean());

            if (!empty($redirect_link)) {
                wp_redirect($redirect_link, 301);
            }
            ?>
            <h1>Done</h1>
            <?php
            exit();
        }

        $nf_entry_pdf = get_query_var( 'nf_entry_pdf' );
        $pdf_id = get_query_var( 'pdf_id' );

        if (!empty($nf_entry_pdf) && !empty($pdf_id)) {
            $pdf = WP2LITMNF_Model::get_entry_field_by_pdf($pdf_id);
            $pdf = apply_filters('wp2leads_itm_nexforms_entry_field_by_pdf', $pdf, $pdf_id);

            if (!empty($pdf["entry_id"])) {
                if (!empty($_GET["action"]) && 'delete' === $_GET["action"]) {
                    do_action('wp2leads_itm_nexforms_before_delete_pdf', $pdf_id);
                    WP2LITMNF_Model::delete_entry_field_by_pdf($pdf);
                    echo "OK";

                    exit();
                }

                do_action('wp2leads_itm_nexforms_before_generate_pdf', $pdf["entry_id"]);

                WP2LITMNF_Submit::generate_pdf($pdf["entry_id"], 'download');
                exit();
            }
        }
    }
}

add_action( 'init', array('WP2LITMNF_Public', 'add_rewrite_rules') );
add_action( 'template_redirect', array('WP2LITMNF_Public', 'template_redirect') );