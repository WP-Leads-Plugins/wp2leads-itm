<?php


class WP2LITMNF_Model {
    private static $table_name = 'wp2leads_nexforms_fields';
    private static $v_table_name = 'wp2leads_v_nexforms_fields';

    private static function get_schema() {
        global $wpdb;

        $table_name = $wpdb->prefix . self::$table_name;

        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';
        $schema = "CREATE TABLE {$table_name} (
ID BIGINT UNSIGNED NOT NULL auto_increment,
entry_id BIGINT UNSIGNED NOT NULL,
field_name MEDIUMTEXT,
field_value MEDIUMTEXT,
field_name_value MEDIUMTEXT,
field_name_single MEDIUMTEXT,
field_value_single MEDIUMTEXT,
PRIMARY KEY  (ID)
) $collate;";

        return $schema;
    }

    private static function get_v_schema() {
        global $wpdb;

        $table_name = $wpdb->prefix . self::$v_table_name;

        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';
        $schema = "CREATE TABLE {$table_name} (
ID BIGINT UNSIGNED NOT NULL auto_increment,
entry_id BIGINT UNSIGNED NOT NULL,
field_name TEXT,
field_value MEDIUMTEXT,
PRIMARY KEY  (ID)
) $collate;";

        return $schema;
    }

    public static function create_table() {
        global $wpdb;
        $wpdb->hide_errors();
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( self::get_schema() );
    }

    public static function create($data) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name;

        if (!empty($data['ID'])) {
            $ID = $data['ID'];
            unset($data['ID']);

            $wpdb->update( $table_name,
                $data,
                array( 'ID' => $ID )
            );
        } else {
            $wpdb->insert( $table_name,
                $data
            );

            $ID = $wpdb->insert_id;
        }

        return $ID;
    }

    public static function is_empty() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name;
        $sql = "SELECT * FROM {$table_name}";
        $result = $wpdb->get_results($sql, ARRAY_A);

        return empty($result);
    }

    public static function import_data() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name;

        $sql = "DELETE FROM {$table_name}";
        $delete = $wpdb->query($sql);

        $data_for_import = self::get_data_for_import();

        if (empty($data_for_import)) {
            return false;
        }

        foreach ($data_for_import as $data) {
            self::create($data);
        }
    }

    public static function get_data_for_import() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'wap_nex_forms_entries';
        $sql = "SELECT * FROM {$table_name}";
        $result = $wpdb->get_results($sql, ARRAY_A);
        $data_for_import = array();

        if (!empty($result)) {
            foreach ($result as $form_entry) {
                if (empty($form_entry['form_data'])) continue;
                $form_data = json_decode( $form_entry['form_data'], true );
                if (empty($form_data)) continue;
                $entry_id = $form_entry['Id'];
                $entry_email = $form_entry['saved_user_email_address'];

                foreach ($form_data as $item) {
                    $field_name = rtrim ($item['field_name'], ':');
                    $field_value = $item['field_value'];
                    $field_name_value = $field_name . ': ' . $field_value;

                    if ($field_value !== $entry_email) {
                        $data_for_import[] = array(
                            'entry_id' => $entry_id,
                            'field_name' => $field_name,
                            'field_value' => $field_value,
                            'field_name_value' => $field_name_value,
                        );
                    }
                }
            }
        }

        return $data_for_import;
    }

    public static function get_entry_field_by_pdf($pdf_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name;
        $sql = "SELECT * FROM {$table_name} WHERE field_name_single = 'user_pdf_link' && field_value_single LIKE ('%{$pdf_id}%')";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!empty($result[0]["entry_id"])) {
            return $result[0];
        }

        return false;
    }

    public static function delete_entry_field_by_pdf($pdf) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name;

        $sql = "DELETE FROM {$table_name} WHERE ID = '{$pdf['ID']}' AND entry_id = '{$pdf['entry_id']}'";
        $delete = $wpdb->query($sql);

        return $delete;
    }
}