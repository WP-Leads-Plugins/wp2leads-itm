<?php


class WP2LITMNF_Notices {
    private static $notices = array(
        'requirements_failed' => array(
            'type' => 'error',
            'position' => ''
        ),
    );

    public static function show_message($notice_id) {
        add_action('admin_notices', 'WP2LITMNF_Notices::' . $notice_id);
    }

    public static function requirements_failed() {
        $message = sprintf( __( 'Please, install all required plugins.', 'wp2leads_itm_nexforms' ) );

        if (!WP2LITMNF_Functions::is_plugin_activated( 'nex-forms', 'main.php' )) {
            $message .= ' ' . sprintf(
                __( 'Visit %1$sNEX-Forms plugin page%2$s to get it.', 'wp2leads_itm_nexforms' ),
                '<a href="https://codecanyon.net/item/nexforms-the-ultimate-wordpress-form-builder/7103891" target="_blank">',
                '</a>'
            );
        }

        if (!WP2LITMNF_Functions::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) {
            $message .= ' ' . sprintf(
                __( 'Please, install WP2LEADS plugin %1$shere%2$s.', 'wp2leads_itm_nexforms' ),
                '<a href="/wp-admin/plugin-install.php?s=WP2LEADS&tab=search&type=term">',
                '</a>'
            );
        }
        ?>
        <div class="notice notice-error">
            <p>
                <strong><?php echo WP2LITMNF_Functions::get_plugin_name(); ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }
}