<?php
class WP2LITMNF_Ajax {
    public static function submit_nex_form() {
        WP2LITMNF_Submit::submit_nex_form();

        die();
    }

    public static function error_response($params = array()) {
        $response = array('success' => 0, 'error' => 1);

        if (!empty($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }

    public static function success_response($params = array()) {
        $response = array('success' => 1, 'error' => 0);

        if (!empty($params) && is_array($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }
}

//add_action('wp_ajax_submit_nex_form', array( 'WP2LITMNF_Ajax', 'submit_nex_form'), 5);
//add_action('wp_ajax_nopriv_submit_nex_form', array( 'WP2LITMNF_Ajax', 'submit_nex_form'), 5);