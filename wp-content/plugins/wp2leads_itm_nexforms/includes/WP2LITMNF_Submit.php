<?php


class WP2LITMNF_Submit {
    public static function submit_nex_form() {
        if(($_POST['company_url']!='' && $_POST['company_url']!='enter company url') || strstr($_POST['email'],'@qq.com'))
            die();

        global $wpdb;

        do_action('wp2leads_itm_nexforms_before_form_submit');

        $nf_functions = new NEXForms_Functions();
        $nf7_functions = new NEXForms_Functions();
        $database_actions = new NEXForms_Database_Actions();

        $response_messages = array();

        $nex_forms_id = isset($_REQUEST['nex_forms_Id']) ? filter_var($_POST['nex_forms_Id'],FILTER_SANITIZE_NUMBER_INT) : '';

        $get_form = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms WHERE Id = %d',$nex_forms_id);
        $form_attr = $wpdb->get_row($get_form);

        /**** INSERT POST DATA *****/

        do_action('wp2leads_itm_nexforms_before_insert_post_data');

        $checked = 'true';
        $entry_id = WP2LITMNF_Submit::insert_post_data($form_attr);
        $user_email = $_REQUEST[$form_attr->user_email_field];
        $update = $wpdb->update ( $wpdb->prefix . 'wap_nex_forms_entries', array('saved_user_email_address' => $user_email), array( 'Id' => $entry_id ));

        do_action('wp2leads_itm_nexforms_after_insert_post_data', $entry_id);

        /**** INSERT POST FILES ****/
        $insert_file = WP2LITMNF_Submit::insert_file($entry_id, $form_attr);
        $response_messages = array_merge($response_messages, $insert_file['errors']);
        $files = $insert_file['files'];

        $email_alerts = explode(',',$form_attr->email_on_payment_success);
        $email_body_array = WP2LITMNF_Submit::generate_mail_body($nex_forms_id, $entry_id);
        $pdf_attached_path = array();

        if (function_exists('NEXForms_export_to_PDF') &&  $form_attr->attach_pdf_to_email != '' ) {
            try {
                $pdf_attached_path = WP2LITMNF_Submit::generate_pdf($entry_id);
            } catch ( Exception $e ) {
                $pdf_attached_path = array();
            }
        }

        $get_entry = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms_entries WHERE Id = %d',$entry_id);
        $form_entry = $wpdb->get_row($get_entry, ARRAY_A);

        $data_for_kt = array();

        if (!empty($email_body_array['user_data'])) {
            $data_for_kt[] = array(
                'entry_id' => $entry_id,
                'field_name_single' => 'user_email_html',
                'field_value_single' => wp_kses_post( $email_body_array['user_data'] ),
            );
        }

        $data_for_kt[] = array(
            'entry_id' => $entry_id,
            'field_name_single' => 'user_pdf_link',
            'field_value_single' => $pdf_attached_path['link'],
        );

        $data_for_kt[] = array(
            'entry_id' => $entry_id,
            'field_name_single' => 'user_pdf_title',
            'field_value_single' => $pdf_attached_path['title'],
        );

        if (!empty($form_entry['form_data'])) {
            $form_data = json_decode( $form_entry['form_data'], true );

            if (!empty($form_data)) {
                $entry_id = $form_entry['Id'];
                $entry_email = $form_entry['saved_user_email_address'];

                foreach ($form_data as $item) {
                    $field_name = rtrim ($item['field_name'], ':');
                    $field_value = $item['field_value'];
                    $field_name_value = $field_name . ': ' . $field_value;

                    if ($field_value !== $entry_email) {
                        if (is_array($field_value)) {
                            foreach ($field_value as $i => $field_item) {
                                $data_for_kt[] = array(
                                    'entry_id' => $entry_id,
                                    'field_name' => $field_name . '_' . $i,
                                    'field_value' => sanitize_text_field($field_item),
                                    'field_name_value' => $field_name . '_' . $i . ': ' . sanitize_text_field($field_item),
                                );
                            }
                        } else {
                            $data_for_kt[] = array(
                                'entry_id' => $entry_id,
                                'field_name' => $field_name,
                                'field_value' => sanitize_text_field($field_value),
                                'field_name_value' => $field_name . ': ' . sanitize_text_field($field_value),
                            );
                        }
                    }
                }
            }
        }

        if (!empty($data_for_kt)) {
            $data_for_kt = apply_filters('wp2leads_itm_nexforms_data_for_kt', $data_for_kt, $entry_id);

            foreach ($data_for_kt as $data) {
                WP2LITMNF_Model::create($data);
            }
        }

        do_action('wp2leads_itm_nexforms_created', $entry_id, $nex_forms_id);

        $attach_to_email = json_decode($form_attr->attachment_settings, true);
        $attach_to_admin_email 	= ($attach_to_email['0']['attach_to_admin_email']) ? $attach_to_email['0']['attach_to_admin_email'] 	: 'true';

        $option_settings = json_decode($form_attr->option_settings,true);
        $send_admin_email 	= ($option_settings['0']['send_admin_email']) ? $option_settings['0']['send_admin_email'] 	: 'true';

        if ($send_admin_email == 'true') {
            WP2LITMNF_Submit::send_admin_email($nex_forms_id, $entry_id, $email_body_array, $pdf_attached_path);
        }

        do_action('wp2leads_itm_nexforms_after_form_submit');

        return array(
            'email' => $user_email,
            'entry_id' => $entry_id,
        );
    }

    public static function send_admin_email($nex_forms_id, $entry_id, $email_body_array, $pdf_attached_path = array()) {
        global $wpdb;

        if (!empty($pdf_attached_path['link'])) {
            $pdf_link_html = '<br /><table width="100%" cellpadding="10" cellspacing="0" style="border-top:1px solid #ddd; border-right:1px solid #ddd; border-left:1px solid #ddd;">';

            $pdf_link_html .= '<tr>
							<td width="35%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f9f9f9;"><strong>'.__('PDF:','wp2leads_itm_nexforms').'</strong></td>
							<td width="65%" style="border-bottom:1px solid #ddd;" valign="top"><span class="payment_status">'.$pdf_attached_path['link'].'</span></td>
						<tr>
						';

            $pdf_link_html .= '</table>';

            if (empty($email_body_array['user_data'])) {
                $email_body_array['user_data'] = '';
            }

            $email_body_array['user_data'] .= $pdf_link_html;
        }

        $get_form = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms WHERE Id = %d',$nex_forms_id);
        $form_attr = $wpdb->get_row($get_form);

        $get_entry = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms_entries WHERE Id = %d',$entry_id);
        $entry_attr = $wpdb->get_row($get_entry);

        $preferences = get_option('nex-forms-preferences');
        $email_pref = $preferences['email_preferences'];
        $other_pref = $preferences['other_preferences'];

        $nf_functions = new NEXForms_Functions();
        $nf7_functions = new NEXForms_Functions();
        $database_actions = new NEXForms_Database_Actions();

        $from_address 						= ($form_attr->from_address) 						? str_replace('\\','',$form_attr->from_address)							: $email_pref['pref_email_from_address'];
        $from_name 							= ($form_attr->from_name) 							? str_replace('\\','',$form_attr->from_name)							: $email_pref['pref_email_from_name'];
        $mail_to 							= ($form_attr->mail_to) 							? str_replace('\\','',$form_attr->mail_to)								: $email_pref['pref_email_recipients'];
        $bcc	 							= ($form_attr->bcc) 								? str_replace('\\','',$form_attr->bcc)									: '';
        $subject 							= ($form_attr->confirmation_mail_subject) 			? str_replace('\\','',$form_attr->confirmation_mail_subject) 			:  str_replace('\\','',$email_pref['pref_email_subject']);
        $admin_body 						= ($form_attr->admin_email_body) 					? str_replace('\\','',$form_attr->admin_email_body) 					:  str_replace('\\','','{{nf_form_data}}');

        if(class_exists('NEXForms_Conditional_Content')) {
            $conditional_blocks = new NEXForms_Conditional_Content();

            $from_address 	= $conditional_blocks->run_content_logic_blocks($from_address);
            $from_name 		= $conditional_blocks->run_content_logic_blocks($from_name);
            $mail_to 		= $conditional_blocks->run_content_logic_blocks($mail_to);
            $bcc 			= $conditional_blocks->run_content_logic_blocks($bcc);
            $subject 		= $conditional_blocks->run_content_logic_blocks($subject);
            $admin_body 	= $conditional_blocks->run_content_logic_blocks($admin_body);
        }

        $email_config = get_option('nex-forms-email-config');
        if($email_config['email_content']!='pt') {
            $email_head = '<head>';
            $email_head .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            $email_head .= '<title>'.$subject.'</title>';
            $email_head .= '<style type="text/css">';

            $email_head .= 'html,body{margin:0;font-family: Arial, Helvetica, sans-serif; font-size:13px; line-height:18px; color:#555}';
            $email_head .= 'h1{font-size:22px; color:#555; padding-bottom: 12px; border-bottom:1px solid #ddd;}';
            $email_head .= 'h2{font-size:20px; color:#555; padding-bottom: 10px; border-bottom:1px solid #ddd;}';
            $email_head .= 'h3{font-size:18px; color:#555; padding-bottom: 8px; border-bottom:1px solid #ddd;}';
            $email_head .= 'h4{font-size:16px; color:#555}';
            $email_head .= 'h5{font-size:15px; color:#555; margin-bottom:0px;}';
            $email_head .= 'h6{font-size:13px; color:#555}';
            $email_head .= 'table,td,th{font-size:12px; color:#555}';
            $email_head .= 'p{line-height:20px; font-size:12px; color:#555}';

            $email_head .= '</style>';
            $email_head .= '</head>';
            $admin_body = '<html>'.$email_head.'<body>'.$admin_body.'</body></html>';
        }

        $_REQUEST['nf_form_data']				= ($email_config['email_content']!='pt') ? $email_body_array['user_data'] : $email_body_array['user_data'];
        $_REQUEST['nf_paypal_data']				= $email_body_array['paypal_data'];
        $_REQUEST['nf_paypal_status']			= '<span class="payment_status">'.$entry_attr->payment_status.'</span>';
        $_REQUEST['nf_paypal_ammount']			= $entry_attr->payment_ammount;
        $_REQUEST['nf_paypal_currency']			= $entry_attr->payment_currency;
        $_REQUEST['nf_paypal_payment_id']		= $entry_attr->paypal_payment_id;
        $_REQUEST['nf_paypal_payment_token']	= $entry_attr->paypal_payment_token;
        $_REQUEST['nf_from_page'] 				= filter_var($_POST['page'],FILTER_SANITIZE_STRING);
        $_REQUEST['nf_form_id'] 				= filter_var($_POST['nex_forms_Id'],FILTER_SANITIZE_NUMBER_INT);
        $_REQUEST['nf_entry_id']				= $entry_id;
        $_REQUEST['nf_entry_date_time'] 		= date(get_option('date_format').' '.get_option('time_format'));
        $_REQUEST['nf_entry_date'] 				= date(get_option('date_format'));
        $_REQUEST['nf_entry_time'] 				= date(get_option('time_format'));
        $_REQUEST['nf_entry_date_year'] 		= date('Y');
        $_REQUEST['nf_entry_date_month'] 		= date('m');
        $_REQUEST['nf_entry_date_day'] 			= date('d');
        $_REQUEST['nf_entry_time_hour'] 		= date('H');
        $_REQUEST['nf_entry_time_minute'] 		= date('i');
        $_REQUEST['nf_entry_time_second'] 		= date('s');
        $_REQUEST['nf_user_ip'] 				= $_SERVER['REMOTE_ADDR'];
        $_REQUEST['nf_form_title'] 				= $form_attr->title;
        $_REQUEST['nf_user_ID'] 				= get_current_user_id();
        $_REQUEST['nf_user_name'] 				= $database_actions->get_username(get_current_user_id());
        $_REQUEST['nf_user_first_name'] 		= $database_actions->get_user_firstname(get_current_user_id());
        $_REQUEST['nf_user_last_name'] 			= $database_actions->get_user_lastname(get_current_user_id());
        $_REQUEST['nf_user_email_address'] 		= $database_actions->get_useremail(get_current_user_id());
        $_REQUEST['nf_user_url'] 				= $database_actions->get_userurl(get_current_user_id());

        $pattern = '({{+([A-Za-z 0-9-:_ÄÖÜäöüßèéîùà-’])+}})';

        //SETUP VALUEPLACEHOLDER - ADMIN EMAIL
        preg_match_all($pattern, $admin_body, $matches2);

        foreach($matches2[0] as $match) {
            $the_val = '';

            if(is_array($_REQUEST[$nf_functions->format_name($match)])) {
                foreach($_REQUEST[$nf_functions->format_name($match)] as $thekey => $value) {
                    $the_val .='- '. $value.' ';
                }

                $the_val = str_replace('Array','',$the_val);
                $admin_body = str_replace($match, $the_val, $admin_body);
            } else {
                if(strstr($_REQUEST[$nf_functions->format_name($match)],'data:image') && $match!='{{nf_form_data}}') {
                    $admin_body = str_replace($match,'<img src="'.$_REQUEST[$nf_functions->format_name($match)].'">',$admin_body);
                } else {
                    $admin_body = str_replace($match,$_REQUEST[$nf_functions->format_name($match)],$admin_body);
                }
            }
        }

        preg_match_all($pattern, $subject, $matches5);
        foreach($matches5[0] as $match) $subject = str_replace($match,$_REQUEST[$nf_functions->format_name($match)],$subject);

        //SETUP CC
        if(strstr($mail_to,',')) $mail_to = explode(',',$mail_to);

        //SETUP BCC
        if(strstr($bcc,',')) $bcc = explode(',',$bcc);

        $admin_body = wpautop($admin_body);
        $message = $admin_body;

        if ($email_config['email_method']=='smtp' || $email_config['email_method']=='php_mailer') {
            date_default_timezone_set('Etc/UTC');
            include_once(ABSPATH . WPINC . '/class-phpmailer.php');

            $mail = new PHPMailer;
            $mail->CharSet = "UTF-8";
            $mail->Encoding = "base64";
            if($email_config['email_content']=='pt') $mail->IsHTML(false);

            //Tell PHPMailer to use SMTP
            if($email_config['email_method']=='smtp') {
                $mail->isSMTP();
                $mail->Host = $email_config['smtp_host'];
                $mail->Port = ($email_config['mail_port']) ? $email_config['mail_port'] : 587;

                //Whether to use SMTP authentication
                if($email_config['smtp_auth']=='1') {
                    $mail->SMTPAuth = true;
                    if($email_config['email_smtp_secure']!='0') {
                        $mail->SMTPSecure  = $email_config['email_smtp_secure']; //Secure conection
                    }
                    $mail->Username = $email_config['set_smtp_user'];
                    $mail->Password = $email_config['set_smtp_pass'];
                } else {
                    $mail->SMTPAuth = false;
                }
            }

            $mail->setFrom($from_address, $from_name);
            //BCC
            if(is_array($bcc)) {
                foreach($bcc as $email)
                    $mail->addBCC($email, $from_name);
            } else {
                $mail->addBCC($bcc, $from_name);
            }
            //CC
            if(is_array($mail_to)) {
                foreach($mail_to as $email){
                    $mail->addCC($email, $from_name);
                }
            } else {
                $mail->AddAddress($mail_to, $from_name);
            }

            $mail->Subject = $subject;

            if($email_config['email_content']!='pt')
                $mail->msgHTML($admin_body, dirname(__FILE__));
            else
                $mail->Body = strip_tags($admin_body);

            $sent = $mail->send();
        } elseif ($email_config['email_method']=='php') {
            $time = md5(time());
            $boundary = "==Multipart_Boundary_x{$time}x";
            $headers = 'From: '.$from_address;
            $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$boundary}\"";
            $message = "--{$boundary}\n" . "Content-type: ".((($email_config['email_content']=='html')) ? 'text/html' : 'text/plain')."; charset=UTF-8\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
            $message .= "--{$boundary}\n";

            if (!empty($mail_to)) {
                if(is_array($mail_to)) {
                    foreach($mail_to as $email) {
                        $sent = mail($email, $subject, $message, $headers);
                    }
                } else {
                    $sent = mail($mail_to, $subject, $message, $headers);
                }
            }

            if (!empty($bcc)) {
                if(is_array($bcc)) {
                    foreach($bcc as $email) {
                        $sent = mail($email,$subject,$message,$headers);
                    }
                } else {
                    $sent = mail($bcc,$subject,$message,$headers);
                }
            }
        } elseif($email_config['email_method']=='wp_mailer' || $email_config['email_method']=='api') {
            $headers[] = 'Content-Type: text/html; charset=UTF-8';
            $headers[] = 'From: '.$from_name.' <'.$from_address.'>';

            if(is_array($mail_to)) {
                foreach($mail_to as $email) {
//                    if($attach_to_admin_email=='true')
//                        $sent = wp_mail($email,$subject,$admin_body,$headers, $files);
//                    else
                    $sent = wp_mail($email,$subject,$admin_body,$headers);
                }
            } else {
//                if($attach_to_admin_email=='true')
//                    $sent = wp_mail($mail_to,$subject,$admin_body,$headers, $files);
//                else
                $sent = wp_mail($mail_to,$subject,$admin_body,$headers);
            }


            if(!empty($bcc)) {
                if(is_array($bcc)) {
                    foreach($bcc as $email) {
//                        if($attach_to_admin_email=='true')
//                            $sent = wp_mail($email,$subject,$admin_body,$headers, $files);
//                        else
                        $sent = wp_mail($email,$subject,$admin_body,$headers);
                    }
                } else {
//                    if($attach_to_admin_email=='true')
//                        $sent = wp_mail($bcc,$subject,$admin_body,$headers, $files);
//                    else
                    $sent = wp_mail($bcc,$subject,$admin_body,$headers);
                }
            }
        }
    }

    public static function generate_pdf($entry_ID, $action = 'link') {
        $download = false;
        $save = false;
        $link = false;

        if ($action === 'download') {
            $download = true;
        } elseif ($action === 'save') {
            $save = true;
        } else {
            $link = true;
        }

        $nf_functions     = new NEXForms_Functions();
        $database_actions = new NEXForms_Database_Actions();
        $pdf = new TCPDF( PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false );
        $pdf = apply_filters('wp2leads_itm_nexforms_pdf_after_created_new', $pdf);

        global $wpdb;
        $form_entry = $wpdb->get_row( 'SELECT * FROM ' . $wpdb->prefix . 'wap_nex_forms_entries WHERE Id="' . $entry_ID . '"' );
        $form_attr  = $wpdb->get_row( 'SELECT * FROM ' . $wpdb->prefix . 'wap_nex_forms WHERE Id="' . $form_entry->nex_forms_Id . '"' );
        $date_time = explode( ' ', $form_entry->date_time );
        $date = explode( '-', $date_time[0] );
        $time = explode( ':', $date_time[1] );

        $upload_path     = wp_upload_dir();
        $set_uploads_dir = $upload_path['path'];
        $form_data = json_decode( $form_entry->form_data );
        $id = sha1(time() . $entry_ID . serialize($form_data));
        $name =  substr( $nf_functions->format_name( $database_actions->get_title2( $form_entry->nex_forms_Id, 'wap_nex_forms' ) ), 0, 10 ) . '_' . $form_entry->Id;
        $name = apply_filters('wp2leads_itm_nexforms_pdf_name', $name, $entry_ID);

        if (!empty($link)) {
            $link = get_site_url('/') . '/nf_entry_pdf/' . $id . '_' . $name;

            return array(
                'link' => $link,
                'title' => $name . '.pdf',
            );
        }

        $pdf->SetDefaultMonospacedFont( PDF_FONT_MONOSPACED );
        // $pdf->SetFont( 'freesans', '', 10 );
        $pdf->SetPrintHeader( false );
        $pdf->SetPrintFooter( false );

        $data_val_array = array();
        foreach ( $form_data as $data ) {
            $data_val_array[ $data->field_name ] = $data->field_value;
        }

        $html = '<table width="100%" cellpadding="3" cellspacing="0">';

        $img_ext_array = array( 'jpg', 'jpeg', 'png', 'tiff', 'gif', 'psd' );
        $sigs_array    = array();
        $i             = 0;
        foreach ( $form_data as $data ) {
            if ( $data->field_name != 'paypal_invoice' ) {
                $html .= '<tr>';
                $html .= '<td width="15%"><strong>' . $nf_functions->unformat_name( $data->field_name ) . '</strong></td>';
                $html .= '<td width="2%"><strong>:</strong></td>';
                $html .= '<td width="83%">';
                if ( is_array( $data->field_value ) ) {
                    foreach ( $data->field_value as $key => $val ) {
                        $html .= '<span class="text-success fa fa-check"></span>&nbsp;&nbsp;' . $val . '<br />';
                    }
                } else {
                    if ( strstr( $data->field_value, 'data:image' ) ) {
                        $signature = str_replace( 'data:image/png;base64,', '', $data->field_value );
                        $sig_rand  = rand( 0, 99999 );
                        file_put_contents( plugin_dir_path( dirname( __FILE__ ) ) . 'tmp/sig' . $sig_rand . '.png', base64_decode( $signature ) );
                        $html .= '<img width="200px" src="' . plugins_url( '/tmp/sig' . $sig_rand . '.png', dirname( __FILE__ ) ) . '">';
                        $sigs_array[ $i ] = plugin_dir_path( dirname( __FILE__ ) ) . 'tmp/sig' . $sig_rand . '.png';
                        $i ++;
                    } else if ( in_array( $nf_functions->get_ext( $data->field_value ), $img_ext_array ) ) {
                        $html .= '<img width="200px" src="' . $data->field_value . '">';
                    } else {
                        $html .= $data->field_value;
                    }
                }
                $html .= '</td>';
                $html .= '</tr>';
            }

        }
        $html .= '</table>';

        $form_attr->pdf_html = str_replace( '\\', '', $form_attr->pdf_html );

        $body = str_replace( '{{nf_form_data}}', $html, $form_attr->pdf_html );


        $body = str_replace( '{{nf_from_page}}', $form_entry->page, $body );
        $body = str_replace( '{{nf_form_id}}', $form_entry->nex_forms_Id, $body );
        $body = str_replace( '{{nf_entry_id}}', $entry_ID, $body );
        $body = str_replace( '{{nf_entry_date}}', date( 'Y-m-d H:i:s' ), $body );
        $body = str_replace( '{{nf_user_ip}}', $form_entry->ip, $body );
        $body = str_replace( '{{nf_form_title}}', $form_attr->title, $body );
        $body = str_replace( '{{nf_user_name}}', $database_actions->get_username( $form_entry->user_Id ), $body );
        $body = str_replace( '{{page_break}}', '___page_break___', $body );


        $pattern = '({{+([A-Za-z 0-9_])+}})';
        if ( ! $body ) {
            $body = $html;
        }

        //SETUP VALUEPLACEHOLDER - USER EMAIL
        preg_match_all( $pattern, $body, $matches );

        foreach ( $matches[0] as $match ) {
            $the_val = '';
            if ( is_array( $data_val_array[ $nf_functions->format_name( $match ) ] ) ) {
                foreach ( $data_val_array[ $nf_functions->format_name( $match ) ] as $thekey => $value ) {
                    $the_val .= '<span class="fa fa-check"></span> ' . $value . ' ';
                }
                $the_val = str_replace( 'Array', '', $the_val );
                $body    = str_replace( $match, $the_val, $body );
            } else if ( strstr( $data_val_array[ $nf_functions->format_name( $match ) ], 'data:image' ) ) {
                $signature = str_replace( 'data:image/png;base64,', '', $data_val_array[ $nf_functions->format_name( $match ) ] );
                $sig_rand  = rand( 0, 99999 );
                file_put_contents( plugin_dir_path( dirname( __FILE__ ) ) . 'tmp/sig' . $sig_rand . '.png', base64_decode( $signature ) );

                $image = '<img width="200px" src="' . plugins_url( '/tmp/sig' . $sig_rand . '.png', dirname( __FILE__ ) ) . '">';
                //$html .= $data->field_value;

                $body = str_replace( $match, $image, $body );

                //$html .= ;
                $sigs_array[ $i ] = plugin_dir_path( dirname( __FILE__ ) ) . 'tmp/sig' . $sig_rand . '.png';
                $i ++;
            } else {
                $body = str_replace( $match, $data_val_array[ $nf_functions->format_name( $match ) ], $body );
            }
        }
        $database = new NEXForms_Database_Actions();

        $body = ( $database->checkout() ) ? $body : 'Sorry, you need to register this plugin to export entries to PDF. Go to global settings on the dashboard and follow the registration procedure.';

        $body_array = explode('___page_break___', $body);

        if (1 === count($body_array)) {
            $pdf->AddPage();
            $pdf->writeHTML( wpautop($body), true, false, false, false, '' );
        } else {
            foreach ($body_array as $body_page) {
                $pdf->AddPage();
                $pdf->writeHTML( wpautop($body_page), true, false, false, false, '' );
            }
        }

        $pdf->lastPage();

        foreach ( $sigs_array as $sigs => $val ) unlink( $val );

        if ($download) {
            $pdf->Output( $name . '.pdf');
        } else {
            $pdf->Output( $set_uploads_dir . '/' . $id . '_' . $name . '.pdf', 'F');
            return array(
                'link' => $upload_path["url"] . '/' . $id . '_' . $name . '.pdf',
                'title' => $id . '_' . $name . '.pdf',
            );
        }
    }

    public static function insert_file($entry_id, $form_attr) {
        global $wpdb;
        $upload_settings = json_decode($form_attr->upload_settings, true);
        $upload_to_server 	= ($upload_settings['0']['upload_to_server']) ? $upload_settings['0']['upload_to_server'] 	: 'true';

        $insert_file_array = array();
        $files = array();
        $insert_file_errors_array = array();

        if (!empty($_FILES)) {
            foreach($_FILES as $key=>$file) {
                $multi_file_array = array();

                if(is_array($_FILES[$key]['name'])) {
                    $group_array  = '';
                    if(strstr($key,'gu__')) {
                        $group_name = str_replace('gu__','',$key);
                        $group_name = explode('__',$group_name);
                    }

                    foreach($_FILES[$key]['name'] as $mkey => $mval) {
                        $multi_file_array[$key.'_'.$mkey] = array(
                            'name'=>$_FILES[$key]['name'][$mkey],
                            'type'=>$_FILES[$key]['type'][$mkey],
                            'tmp_name'=>$_FILES[$key]['tmp_name'][$mkey],
                            'error'=>$_FILES[$key]['error'][$mkey],
                            'size'=>$_FILES[$key]['size'][$mkey]
                        );

                    }

                    $file_names = '';
                    $group_num = 1;

                    foreach($multi_file_array as $ukey => $ufile) {
                        $uploadedfile = $multi_file_array[$ukey];
                        $upload_overrides = array( 'test_form' => false );

                        $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

                        if ( $movefile ) {
                            if($movefile['file']) {
                                $insert_file_array[$uploadedfile['name']] = array(
                                    'name' 		=> $uploadedfile['name'],
                                    'type' 		=> $uploadedfile['type'],
                                    'size' 		=> $uploadedfile['size'],
                                    'location' 	=> $movefile['file'],
                                    'url' 		=> $movefile['url'],
                                );
                                $set_file_name = str_replace(ABSPATH,'',$movefile['file']);
                                $file_names .= get_option('siteurl').'/'.$set_file_name. ',';
                                $files[] = $movefile['file'];
                                $filenames[] = get_option('siteurl').'/'.$set_file_name;
                                $_POST[$group_name[0]][$group_num][$group_name[1]] = get_option('siteurl').'/'.$set_file_name;
                                $group_num++;
                            }
                        } else {
                            $insert_file_errors_array[] = "Possible file upload attack!\n".$movefile['error'];
                        }
                    }

                    $_POST[$key] = $file_names;
                } else {
                    $uploadedfile = $_FILES[$key];
                    $upload_overrides = array( 'test_form' => false );
                    //if($upload_to_server=='true')
                    $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

                    if ( $movefile ) {
                        if($movefile['file']) {
                            $insert_file_array[$uploadedfile['name']] = array(
                                'name' 		=> $uploadedfile['name'],
                                'type' 		=> $uploadedfile['type'],
                                'size' 		=> $uploadedfile['size'],
                                'location' 	=> $movefile['file'],
                                'url' 		=> $movefile['url'],
                            );
                            $set_file_name = str_replace(ABSPATH,'',$movefile['file']);
                            $set_file_name = str_replace(ABSPATH,'',$movefile['file']);
                            $_POST[$key] = get_option('siteurl').'/'.$set_file_name;
                            $files[] = $movefile['file'];
                            $filenames[] = get_option('siteurl').'/'.$set_file_name;
                        }
                    } else {
                        $insert_file_errors_array[] = "Possible file upload attack!\n".$movefile['error'];
                    }
                }
            }
        }

        if($upload_to_server=='true') {
            $setup_file_insert_array = array();

            foreach($insert_file_array as $key=>$val) {
                $setup_file_insert_array[$key] = array(
                    'name' 		=> $val['name'],
                    'type' 		=> $val['type'],
                    'size' 		=> $val['size'],
                    'location' 	=> $val['location'],
                    'url' 		=> $val['url'],
                    'nex_forms_Id'=>filter_var($_REQUEST['nex_forms_Id'],FILTER_SANITIZE_NUMBER_INT),
                    'entry_Id' => $entry_id
                );
            }

            foreach($setup_file_insert_array as $insert_file) {
                $wpdb->insert($wpdb->prefix.'wap_nex_forms_files',$insert_file);
            }
        }

        return array(
            'errors' => $insert_file_errors_array,
            'files' => $files,
            'insert_file' => $insert_file_array
        );
    }

    /**************************************************/
    /**************** INSERT POST DATA ****************/
    /**************************************************/
    public static function insert_post_data($form_attr) {
        global $wpdb;
        $data_array 	= array();
        $i				= 1;
        $nf_functions = new NEXForms_Functions();
        $nf7_functions = new NEXForms_Functions();

        foreach($_POST as $key=>$val) {
            if (
                $key!='paypal_invoice' &&
                $key!='paypal_return_url' &&
                $key!='math_result' &&
                $key!='set_file_ext' &&
                $key!='format_date' &&
                $key!='action' &&
                $key!='set_radio_items' &&
                $key!='change_button_layout' &&
                $key!='set_check_items' &&
                $key!='set_autocomplete_items' &&
                $key!='required' &&
                $key!='xform_submit' &&
                $key!='current_page' &&
                $key!='ajaxurl' &&
                $key!='page_id' &&
                $key!='page' &&
                $key!='ip' &&
                $key!='nex_forms_Id' &&
                $key!='company_url' &&
                $key!='ms_current_step' &&
                $key!='submit' &&
                !strstr($key,'real_val') &&
                !strstr($key,'gu__')
            ) {

                if(array_key_exists('real_val__'.$key,$_POST)) {
                    $admin_val = $_POST['real_val__'.$key];
                    $val = $_POST['real_val__'.$key];
                }

                $data_array[] = array('field_name'=>$key,'field_value'=>str_replace('\\','',$val));
                $i++;
            }
        }

        if($form_attr->is_paypal=='yes') {
            if(function_exists('run_nf_adv_paypal')) $paypal_transaction = run_nf_adv_paypal($form_attr->Id, $_POST, $_POST['paypal_return_url']);
        }

        $geo_data = json_decode($nf7_functions->get_geo_location($_SERVER['REMOTE_ADDR']));

        $insert = $wpdb->insert($wpdb->prefix.'wap_nex_forms_entries',
            array(
                'nex_forms_Id'			=>	filter_var($_REQUEST['nex_forms_Id'],FILTER_SANITIZE_NUMBER_INT),
                'page'					=>	filter_var($_POST['page'],FILTER_SANITIZE_URL),
                'ip'					=>  filter_var($_POST['ip'],FILTER_SANITIZE_STRING),
                'paypal_invoice'		=>  filter_var($_POST['paypal_invoice'],FILTER_SANITIZE_NUMBER_INT),
                'user_Id'				=>	get_current_user_id(),
                'hostname'				=>	!empty($geo_data->hostname) ? $geo_data->hostname : '',
                'city'					=>	!empty($geo_data->city) ? $geo_data->city : '',
                'region'				=>	!empty($geo_data->region) ? $geo_data->region : '',
                'country'				=>	!empty($geo_data->country) ? $geo_data->country : '',
                'loc'					=>	!empty($geo_data->loc) ? $geo_data->loc : '',
                'org'					=>	!empty($geo_data->org) ? $geo_data->org : '',
                'postal'				=>	!empty($geo_data->postal) ? $geo_data->postal : '',
                'date_time'				=>  date('Y-m-d H:i:s'),
                'form_data'				=>	json_encode($data_array),
                'paypal_payment_token'	=>  $paypal_transaction['payment_token'],
                'paypal_payment_id'		=>  $paypal_transaction['payment_id'],
                'payment_ammount'		=>  $paypal_transaction['payment_ammount'],
                'payment_currency'		=>  $paypal_transaction['payment_currency'],
                'payment_status'		=>  'pending',
            )
        );

        $entry_id = $wpdb->insert_id;

        return $entry_id;
    }

    public static function generate_mail_body($nex_forms_id, $entry_id) {
        global $wpdb;
        $get_form = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms WHERE Id = %d',$nex_forms_id);
        $form_attr = $wpdb->get_row($get_form);

        $get_entry = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'wap_nex_forms_entries WHERE Id = %d',$entry_id);
        $entry_attr = $wpdb->get_row($get_entry);


        $nf_functions = new NEXForms_Functions();
        $nf7_functions = new NEXForms_Functions();
        $database_actions = new NEXForms_Database_Actions();
        $attach_to_email = json_decode($form_attr->attachment_settings, true);
        $attach_to_admin_email 	= ($attach_to_email['0']['attach_to_admin_email']) ? $attach_to_email['0']['attach_to_admin_email'] 	: 'true';
        $option_settings = json_decode($form_attr->option_settings,true);
        $send_admin_email 	= ($option_settings['0']['send_admin_email']) ? $option_settings['0']['send_admin_email'] 	: 'true';


        $user_fields = '<table width="100%" cellpadding="10" cellspacing="0" style="border-top:1px solid #ddd; border-right:1px solid #ddd; border-left:1px solid #ddd;">';
        $user_fields .= '<tr><td  valign="top" style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #ddd; background-color:#f9f9f9;" colspan="2"><strong>'.str_replace('\\','',$form_attr->title).'</strong></td><tr>';
        $i				= 1;

        foreach($_POST as $key => $val) {
            if (
                $key!='paypal_invoice' &&
                $key!='paypal_return_url' &&
                $key!='math_result' &&
                $key!='set_file_ext' &&
                $key!='format_date' &&
                $key!='action' &&
                $key!='set_radio_items' &&
                $key!='change_button_layout' &&
                $key!='set_check_items' &&
                $key!='set_autocomplete_items' &&
                $key!='required' &&
                $key!='xform_submit' &&
                $key!='current_page' &&
                $key!='ajaxurl' &&
                $key!='page_id' &&
                $key!='page' &&
                $key!='ip' &&
                $key!='nf_page_id' &&
                $key!='nf_page_title' &&
                $key!='nex_forms_Id' &&
                $key!='company_url' &&
                $key!='submit' &&
                $key!='ms_current_step' &&
                !strstr($key,'real_val') &&
                !strstr($key,'gu__')
            ) {
                $img_ext_array = array('jpg','jpeg','png','tiff','gif','psd');
                $admin_val = '';

                if($val != 'NaN') {
                    if(is_array($val)) {
                        $i = 1;

                        $admin_val 	.= '<table width="100%" cellpadding="10" cellspacing="0" style="margin-left:-10px;margin-top:-10px;margin-bottom:-10px;margin-right:-20px;">';

                        foreach($val as $thekey => $value) {
                            if(is_array($value)) {
                                if($i==1) {
                                    $admin_val .= '<tr>';

                                    foreach($value as $innerkey=>$innervalue) {
                                        if(!strstr($innerkey,'real_val__') && $nf_functions->unformat_name($innerkey)!='Undefined')
                                            $admin_val .= '<td style="background-color:#f5f5f5;border-bottom:1px solid #ddd;border-right:1px solid #ddd;">'.$nf_functions->unformat_name($innerkey).'</td>';
                                    }

                                    $admin_val .= '</tr>';
                                }

                                $admin_val .= '<tr>';

                                foreach($value as $innerkey=>$innervalue) {
                                    if(array_key_exists('real_val__'.$innerkey,$value)) {
                                        $innervalue = rtrim($value['real_val__'.$innerkey.''],', ');
                                    }

                                    if(!strstr($innerkey,'real_val__') && $nf_functions->unformat_name($innerkey)!='Undefined') {
                                        if(in_array($nf_functions->get_ext($innervalue),$img_ext_array))
                                            $admin_val .= '<td style="border-right:1px solid #ddd;"><img src="'.rtrim($innervalue,', ').'" style="max-width:150px;" /></td>';
                                        else
                                            $admin_val .= '<td style="border-right:1px solid #ddd;">'.rtrim($innervalue,', ').'</td>';
                                    }
                                }

                                $admin_val .= '</tr>';
                            } else {
                                $admin_val .='<tr><td>'.rtrim($value,', ').'</td></tr>';
                            }

                            $i++;
                        }
                        $admin_val .= '</table>';

                        if (array_key_exists('real_val__'.$key,$_POST)) {
                            $admin_val = rtrim($_POST['real_val__'.$key.''][0],', ');
                            $val = rtrim($_POST['real_val__'.$key.''][0],', ');
                        }

                        if($admin_val != '0') {
                            $user_fields .= '<tr>
												<td width="30%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f5f5f5;"><strong>'.$nf_functions->unformat_name($key).'</strong></td>
												<td width="70%" style="border-bottom:1px solid #ddd;" valign="top">'.nl2br(str_replace('\\','',$admin_val)).'</td>
											<tr>
											';
                        }
                    } else {
                        $admin_val = $val;

                        if($admin_val!='') {
                            if(array_key_exists('real_val__'.$key,$_POST)) {
                                $admin_val = $_POST['real_val__'.$key];
                                $val = $_POST['real_val__'.$key];
                            }
                            if(strstr($admin_val,'data:image'))
                                $admin_val = '<img src="'.$admin_val.'" />';

                            if($admin_val != '0') {
                                $user_fields .= '<tr>
														<td width="30%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f5f5f5;"><strong>'.$nf_functions->unformat_name($key).'</strong></td>
														<td width="70%" style="border-bottom:1px solid #ddd;" valign="top">'.nl2br(str_replace('\\','',$admin_val)).'</td>
													<tr>';
                            }
                        }
                    }
                }

                $i++;
            }
        }

        $user_fields .= '</table>';

        $paypal_data 	= '<br /><table width="100%" cellpadding="10" cellspacing="0" style="border-top:1px solid #ddd; border-right:1px solid #ddd; border-left:1px solid #ddd;">';
        $paypal_data .= '<tr>
							<td  valign="top" style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #ddd; background-color:#f9f9f9;" colspan="2"><strong>'.__('PayPal','nex-forms').'</strong></td>
						<tr>
						';
        $paypal_data .= '<tr>
							<td width="35%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f9f9f9;"><strong>'.__('Status:','nex-forms').'</strong></td>
							<td width="65%" style="border-bottom:1px solid #ddd;" valign="top"><span class="payment_status">'.$entry_attr->payment_status.'</span></td>
						<tr>
						';
        $paypal_data .= '<tr>
							<td width="35%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f9f9f9;"><strong>'.__('Ammount:','nex-forms').'</strong></td>
							<td width="65%" style="border-bottom:1px solid #ddd;" valign="top">'.$entry_attr->payment_ammount.'</td>
						<tr>
						';
        $paypal_data .= '<tr>
							<td width="35%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f9f9f9;"><strong>'.__('Currency:','nex-forms').'</strong></td>
							<td width="65%" style="border-bottom:1px solid #ddd;" valign="top">'.$entry_attr->payment_currency.'</td>
						<tr>
						';
        $paypal_data .= '<tr>
							<td width="35%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f9f9f9;"><strong>'.__('Payment ID:','nex-forms').'</strong></td>
							<td width="65%" style="border-bottom:1px solid #ddd;" valign="top">'.$entry_attr->paypal_payment_id.'</td>
						<tr>
						';
        $paypal_data .= '<tr>
							<td width="35%" valign="top" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd; background-color:#f9f9f9;"><strong>'.__('Payment Token:','nex-forms').'</strong></td>
							<td width="65%" style="border-bottom:1px solid #ddd;" valign="top">'.$entry_attr->paypal_payment_token.'</td>
						<tr>
						';

        $paypal_data .= '</table>';

        return array(
            'user_data' => $user_fields,
            'paypal_data' => $paypal_data,
        );
    }

    public static function get_optin_redirect_link($form_id, $entry_id, $email) {
        $forms_to_map = WP2LITMNF_Map::get_form_to_map_connection();
        $forms_to_map = apply_filters('wp2leads_itm_nexforms_forms_to_map', $forms_to_map, $form_id);

        if (empty($forms_to_map[$form_id])) {
            return false;
        }

        $map = $forms_to_map[$form_id];
        $mapping = unserialize($map["mapping"]);
        $info = unserialize($map["info"]);
        $api = unserialize($map['api']);

        if (empty($info['initial_settings']) || empty($api["default_optin"])) {
            return false;
        }

        if (empty($mapping["comparisons"]) || !is_array($mapping["comparisons"])) {
            $mapping["comparisons"] = array();
        }

        $condition = array(
            'tableColumn' => WP2LITMNF_Instant_Transfer::get_required_column(),
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $entry_id
                )
            )
        );

        $mapping["comparisons"][] = $condition;

        $results = MapsModel::get_map_query_results($mapping);
        if (empty($results)) {
            return false;
        }

        $result = json_decode(json_encode($results[0]), true);

        $optin = $api['default_optin'];

        if (!empty($api['conditions']) && !empty($api['conditions']['donot_optins'])) {
            $do_not_optin = false;

            if ($do_not_optin) {
                return false;
            }
        }

        if (!empty($api['conditions']) && !empty($api['conditions']['optins'])) {
            foreach ($api['conditions']['optins'] as $condition) {
                if (!isset($condition['option']) || !isset($condition['string']) || !isset($condition['operator']) || !isset($condition['connectTo'])) {
                    continue;
                }

                $option = $condition["option"];

                foreach ($result as $key_option => $value) {
                    $key_option_filtered = ApiHelper::filterBeforeOutput(ApiHelper::filterForbidenKTSymbols(ApiHelper::remove_wp_emoji(ApiHelper::maybe_json_decode($key_option))));
                    $key_option_filtered = utf8_encode($key_option_filtered);

                    $is_selected = $option === $key_option_filtered;

                    if ($is_selected) {
                        $option = $key_option;
                    }
                }

                if (!isset($result[$option])) {
                    continue;
                }

                $string = trim($condition["string"]);
                $operator = $condition["operator"];
                $connectTo = $condition["connectTo"];
                $value = trim($result[$option]);

                $condition_result = false;

                switch ($operator) {
                    case 'is like':
                    case 'like':
                        $condition_result = (string)$value === (string)$string;
                        break;
                    case 'not-like':
                        $condition_result = (string)$value !== (string)$string;
                        break;
                    case 'contains':
                        $condition_result = strpos($value, $string) !== false;
                        break;
                    case 'not contains':
                        $condition_result = strpos($value, $string) === false;
                        break;
                    case 'bigger as':
                        $condition_result = (float) $value > (float)$string;
                        break;
                    case 'smaller as':
                        $condition_result = (float) $value < (float)$string;
                        break;
                }

                if ($condition_result) {
                    $optin = $connectTo;
                }
            }
        }

        $connector = new Wp2leads_KlicktippConnector();
        $logged_in = $connector->login(get_option('wp2l_klicktipp_username'), get_option('wp2l_klicktipp_password'));
        if(!$logged_in) return false;

        $link = $connector->subscription_process_redirect($optin, $email);

        // for new user we should add it at first
        if (!$link) {
            $user = $connector->subscribe($email, $optin);
            // try once more
            $link = $connector->subscription_process_redirect($optin, $email);
        }

        return $link;
    }
}