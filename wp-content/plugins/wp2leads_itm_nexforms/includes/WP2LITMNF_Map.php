<?php

class WP2LITMNF_Map {
    private static $map_id = 'e910966101787d8c57e7b74c7e8e39da';

    public static function get_master_map() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2l_maps';
        $map_id = WP2LITMNF_MAP_ID;

        $sql = "SELECT * FROM {$table_name} WHERE id = '{$map_id}'";
        $map = $wpdb->get_row($sql, ARRAY_A);

        return $map;
    }
    public static function ajax_generate_map() {
        if (empty(sanitize_text_field($_POST["formId"]))) WP2LITMNF_Ajax::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_nexforms')));
        if (empty(sanitize_text_field($_POST["mapName"]))) WP2LITMNF_Ajax::error_response(array('message' => __('Map name is required.', 'wp2leads_itm_nexforms')));

        $form_id = sanitize_text_field($_POST["formId"]);
        $map_name = sanitize_text_field($_POST["mapName"]);

        $map_id = self::generate_map($form_id, $map_name);
        WP2LITMNF_Settings::set_form_to_map($form_id, $map_id);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_nexforms') . ': ' . __('Map generated and connected to form.', 'wp2leads_itm_nexforms'),
            'reload' => 1,
        );

        WP2LITMNF_Ajax::success_response($params);
    }

    public static function ajax_delete_map() {
        if (empty(sanitize_text_field($_POST["formId"])) || empty(sanitize_text_field($_POST["mapId"]))) WP2LITMNF_Ajax::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_nexforms')));

        $form_id = sanitize_text_field($_POST["formId"]);
        $map_id = sanitize_text_field($_POST["mapId"]);

        WP2LITMNF_Settings::delete_form_to_map($form_id);
        WP2LITMNF_Map::delete_map($map_id);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_nexforms') . ': ' . __('Connection removed and map deleted.', 'wp2leads_itm_nexforms'),
            'reload' => 1,
        );

        WP2LITMNF_Ajax::success_response($params);
    }

    public static function generate_map($form_id, $map_name) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2l_maps';



        $master_map = self::download_map();
        $master_map_local = self::get_master_map();

        $name = $map_name;
        $mapping = unserialize($master_map["mapping"]);

        $api = unserialize($master_map["api"]);
        $api['tags_prefix'] = '';

        $info = unserialize($master_map["info"]);
        $info['initial_settings'] = false;
        unset($info["publicMapHash"]);
        unset($info["publicMapContent"]);
        unset($info["publicMapOwner"]);
        unset($info["publicMapStatus"]);
        unset($info["publicMapVersion"]);

        $form_id_comparison = array(
            'tableColumn' => 'wap_nex_forms_entries.nex_forms_Id',
            'conditions' => array(
                array(
                    'operator' => 'like',
                    'string' => $form_id,
                )
            )
        );

        $map_comparisons = array();

        if (!empty($mapping["comparisons"])) {
            foreach ($mapping["comparisons"] as $comparison) {
                if ($comparison['tableColumn'] !== 'wap_nex_forms_entries.nex_forms_Id') {
                    $map_comparisons[] = $comparison;
                }
            }
        }

        $map_comparisons[] = $form_id_comparison;
        $mapping["comparisons"] = $map_comparisons;

        $result = $wpdb->insert(
            $table_name,
            [
                'time' => current_time( 'mysql'),
                'name' => sanitize_text_field($name),
                'mapping' => serialize($mapping),
                'api' => serialize($api),
                'info'  => serialize($info),
            ],
            ['%s', '%s', '%s', '%s', '%s']
        );

        $map_id = $wpdb->insert_id;

        return $map_id;
    }

    public static function delete_map($map_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2l_maps';

        $sql = "DELETE FROM {$table_name} WHERE id = '{$map_id}'";
        return $wpdb->query($sql);
    }

    public static function get_form_to_map_connection() {
        $forms_to_map = WP2LITMNF_Settings::get_forms_to_map();

        if (empty($forms_to_map)) {
            return array();
        }

        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2l_maps';
        $forms_to_map_array = array();

        foreach ($forms_to_map as $form_id => $map_id) {
            $map_sql = "SELECT * FROM {$table_name} WHERE id = '{$map_id}'";
            $map_result = $wpdb->get_row($map_sql, ARRAY_A);

            if (empty($map_result)) {
                unset($forms_to_map[$form_id]);
            } else {
                $forms_to_map_array[$form_id] = $map_result;
            }
        }

        if (empty($forms_to_map)) {
            delete_option('wp2leads_itm_nexforms_forms_to_map');
        } else {
            update_option('wp2leads_itm_nexforms_forms_to_map', $forms_to_map);
        }

        return $forms_to_map_array;
    }

    public static function download_map() {
        $license_info = Wp2leads_License::get_lecense_info();
        $license_email = $license_info['email'];
        $license_key = $license_info['key'];
        $site_url = Wp2leads_License::get_current_site();

        $parameters = array (
            'license_email' => $license_email,
            'license_key' => $license_key,
            'site_url' => $site_url,
            'event' => 'import',
            'maps'  =>  serialize(array(self::$map_id))
        );

        $request = wp_remote_post(
            base64_decode(MapBuilderManager::get_server()),
            array(
                'body'    => $parameters,
            )
        );

        $response = json_decode(wp_remote_retrieve_body( $request ), true);

        if (200 !== $response['code']) {
            return false;
        }

        if(empty($response['body']['maps'][0])) {
            return false;
        }

        return $response['body']['maps'][0];
    }
}

add_action('wp_ajax_wp2litmnf_generate_map', array( 'WP2LITMNF_Map', 'ajax_generate_map'));
add_action('wp_ajax_nopriv_wp2litmnf_generate_map', array( 'WP2LITMNF_Map', 'ajax_generate_map'));

add_action('wp_ajax_wp2litmnf_delete_map', array( 'WP2LITMNF_Map', 'ajax_delete_map'));
add_action('wp_ajax_nopriv_wp2litmnf_delete_map', array( 'WP2LITMNF_Map', 'ajax_delete_map'));