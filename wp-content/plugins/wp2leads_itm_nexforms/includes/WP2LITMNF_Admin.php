<?php

class WP2LITMNF_Admin {
    public static function add_submenu_page() {
        add_submenu_page(
            'wp2l-admin',
            __("NEX-Forms to KlickTipp", 'wp2leads_itm_nexforms'),
            __("NEX-Forms to KlickTipp", 'wp2leads_itm_nexforms'),
            'manage_options',
            'wp2leads_itm_nexforms',
            'WP2LITMNF_Admin::display_submenu_page'
        );
    }

    public static function display_submenu_page() {
        include_once plugin_dir_path( WP2LITM_NEXFORMS_PLUGIN_FILE ) . 'templates/admin-page.php';
    }

    public static function enqueue_scripts() {
        if (!empty($_GET['page']) && $_GET['page'] === 'wp2leads_itm_nexforms') {
            wp_enqueue_script( 'wp2leads-itm-tippy', plugin_dir_url( WP2LITM_NEXFORMS_PLUGIN_FILE ) . 'assets/js/tippy.js', array(), '6.2.7', false );
            wp_enqueue_style( 'wp2leads-itm-nexforms', plugin_dir_url( WP2LITM_NEXFORMS_PLUGIN_FILE ) . 'assets/css/admin.css', array(), WP2LITM_NEXFORMS_VERSION . time(), 'all' );
            wp_enqueue_script( 'wp2leads-itm-nexforms', plugin_dir_url( WP2LITM_NEXFORMS_PLUGIN_FILE ) . 'assets/js/admin.js', array( 'jquery' ), WP2LITM_NEXFORMS_VERSION . time(), true );
        }
    }
}

add_action('admin_menu', array ('WP2LITMNF_Admin', 'add_submenu_page'));
add_action( 'admin_enqueue_scripts', array( 'WP2LITMNF_Admin', 'enqueue_scripts' ) );