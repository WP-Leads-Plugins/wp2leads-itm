<?php


class WP2LITMNF_Update {
    public static function update_1_1_4() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'wp2leads_nexforms_fields';
        $sql = "SELECT * FROM {$table_name} WHERE field_name_single = 'user_pdf_link'";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!empty($result)) {
            foreach ($result as $pdf_link) {
                $site_url = get_site_url('/');

                if (empty($pdf_link['field_value_single']) || false === strpos($pdf_link['field_value_single'], $site_url)) {
                    WP2LITMNF_Model::delete_entry_field_by_pdf($pdf_link);
                    continue;
                }

                if (false === strpos($pdf_link['field_value_single'], '/nf_entry_pdf/')) {
                    $url = rtrim($pdf_link['field_value_single'], '.pdf');
                    $url_array = explode('/', $url);

                    $last = count($url_array) - 1;
                    $title = $url_array[$last];
                    $new_url = $site_url . '/nf_entry_pdf/' . $title;

                    $data = array(
                        'ID' => $pdf_link['ID'],
                        'field_name_single' => 'user_pdf_link',
                        'field_value_single' => $new_url,
                        'entry_id' => $pdf_link['entry_id'],
                    );

                    WP2LITMNF_Model::create($data);

                    $sql_title = "SELECT * FROM {$table_name} WHERE field_name_single = 'user_pdf_title' AND entry_id = '{$pdf_link['entry_id']}'";
                    $result_title = $wpdb->get_results($sql_title, ARRAY_A);

                    if (empty($result_title)) {
                        $title_array = explode('_', $title);
                        unset($title_array[0]);
                        $pdf_title = implode('_',$title_array) . '.pdf';

                        $data_title = array(
                            'field_name_single' => 'user_pdf_title',
                            'field_value_single' => $pdf_title,
                            'entry_id' => $pdf_link['entry_id'],
                        );

                        WP2LITMNF_Model::create($data_title);
                    }
                }
            }
        }


        update_option('wp2leads_itm_nexforms_update_1_1_4', 1);
    }
}