<?php


class WP2LITMNF_Settings {
    public static function get_forms_to_map() {
        $forms_to_map = get_option('wp2leads_itm_nexforms_forms_to_map', array());
        return $forms_to_map;
    }

    public static function get_form_to_map($form_id) {
        $forms_to_map = self::get_forms_to_map();

        if (empty($forms_to_map[$form_id])) {
            return false;
        }

        return $forms_to_map[$form_id];
    }

    public static function set_form_to_map($form_id, $map_id) {
        $forms_to_map = self::get_forms_to_map();

        if (!empty($forms_to_map[$form_id])) {
            unset($forms_to_map[$form_id]);
        }

        $forms_to_map[$form_id] = $map_id;

        update_option('wp2leads_itm_nexforms_forms_to_map', $forms_to_map);
    }

    public static function delete_form_to_map($form_id) {
        $forms_to_map = self::get_forms_to_map();

        if (!empty($forms_to_map[$form_id])) unset($forms_to_map[$form_id]);
        if (empty($forms_to_map)) delete_option('wp2leads_itm_nexforms_forms_to_map');
        else update_option('wp2leads_itm_nexforms_forms_to_map', $forms_to_map);
    }
}