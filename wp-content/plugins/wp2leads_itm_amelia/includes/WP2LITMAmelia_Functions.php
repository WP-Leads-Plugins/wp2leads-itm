<?php


class WP2LITMAmelia_Functions {
    public static function init() {
        $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

        if (!$is_ajax) {
            self::check_version();
            self::check_updates();
            self::show_notices();
        }
    }

    public static function check_version() {
        $version = get_option( 'wp2leads_itm_amelia_version' );
        $dbversion = get_option( 'wp2leads_itm_amelia_db_version' );

        if (
            empty($version) || version_compare( $version, WP2LITM_AMELIA_VERSION, '<' ) ||
            empty($dbversion) || version_compare( $dbversion, WP2LITM_AMELIA_DB_VERSION, '<' )
        ) {
            self::install();
            do_action( 'wp2leads_itm_amelia_updated' );
        }

        update_option('wp2leads_itm_amelia_version', WP2LITM_AMELIA_VERSION);
        update_option('wp2leads_itm_amelia_db_version', WP2LITM_AMELIA_DB_VERSION);
    }

    public static function install() {
        WP2LITMAmelia_Model::create_booking_table();
    }

    public static function check_updates() {
        $update_1_1_9 = get_option('wp2leads_itm_amelia__update_1_1_9');

        if (empty($update_1_1_9)) {
            require_once 'WP2LITMAmelia_Update.php';

            WP2LITMAmelia_Update::update_1_1_9();
        }

        $update_1_1_10 = get_option('wp2leads_itm_amelia__update_1_1_10');

        if (empty($update_1_1_10)) {
            require_once 'WP2LITMAmelia_Update.php';

            WP2LITMAmelia_Update::update_1_1_10();
        }
    }

    public static function show_notices() {
        if (!get_option( 'wp2leads_itm_amelia_notice_update_1_1_10' )) {
            require_once 'WP2LITMAmelia_Notices.php';

            WP2LITMAmelia_Notices::show_message('update_1_1_10');
        }
    }

    public static function get_plugin_name() {
        $data = get_plugin_data( WP2LITM_AMELIA_PLUGIN_FILE );

        return $data['Name'];
    }

    public static function enqueue_scripts() {
        wp_enqueue_script( 'wp2leads_itm_amelia', plugin_dir_url( WP2LITM_AMELIA_PLUGIN_FILE ) . 'assets/js/admin.js', array( 'jquery' ), WP2LITM_AMELIA_VERSION . time(), true );
    }

    public static function ajax_notice_dismiss() {
        if (empty($_POST["id"])) self::success_response(array('done' => 1));

        update_option('wp2leads_itm_amelia_notice_' . sanitize_text_field($_POST["id"]), 1);

        self::success_response(array('done' => 1));
    }

    public static function error_response($params = array()) {
        $response = array('success' => 0, 'error' => 1);
        if (!empty($params)) $response = array_merge($response, $params);
        echo json_encode($response);
        wp_die();
    }

    public static function success_response($params = array()) {
        $response = array('success' => 1, 'error' => 0);
        if (!empty($params) && is_array($params)) $response = array_merge($response, $params);
        echo json_encode($response);
        wp_die();
    }
}
add_action( 'admin_enqueue_scripts', array( 'WP2LITMAmelia_Functions', 'enqueue_scripts' ) );
add_action('wp_ajax_wp2leads_itm_amelia_notice_dismiss', array( 'WP2LITMAmelia_Functions', 'ajax_notice_dismiss'), 5);
add_action('wp_ajax_nopriv_wp2leads_itm_amelia_notice_dismiss', array( 'WP2LITMAmelia_Functions', 'ajax_notice_dismiss'), 5);
