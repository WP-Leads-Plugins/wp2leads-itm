<?php


class WP2LITMAmelia_Update {
    public static function update_1_1_10() {
        $metas = WP2LITMAmelia_Model::get_all();

        if (!empty($metas)) {
            foreach ($metas as $meta) {
                $data_update = array(
                    'ID' => $meta['ID'],
                );

                if (!empty($meta['field_name'])) {
                    $slug = str_replace('-', '_', sanitize_title($meta['field_name']));
                    $data_update['field_slug'] = $slug;
                } elseif (!empty($meta['field_name_single'])) {
                    $slug = str_replace('-', '_', sanitize_title($meta['field_name_single']));
                    $data_update['field_slug_single'] = $slug;
                }

                WP2LITMAmelia_Model::create_booking($data_update);
            }
        }

        update_option('wp2leads_itm_amelia__update_1_1_10', 1);
    }

    public static function update_1_1_9() {
        global $wpdb;
        $booking_table = $wpdb->prefix . 'amelia_customer_bookings';
        $sql = "SELECT * FROM {$booking_table} WHERE customFields LIKE ('%{\"label\":%')";
        $result = $wpdb->get_results($sql, ARRAY_A);
        $data_to_add = array();

        if (!empty($result)) {
            foreach ($result as $booking) {
                $custom_fields = json_decode($booking['customFields'], true);

                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $custom_field) {
                        if ('' !== trim($custom_field['value'])) {
                            $data_to_add[] = array(
                                'booking_id' => $booking["id"],
                                'field_name' => $custom_field['label'],
                                'field_value' => $custom_field['value'],
                                'field_name_value' => $custom_field['label'] . " : " . $custom_field['value'],
                            );
                        }
                    }
                }
            }
        }

        $sql = "SELECT * FROM {$booking_table} WHERE info LIKE ('%\"firstName\":%')";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!empty($result)) {
            foreach ($result as $booking) {
                $info = json_decode($booking['info'], true);
                if (!empty($info)) {
                    foreach ($info as $fn => $fv) {
                        if ('' !== trim($fv)) {
                            $data_to_add[] = array(
                                'booking_id' => $booking["id"],
                                'field_name_single' => $fn,
                                'field_value_single' => $fv,
                            );
                        }
                    }
                }
            }
        }

        if (!empty($data_to_add)) {
            WP2LITMAmelia_Model::clear_booking_table();
            foreach ($data_to_add as $add_item) WP2LITMAmelia_Model::create_booking($add_item);
        }

         update_option('wp2leads_itm_amelia__update_1_1_9', 1);
    }
}