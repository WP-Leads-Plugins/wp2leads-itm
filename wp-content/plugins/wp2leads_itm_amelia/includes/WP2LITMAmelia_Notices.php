<?php


class WP2LITMAmelia_Notices {
    public static function show_message($notice_id) {
        add_action('admin_notices', 'WP2LITMAmelia_Notices::' . $notice_id);
    }

    public static function update_1_1_10() {
        ?>
        <div class="notice notice-info is-dismissible" data-id="update_1_1_10">
            <p>
                <strong><?php echo WP2LITMAmelia_Functions::get_plugin_name(); ?>:</strong>
                <?php echo __('From version 1.1.11 we added "field_slug" and "field_slug_single", so from now you can use them instead of "field_name" and "field_name_single" when virtual column selected in API fields. It will make your settings more clear, and in some cases it can allow to avoid issues with special characters.', 'wp2leads_itm_amelia'); ?>
            </p>

            <p>
                <?php echo __('This feature <strong>absolutely optional</strong> and there is no need to update it on your current Map to API settings.', 'wp2leads_itm_amelia'); ?>
                <?php echo __('But it already included into Catalog item map from map server.', 'wp2leads_itm_amelia'); ?>
            </p>

            <p>
                <?php echo __('Example'); ?>:
                <strong>"v.wp2leads_amelia_customer_bookings_meta-Some text / used in custom field Label ?"</strong> =>
                <strong>"wp2leads_amelia_customer_bookings_meta-some_text_used_in_custom_field_Label"</strong>
            </p>
        </div>
        <?php
    }
}