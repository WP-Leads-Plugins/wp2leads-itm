<?php


class WP2LITMAmelia_Model {
    private static $table_name_bookings = 'wp2leads_amelia_customer_bookings_meta';

    private static function get_booking_schema() {
        global $wpdb;

        $table_name = $wpdb->prefix . self::$table_name_bookings;
        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';
        $schema = "CREATE TABLE {$table_name} (
ID BIGINT UNSIGNED NOT NULL auto_increment,
booking_id BIGINT UNSIGNED NOT NULL,
field_slug MEDIUMTEXT,
field_name MEDIUMTEXT,
field_value MEDIUMTEXT,
field_name_value MEDIUMTEXT,
field_slug_single MEDIUMTEXT,
field_name_single MEDIUMTEXT,
field_value_single MEDIUMTEXT,
PRIMARY KEY  (ID)
) $collate;";

        return $schema;
    }

    public static function get_all() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name_bookings;
        $sql = "SELECT * FROM {$table_name}";

        return $wpdb->get_results($sql, ARRAY_A);
    }

    public static function clear_booking_table() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name_bookings;
        $sql = "DELETE FROM {$table_name}";
        $wpdb->query($sql);
    }

    public static function create_booking_table() {
        global $wpdb;
        $wpdb->hide_errors();
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( self::get_booking_schema() );
    }

    public static function create_booking($data) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$table_name_bookings;
        return self::create($table_name, $data);
    }

    public static function create($table_name, $data) {
        global $wpdb;

        if (!empty($data['ID'])) {
            $ID = $data['ID'];
            unset($data['ID']);
            $wpdb->update( $table_name, $data, array( 'ID' => $ID ));
        } else {
            $wpdb->insert( $table_name, $data );
            $ID = $wpdb->insert_id;
        }

        return $ID;
    }
}