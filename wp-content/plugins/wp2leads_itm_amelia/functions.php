<?php
/**
 * Helpers library
 */

function wp2leads_itm_amelia_load_plugin_textdomain() {
    add_filter( 'plugin_locale', 'wp2leads_itm_amelia_check_de_locale');

    load_plugin_textdomain(
        'wp2leads_itm_gf',
        false,
        dirname( plugin_basename( __FILE__ ) ) . '/languages/'
    );

    remove_filter( 'plugin_locale', 'wp2leads_itm_amelia_check_de_locale');
}

function wp2leads_itm_amelia_check_de_locale($domain) {
    $site_lang = get_user_locale();
    $de_lang_list = array(
        'de_CH_informal',
        'de_DE_formal',
        'de_AT',
        'de_CH',
        'de_DE'
    );

    if (in_array($site_lang, $de_lang_list)) return 'de_DE';
    return $domain;
}

function wp2leads_itm_amelia_requirement() {
    $wp2leads_installed = function_exists('run_wp2leads') && class_exists('Wp2leads_Background_Module_Transfer');
    if (!$wp2leads_installed) return false;
    $amelia_installed = wp2leads_itm_amelia_is_plugin_activated( 'ameliabooking', 'ameliabooking.php' );
    if (!$amelia_installed) return false;

    return true;
}

function wp2leads_itm_amelia_is_plugin_activated( $plugin_folder, $plugin_file ) {
    if ( wp2leads_itm_amelia_is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
    else return wp2leads_itm_amelia_is_plugin_active_by_file( $plugin_file );
}

function wp2leads_itm_amelia_is_plugin_active_simple( $plugin ) {
    return (
        in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
        ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
    );
}

function wp2leads_itm_amelia_is_plugin_active_by_file( $plugin_file ) {
    foreach ( wp2leads_itm_amelia_get_active_plugins() as $active_plugin ) {
        $active_plugin = explode( '/', $active_plugin );
        if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
    }

    return false;
}

function wp2leads_itm_amelia_get_active_plugins() {
    $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
    if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

    return $active_plugins;
}

function wp2leads_itm_amelia_add_webhooks() {
    $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
    if ($is_ajax) return true;

    $amelia_settings = get_option('amelia_settings');
    if (empty($amelia_settings)) return true;

    $amelia_settings = json_decode($amelia_settings);
    if (empty($amelia_settings)) return true;

    if (!property_exists($amelia_settings, 'webHooks')) $amelia_settings->webHooks = array();

    $webHooks = $amelia_settings->webHooks;
    $Wp2leadsBookingStatusUpdated = false;
    $Wp2leadsBookingTimeUpdated = false;
    $Wp2leadsBookingAdded = false;
    $Wp2leadsBookingCanceled = false;
    $Wp2leadsEventStatusUpdated = false;
    $Wp2leadsEventTimeUpdated = false;
    $Wp2leadsEventAdded = false;
    $Wp2leadsEventCanceled = false;

    if (!empty($webHooks) && is_array($webHooks)) {
        foreach ($webHooks as $web_hook) {
            if (is_object($web_hook) && property_exists($web_hook, 'name')) {
                if ($web_hook->name === 'Wp2leadsBookingStatusUpdated') {
                    $Wp2leadsBookingStatusUpdated = true;
                }

                if ($web_hook->name === 'Wp2leadsBookingTimeUpdated') {
                    $Wp2leadsBookingTimeUpdated = true;
                }

                if ($web_hook->name === 'Wp2leadsBookingAdded') {
                    $Wp2leadsBookingAdded = true;
                }

                if ($web_hook->name === 'Wp2leadsBookingCanceled') {
                    $Wp2leadsBookingCanceled = true;
                }

                if ($web_hook->name === 'Wp2leadsEventStatusUpdated') {
                    $Wp2leadsEventStatusUpdated = true;
                }

                if ($web_hook->name === 'Wp2leadsEventTimeUpdated') {
                    $Wp2leadsEventTimeUpdated = true;
                }

                if ($web_hook->name === 'Wp2leadsEventAdded') {
                    $Wp2leadsEventAdded = true;
                }

                if ($web_hook->name === 'Wp2leadsEventCanceled') {
                    $Wp2leadsEventCanceled = true;
                }
            }
        }
    } else {
        $webHooks = array();
    }

    if (
        !$Wp2leadsBookingStatusUpdated ||
        !$Wp2leadsBookingTimeUpdated ||
        !$Wp2leadsBookingAdded ||
        !$Wp2leadsBookingCanceled ||

        !$Wp2leadsEventStatusUpdated ||
        !$Wp2leadsEventTimeUpdated ||
        !$Wp2leadsEventAdded ||
        !$Wp2leadsEventCanceled
    ) {
        if (!$Wp2leadsBookingStatusUpdated) {
            $Wp2leadsBookingStatusUpdated = wp2leads_itm_amelia_generate_webhook('Wp2leadsBookingStatusUpdated', 'bookingStatusUpdated');
            $webHooks[] = $Wp2leadsBookingStatusUpdated;
        }

        if (!$Wp2leadsBookingTimeUpdated) {
            $Wp2leadsBookingTimeUpdated = wp2leads_itm_amelia_generate_webhook('Wp2leadsBookingTimeUpdated', 'bookingTimeUpdated');
            $webHooks[] = $Wp2leadsBookingTimeUpdated;
        }

        if (!$Wp2leadsBookingAdded) {
            $Wp2leadsBookingAdded = wp2leads_itm_amelia_generate_webhook('Wp2leadsBookingAdded', 'bookingAdded');
            $webHooks[] = $Wp2leadsBookingAdded;
        }

        if (!$Wp2leadsBookingCanceled) {
            $Wp2leadsBookingCanceled = wp2leads_itm_amelia_generate_webhook('Wp2leadsBookingCanceled', 'bookingCanceled');
            $webHooks[] = $Wp2leadsBookingCanceled;
        }

        if (!$Wp2leadsEventStatusUpdated) {
            $Wp2leadsEventStatusUpdated = wp2leads_itm_amelia_generate_webhook('Wp2leadsEventStatusUpdated', 'bookingStatusUpdated', 'event');
            $webHooks[] = $Wp2leadsEventStatusUpdated;
        }

        if (!$Wp2leadsEventTimeUpdated) {
            $Wp2leadsEventTimeUpdated = wp2leads_itm_amelia_generate_webhook('Wp2leadsEventTimeUpdated', 'bookingTimeUpdated', 'event');
            $webHooks[] = $Wp2leadsEventTimeUpdated;
        }

        if (!$Wp2leadsEventAdded) {
            $Wp2leadsEventAdded = wp2leads_itm_amelia_generate_webhook('Wp2leadsEventAdded', 'bookingAdded', 'event');
            $webHooks[] = $Wp2leadsEventAdded;
        }

        if (!$Wp2leadsEventCanceled) {
            $Wp2leadsEventCanceled = wp2leads_itm_amelia_generate_webhook('Wp2leadsEventCanceled', 'bookingCanceled', 'event');
            $webHooks[] = $Wp2leadsEventCanceled;
        }

        $amelia_settings->webHooks = $webHooks;

        update_option('amelia_settings', json_encode($amelia_settings));
    }

    return $webHooks;
}

function wp2leads_itm_amelia_generate_webhook($name, $action, $type = 'appointment') {
    $webhook = new stdClass();
    $webhook->name = $name;
    $webhook->url = get_site_url() . '/wp-json/wp2leads/v2/module/amelia/' . $action . '/' . $type;
    $webhook->type = $type;
    $webhook->action = $action;

    return $webhook;
}


/**
 * Handle Webhook request
 *
 * @param $request
 *
 * @return string
 */
function wp2leads_itm_amelia_handle_webhook_bookingStatusUpdated($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingStatusUpdated');
}

function wp2leads_itm_amelia_handle_webhook_bookingTimeUpdated($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingTimeUpdated');
}

function wp2leads_itm_amelia_handle_webhook_bookingAdded($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingAdded');
}

function wp2leads_itm_amelia_handle_webhook_bookingCanceled($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingCanceled');
}

function wp2leads_itm_amelia_handle_webhook_eventStatusUpdated($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingStatusUpdated', 'event');
}

function wp2leads_itm_amelia_handle_webhook_eventTimeUpdated($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingTimeUpdated', 'event');
}

function wp2leads_itm_amelia_handle_webhook_eventAdded($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingAdded', 'event');
}

function wp2leads_itm_amelia_handle_webhook_eventCanceled($request) {
    return wp2leads_itm_amelia_handle_webhook($request, 'bookingCanceled', 'event');
}

function wp2leads_itm_amelia_handle_webhook($request, $action, $type = 'appointment') {
    $body = $request->get_body();
    if (empty($body)) return "Done";

    $body_decoded = json_decode($body);
    if (empty($body) || !is_object($body_decoded)) return "Done";

    ob_start();
    $booking_type = new stdClass();

    if (!property_exists($body_decoded, $type)) return "Done";

    if ($type === 'appointment') {
        $booking_type = $body_decoded->appointment;
    } elseif ($type === 'event') {
        $booking_type = $body_decoded->event;
    }

    if (!property_exists($booking_type, 'id')) return "Done";
    $id = $booking_type->id;

    $bookings = property_exists($booking_type, 'bookings') ? $booking_type->bookings : new stdClass();
    $bookings = json_encode($bookings);

    if ($type === 'event' && 'bookingAdded' === $action) {
        $bookings_array = json_decode($bookings, true);

        if (!empty($bookings_array)) {
            $last_count = count($bookings_array) - 1;

            $booking = $bookings_array[$last_count];
            // error_log(json_encode($booking));
            if (!empty($booking['info'])) {
                // error_log($booking['info']);
                $info_array = json_decode($booking['info'], true);

                if (!empty($info_array)) {
                    foreach ($info_array as $fn => $fv) {
                        if ('' !== trim($fv)) {
                            $data = array(
                                'booking_id' => $booking["id"],
                                'field_slug_single' => str_replace('-', '_', sanitize_title($fn)),
                                'field_name_single' => $fn,
                                'field_value_single' => $fv,
                            );

                            WP2LITMAmelia_Model::create_booking($data);
                        }
                    }
                }
            }

            if (!empty($booking['customFields'])) {
//                ob_start();
//                var_dump($booking['customFields']);
//                error_log(ob_get_clean());
                foreach ($booking['customFields'] as $custom_field) {
                    if ('' !== trim($custom_field['value'])) {
                        $data = array(
                            'booking_id' => $booking["id"],
                            'field_slug' => str_replace('-', '_', sanitize_title($custom_field['label'])),
                            'field_name' => $custom_field['label'],
                            'field_value' => $custom_field['value'],
                            'field_name_value' => $custom_field['label'] . " : " . $custom_field['value'],
                        );

                        WP2LITMAmelia_Model::create_booking($data);
                    }
                }
            }

            wp2leads_itm_amelia_events_generate_metadata( $booking["id"] );
        }
    }

    if ($type === 'appointment' && 'bookingAdded' === $action) {
        $bookings_array = json_decode($bookings, true);

        if (!empty($bookings_array)) {
            $last_count = count($bookings_array) - 1;

            $booking = $bookings_array[$last_count];
            // error_log(json_encode($booking));
            if (!empty($booking['info'])) {
                // error_log($booking['info']);
                $info_array = json_decode($booking['info'], true);

                if (!empty($info_array)) {
                    foreach ($info_array as $fn => $fv) {
                        if ('' !== trim($fv)) {
                            $data = array(
                                'booking_id' => $booking["id"],
                                'field_slug_single' => str_replace('-', '_', sanitize_title($fn)),
                                'field_name_single' => $fn,
                                'field_value_single' => $fv,
                            );

                            WP2LITMAmelia_Model::create_booking($data);
                        }
                    }
                }
            }

            if (!empty($booking['customFields'])) {
                foreach ($booking['customFields'] as $custom_field) {
                    if ('' !== trim($custom_field['value'])) {
                        $data = array(
                            'booking_id' => $booking["id"],
                            'field_name' => $custom_field['label'],
                            'field_value' => $custom_field['value'],
                            'field_name_value' => $custom_field['label'] . " : " . $custom_field['value'],
                        );

                        WP2LITMAmelia_Model::create_booking($data);
                    }
                }
            }
        }
    }

    do_action('wp2leads_itm_amelia_' . $action . '_' . $type, $id, $action, $type, $bookings);
    do_action('wp2leads_itm_amelia_' . $action, $id, $action, $type, $bookings);
    do_action('wp2leads_itm_amelia_' . $type, $id, $action, $type, $bookings);
    do_action('wp2leads_itm_amelia_action', $id, $action, $type, $bookings);

    $params_string = ob_get_clean();
    // error_log($params_string);

    return "Done";
}

function wp2leads_itm_amelia_events_generate_metadata( $booking_id ) {
    global $wpdb;
    $sql = "
SELECT acb.id, acb.customerId,
       acbep.customerBookingId,
       aep.periodStart AS event_periodStart, aep.periodEnd AS event_periodEnd, aep.eventId,
       ae.name AS event_name, ae.status, ae.description AS event_description
FROM {$wpdb->prefix}amelia_customer_bookings AS acb
LEFT JOIN {$wpdb->prefix}amelia_users AS au ON acb.customerId = au.id
LEFT JOIN {$wpdb->prefix}amelia_customer_bookings_to_events_periods AS acbep ON acb.id = acbep.customerBookingId
LEFT JOIN {$wpdb->prefix}amelia_events_periods AS aep ON acbep.eventPeriodId = aep.id
LEFT JOIN {$wpdb->prefix}amelia_events AS ae ON ae.id = aep.eventId
WHERE acb.id = {$booking_id};
";

    $result = $wpdb->get_row($sql, ARRAY_A);

    error_log(json_encode($result));

    if (!empty($result) && !empty($result['event_periodStart']) && (!empty($result['event_name']) || !empty($result['event_description']))) {
        extract($result);
        $description = !empty($result['event_description']) ? $result['event_description'] : $result['event_name'];

//        $data = array(
//            'booking_id' => $booking_id,
//            'field_slug' => 'event_description_periodStart',
//            'field_name' => 'Event Description - Period Start',
//            'field_value' => $description . ' ' . $result['event_periodStart'],
//            'field_name_value' => 'Event Description - Period Start' . " : " . $description . ' ' . $result['event_periodStart'],
//        );

        $data = array(
            'booking_id' => $booking_id,
            'field_slug_single' => 'event_description_periodStart',
            'field_name_single' => 'Event Description - Period Start',
            'field_value_single' => $description . ' ' . $result['event_periodStart'],
        );

        WP2LITMAmelia_Model::create_booking($data);
    }
}

function wp2leads_itm_amelia_handle_webhook_permission($request) {
    return true;
}

function wp2leads_itm_amelia_modules_init() {
    include_once 'transfer-modules/Wp2leads_Event_Booking_Status_Updated.php';
    include_once 'transfer-modules/Wp2leads_Booking_Status_Updated.php';
}

function wp2leads_itm_amelia_init() {
    $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
}

function wp2leads_itm_amelia_convert_date_time_to_timezone($dateString, $timezone) {
    try {
        $timezone = trim($timezone);
        $gmtDateTime = new DateTime($dateString, new DateTimeZone('GMT'));
        $gmtDateTime->setTimezone(new DateTimeZone($timezone));
        return $gmtDateTime->format('Y-m-d H:i:s');
    } catch (Exception $e) {
        error_log('Caught exception: ' . $e->getMessage());
        return $dateString;
    }
}

function wp2leads_itm_amelia_map_results_to_timezone($results, $map) {
    if (empty($map["from"]) || $map["from"] !== 'amelia_customer_bookings' || empty($map["keyBy"]) || $map["keyBy"] !== 'amelia_customer_bookings.id') {
        return $results;
    }

    foreach ($results as $index => $result) {
        if (
            (
                !empty($result->{'amelia_events_periods.periodStart'}) ||
                !empty($result->{'amelia_events_periods.periodEnd'})
            ) && !empty($result->{'v.wp2leads_amelia_customer_bookings_meta-timezone'})
        ) {
            if (!empty($result->{'amelia_events_periods.periodStart'})) {
                $oldDate = $result->{'amelia_events_periods.periodStart'};
                $newDate = wp2leads_itm_amelia_convert_date_time_to_timezone(
                    $oldDate,
                    $result->{'v.wp2leads_amelia_customer_bookings_meta-timezone'}
                );
                $result->{'amelia_events_periods.periodStart'} = $newDate;

                if (!empty($result->{"v.wp2leads_amelia_customer_bookings_meta-event_description_periodStart"})) {
                    $search = $oldDate;
                    $replace = $newDate;
                    $subject = $result->{"v.wp2leads_amelia_customer_bookings_meta-event_description_periodStart"};
                    $result->{"v.wp2leads_amelia_customer_bookings_meta-event_description_periodStart"} = str_replace($search, $replace, $subject);
                }
            }

            if (!empty($result->{'amelia_events_periods.periodEnd'})) {
                $oldDate = $result->{'amelia_events_periods.periodEnd'};
                $newDate = wp2leads_itm_amelia_convert_date_time_to_timezone(
                    $oldDate,
                    $result->{'v.wp2leads_amelia_customer_bookings_meta-timezone'}
                );
                $result->{'amelia_events_periods.periodEnd'} = $newDate;
            }
        }
    }
    return $results;
}

add_filter('wp2leads_map_query_results', 'wp2leads_itm_amelia_map_results_to_timezone', 15, 2);
