<?php
/**
 * Modules for transfering data
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class Wp2leads_Event_Booking_Status_Updated
 */
class Wp2leads_Event_Booking_Status_Updated {
    private static $key = 'wp2leads_event_booking_status_updated';
    private static $required_column = 'amelia_customer_bookings.id';

    public static function transfer_init() {
        add_action('wp2leads_itm_amelia_event', 'Wp2leads_Event_Booking_Status_Updated::status_changed', 15, 4);
    }

    public static function get_label() {
        return __('Amelia Event Booking status updated', 'wp2leads_itm_amelia');
    }

    public static function get_description() {
        return __('This module will transfer user data to KT once Amelia event booking created or status changed', 'wp2leads_itm_amelia');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Amelia Event Booking maps.', 'wp2leads_itm_amelia') ?></p>
        <p><?php _e('Once Amelia event booking created or status changed user data will be transfered to KT account.', 'wp2leads_itm_amelia') ?></p>
        <p><?php _e('Requirement: <strong>amelia_customer_bookings.id</strong> column within selected data.', 'wp2leads_itm_amelia') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function status_changed( $id, $action, $type, $bookings ) {
        $bookings_decoded = json_decode($bookings, true);

        if (!empty($bookings_decoded) && is_array($bookings_decoded)) {
            foreach ($bookings_decoded as $booking) {
                if (!empty($booking['id'])) {
                    if ('bookingStatusUpdated' === $action) {
                        self::transfer($booking['id']);
                    } else {
                        if (!empty($booking['isChangedStatus'])) {
                            self::transfer($booking['id']);
                        }
                    }
                }
            }
        }
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_event_booking_status_updated($transfer_modules) {
    $transfer_modules['wp2leads_event_booking_status_updated'] = 'Wp2leads_Event_Booking_Status_Updated';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_event_booking_status_updated');