<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for Amelia booking plugin
 * Description:
 * Version:         1.3.0
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_amelia
 *
 * Requires at least: 5.0
 * Tested up to: 5.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_AMELIA_VERSION', '1.3.0' );
define( 'WP2LITM_AMELIA_DB_VERSION', '1.1.11' );
define( 'WP2LITM_AMELIA_PLUGIN_FILE', __FILE__ );
define( 'WP2LITM_AMELIA_PLUGIN_REL_FILE', dirname( plugin_basename( __FILE__ ) ) );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_amelia.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_amelia'
);

require_once('functions.php');
require_once('includes/WP2LITMAmelia_Model.php');
require_once('includes/WP2LITMAmelia_Functions.php');

add_action( 'plugins_loaded', 'wp2leads_itm_amelia_load_plugin_textdomain' );

// If not met requirement do not run
if (!wp2leads_itm_amelia_requirement()) return;

wp2leads_itm_amelia_add_webhooks();

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingStatusUpdated/appointment/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_bookingStatusUpdated',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );

    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingTimeUpdated/appointment/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_bookingTimeUpdated',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );

    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingAdded/appointment/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_bookingAdded',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );

    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingCanceled/appointment/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_bookingCanceled',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );

    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingStatusUpdated/event/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_eventStatusUpdated',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );

    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingTimeUpdated/event/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_eventTimeUpdated',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );

    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingAdded/event/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_eventAdded',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );

    register_rest_route( 'wp2leads/v2', '/module/amelia/bookingCanceled/event/', array(
        'methods' => WP_REST_Server::ALLMETHODS,
        'callback' => 'wp2leads_itm_amelia_handle_webhook_eventCanceled',
        'permission_callback' => 'wp2leads_itm_amelia_handle_webhook_permission'
    ) );
} );
add_action( 'init', 'WP2LITMAmelia_Functions::init' );
wp2leads_itm_amelia_modules_init();
