(function( $ ) {
    $(document.body).on('click', 'button.notice-dismiss', function() {
        var btn = $(this);
        var id = btn.parents('.notice.is-dismissible').data('id');

        if (id) {
            console.log(id);

            var data = {
                action: 'wp2leads_itm_amelia_notice_dismiss',
                id: id
            };

            ajaxRequest(data, function() {}, function() {});
            return true;
        } else {
            console.log('No id');
            return true;
        }
    });

    function ajaxRequest(data, cb, cbError) {
        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function (response) {
                var decoded;

                console.log(response);

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (decoded.url) {
                            window.location.replace(decoded.url);
                        } else if (decoded.reload) {
                            window.location.reload();
                        }

                        if (typeof cb === 'function') {
                            cb();
                        }
                    } else {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (typeof cbError === 'function') {
                            cbError();
                        }
                    }
                } else {
                    alert('Something went wrong');
                }
            }
        });
    }
})( jQuery );