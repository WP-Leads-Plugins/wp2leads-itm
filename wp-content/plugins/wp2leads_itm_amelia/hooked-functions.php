<?php
//// Appointments
//add_action('wp2leads_itm_amelia_bookingStatusUpdated_appointment', 'wp2leads_itm_amelia_bookingStatusUpdated_appointment_hook', 15);
//add_action('wp2leads_itm_amelia_bookingTimeUpdated_appointment', 'wp2leads_itm_amelia_bookingTimeUpdated_appointment_hook', 15);
//add_action('wp2leads_itm_amelia_bookingAdded_appointment', 'wp2leads_itm_amelia_bookingAdded_appointment_hook', 15);
//add_action('wp2leads_itm_amelia_bookingCanceled_appointment', 'wp2leads_itm_amelia_bookingCanceled_appointment_hook', 15);

//// Events
//add_action('wp2leads_itm_amelia_bookingStatusUpdated_event', 'wp2leads_itm_amelia_bookingStatusUpdated_event_hook', 15);
//add_action('wp2leads_itm_amelia_bookingTimeUpdated_event', 'wp2leads_itm_amelia_bookingTimeUpdated_event_hook', 15);
//add_action('wp2leads_itm_amelia_bookingAdded_event', 'wp2leads_itm_amelia_bookingAdded_event_hook', 15);
//add_action('wp2leads_itm_amelia_bookingCanceled_event', 'wp2leads_itm_amelia_bookingCanceled_event_hook', 15);

//// Actions
//add_action('wp2leads_itm_amelia_bookingStatusUpdated', 'wp2leads_itm_amelia_bookingStatusUpdated_event_hook', 15);
//add_action('wp2leads_itm_amelia_bookingTimeUpdated', 'wp2leads_itm_amelia_bookingTimeUpdated_event_hook', 15);
//add_action('wp2leads_itm_amelia_bookingAdded', 'wp2leads_itm_amelia_bookingAdded_event_hook', 15);
//add_action('wp2leads_itm_amelia_bookingCanceled', 'wp2leads_itm_amelia_bookingCanceled_event_hook', 15);

// Types
 add_action('wp2leads_itm_amelia_appointment', 'wp2leads_itm_amelia_appointment_hook', 15, 4);
 add_action('wp2leads_itm_amelia_event', 'wp2leads_itm_amelia_event_hook', 15, 4);

// Action
// add_action('wp2leads_itm_amelia_action', 'wp2leads_itm_amelia_action_hook', 15, 4);

function wp2leads_itm_amelia_action_hook($id, $action, $type, $bookings) {
    error_log('Booking changed: Type ' . $type . ' - Action ' . $action);
    error_log($id);
    error_log($action);
    error_log($type);
    // error_log($bookings);

    $bookings_decoded = json_decode($bookings, true);

    if (!empty($bookings_decoded) && is_array($bookings_decoded)) {
        foreach ($bookings_decoded as $booking) {
            if (!empty($booking['id'])) {
                error_log($booking['id']);

                if (!empty($booking['isChangedStatus'])) {
                    error_log('Status changed');
                } else {
                    error_log('Status not changed');
                }
            }
        }
    }
}

function wp2leads_itm_amelia_appointment_hook($id, $action, $type, $bookings) {
    error_log('By Type: ' . $type . ' - ' . $action);
    error_log($id);
    error_log($action);
    error_log($type);
    error_log($bookings);

    $bookings_decoded = json_decode($bookings, true);

    if (!empty($bookings_decoded) && is_array($bookings_decoded)) {
        foreach ($bookings_decoded as $booking) {
            if (!empty($booking['id'])) {
                error_log($booking['id']);

                if (!empty($booking['isChangedStatus'])) {
                    error_log('Status changed');
                } else {
                    error_log('Status not changed');
                }
            }
        }
    }
}

function wp2leads_itm_amelia_event_hook($id, $action, $type, $bookings) {
    error_log('By Type: ' . $type . ' - ' . $action);
    error_log($id);
    error_log($action);
    error_log($type);
    // error_log($bookings);

    $bookings_decoded = json_decode($bookings, true);

    if (!empty($bookings_decoded) && is_array($bookings_decoded)) {
        foreach ($bookings_decoded as $booking) {
            if (!empty($booking['id'])) {
                error_log($booking['id']);

                if (!empty($booking['isChangedStatus'])) {
                    error_log('Status changed');
                } else {
                    error_log('Status not changed');
                }
            }
        }
    }
}