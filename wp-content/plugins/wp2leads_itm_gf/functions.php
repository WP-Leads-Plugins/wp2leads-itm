<?php
/**
 * Helpers library
 */

function wp2leads_itm_gf_load_plugin_textdomain() {
    add_filter( 'plugin_locale', 'wp2leads_itm_gf_check_de_locale');

    load_plugin_textdomain(
        'wp2leads_itm_gf',
        false,
        dirname( plugin_basename( __FILE__ ) ) . '/languages/'
    );

    remove_filter( 'plugin_locale', 'wp2leads_itm_gf_check_de_locale');
}

function wp2leads_itm_gf_check_de_locale($domain) {
    $site_lang = get_user_locale();
    $de_lang_list = array(
        'de_CH_informal',
        'de_DE_formal',
        'de_AT',
        'de_CH',
        'de_DE'
    );

    if (in_array($site_lang, $de_lang_list)) return 'de_DE';
    return $domain;
}

function wp2leads_itm_gf_requirement() {
    $wp2leads_installed = function_exists('run_wp2leads') && class_exists('Wp2leads_Background_Module_Transfer');
    if (!$wp2leads_installed) return false;
    $amelia_installed = wp2leads_itm_gf_is_plugin_activated( 'gravityforms', 'gravityforms.php' );
    if (!$amelia_installed) return false;

    return true;
}

function wp2leads_itm_gf_is_plugin_activated( $plugin_folder, $plugin_file ) {
    if ( wp2leads_itm_gf_is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
    else return wp2leads_itm_gf_is_plugin_active_by_file( $plugin_file );
}

function wp2leads_itm_gf_is_plugin_active_simple( $plugin ) {
    return (
        in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
        ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
    );
}

function wp2leads_itm_gf_is_plugin_active_by_file( $plugin_file ) {
    foreach ( wp2leads_itm_gf_get_active_plugins() as $active_plugin ) {
        $active_plugin = explode( '/', $active_plugin );
        if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
    }

    return false;
}

function wp2leads_itm_gf_get_active_plugins() {
    $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
    if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

    return $active_plugins;
}

function wp2leads_itm_gf_modules_init() {
    include_once 'transfer-modules/Wp2leads_GF_Entry_Updated.php';
}

function wp2leads_itm_gf_after_submission($entry, $form) {
    $entry_json = json_encode($entry, true);
    $form_json = json_encode($form, true);
    $entry_id = $entry['id'];

    $log = PHP_EOL . "===========================================================";
    $log .=  PHP_EOL . "Module after submission. Entry ID: " . $entry_id;
    $log .=  PHP_EOL . "===========================================================";
    $log .=  PHP_EOL . $entry_json;
    $log .=  PHP_EOL . $form_json;

    error_log($log);
}

// add_action('gform_after_submission', 'wp2leads_itm_gf_after_submission', 15, 2);

