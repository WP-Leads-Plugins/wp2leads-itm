<?php
/**
 * Modules for transfering data
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_GF_Entry_Updated {
    private static $key = 'wp2leads_gf_entry_updated';
    private static $required_column = 'gf_entry.id';

    public static function transfer_init() {
        add_action('gform_after_submission', 'Wp2leads_GF_Entry_Updated::after_submission', 15, 2);
    }

    public static function get_label() {
        return __('Gravity Forms Entry Updated', 'wp2leads_itm_gf');
    }

    public static function get_description() {
        return __('This module will transfer user data to KT once Gravity Forms Entry created or status changed', 'wp2leads_itm_gf');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Gravity Forms Entry maps.', 'wp2leads_itm_gf') ?></p>
        <p><?php _e('Once Gravity Forms Entry created or status changed user data will be transfered to KT account.', 'wp2leads_itm_gf') ?></p>
        <p><?php _e('Requirement: <strong>gf_entry.id</strong> column within selected data.', 'wp2leads_itm_gf') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function after_submission( $entry, $form ) {
        $entry_id = $entry['id'];

        if (!empty($entry_id)) {
            self::transfer($entry_id);
        }
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_gf_entry_updated($transfer_modules) {
    $transfer_modules['wp2leads_gf_entry_updated'] = 'Wp2leads_GF_Entry_Updated';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_gf_entry_updated');