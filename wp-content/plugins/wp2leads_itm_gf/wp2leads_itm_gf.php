<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for Gravity Form
 * Description:     Transfer your Gravity Forms entries users data to Klick-Tipp instantly
 * Version:         1.0.1
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_gf
 *
 * Requires at least: 5.0
 * Tested up to: 5.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_GF_VERSION', '1.0.1' );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_gf.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_gf'
);

require_once('functions.php');

add_action( 'plugins_loaded', 'wp2leads_itm_gf_load_plugin_textdomain' );

// If not met requirement do not run
if (!wp2leads_itm_gf_requirement()) return;

wp2leads_itm_gf_modules_init();
