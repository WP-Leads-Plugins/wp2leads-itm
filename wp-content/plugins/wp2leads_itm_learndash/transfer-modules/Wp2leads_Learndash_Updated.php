<?php
/**
 * Modules for transfering data
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_Learndash_Updated {
    private static $key = 'wp2leads_learndash_updated';
    private static $required_column = 'learndash_user_activity.activity_id';

    public static function transfer_init() {
        add_action('learndash_update_user_activity', 'Wp2leads_Learndash_Updated::update_user_activity', 15);
    }

    public static function get_label() {
        return __('Learndash item Updated', 'wp2leads_itm_learndash');
    }

    public static function get_description() {
        return __('This module will transfer user data to KT once Learndash created or status changed', 'wp2leads_itm_learndash');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Learndash courses, lessons and quizzes maps.', 'wp2leads_itm_learndash') ?></p>
        <p><?php _e('Once Learndash course started, lesson or topic completed or quizz finished user data will be transfered to KT account.', 'wp2leads_itm_learndash') ?></p>
        <p><?php _e('Requirement: <strong>learndash_user_activity.activity_id</strong> column within selected data.', 'wp2leads_itm_learndash') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function update_user_activity( $args ) {
        $id = $args['activity_id'];
        $course_id = $args['course_id'];
        $transfer = false;

        if ($args['activity_type'] === 'access' && $args["activity_action"] === "insert") {
            error_log("Course enrolled " . $id . " - " . $course_id);
            error_log(json_encode($args));

            self::transfer($id);
        }

        if ($args['activity_type'] === 'course') {
            if (empty($args['activity_completed'])) {
                error_log("Course in progress " . $id . " - " . $course_id);
            } else {
                error_log("Course Completed " . $id . " - " . $course_id);
                error_log(json_encode($args));

                self::transfer($id);
            }

            self::transfer($id);
        }

        if ($args['activity_type'] === 'lesson') {
            if (empty($args['activity_completed'])) {
                error_log("Lesson Started " . $id . " - " . $course_id);
            } else {
                error_log("Lesson Completed " . $id . " - " . $course_id);
            }

            error_log(json_encode($args));

            self::transfer($id);
        }

        if ($args['activity_type'] === 'topic') {
            if (empty($args['activity_completed'])) {
                error_log("Topic Started " . $id . " - " . $course_id);
            } else {
                error_log("Topic Completed " . $id . " - " . $course_id);
            }

            error_log(json_encode($args));

            self::transfer($id);
        }

        if ($args['activity_type'] === 'quiz') {
            if (!empty($args['activity_completed'])) {
                error_log("Quiz Completed " . $id . " - " . $course_id);
                error_log(json_encode($args));

                self::transfer($id);
            }
        }

//        if (!empty($transfer) && !empty($id)) {
//            self::transfer($id);
//        }
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_learndash_updated($transfer_modules) {
    $transfer_modules['wp2leads_learndash_updated'] = 'Wp2leads_Learndash_Updated';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_learndash_updated');