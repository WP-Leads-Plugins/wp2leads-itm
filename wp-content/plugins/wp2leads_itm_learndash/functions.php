<?php
/**
 * Helpers library
 */

function wp2leads_itm_learndash_load_plugin_textdomain() {
    add_filter( 'plugin_locale', 'wp2leads_itm_learndash_check_de_locale');

    load_plugin_textdomain(
        'wp2leads_itm_learndash',
        false,
        dirname( plugin_basename( __FILE__ ) ) . '/languages/'
    );

    remove_filter( 'plugin_locale', 'wp2leads_itm_learndash_check_de_locale');
}

function wp2leads_itm_learndash_check_de_locale($domain) {
    $site_lang = get_user_locale();
    $de_lang_list = array(
        'de_CH_informal',
        'de_DE_formal',
        'de_AT',
        'de_CH',
        'de_DE'
    );

    if (in_array($site_lang, $de_lang_list)) return 'de_DE';
    return $domain;
}

function wp2leads_itm_learndash_requirement() {
    $wp2leads_installed = function_exists('run_wp2leads') && class_exists('Wp2leads_Background_Module_Transfer');
    if (!$wp2leads_installed) return false;
    $amelia_installed = wp2leads_itm_learndash_is_plugin_activated( 'gravityforms', 'gravityforms.php' );
    if (!$amelia_installed) return false;

    return true;
}

function wp2leads_itm_learndash_is_plugin_activated( $plugin_folder, $plugin_file ) {
    if ( wp2leads_itm_learndash_is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
    else return wp2leads_itm_learndash_is_plugin_active_by_file( $plugin_file );
}

function wp2leads_itm_learndash_is_plugin_active_simple( $plugin ) {
    return (
        in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
        ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
    );
}

function wp2leads_itm_learndash_is_plugin_active_by_file( $plugin_file ) {
    foreach ( wp2leads_itm_learndash_get_active_plugins() as $active_plugin ) {
        $active_plugin = explode( '/', $active_plugin );
        if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
    }

    return false;
}

function wp2leads_itm_learndash_get_active_plugins() {
    $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
    if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

    return $active_plugins;
}

function wp2leads_itm_learndash_modules_init() {
    include_once 'transfer-modules/Wp2leads_Learndash_Updated.php';
}

function wp2leads_itm_learndash_update_course( $args ) {
    $id = $args['activity_id'];
    $course_id = $args['course_id'];

    if ($args['activity_type'] === 'access' && $args["activity_action"] === "insert") {
        error_log("Course enrolled " . $id . " - " . $course_id);
        error_log(json_encode($args));
        error_log("=================================================");
    }

    if ($args['activity_type'] === 'course') {
        if (empty($args['activity_completed'])) {
            error_log("Course in progress " . $id . " - " . $course_id);
        } else {
            error_log("Course Completed " . $id . " - " . $course_id);
        }

        error_log(json_encode($args));
        error_log("=================================================");
    }

    if ($args['activity_type'] === 'lesson') {
        if (empty($args['activity_completed'])) {
            error_log("Lesson Started " . $id . " - " . $course_id);
        } else {
            error_log("Lesson Completed " . $id . " - " . $course_id);
        }

        error_log(json_encode($args));
        error_log("=================================================");
    }

    if ($args['activity_type'] === 'topic') {
        if (empty($args['activity_completed'])) {
            error_log("Topic Started " . $id . " - " . $course_id);
        } else {
            error_log("Topic Completed " . $id . " - " . $course_id);
        }
        error_log(json_encode($args));
        error_log("=================================================");
    }

    if ($args['activity_type'] === 'quiz') {
        if (!empty($args['activity_completed'])) {
            error_log("Quiz Completed " . $id . " - " . $course_id);
            error_log(json_encode($args));
            error_log("=================================================");
        }
    }
}

// add_action( 'learndash_update_user_activity', 'wp2leads_itm_learndash_update_course', 15 );

function wp2leads_itm_learndash_update_lesson( $args ) {
    if ($args['activity_type'] === 'lesson' || $args['activity_type'] === 'topic') {
        error_log(json_encode($args));
    }
}

// add_action( 'learndash_update_user_activity', 'wp2leads_itm_learndash_update_lesson', 15 );

function wp2leads_itm_learndash_update_quiz( $args ) {
    if ($args['activity_type'] === 'quiz') {
        error_log(json_encode($args));
    }
}

// add_action( 'learndash_update_user_activity', 'wp2leads_itm_learndash_update_quiz', 15 );
