<?php
/**
 * @var $admin_page
 * @var $page_title
 * @var $subpages
 */

if ( ! defined( 'ABSPATH' ) ) exit;
$subpages_array = array_keys($subpages);
$active_tab = !empty($_GET['tab']) && in_array($_GET['tab'], $subpages_array) ? $_GET['tab'] : $subpages_array[0];
?>

<div class="wrap">
    <h1><?php echo $page_title; ?></h1>
    <?php settings_errors(); ?>

    <?php include_once WP2LEADSADMIN_PATH . '/templates/admin-tabs.php';?>

    <?php
    $tab_template = WP2LEADSADMIN_PATH . '/templates/'.$admin_page.'/'.$active_tab.'.php';
    if (file_exists($tab_template)) {
        ?>
        <div class="wptl-container-fluid">
            <div class="wptl-row">
                <div class="wptl-col-xs">
                    <?php include_once $tab_template; ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
