<?php
global $wpdb;
$sql = "
SELECT * FROM {$wpdb->posts} 
WHERE post_type = 'shop_order' 
  AND post_status IN ('wc-completed', 'wc-processing', 'wc-pending', 'wc-refunded', 'wc-on-hold', 'wc-cancelled', 'wc-failed')
ORDER BY ID DESC;";
$orders = $wpdb->get_results($sql, ARRAY_A);

$ts_request_body = '{"ts_result":"No orders"}';
$trustpilot_request_body = '{"$trustpilot_result":"No orders"}';
if (!empty($orders)) {
    $order = $orders[0];
    $ts_request_body = ReviewServiceManager::get_api_review_request_body($order['ID']);
    $trustpilot_request_body = ReviewServiceManager::get_api_review_request_body($order['ID'], 'trustpilot');
}
?>

<h2>Service requests</h2>

<p>
    <select name="" id="service_request_order">
        <?php
        foreach ($orders as $order) {
            ?>
            <option value="<?php echo $order['ID'] ?>"><?php echo $order['post_title'] ?> (<?php echo $order['ID'] ?>)</option>
            <?php
        }
        ?>
    </select>
</p>

<div class="wptl-row">
    <div class="wptl-col-xs-12 wptl-col-sm-12 wptl-col-md-6">
        <h3>Trustedshops request</h3>
        <?php
        $id = sha1($ts_request_body);
        ?>
        <div id="ts_request_container">
            <?php WP2LEADSADMIN_Fn::show_json_editor($id, $ts_request_body, array('mode' => 'code')); ?>
        </div>
    </div>

    <div class="wptl-col-xs-12 wptl-col-sm-12 wptl-col-md-6">
        <h3>Trustpilot request</h3>
        <?php
        $id = sha1($trustpilot_request_body);
        ?>

        <div id="trustpilot_request_container">
            <?php WP2LEADSADMIN_Fn::show_json_editor($id, $trustpilot_request_body, array('mode' => 'code')); ?>
        </div>
    </div>
</div>

<?php // var_dump($orders); ?>
