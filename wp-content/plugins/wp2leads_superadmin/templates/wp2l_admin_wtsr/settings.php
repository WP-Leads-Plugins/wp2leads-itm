<?php
global $wpdb;
$sql = "
SELECT * 
FROM {$wpdb->options} 
WHERE option_name LIKE 'wtsr_%'
  AND option_name NOT LIKE '%_update'
  AND option_name NOT LIKE 'wtsr_product_reviews_%'
  AND option_name NOT LIKE 'wtsr_transferred_%'
  AND option_name NOT LIKE 'wtsr_reviewed_by_review_id_%'
  AND option_name NOT LIKE 'wtsr_ts_product_reviews_import'
  AND option_name NOT LIKE 'wtsr_product_trustpilot_reviews_%'
;";

$wtsr_options = $wpdb->get_results($sql, ARRAY_A);

$sql = "
SELECT * 
FROM {$wpdb->options} 
WHERE option_name LIKE '_transient_wtsr_%'
  AND option_name NOT LIKE '%_update'
  AND option_name NOT LIKE 'wtsr_product_reviews_%'
  AND option_name NOT LIKE 'wtsr_transferred_%'
  AND option_name NOT LIKE 'wtsr_reviewed_by_review_id_%'
  AND option_name NOT LIKE 'wtsr_product_trustpilot_reviews_%'
;";
$wtsr_transients = $wpdb->get_results($sql, ARRAY_A);
?>

<h2>Settings</h2>

<h3>Options</h3>
<?php
foreach ($wtsr_options as $wtsr_option) {
    ?>
    <h4><?php echo $wtsr_option['option_name'] ?> (<?php echo $wtsr_option['option_id'] ?>)</h4>
    <p>
        value: <pre><?php echo $wtsr_option['option_value'] ?></pre>
        <button
                type="button"
                class="button button-primary button-small wp2l_admin_wtsr_delete_option"
                data-option="<?php echo $wtsr_option['option_name'] ?>"
        >
            Delete option
        </button>
    </p>
    <?php
}
?>
<h3>Transients</h3>
<?php
foreach ($wtsr_transients as $wtsr_option) {
    $transient_title_array = explode('_', $wtsr_option['option_name']);
    array_shift($transient_title_array);
    array_shift($transient_title_array);
    $transient_title = implode('_', $transient_title_array);
    ?>
    <h4><?php echo $transient_title ?> (<?php echo $wtsr_option['option_id'] ?>)</h4>
    <p>
        value: <pre><?php echo $wtsr_option['option_value'] ?></pre>
        <button
                type="button"
                class="button button-primary button-small wp2l_admin_wtsr_delete_transient"
                data-option="<?php echo $transient_title ?>"
        >
            Delete transient
        </button>
    </p>
    <?php
}
?>
