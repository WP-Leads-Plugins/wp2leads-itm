<?php
$reviews = WP2LEADSADMIN_WTSR_Model::get_services_reviews();
?>
<h2>Service integration</h2>

<div class="wptl-row">
    <div class="wptl-col-xs-12 wptl-col-sm-12 wptl-col-md-6">
        <h3>Trustedshops reviews</h3>
        <?php
        if (!empty($reviews['ts_reviews'])) {
            $reviews_array = $reviews['ts_reviews'];
            foreach ($reviews_array as $reviews_item) {
                $id = 'reviews_' . $reviews_item['option_id'];
                $option_name_array = explode('_', $reviews_item['option_name']);
                $date_i = count($option_name_array) - 1;
                $date = $option_name_array[$date_i];
                $product_reviews = !empty($reviews_item['option_value']) ? $reviews_item['option_value'] : '{}';
                ?>
                <div id="<?php echo $id; ?>_container">
                    <h4><?php echo $date ?></h4>
                    <p>
                        <button class="wp2l_admin_wtsr_delete_reviews button" data-option_id="<?php echo $reviews_item['option_name']; ?>">
                            Delete reviews
                        </button>
                    </p>
                    <?php WP2LEADSADMIN_Fn::show_json_editor($id, $product_reviews, array('mode' => 'form')); ?>
                </div>
                <?php
            }
        } else {
            ?>
            <p>
                <button class="wp2l_admin_wtsr_check_reviews button">
                    Check reviews
                </button>
            </p>
            <?php
        }
        ?>
    </div>

    <div class="wptl-col-xs-12 wptl-col-sm-12 wptl-col-md-6">
        <h3>Trustpilot reviews</h3>
        <?php
        if (!empty($reviews['trustpilot_reviews'])) {
            $reviews_array = $reviews['trustpilot_reviews'];
            foreach ($reviews_array as $reviews_item) {
                $id = 'reviews_' . $reviews_item['option_id'];
                $option_name_array = explode('_', $reviews_item['option_name']);
                $date_i = count($option_name_array) - 1;
                $date = $option_name_array[$date_i];
                $product_reviews = !empty($reviews_item['option_value']) ? $reviews_item['option_value'] : '{}';
                ?>
                <div id="<?php echo $id; ?>_container">
                    <h4><?php echo $date ?></h4>
                    <p>
                        <button class="wp2l_admin_wtsr_delete_reviews button" data-option_id="<?php echo $reviews_item['option_name']; ?>">
                            Delete reviews
                        </button>
                    </p>
                    <?php WP2LEADSADMIN_Fn::show_json_editor($id, $product_reviews, array('mode' => 'form')); ?>
                </div>
                <?php
            }
        } else {
            ?>
            <p>
                <button class="wp2l_admin_wtsr_check_reviews button">
                    Check reviews
                </button>
            </p>
            <?php
        }
        ?>
    </div>
</div>

<?php // var_dump($reviews); ?>
