<?php

class WP2LEADSADMIN_WTSR_Model {
    public static function get_services_reviews() {
        global $wpdb;

        $sql = "SELECT * FROM $wpdb->options WHERE option_name LIKE '%wtsr_product_trustpilot_reviews_%' ORDER BY 'option_id' DESC";
        $result_trustpilot_reviews = $wpdb->get_results($sql, ARRAY_A);

        $sql = "SELECT * FROM $wpdb->options WHERE option_name LIKE '%wtsr_product_reviews_%' ORDER BY 'option_id' DESC";
        $result_ts_reviews = $wpdb->get_results($sql, ARRAY_A);

        return array(
            'trustpilot_reviews' => $result_trustpilot_reviews,
            'ts_reviews' => $result_ts_reviews,
        );
    }
}