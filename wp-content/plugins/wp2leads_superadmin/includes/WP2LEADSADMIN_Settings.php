<?php


class WP2LEADSADMIN_Settings {
    public static function get_submenues() {
        return array(
            'wp2l_admin_wtsr' => array(
                'parent_slug' => 'wp2leads-wtsr',
                'page_title' => 'Superadmin Get Better Reviews for WooCommerce',
                'menu_title' => 'Superadmin',
                'required_plugins' => array(
                    'more-better-reviews-for-woocommerce' => array(
                        'file' => 'more-better-reviews-for-woocommerce.php',
                        'min' => '3.0.0',
                    ),
                ),
                'subpages' => array(
                    'overview' => array(
                        'title' => 'Overview'
                    ),
                    'services_request' => array(
                        'title' => 'Services Reviews Request'
                    ),
                    'services_integration' => array(
                        'title' => 'Services Reviews'
                    ),
                    'settings' => array(
                        'title' => 'Settings'
                    ),
                    'kt' => array(
                        'title' => 'KlickTipp'
                    ),
                ),
                'libs' => array(
                    'WTSR_Model',
                    'WTSR_Ajax',
                )
            ),
        );
    }
}