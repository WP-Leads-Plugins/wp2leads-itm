<?php


class WP2LEADSADMIN_Fn {
    public static function load_plugin_textdomain() {
        add_filter( 'plugin_locale', 'WP2LEADSADMIN_Fn::check_de_locale');

        load_plugin_textdomain(
            'wp2leads_itm_mid',
            false,
            WP2LITM_MID_PLUGIN_REL_FILE . '/languages/'
        );

        remove_filter( 'plugin_locale', 'WP2LEADSADMIN_Fn::check_de_locale');
    }

    public static function check_de_locale($domain) {
        $site_lang = get_user_locale();
        $de_lang_list = array(
            'de_CH_informal',
            'de_DE_formal',
            'de_AT',
            'de_CH',
            'de_DE'
        );

        if (in_array($site_lang, $de_lang_list)) return 'de_DE';
        return $domain;
    }

    public static function requirement() {
        if (!self::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) return false;

        return true;
    }

    public static function is_plugin_activated( $plugin_folder, $plugin_file ) {
        if ( self::is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
        else return self::is_plugin_active_by_file( $plugin_file );
    }

    public static function is_plugin_active_simple( $plugin ) {
        return (
            in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
            ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
        );
    }

    public static function is_plugin_active_by_file( $plugin_file ) {
        foreach ( self::get_active_plugins() as $active_plugin ) {
            $active_plugin = explode( '/', $active_plugin );
            if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
        }

        return false;
    }

    public static function get_active_plugins() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
        if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

        return $active_plugins;
    }

    public static function show_json_editor($id, $json = '{}', $args = array()) {
        $default_args = array(
            'mode' => 'tree'
        );

        $args = array_merge($default_args, $args);

        /**
         * @var $mode
         */
        extract($args);
        ob_start();
        ?>
        <div id="<?php echo $id ?>_json_container" class="json_container">
            <textarea
                id="<?php echo $id ?>_json_input"
                class="json_input"
                style="display: none;"
            ><?php echo $json; ?></textarea>
            <div id="<?php echo $id ?>_json_display" class="json_display" data-mode="<?php echo $mode; ?>"></div>
        </div>
        <?php
        $html = ob_get_clean();

        echo $html;
    }
}