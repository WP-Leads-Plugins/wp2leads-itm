<?php


class WP2LEADSADMIN_WTSR_Ajax {
    private static $actions = array(
        'test',
        'delete_reviews',
        'check_reviews',
        'get_service_request_order',
        'delete_transient',
    );

    public static function init() {
        foreach (self::$actions as $action) {
            add_action('wp_ajax_nopriv_wp2l_admin_wtsr_' . $action, array('WP2LEADSADMIN_WTSR_Ajax', $action));
            add_action('wp_ajax_wp2l_admin_wtsr_' . $action, array('WP2LEADSADMIN_WTSR_Ajax', $action));
        }
    }

    public static function test() {
        $params = array(
            'message' => 'Test Ajax',
            'reload' => 0,
        );

        WP2LEADSADMIN_Ajax::success_response($params);
    }

    public static function check_reviews() {
        ReviewServiceManager::check_product_reviews();

        $params = array(
            'message' => 'Updating reviews in Background. It could take some time.',
            'reload' => 1,
        );

        WP2LEADSADMIN_Ajax::success_response($params);
    }

    public static function delete_reviews() {
        $option_id = !empty($_POST['option_id']) ? sanitize_text_field($_POST['option_id']) : false;

        if (empty($option_id)) {
            $params = array(
                'message' => 'Select reviews to delete',
            );

            WP2LEADSADMIN_Ajax::error_response($params);
        }

        delete_option($option_id);

        $params = array(
            'message' => 'Reviews '. $option_id .' deleted',
            'reload' => 1,
        );

        WP2LEADSADMIN_Ajax::success_response($params);
    }

    public static function delete_transient() {
        $transient = !empty($_POST['transient']) ? sanitize_text_field($_POST['transient']) : false;

        if (empty($transient)) {
            $params = array(
                'message' => 'Select transient to delete',
            );

            WP2LEADSADMIN_Ajax::error_response($params);
        }

        delete_transient($transient);

        $params = array(
            'message' => 'Transient '. $transient .' deleted',
            'reload' => 1,
        );

        WP2LEADSADMIN_Ajax::success_response($params);
    }

    public static function get_service_request_order() {
        $order_id = !empty($_POST['order_id']) ? sanitize_text_field($_POST['order_id']) : false;

        if (empty($order_id)) {
            $params = array(
                'message' => 'Select reviews to delete',
            );

            WP2LEADSADMIN_Ajax::error_response($params);
        }

        $requests = array(
            'ts_request_body' => ReviewServiceManager::get_api_review_request_body($order_id),
            'trustpilot_request_body' => ReviewServiceManager::get_api_review_request_body($order_id, 'trustpilot'),
        );

        $params = array(
            'requests' => $requests,
        );

        WP2LEADSADMIN_Ajax::success_response($params);
    }
}

WP2LEADSADMIN_WTSR_Ajax::init();