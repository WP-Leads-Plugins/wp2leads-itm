<?php


class WP2LEADSADMIN_Admin {
    private static $submenues;

    public static function init() {
        $submenues = WP2LEADSADMIN_Settings::get_submenues();

        foreach ($submenues as $menu_slug => $submenu) {
            $is_allowed = true;

            foreach ($submenu['required_plugins'] as $pl_slug => $pl_file) {
                if (is_array($pl_file)) {
                    $pl_file = $pl_file['file'];
                }
                if (!WP2LEADSADMIN_Fn::is_plugin_activated( $pl_slug, $pl_file )) {
                    $is_allowed = false;
                    break;
                }
            }

            if (!$is_allowed) unset($submenues[$menu_slug]);
        }

        self::$submenues = $submenues;
        self::includes();
    }

    public static function includes() {
        foreach (self::$submenues as $menu_slug => $submenu) {
            foreach ($submenu['libs'] as $lib) {
                $lib = WP2LEADSADMIN_PATH . "includes/WP2LEADSADMIN_{$lib}.php";

                if (file_exists($lib)) {
                    require_once $lib;
                }
            }
        }
    }

    public static function add_submenu_page() {
        foreach (self::$submenues as $menu_slug => $submenu) {
            add_submenu_page(
                $submenu['parent_slug'],
                $submenu['page_title'],
                $submenu['menu_title'],
                'manage_options',
                $menu_slug,
                'WP2LEADSADMIN_Admin::display_submenu_page'
            );
        }
    }

    public static function display_submenu_page() {
        $submenues = self::$submenues;
        $submenues_array = array_keys($submenues);
        $admin_page = $_GET['page'];

        if (!empty($_GET['page']) && in_array($admin_page, $submenues_array)) {
            extract($submenues[$_GET['page']]);
            include_once WP2LEADSADMIN_PATH . '/templates/admin-page.php';
        }
    }

    public static function enqueue_scripts() {
        $submenues = self::$submenues;
        $submenues_array = array_keys($submenues);

        if (!empty($_GET['page']) && in_array($_GET['page'], $submenues_array)) {
            $admin_page = sanitize_text_field($_GET['page']);

            wp_enqueue_style( 'wp2leads_jsoneditor', WP2LEADSADMIN_URL . 'assets/css/jsoneditor.css', array(), '9.2.0-' . time(), 'all' );
            wp_enqueue_style( 'wp2leads_superadmin', WP2LEADSADMIN_URL . 'assets/css/admin.css', array(), WP2LEADSADMIN_VER . '-' . time(), 'all' );

            wp_enqueue_script( 'wp2leads_jsoneditor', WP2LEADSADMIN_URL . 'assets/js/jsoneditor.js', array(),'9.2.0-' . time(), true );
            wp_enqueue_script( 'wp2leads_superadmin', WP2LEADSADMIN_URL . 'assets/js/admin.js', array( 'jquery' ), WP2LEADSADMIN_VER . '-' . time(), true );
            wp_enqueue_script( 'wp2leads_superadmin'.$admin_page, WP2LEADSADMIN_URL . 'assets/js/admin-'.$admin_page.'.js', array( 'wp2leads_superadmin' ), WP2LEADSADMIN_VER . time(), true );
        }
    }
}

WP2LEADSADMIN_Admin::init();
add_action('admin_menu', array ('WP2LEADSADMIN_Admin', 'add_submenu_page'));
add_action( 'admin_enqueue_scripts', array( 'WP2LEADSADMIN_Admin', 'enqueue_scripts' ) );