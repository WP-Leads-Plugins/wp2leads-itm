(function( $ ) {
    var action_prefix = 'wp2l_admin_wtsr_';
    $(document.body).on('click', '#wtsr_test_btn', function() {
        var data = {
            action: action_prefix + 'test'
        };

        SuperAdmin.ajaxRequest(data, function(decoded) {}, function(decoded) {});
    });

    $(document.body).on('click', '.wp2l_admin_wtsr_delete_transient', function() {
        var btn = $(this);
        var transient = btn.data('option');

        var data = {
            action: action_prefix + 'delete_transient',
            transient: transient,
        };

        SuperAdmin.ajaxRequest(data, function(decoded) {}, function(decoded) {});
    });

    $(document.body).on('click', '.wp2l_admin_wtsr_delete_reviews', function() {
        var btn = $(this);
        var option_id = btn.data('option_id');

        var data = {
            action: action_prefix + 'delete_reviews',
            option_id: option_id,
        };

        SuperAdmin.ajaxRequest(data, function(decoded) {}, function(decoded) {});
    });

    $(document.body).on('click', '.wp2l_admin_wtsr_check_reviews', function() {
        var data = {
            action: action_prefix + 'check_reviews',
        };

        SuperAdmin.ajaxRequest(data, function(decoded) {}, function(decoded) {});
    });

    $(document.body).on('change', '#service_request_order', function() {
        var data = {
            order_id: $(this).val(),
            action: action_prefix + 'get_service_request_order',
        };

        SuperAdmin.ajaxRequest(data, function(decoded) {
            console.log(decoded);
            if (decoded.requests.ts_request_body) {
                $('#ts_request_container .json_input').val(decoded.requests.ts_request_body).trigger('change');
            }

            if (decoded.requests.trustpilot_request_body) {
                $('#trustpilot_request_container .json_input').val(decoded.requests.trustpilot_request_body).trigger('change');
            }
        }, function(decoded) {});
    });
})( jQuery );