var SuperAdmin = SuperAdmin || {};
var JsonEditors = {};

(function( $ ) {
    SuperAdmin['ajaxRequest'] = function(data, cb, cbError) {
        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
            },
            success: function (response) {
                var decoded;

                console.log(response);

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (decoded.url) {
                            window.location.replace(decoded.url);
                        } else if (decoded.reload) {
                            window.location.reload();
                        }

                        if (typeof cb === 'function') {
                            cb(decoded);
                        }
                    } else {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (typeof cbError === 'function') {
                            cbError(decoded);
                        }
                    }
                } else {
                    alert('Something went wrong');
                }
            }
        });
    }

    $(document.body).on('change', '.json_input', function() {
        var json_string = $(this).val();
        var container = $(this).parents('.json_container');
        var pre =  container.find('.json_display');
        var pre_id =  container.find('.json_display').attr('id');
        var json = {};

        try {
            json = JSON.parse(json_string);
        } catch (ex) {
            json = {
                message: 'Wrong JSON Format'
            }
        }

        if (JsonEditors[pre_id]) {
            JsonEditors[pre_id].set(json);
        }
    });

    $(document).ready(function() {
        var jsonTextareas = $('.json_input');

        if (jsonTextareas.length) {
            jsonTextareas.each(function(i, val) {
                var textarea = $(this);
                var container = textarea.parents('.json_container');

                var pre =  container.find('.json_display');
                var pre_id =  container.find('.json_display').attr('id');
                var json = {};

                try {
                    json = JSON.parse(textarea.val());
                } catch (ex) {
                    json = {
                        message: 'Wrong JSON Format'
                    }
                }

                var editor;

                var jsoncontainer = document.getElementById(pre_id)
                var editor_mode = pre.data('mode') || 'tree';
                var options = {
                    mode: editor_mode,
                    modes: ['code', 'form', 'text', 'tree', 'view', 'preview'], // allowed modes
                    onError: function (err) {
                        alert(err.toString())
                    },
                    onModeChange: function (newMode, oldMode) {
                        console.log('Mode switched from', oldMode, 'to', newMode)
                    },
                    onChangeText: function (jsonString) {
                        try {
                            json = JSON.parse(jsonString);
                        } catch (ex) {
                            json = {
                                message: 'Wrong JSON Format'
                            }
                        }

                        // ser.text(jsonSerialize(json));
                    }
                };
                editor = new JSONEditor(jsoncontainer, options);
                editor.set(json);

                JsonEditors[pre_id] = editor;
            });
        }
    });
})( jQuery );