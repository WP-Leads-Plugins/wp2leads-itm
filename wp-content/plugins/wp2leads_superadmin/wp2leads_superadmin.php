<?php
/**
 * Plugin Name:     Wp2Leads Superadmin
 * Description:
 * Version:         1.0.0
 * Text Domain:     wp2leads_superadmin
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LEADSADMIN_VER', '1.0.0' );
define( 'WP2LEADSADMIN_PLUGIN_FILE', __FILE__ );
define( 'WP2LEADSADMIN_URL', plugins_url('/', __FILE__) );
define( 'WP2LEADSADMIN_PATH', plugin_dir_path(__FILE__) );

require_once(WP2LEADSADMIN_PATH . 'includes/WP2LEADSADMIN_KlickTippConnector.php');
require_once(WP2LEADSADMIN_PATH . 'includes/WP2LEADSADMIN_Fn.php');
require_once(WP2LEADSADMIN_PATH . 'includes/WP2LEADSADMIN_Settings.php');
require_once(WP2LEADSADMIN_PATH . 'includes/WP2LEADSADMIN_Ajax.php');
require_once(WP2LEADSADMIN_PATH . 'includes/WP2LEADSADMIN_Admin.php');