<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for Affiliate For WooCommerce
 * Description:
 * Version:         0.0.3
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_afwc
 *
 * Requires at least: 5.0
 * Tested up to: 6.3.1
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_AFWC_VERSION', '0.0.3' );
define( 'WP2LITM_AFWC_PLUGIN_FILE', __FILE__ );
define( 'WP2LITM_AFWC_PLUGIN_REL_FILE', dirname( plugin_basename( __FILE__ ) ) );

require 'plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_afwc.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_afwc'
);

require_once('includes/WP2LITM_AFWC_Functions.php');
require_once('includes/WP2LITM_AFWC_Notices.php');

add_action( 'plugins_loaded', 'WP2LITM_AFWC_Functions::load_plugin_textdomain' );

if (!WP2LITM_AFWC_Functions::requirement()) {
    WP2LITM_AFWC_Notices::show_message('requirements_failed');

    return;
}

require_once('includes/WP2LITM_AFWC_Background.php');
require_once('transfer-modules/WP2LITM_AFWC_User_Instant_Transfer.php');

add_filter('wp2leads_map_query_results', 'WP2LITM_AFWC_Functions::map_results_dynamic', 15, 2);
add_filter('wp2leads_map_query_results_before_comparison', 'WP2LITM_AFWC_Functions::map_results_dynamic_before_comparison', 15, 2);
add_filter('wp2leads_all_columns_for_map', 'WP2LITM_AFWC_Functions::columns_for_map_dynamic', 15, 2);
add_filter('wp2leads_map_selects_only', 'WP2LITM_AFWC_Functions::map_selects_only_dynamic', 15, 2);
add_filter('wp2leads_available_options_map_columns', 'WP2LITM_AFWC_Functions::available_options_map_columns', 15, 2);

add_action( 'afwc_affiliate_approved', 'WP2LITM_AFWC_Functions::affiliate_approved' );
add_action( 'afwc_conversion_tracked', 'WP2LITM_AFWC_Functions::conversion_tracked' );
add_action( 'profile_update', 'WP2LITM_AFWC_Functions::profile_update' );
add_action( "update_option_affiliate_users_roles", 'WP2LITM_AFWC_Functions::affiliate_users_roles_publish_bg', 15, 3 );

add_action('woocommerce_order_status_changed', 'WP2LITM_AFWC_Functions::order_changed', 100);
add_action('woocommerce_process_shop_order_meta', 'WP2LITM_AFWC_Functions::order_changed', 100);
// add_action('updated_post_meta', 'WP2LITM_AFWC_Functions::woocommerce_new_coupon', 100, 4 );

//add_filter( "update_post_metadata", 'WP2LITM_AFWC_Functions::woocommerce_update_coupon_affiliate', 100, 5 );
//add_filter( "delete_post_metadata", 'WP2LITM_AFWC_Functions::woocommerce_delete_coupon_affiliate', 100, 5 );
// apply_filters( "update_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $prev_value );
// do_action( 'woocommerce_new_coupon', $coupon_id, $coupon );

add_action( "wp2litm_afwc_affiliate_users_roles_updated", 'WP2LITM_AFWC_Functions::affiliate_users_roles', 15 );
add_action( "wp2litm_afwc_affiliate_profile_updated", 'WP2LITM_AFWC_Functions::affiliate_profile_updated', 15 );
add_action( "wp2litm_afwc_conversion_tracked", 'WP2LITM_AFWC_Functions::affiliate_conversion_tracked', 15 );
add_action( "wp2litm_afwc_affiliate_order_changed", 'WP2LITM_AFWC_Functions::affiliate_order_changed', 15 );
// do_action( "update_option_{$option}", $old_value, $value, $option );
