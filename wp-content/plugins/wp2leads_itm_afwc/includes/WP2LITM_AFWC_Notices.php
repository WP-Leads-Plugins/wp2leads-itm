<?php
class WP2LITM_AFWC_Notices {
    public static function show_message($notice_id) {
        add_action('admin_notices', 'WP2LITM_AFWC_Notices::' . $notice_id);
    }

    public static function requirements_failed() {
        $message = sprintf( __( 'Please, install all required plugins.', 'wp2leads_itm_afwc' ) );

        if (!WP2LITM_AFWC_Functions::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) {
            $message .= ' <br> - ' . sprintf(
                __( 'WP2LEADS plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_afwc' ),
                '<a href="/wp-admin/plugin-install.php?s=WP2LEADS&tab=search&type=term">',
                '</a>'
            );
        }

        if (!WP2LITM_AFWC_Functions::is_wp2leads_correct_version()) {
            $message .= ' <br> - ' . sprintf(
                __( 'WP2LEADS plugin minimal version %1$s is required. Update plugin %2$shere%3$s.', 'wp2leads_itm_afwc' ),
                WP2LITM_AFWC_Functions::$wp2leads_min_version,
                '<a href="/wp-admin/plugins.php?plugin_status=active" target="_blank">',
                '</a>'
            );
        }

        if (!WP2LITM_AFWC_Functions::is_plugin_activated( 'woocommerce', 'woocommerce.php' )) {
            $message .= ' <br> - ' . sprintf(
                __( 'Woocommerce plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_afwc' ),
                '<a href="/wp-admin/plugin-install.php?s=Woocommerce&tab=search&type=term">',
                '</a>'
            );
        }

        if (!WP2LITM_AFWC_Functions::is_plugin_activated( 'affiliate-for-woocommerce', 'affiliate-for-woocommerce.php' )) {
            $message .= ' <br> - ' . sprintf(
                    __( 'Visit %1$sAffiliate For WooCommerce%2$s to get it.', 'wp2leads_itm_afwc' ),
                    '<a href="https://woocommerce.com/products/affiliate-for-woocommerce/" target="_blank">',
                    '</a>'
                );
        }
        ?>
        <div class="notice notice-error">
            <p>
                <strong><?php echo WP2LITM_AFWC_Functions::get_plugin_name(); ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }
}
