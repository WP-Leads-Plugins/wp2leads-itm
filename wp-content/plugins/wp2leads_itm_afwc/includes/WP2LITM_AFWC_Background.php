<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class WP2LITM_AFWC_Background {
    protected static $bg_process;

    public static function init() {
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/WP2LITM_AFWC_Abstract_Import_Background.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/WP2LITM_AFWC_Import_Background_Process.php';

        self::$bg_process = new WP2LITM_AFWC_Import_Background_Process();
    }

    public static function bg_process($action, $value) {
        self::$bg_process->push_to_queue( ["action" => $action, "value" => $value] );

        self::$bg_process->save()->dispatch();

        return true;
    }

}

add_action( 'init', array( 'WP2LITM_AFWC_Background', 'init' ) );
