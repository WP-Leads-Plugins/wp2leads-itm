<?php

class WP2LITM_AFWC_Functions {
    public static $wp2leads_min_version = '3.3.1';

    public static function load_plugin_textdomain() {
        add_filter( 'plugin_locale', 'WP2LITM_AFWC_Functions::check_de_locale');

        load_plugin_textdomain(
            'wp2leads_itm_afwc',
            false,
            WP2LITM_AFWC_PLUGIN_REL_FILE . '/languages/'
        );

        remove_filter( 'plugin_locale', 'WP2LITM_AFWC_Functions::check_de_locale');
    }

    public static function check_de_locale($domain) {
        $site_lang = get_user_locale();
        $de_lang_list = array(
            'de_CH_informal',
            'de_DE_formal',
            'de_AT',
            'de_CH',
            'de_DE'
        );

        if (in_array($site_lang, $de_lang_list)) return 'de_DE';
        return $domain;
    }
    public static function requirement() {
        if (!self::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) return false;
        if (!self::is_wp2leads_correct_version()) return false;
        if (!self::is_plugin_activated( 'woocommerce', 'woocommerce.php'  )) return false;
        if (!self::is_plugin_activated( 'affiliate-for-woocommerce', 'affiliate-for-woocommerce.php'  )) return false;

        return true;
    }

    public static function is_wp2leads_correct_version() {
        $plugin_version = self::get_plugin_version('wp2leads/wp2leads.php');

        return $plugin_version && version_compare($plugin_version, self::$wp2leads_min_version, '>=');
    }

    public static function get_plugin_version($plugin_slug) {
        // Include the necessary file if it's not already included
        if (!function_exists('get_plugin_data')) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }

        // Get the plugin data
        $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/' . $plugin_slug);

        // Return the version if it exists
        return isset($plugin_data['Version']) ? $plugin_data['Version'] : null;
    }

    public static function is_plugin_activated( $plugin_folder, $plugin_file ) {
        if ( self::is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
        else return self::is_plugin_active_by_file( $plugin_file );
    }

    public static function is_plugin_active_simple( $plugin ) {
        return (
            in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
            ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
        );
    }

    public static function is_plugin_active_by_file( $plugin_file ) {
        foreach ( self::get_active_plugins() as $active_plugin ) {
            $active_plugin = explode( '/', $active_plugin );
            if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
        }

        return false;
    }

    public static function get_active_plugins() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
        if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

        return $active_plugins;
    }

    public static function get_plugin_name() {
        $data = get_plugin_data( WP2LITM_AFWC_PLUGIN_FILE );

        return $data['Name'];
    }

    public static function ends_with($string, $end_string) {
        $length = strlen($end_string);
        if ($length == 0) {
            return true;
        }
        return (substr($string, -$length) === $end_string);
    }

    public static function map_results_dynamic_before_comparison($results, $map) {
        if (empty($results) || empty($map) || empty($map['transferModule']) || 'wp2litm_afwc_user_transfer' !== $map['transferModule']) {
            return $results;
        }

        global $wpdb;

        $sql = "
            SELECT u.ID, um.meta_key, um.meta_value 
            FROM $wpdb->users as u 
                LEFT JOIN $wpdb->usermeta as um 
                    ON u.ID = um.user_id 
            WHERE   um.meta_key = 'afwc_is_affiliate'
                OR  um.meta_key LIKE '%_capabilities'
            ORDER BY u.id, um.meta_key;";

        $affiliate_roles = get_option( 'affiliate_users_roles', array() );

        $users = $wpdb->get_results($sql, ARRAY_A);
        $usermeta_by_id = [];

        foreach ($users as $usermeta) {
            if (empty($usermeta_by_id[$usermeta['ID']])) {
                $usermeta_by_id[$usermeta['ID']] = [];
            }

            if (self::ends_with($usermeta['meta_key'], '_capabilities')) {
                $user_roles_unserialize = maybe_unserialize( $usermeta['meta_value'] );

                if (is_array($user_roles_unserialize)) {
                    $user_roles = [];

                    foreach ($user_roles_unserialize as $user_role => $is_true) {
                        if (!empty($is_true)) {
                            $user_roles[] = $user_role;
                        }
                    }

                    $usermeta_by_id[$usermeta['ID']][$usermeta['meta_key']] = $user_roles;
                } else {
                    $usermeta_by_id[$usermeta['ID']][$usermeta['meta_key']] = array();
                }
            } else {
                $usermeta_by_id[$usermeta['ID']][$usermeta['meta_key']] = $usermeta['meta_value'];
            }
        }

        foreach ($results as $result) {
            if (!empty($result->{'users.ID'})) {
                $user_id = (int)$result->{'users.ID'};

                $result->{'v.users-is_affiliate'} = 'not_registered';

                if (!empty($usermeta_by_id[$user_id])) {
                    $user_meta = $usermeta_by_id[$user_id];

                    if (!empty($user_meta['afwc_is_affiliate'])) {
                        $result->{'v.users-is_affiliate'} = $user_meta['afwc_is_affiliate'];
                    } elseif (!empty($user_meta['am43_capabilities']) && count( array_intersect( $affiliate_roles, $user_meta['am43_capabilities'] ) ) > 0) {
                        $result->{'v.users-is_affiliate'} = 'yes';
                    }
                }

            }
        }

        return $results;
    }

    public static function map_results_dynamic($results, $map) {
        if (empty($results) || empty($map) || empty($map['transferModule']) || 'wp2litm_afwc_user_transfer' !== $map['transferModule']) {
            return $results;
        }

        global $wpdb;

        $users_result = count($results);
        $affiliates_ids = [];

        foreach ($results as $result) {
            $affiliates_ids[] = $result->{'users.ID'};
        }

        $in_ids = "'" . implode("','", $affiliates_ids) . "'";

//        $sql = "
//            SELECT u.ID, um.meta_key, um.meta_value
//            FROM $wpdb->users as u
//                LEFT JOIN $wpdb->usermeta as um
//                    ON u.ID = um.user_id
//            WHERE
//                u.ID IN ({$in_ids}) AND
//                (
//                        um.meta_key = 'first_name'
//                    OR  um.meta_key = 'last_name'
//                    OR  um.meta_key = 'last_update'
//                    OR  um.meta_key = 'afwc_parent_chain'
//                    OR  um.meta_key = 'afwc_ltc_customers'
//                    OR  um.meta_key = 'afwc_paypal_email'
//                    OR  um.meta_key = 'afwc_ref_url_id'
//                    OR  um.meta_key LIKE '%_capabilities'
//                )
//            ORDER BY u.id, um.meta_key;";

        $sql = "
            SELECT u.ID, um.meta_key, um.meta_value 
            FROM $wpdb->users as u 
                LEFT JOIN $wpdb->usermeta as um 
                    ON u.ID = um.user_id 
            WHERE   
                1 = 1 AND
                (
                        um.meta_key = 'first_name'
                    OR  um.meta_key = 'last_name'
                    OR  um.meta_key = 'last_update'
                    OR  um.meta_key = 'afwc_parent_chain'
                    OR  um.meta_key = 'afwc_ltc_customers'
                    OR  um.meta_key = 'afwc_paypal_email'
                    OR  um.meta_key = 'afwc_ref_url_id'
                    OR  um.meta_key LIKE '%_capabilities'
                )
            ORDER BY u.id, um.meta_key;";

        $users = $wpdb->get_results($sql, ARRAY_A);
        $usermeta_by_id = [];

        $referrals_sql = "
            SELECT affiliate_id, amount, currency_id, status, order_status
            FROM {$wpdb->prefix}afwc_referrals
            ORDER BY affiliate_id ASC;
        ";

        $referrals_result = $wpdb->get_results($referrals_sql, ARRAY_A);

        $refferal_coupons_sql = "
            SELECT p.ID, p.post_title as coupon_code, pm.meta_key, pm.meta_value as affiliate_id
            FROM $wpdb->posts as p
                LEFT JOIN $wpdb->postmeta as pm
                    ON p.ID = pm.post_id
            WHERE pm.meta_key = 'afwc_referral_coupon_of'
                AND p.post_type = 'shop_coupon'
                AND p.post_status = 'publish'
            ORDER BY pm.meta_value ASC;
        ";

        $refferal_coupons_results = $wpdb->get_results($refferal_coupons_sql, ARRAY_A);

        foreach ($users as $usermeta) {
            if (empty($usermeta_by_id[$usermeta['ID']])) {
                $usermeta_by_id[$usermeta['ID']] = [];
            }

            $usermeta_by_id[$usermeta['ID']][$usermeta['meta_key']] = $usermeta['meta_value'];
        }

        if (!empty($refferal_coupons_results)) {
            foreach ($refferal_coupons_results as $coupon) {
                $user_id = (int)$coupon['affiliate_id'];

                if (!isset($usermeta_by_id[$user_id]['payment_paid'])) {
                    $usermeta_by_id[$user_id]['coupons'] = [];
                }

                $usermeta_by_id[$user_id]['coupons'][] = $coupon['coupon_code'];
            }
        }

        $allow_custom_affiliate_identifier = get_option( 'afwc_allow_custom_affiliate_identifier', 'yes' );
        $pname = afwc_get_pname();

        if (!empty($referrals_result)) {
            foreach ($referrals_result as $result) {
                if (empty($usermeta_by_id[$result['affiliate_id']])) {
                    continue;
                }

                if (!isset($usermeta_by_id[$result['affiliate_id']]['payment_paid'])) {
                    $usermeta_by_id[$result['affiliate_id']]['payment_paid'] = 0;
                }

                if (!isset($usermeta_by_id[$result['affiliate_id']]['payment_unpaid'])) {
                    $usermeta_by_id[$result['affiliate_id']]['payment_unpaid'] = 0;
                }

                if (!isset($usermeta_by_id[$result['affiliate_id']]['payment_rejected'])) {
                    $usermeta_by_id[$result['affiliate_id']]['payment_rejected'] = 0;
                }

                if (!isset($usermeta_by_id[$result['affiliate_id']]['payment_draft'])) {
                    $usermeta_by_id[$result['affiliate_id']]['payment_draft'] = 0;
                }

                $amount = number_format((float)$result['amount'], 2, '.', '');

                $usermeta_by_id[$result['affiliate_id']]["payment_{$result['status']}"] += (float)$amount;
            }
        }

        foreach ($results as $index => $result) {
            if (!empty($result->{'users.ID'})) {
                $user_id = (int)$result->{'users.ID'};
                $results[$index]->{'v.users-first_name'} = !empty($usermeta_by_id[$user_id]['first_name']) ? $usermeta_by_id[$user_id]['first_name'] : '';
                $results[$index]->{'v.users-last_name'} = !empty($usermeta_by_id[$user_id]['last_name']) ? $usermeta_by_id[$user_id]['last_name'] : '';
                $parent_id = 0;

                if (!empty($usermeta_by_id[$user_id]['afwc_parent_chain'])) {
                    $parent_id = intval( current( array_filter( explode( '|', $usermeta_by_id[$user_id]['afwc_parent_chain'] ) ) ) );
                }

                if (!empty($parent_id) && !empty($usermeta_by_id[$user_id])) {
                    $results[$index]->{'v.users-parent_id'} = $parent_id;
                    $results[$index]->{'v.users-parent_first_name'} = !empty($usermeta_by_id[$parent_id]['first_name']) ? $usermeta_by_id[$parent_id]['first_name'] : '';
                    $results[$index]->{'v.users-parent_last_name'} = !empty($usermeta_by_id[$parent_id]['last_name']) ? $usermeta_by_id[$parent_id]['last_name'] : '';
                } else {
                    $results[$index]->{'v.users-parent_id'} = 0;
                    $results[$index]->{'v.users-parent_first_name'} = '';
                    $results[$index]->{'v.users-parent_last_name'} = '';
                }

                $results[$index]->{'v.users-payment_paid'} = !empty($usermeta_by_id[$user_id]['payment_paid']) ? number_format($usermeta_by_id[$user_id]['payment_paid'], 2, '.', '') : number_format(0, 2, '.', '');
                $results[$index]->{'v.users-payment_unpaid'} = !empty($usermeta_by_id[$user_id]['payment_unpaid']) ? number_format($usermeta_by_id[$user_id]['payment_unpaid'], 2, '.', '') : number_format(0, 2, '.', '');
                $results[$index]->{'v.users-payment_rejected'} = !empty($usermeta_by_id[$user_id]['payment_rejected']) ? number_format($usermeta_by_id[$user_id]['payment_rejected'], 2, '.', '') : number_format(0, 2, '.', '');
                $results[$index]->{'v.users-payment_draft'} = !empty($usermeta_by_id[$user_id]['payment_draft']) ? number_format($usermeta_by_id[$user_id]['payment_draft'], 2, '.', '') : number_format(0, 2, '.', '');

                $ref_url_id = ( 'yes' === $allow_custom_affiliate_identifier ) ? $usermeta_by_id[$user_id]['afwc_ref_url_id'] : '';
                $affiliate_identifier = ( ! empty( $ref_url_id ) ) ? $ref_url_id : $user_id;

                $url = trailingslashit( home_url() );

                if ( 'yes' === get_option( 'afwc_use_pretty_referral_links', 'no' ) ) {
                    $url_path = wp_parse_url( $url, PHP_URL_PATH );
                    // Update the path by appending referral tracking param.
                    $update_path   = trailingslashit( ( ! empty( $url_path ) ? $url_path : '' ) ) . $pname . '/' . $affiliate_identifier;
                    $affiliate_url = afwc_process_url( $url, array( 'path' => trailingslashit( $update_path ) ) );
                } else {
                    $affiliate_url = add_query_arg( $pname, $affiliate_identifier, $url );
                }

                $results[$index]->{'v.users-affiliate_url'} = $affiliate_url;
                $results[$index]->{'v.users-coupons'} = !empty($usermeta_by_id[$user_id]['coupons']) ? implode(', ', $usermeta_by_id[$user_id]['coupons']) : '';
            }
        }

        return $results;
    }

    public static function map_selects_only_dynamic($columns, $map) {
        if (empty($columns) || empty($map) || empty($map['transferModule']) || 'wp2litm_afwc_user_transfer' !== $map['transferModule']) {
            return $columns;
        }

        foreach ($columns  as $index => $column) {
            if (in_array($column, ['v.users-is_affiliate'])) {
                unset($columns[$index]);
            }
        }

        return $columns;
    }

    public static function set_all_columns($columns) {
        if (!in_array('v.users-is_affiliate', $columns)) $columns[] = 'v.users-is_affiliate';
        if (!in_array('v.users-first_name', $columns)) $columns[] = 'v.users-first_name';
        if (!in_array('v.users-last_name', $columns)) $columns[] = 'v.users-last_name';
        if (!in_array('v.users-parent_id', $columns)) $columns[] = 'v.users-parent_id';
        if (!in_array('v.users-parent_first_name', $columns)) $columns[] = 'v.users-parent_first_name';
        if (!in_array('v.users-parent_last_name', $columns)) $columns[] = 'v.users-parent_last_name';
        if (!in_array('v.users-payment_paid', $columns)) $columns[] = 'v.users-payment_paid';
        if (!in_array('v.users-payment_unpaid', $columns)) $columns[] = 'v.users-payment_unpaid';
        if (!in_array('v.users-payment_rejected', $columns)) $columns[] = 'v.users-payment_rejected';
        if (!in_array('v.users-payment_draft', $columns)) $columns[] = 'v.users-payment_draft';
        if (!in_array('v.users-affiliate_url', $columns)) $columns[] = 'v.users-affiliate_url';
        if (!in_array('v.users-coupons', $columns)) $columns[] = 'v.users-coupons';

        return $columns;
    }

    public static function columns_for_map_dynamic($columns, $map_id) {
        $map = MapsModel::get($map_id);
        if (empty($map)) {
            return $columns;
        }
        $mapping = unserialize($map->mapping);

        if (empty($mapping["transferModule"]) || 'wp2litm_afwc_user_transfer' !== $mapping["transferModule"]) {
            return $columns;
        }

        return self::set_all_columns($columns);
    }

    public static function available_options_map_columns($columns, $mapping) {
        if (empty($mapping["transferModule"]) || 'wp2litm_afwc_user_transfer' !== $mapping["transferModule"]) {
            return $columns;
        }

        return self::set_all_columns($columns);
    }

    public static function affiliate_approved($user_id) {
        do_action('wp2litm_afwc_transfer_affiliate', $user_id);
    }

    public static function affiliate_users_roles_publish_bg($old_value, $value, $option) {
        $roles = [];

        if (is_array($old_value) && !empty($old_value)) {
            $roles = array_merge($roles, $old_value);
        }

        if (is_array($value) && !empty($value)) {
            $roles = array_merge($roles, $value);
        }

        $roles = array_unique($roles);


        if (!empty($roles)) {
            WP2LITM_AFWC_Background::bg_process('affiliate_users_roles_updated', $roles);
        }
    }

    public static function affiliate_users_roles($roles) {
        $args = array(
            'role__in' => $roles,
            'orderby'  => 'display_name', // You can order by other fields if needed
            'order'    => 'ASC',
        );

        $user_query = new WP_User_Query($args);
        $users = $user_query->get_results();

        if (!empty($users)) {
            foreach ($users as $user) {
                do_action('wp2litm_afwc_transfer_affiliate', $user->ID);
            }
        }
    }

    public static function profile_update($user_id) {
        WP2LITM_AFWC_Background::bg_process('affiliate_profile_updated', $user_id);
    }

    public static function affiliate_profile_updated($user_id) {
        sleep(5);
        do_action('wp2litm_afwc_transfer_affiliate', $user_id);
    }

    public static function conversion_tracked($data) {
        if (empty($data['conversion_data'])) return;
        WP2LITM_AFWC_Background::bg_process('conversion_tracked', $data['conversion_data']);
    }

    public static function affiliate_conversion_tracked($conversion_data) {
        sleep(5);
        if (empty($conversion_data['affiliate_id'])) return;
        do_action('wp2litm_afwc_transfer_affiliate', $conversion_data['affiliate_id']);
    }

    public static function order_changed($order_id) {
        if (empty($order_id)) return;

        WP2LITM_AFWC_Background::bg_process('affiliate_order_changed', $order_id);
    }

    public static function affiliate_order_changed($order_id) {
        if (empty($order_id)) return;
        sleep(15);

        global $wpdb;

        $affiliate_details = $wpdb->get_row( // phpcs:ignore
            $wpdb->prepare(
                "SELECT affiliate_id
							FROM {$wpdb->prefix}afwc_referrals
							WHERE post_id = %d AND reference = ''",
                $order_id
            ),
            'ARRAY_A'
        );

        if (empty($affiliate_details) || empty($affiliate_details['affiliate_id'])) return;
        // WP2LITM_AFWC_Background::bg_process('affiliate_order_changed', $order_id);
    }

//    public static function woocommerce_new_coupon($meta_id, $object_id, $meta_key, $meta_value) {
//        if ('afwc_referral_coupon_of' !== $meta_key) return;
//        error_log('============================ Coupon created ============================');
//
//        error_log($object_id);
//        error_log($meta_key);
//        error_log($meta_value);
//
//        if (empty($meta_id)) return;
//
//        // WP2LITM_AFWC_Background::bg_process('affiliate_order_changed', $order_id);
//    }

    public static function woocommerce_update_coupon_affiliate($meta_id, $object_id, $meta_key, $meta_value, $prev_value) {
        error_log('============================ Coupon updated ============================');
        error_log($meta_key);
        if ('afwc_referral_coupon_of' !== $meta_key) return $meta_id;

        error_log($object_id);
        error_log($meta_value);
        error_log($prev_value);

        // WP2LITM_AFWC_Background::bg_process('affiliate_order_changed', $order_id);

        return $meta_id;
    }

    public static function woocommerce_delete_coupon_affiliate($meta_id, $object_id, $meta_key, $meta_value, $prev_value) {
        error_log('============================ Coupon deleted ============================');
        error_log($meta_key);
        if ('afwc_referral_coupon_of' !== $meta_key) return $meta_id;

        error_log($object_id);
        error_log($meta_value);

        // WP2LITM_AFWC_Background::bg_process('affiliate_order_changed', $order_id);

        return $meta_id;
    }
}
