<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class WP2LITM_AFWC_Instant_Transfer {
    private static $key = 'wp2litm_afwc_transfer';
    private static $required_column = 'posts.ID';

    public static function transfer_init() {
        add_action( 'woocommerce_checkout_order_processed', 'WP2LITM_AFWC_Instant_Transfer::checkout_order_processed', 10, 3 );
        add_action('woocommerce_order_status_changed', 'WP2LITM_AFWC_Instant_Transfer::order_status_changed', 50, 4);
    }

    public static function get_label() {
        return __('Affiliate For WooCommerce: Order created / updated', 'wp2leads_itm_afwc');
    }

    public static function get_description() {
        return __('This module will transfer user data once order will be created or order\'s data will be changed', 'wp2leads_itm_afwc');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Affiliate For WooCommerce orders maps.', 'wp2leads_itm_afwc') ?></p>
        <p><?php _e('Once order will be created or order\'s data changed user data will be transfered to Klick-Tipp account.', 'wp2leads_itm_afwc') ?></p>
        <p><?php _e('Requirement: <strong>posts.ID</strong> column withing selected data.', 'wp2leads_itm_afwc') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function checkout_order_processed($order_id, $order_data, $order) {
        self::transfer($order_id);
    }

    public static function order_status_changed($order_id, $from, $to, $order_data) {
        self::transfer($order_id);
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => 'posts.ID',
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2litm_afwc_transfer_init($transfer_modules) {
    $transfer_modules['wp2litm_afwc_transfer'] = 'WP2LITM_AFWC_Instant_Transfer';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2litm_afwc_transfer_init');
