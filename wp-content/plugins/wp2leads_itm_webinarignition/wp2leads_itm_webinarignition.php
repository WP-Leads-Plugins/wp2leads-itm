<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for Webinarignition plugin
 * Description:
 * Version:         2.4.0
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_webinarignition
 *
 * Requires at least: 5.0
 * Tested up to: 6.7.2
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_WEBINARIGNITION_VERSION', '2.4.0' );
define( 'WP2LITM_WEBINARIGNITION_DB_VERSION', '2.0.0' );
define( 'WP2LITM_WEBINARIGNITION_MIN_VERSION', '2.4.0' );
define( 'WP2LITM_WP2LEADS_MIN_VERSION', '3.4.6' );

if ( ! defined( 'WP2LITM_WEBINARIGNITION_PLUGIN_FILE' ) ) {
    define( 'WP2LITM_WEBINARIGNITION_PLUGIN_FILE', __FILE__ );
}

define( 'WP2LITM_WEBINARIGNITION_DELAY', 300 );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_webinarignition.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_webinarignition'
);

require_once('functions.php');
require_once ('includes/Wp2leadsItmWebinarignitionNotices.php');

add_action( 'plugins_loaded', 'wp2leads_itm_webinarignition_load_plugin_textdomain' );

if (!wp2leads_itm_webinarignition_requirement()) {
    Wp2leadsItmWebinarignitionNotices::show_message('requirements_failed');
    return;
}

$wi_version = WEBINARIGNITION_VERSION;
$wp2leads_version = WP2LEADS_VERSION;

if (empty($wi_version) || version_compare( $wi_version, WP2LITM_WEBINARIGNITION_MIN_VERSION, '<' )) {
    add_action('admin_notices', function() {
        /* translators: minimal Webinarignition version compatible */
        $message = sprintf( __( 'Please, upgrade Webinarignition plugin. Minimum required version is %s', 'wp2leads_itm_webinarignition' ), WP2LITM_WEBINARIGNITION_MIN_VERSION );
        $data = get_plugin_data( WP2LITM_WEBINARIGNITION_PLUGIN_FILE );
        ?>
        <div class="notice notice-error">
            <p>
                <strong><?php echo $data['Name']; ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    });
    return;
}

if (empty($wp2leads_version) || version_compare( $wp2leads_version, WP2LITM_WP2LEADS_MIN_VERSION, '<' )) {
    add_action('admin_notices', function() {
        /* translators: minimal Webinarignition version compatible */
        $message = sprintf( __( 'Please, upgrade WP2LEADS plugin. Minimum required version is %s', 'wp2leads_itm_webinarignition' ), WP2LITM_WP2LEADS_MIN_VERSION );
        $data = get_plugin_data( WP2LITM_WEBINARIGNITION_PLUGIN_FILE );
        ?>
        <div class="notice notice-error">
            <p>
                <strong><?php echo $data['Name']; ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    });
    return;
}
wp2leads_itm_webinarignition_load_dependencies();

require_once('includes/Wp2leadsItmWebinarignitionWp2leadsHooks.php');
add_filter('wp2leads_map_query_results_before_comparison', 'Wp2leadsItmWebinarignitionWp2leadsHooks::map_results_dynamic_before_comparison', 15, 2);
add_filter('wp2leads_available_options_map_columns', 'Wp2leadsItmWebinarignitionWp2leadsHooks::available_options_map_columns', 15, 2);

require_once('options-functions.php');
require_once('ajax-functions.php');
require_once('templates-functions.php');

add_action( 'init', 'wp2leads_itm_webinarignition_init' );
add_action( 'init', 'wp2leads_itm_webinarignition_options_alt' );
add_action('template_include', 'wp2leads_itm_webinarignition_checkpost', 5);
add_action( 'init', 'wp2leads_itm_webinarignition_check_leads_timestamp' );

wp2leads_itm_webinarignition_modules_init();
