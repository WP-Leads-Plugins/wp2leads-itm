(function( $ ) {
    function openSelectedTab() {
        let fullHash = window.location.hash; // This would be "#section1"
        let tabValue = window.location.hash.substring(1); // This would be "section1"

        if (!tabValue) return;
        history.replaceState(null, null, window.location.href.split('#')[0]);
        let tab = $('[tab="' + tabValue + '"]');

        if (!tab) return;

        $(".editItem").removeClass("editSelected");
        tab.addClass("editSelected");

        $(".tabber").hide();
        $("#" + tabValue + "").show();
    }

    $(document).ready(function () {
        openSelectedTab();
    });
})( jQuery );
