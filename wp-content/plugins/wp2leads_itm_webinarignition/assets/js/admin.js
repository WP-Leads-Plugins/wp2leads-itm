(function( $ ) {
    $(document.body).on('click', '#wp2leads_itm_webinarignition_settings_save', function() {
        var formData = $("#wp2leads_itm_webinarignition_settings_form").serializeArray();

        var data = {
            action: 'wp2leads_itm_webinarignition_settings_save',
            formData: formData
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '.save_webinar_to_map_settings', function() {
        var btn = $(this);
        var app_id = btn.data('id');
        var email_signup = $('#wp2leads_itm_webinarignition_email_signup_' + app_id).is(':checked') ? 'on' : 'off';
        var email_notiff_1 = $('#wp2leads_itm_webinarignition_email_notiff_1_' + app_id).is(':checked') ? 'on' : 'off';
        var email_notiff_2 = $('#wp2leads_itm_webinarignition_email_notiff_2_' + app_id).is(':checked') ? 'on' : 'off';
        var email_notiff_3 = $('#wp2leads_itm_webinarignition_email_notiff_3_' + app_id).is(':checked') ? 'on' : 'off';
        var email_notiff_4 = $('#wp2leads_itm_webinarignition_email_notiff_4_' + app_id).is(':checked') ? 'on' : 'off';
        var email_notiff_5 = $('#wp2leads_itm_webinarignition_email_notiff_5_' + app_id).is(':checked') ? 'on' : 'off';

        let webinar_to_map_settings = {
            email_signup, email_notiff_1, email_notiff_2, email_notiff_3, email_notiff_4, email_notiff_5
        };

        var data = {
            action: 'wp2leads_itm_webinarignition_save_webinar_to_map_settings',
            app_id,
            webinar_to_map_settings,
        }

        console.log(data);

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '.generate_webinarignition_map', function() {
        var btn = $(this);
        var app_id = btn.data('id');
        var app_type = btn.data('type');
        var map_title = $('#wp2leads_itm_webinarignition_map_title_' + app_id).val();
        var add_user = $('#wp2leads_itm_webinarignition_dummy_user_' + app_id).val();

        var data = {
            action: 'wp2leads_itm_webinarignition_generate_webinar_map',
            app_id: app_id,
            app_type: app_type,
            map_title: map_title,
            add_user: add_user
        };

        console.log(data);

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '.generate_optin_page', function() {
        var btn = $(this);
        var page_type = btn.data('type');
        var page_title = $('#wp2leads_itm_webinarignition_' + page_type + '_page_title').val();

        var data = {
            action: 'wp2leads_itm_webinarignition_generate_optin_page',
            page_type: page_type,
            page_title: page_title
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '.wptl-settings-group-header.wptl-settings-group-header-accordeon', function() {
        var container = $(this).parents('.wptl-settings-group');
        var body = container.find('.wptl-settings-group-body');

        if (container.hasClass('open')) {
            container.removeClass('open').addClass('closed');
            body.slideUp();
        } else {
            container.removeClass('closed').addClass('open');
            body.slideDown();
        }
    });

    $(document.body).on('click', '.delete_optin_page', function() {
        var btn = $(this);
        var page_type = btn.data('type');
        var confirm_message = btn.data('confirm');
        var confirmed = confirm(confirm_message);

        if (confirmed) {
            var data = {
                action: 'wp2leads_itm_webinarignition_delete_optin_page',
                page_type: page_type
            };

            ajaxRequest(
                data,
                function() {},
                function() {}
            );
        }
    });

    $(document.body).on('click', '.copy_block_content', function() {
        var btn = $(this);
        var copy = btn.data('copy');
        var confirm = btn.data('confirm');
        var toCopy = $('#' + copy);

        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(toCopy.text()).select();
        document.execCommand("copy");
        $temp.remove();

        alert(confirm);
    });

    $(document.body).on('click', '.delete_webinarignition_map', function() {
        var btn = $(this);
        var app_id = btn.data('id');

        var data = {
            action: 'wp2leads_itm_webinarignition_delete_webinar_map',
            app_id: app_id
        };

        console.log(data);

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('change', '#wp2leads_itm_webinarignition_optin_process', function() {
        var optin = $('#wp2leads_itm_webinarignition_optin_process').val();
        var selected_option = $('#wp2leads_itm_webinarignition_optin_process option:selected');
        var pending_url = selected_option.data('pending-url');
        var thankyou_url = selected_option.data('thankyou-url');

        $('#pendingurl_label_holder').text(pending_url);
        $('#thankyouurl_label_holder').text(thankyou_url);
    });

    $(document.body).on('click', '#wp2leads_itm_webinarignition_optin_process_save', function() {
        var optin = $('#wp2leads_itm_webinarignition_optin_process').val();

        var data = {
            action: 'wp2leads_itm_webinarignition_save_optin_process',
            optin: optin
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#wp2leads_itm_webinarignition_save_apikey', function() {
        var apikey = $('#wp2leads_itm_webinarignition_apikey').val();

        var data = {
            action: 'wp2leads_itm_webinarignition_save_apikey',
            apikey: apikey
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#wp2leads_itm_webinarignition_delete_apikey', function() {
        var btn = $(this);
        var confirm_message = btn.data('confirm');
        var confirmed = confirm(confirm_message);

        if (confirmed) {
            var data = {
                action: 'wp2leads_itm_webinarignition_delete_apikey'
            };

            ajaxRequest(
                data,
                function() {},
                function() {}
            );
        }

    });

    function ajaxRequest(data, cb, cbError) {
        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function (response) {
                var decoded;

                console.log(response);

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (decoded.url) {
                            window.location.replace(decoded.url);
                        } else if (decoded.reload) {
                            window.location.reload();
                        }

                        if (typeof cb === 'function') {
                            cb();
                        }
                    } else {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (typeof cbError === 'function') {
                            cbError();
                        }
                    }
                } else {
                    alert('Something went wrong');
                }
            }
        });
    }
})( jQuery );
