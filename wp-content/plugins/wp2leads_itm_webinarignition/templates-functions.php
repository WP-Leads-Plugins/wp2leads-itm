<?php

function wp2leads_itm_webinarignition_checkpost($original_template) {
    $input_get     = filter_input_array(INPUT_GET);
    global $post;

    if (!empty($post)) {
        $values = get_post_custom($post->ID);

        if ( empty( $values['webinarignitionx_meta_box_select'] ) || empty( $values['webinarignitionx_meta_box_select'][0] ) || ( $values['webinarignitionx_meta_box_select'][0] == '0') ) { return $original_template; }

        $webinarignitionxSelected = $values['webinarignitionx_meta_box_select'];
        $webinar_id = $webinarignitionxSelected[0];

        if ($webinar_id !== "0" && !empty($webinar_id)) {
            // $is_confirmed = Wp2leadsItmWebinarignitionKlickTipp::is_user_confirmed('i.synthetica@gmail.com');

            $client         = urlencode($webinar_id); // used as global, do not remove
            $webinar_id     = urlencode($webinar_id);

            // Return Option Object:
            $webinar_data = get_option('webinarignition_campaign_' . $webinar_id);

            // Thank You Page
            if (isset($input_get['confirmed'])) {

            } else if (isset($input_get['live'])) {
                if ($webinar_data->webinar_date == "AUTO") {

                } else {

                }
            }
        }
    }

    return $original_template;
}

function wp2leads_itm_webinarignition_get_confirmation_page_content() {
    ob_start();

    ?>
    <p>
        <?php echo __('Please visit your mailbox to confirm your email', 'wp2leads_itm_webinarignition'); ?>
    </p>
    <?php

    return ob_get_clean();
}