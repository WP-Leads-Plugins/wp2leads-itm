<?php


if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_Webinarignition_Live {
    private static $key = 'wp2leads_webinarignition_live';
    private static $required_column = 'webinarignition_leads.ID';

    public static function get_label() {
        return __('Webinarignition Live', 'wp2leads_itm_webinarignition');
    }

    public static function get_description() {
        return __('This module will transfer user data to KlickTipp once Webinarignition Live lead created or existed updated', 'wp2leads_itm_webinarignition');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Webinarignition Evergreen webinar.', 'wp2leads_itm_webinarignition') ?></p>
        <p><?php _e('Once new Webinarignition Live lead created or existed updated user data will be transfered to KlickTipp account.', 'wp2leads_itm_webinarignition') ?></p>
        <p><?php _e('Requirement: <strong>webinarignition_leads.ID</strong> column within selected data.', 'wp2leads_itm_webinarignition') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function transfer_init() {
        add_action('wp2leads_itm_webinarignition_leads_updated', 'Wp2leads_Webinarignition_Live::lead_updated', 30, 2);
    }

    public static function lead_updated($lid, $webinar_id) {
        $license_level = Wp2leadsItmWebinarignitionManager::get_webinarignition_license_level();
        if ('ultimate_plus' !== $license_level['license_level']) return;
        self::transfer($lid, $webinar_id);
    }

    public static function transfer($id, $webinar_id) {
        $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());

        if (empty($webinar_to_map[$webinar_id])) {
            return;
        }
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                if ((int)$map_id === (int)$webinar_to_map[$webinar_id]) {
                    $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
                }
            }
        }
    }
}

function wp2leads_webinarignition_live($transfer_modules) {
    $transfer_modules['wp2leads_webinarignition_live'] = 'Wp2leads_Webinarignition_Live';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_webinarignition_live');
