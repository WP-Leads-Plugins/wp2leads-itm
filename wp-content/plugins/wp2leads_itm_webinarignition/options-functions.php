<?php
function wp2leads_itm_webinarignition_options_alt() {
    $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

    if ($is_ajax && !empty($_POST['action']) && $_POST['action'] === 'heartbeat') {
        return;
    }
    $webinars = wp2leads_itm_webinarignition_get_all_webinars();

    if (!empty($webinars)) {
        foreach ($webinars as $webinar) {
            add_filter( "option_webinarignition_campaign_{$webinar['ID']}", 'wp2leads_itm_webinarignition_option_alt', 15, 2 );
        }
    }
}

/**
 * Overwrite webinar settings if WP2LEADS map set up for current webinar
 *
 * @param object $value Webinar settings
 * @param string $option The option name
 * @return object The modified value
 */
function wp2leads_itm_webinarignition_option_alt($value , $option) {
    if (strpos($option, 'webinarignition_campaign_') !== 0) {
        return $value;
    }

    global $post;
    global $wp2leads_itm_webinar_to_map_option;
    global $wp2leads_itm_webinar_maps;

    $parts = explode('_', $option);
    $wi_id = !empty($value->id) ? $value->id : $parts[count($parts) - 1];

    if (empty($wp2leads_itm_webinar_to_map_option) && !is_array($wp2leads_itm_webinar_to_map_option)) {
        $wp2leads_itm_webinar_to_map_option = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());
    }

    if (empty($wp2leads_itm_webinar_to_map_option) || !isset($wp2leads_itm_webinar_to_map_option[$wi_id])) {
        return $value;
    }

    $map_id = $wp2leads_itm_webinar_to_map_option[$wi_id];

    if (empty($wp2leads_itm_webinar_maps) && !is_array($wp2leads_itm_webinar_maps)) {
        $wp2leads_itm_webinar_maps = [];
        global $wpdb;
        $maps_ids = [];

        foreach ($wp2leads_itm_webinar_to_map_option as $map_id) {
            $maps_ids[] = $map_id;
        }

        $maps_ids_string = implode(',', array_map('intval', $maps_ids));

        $table_name = $wpdb->prefix . 'wp2l_maps';
        $sql = "
            SELECT id, info
            FROM {$table_name}
            WHERE ID IN ($maps_ids_string)
        ";

        $results = $wpdb->get_results($sql);

        foreach ($results as $map) {
            $map_info = unserialize($map->info);
            $initial_settings_done = !empty($map_info['initial_settings']);
            $wp2leads_itm_webinar_maps[$map->id] = $initial_settings_done;
        }
    }

    if (empty($wp2leads_itm_webinar_maps[$map_id])) {
        return $value;
    }

    $license_level = Wp2leadsItmWebinarignitionManager::get_webinarignition_license_level();

    if ('ultimate_plus' === $license_level['license_level']) {
        $webinar_to_map_settings = get_option('wp2leads_itm_webinarignition_webinar_to_map_settings_' . $wi_id, [
            'email_signup' => 'off',
            'email_notiff_1' => 'off',
            'email_notiff_2' => 'off',
            'email_notiff_3' => 'off',
            'email_notiff_4' => 'off',
            'email_notiff_5' => 'off',
        ]);

        $new_options = wp2leads_itm_webinarignition_get_custom_options();

        if ($webinar_to_map_settings['email_signup'] === 'off') {
            $value->email_signup = $new_options['email_signup'];
        }

        if ($webinar_to_map_settings['email_notiff_1'] === 'off') {
            $value->email_notiff_1 = $new_options['email_notiff_1'];
        }

        if ($webinar_to_map_settings['email_notiff_2'] === 'off') {
            $value->email_notiff_2 = $new_options['email_notiff_2'];
        }

        if ($webinar_to_map_settings['email_notiff_3'] === 'off') {
            $value->email_notiff_3 = $new_options['email_notiff_3'];
        }

        if ($webinar_to_map_settings['email_notiff_4'] === 'off') {
            $value->email_notiff_4 = $new_options['email_notiff_4'];
        }

        if ($webinar_to_map_settings['email_notiff_5'] === 'off') {
            $value->email_notiff_5 = $new_options['email_notiff_5'];
        }
    }

    if (!empty($post)) {
        $input_get     = filter_input_array(INPUT_GET);
        $webinar_values = get_post_custom($post->ID);

        $is_preview = isset($input_get['preview']) ||
            ( !empty($input_get['lid']) && '[lead_id]' === $input_get['lid']) ||
            isset( $input_get['preview-countdown'] ) ||
            isset( $input_get['preview-webinar'] ) ||
            isset( $input_get['preview-replay'] );

        if ($is_preview) {
            return $value;
        }

        if (
            !empty( $webinar_values['webinarignitionx_meta_box_select'] )
            && !empty( $webinar_values['webinarignitionx_meta_box_select'][0] )
            && ( $webinar_values['webinarignitionx_meta_box_select'][0] != '0')
        ) {

            $webinarignitionxSelected = $webinar_values['webinarignitionx_meta_box_select'];
            $webinar_id = $webinarignitionxSelected[0];

            if ($webinar_id !== "0" && !empty($webinar_id)) {
                $client         = urlencode($webinar_id); // used as global, do not remove
                $webinar_id     = urlencode($webinar_id);
                $new_custom_options = array();

                // Thank You Page
                if (isset($input_get['confirmed'])) {
                    $new_custom_options = wp2leads_itm_webinarignition_get_custom_template_options($webinar_id,'confirmed', $value);
                } else if (isset($input_get['live'])) {
                    $leadID = $input_get['lid'];
                    $w_status = wp2leads_itm_webinarignition_get_webinar_status($leadID, $value);
                    $webinar_status = !empty($w_status) ? $w_status : 'unknown';

                    $new_custom_options = wp2leads_itm_webinarignition_get_custom_template_options($webinar_id, $webinar_status, $value);
                } else if (isset($input_get['register-now'])) {

                } else {
                    $new_custom_options = wp2leads_itm_webinarignition_get_custom_template_options($webinar_id, 'register', $value);
                }

                if (!empty($new_custom_options['footer_code'])) {
                    $value->footer_code = $value->footer_code . $new_custom_options['footer_code'];
                }
            }
        }
    }

    return $value;
}

add_action( 'wp_footer', 'wp2leads_itm_webinarignition_print_footer_scripts', 30 );

function wp2leads_itm_webinarignition_print_footer_scripts() {
    global $post;
    if (!empty($post)) {
        $input_get     = filter_input_array(INPUT_GET);
        $webinar_values = get_post_custom($post->ID);

        $is_preview = isset($input_get['preview']) ||
                      ( !empty($input_get['lid']) && '[lead_id]' === $input_get['lid']) ||
                      isset( $input_get['preview-countdown'] ) ||
                      isset( $input_get['preview-webinar'] ) ||
                      isset( $input_get['preview-replay'] );

        if (!$is_preview && !empty($input_get["webinar"])) {
            $webinar_id = urlencode( $input_get["webinar"] );
            $is_webinar_id_protected = WebinarignitionManager::is_webinar_id_protected($webinar_id);

            if (!empty($is_webinar_id_protected)) {
                $webinar_id = $is_webinar_id_protected;
            }

            $webinar_data = get_option('webinarignition_campaign_' . $webinar_id);

            if ($webinar_id !== "0" && !empty($webinar_id)) {
                $is_thankyou_page = false;
                $is_webinar_page = false;
                $is_countdown_page = false;
                $is_replay_page = false;
                $is_closed_page = false;

                if ( !empty($webinar_data->custom_thankyou_page) ) {
                    $is_thankyou_page = (int) $webinar_data->custom_thankyou_page === (int) $post->ID;
                }

                if ( !empty($webinar_data->custom_webinar_page) ) {
                    $is_webinar_page = (int) $webinar_data->custom_webinar_page === (int) $post->ID;
                }

                if ( !empty($webinar_data->custom_countdown_page) ) {
                    $is_countdown_page = (int) $webinar_data->custom_countdown_page === (int) $post->ID;
                }

                if ( !empty($webinar_data->custom_replay_page) ) {
                    $is_replay_page = (int) $webinar_data->custom_replay_page === (int) $post->ID;
                }

                if ( !empty($webinar_data->custom_closed_page) ) {
                    $is_closed_page = (int) $webinar_data->custom_closed_page === (int) $post->ID;
                }

                $new_custom_options = array();

                if ($is_thankyou_page) {
                    $new_custom_options = wp2leads_itm_webinarignition_get_custom_template_options($webinar_id,'confirmed', $webinar_data);
                } elseif (
                    $is_webinar_page
                    || $is_countdown_page
                    || $is_replay_page
                    || $is_closed_page
                ) {
                    $leadID = $input_get['lid'];
                    $lead = webinarignition_get_lead_info($leadID, $webinar_data);
                    $leadID = $lead->ID;
                    $w_status = wp2leads_itm_webinarignition_get_webinar_status($leadID, $webinar_data);
                    $webinar_status = !empty($w_status) ? $w_status : 'unknown';
                    $new_custom_options = wp2leads_itm_webinarignition_get_custom_template_options($webinar_id, $webinar_status, $webinar_data);
                } else {
                    $new_custom_options = wp2leads_itm_webinarignition_get_custom_template_options($webinar_id, 'register', $webinar_data);
                }

                if (!empty($new_custom_options['footer_code'])) {
                    echo $new_custom_options['footer_code'];
                }
            }
        }
    }
}

/**
 * Disable email notification
 *
 * @return array
 */
function wp2leads_itm_webinarignition_get_custom_options() {
    $options = array(
        'email_signup' => 'off',
        'email_notiff_1' => 'off',
        'email_notiff_2' => 'off',
        'email_notiff_3' => 'off',
        'email_notiff_4' => 'off',
        'email_notiff_5' => 'off',
        'custom_lp_js' => '',
        'custom_ty_js' => '',
        'custom_webinar_js' => '',
        'custom_replay_js' => '',
    );

    return $options;
}

function wp2leads_itm_webinarignition_get_custom_template_options($webinar_id, $template, $settings) {
    $options = array(
        'footer_code' => wp2leads_itm_webinarignition_get_custom_footer_code($webinar_id, $template, $settings),
    );

    return $options;
}

function wp2leads_itm_webinarignition_get_custom_footer_code($webinar_id, $template, $settings) {
    $input_get     = filter_input_array(INPUT_GET);

    $webinar_type = webinarignition_is_auto($settings) ? 'evergreen' : 'live';

    // Retrieve lead ID
    if (empty($input_get['lid']) && !empty($_COOKIE['we-trk-' . $webinar_id])) {
        $input_get['lid'] = $_COOKIE['we-trk-' . $webinar_id];
    }

    $is_webinar_page = in_array($template, ['countdown', 'replay', 'online', 'closed']);
    $webinar_page = $is_webinar_page ? 'webinar' : $template;
    $app_id = $webinar_id;

    // Fetch lead data if available
    $leadID = !empty($input_get['lid']) ? webinarignition_get_lead_info($input_get['lid'], $settings)->ID : null;

    ob_start();
    ?>
    <script data-test="ytut" data-test-test="kdjsnfkjdsk">
        (function($) {
            const ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            const webinarPage = '<?php echo $webinar_page; ?>';
            const webinarType = '<?php echo $webinar_type; ?>';
            const appId = '<?php echo $app_id; ?>';
            const leadId = '<?php echo sanitize_text_field($leadID); ?>';
            const status = '<?php echo sanitize_text_field($template); ?>';

            function sendAjaxRequest(action, additionalData = {}) {
                const data = Object.assign({
                    action,
                    page: webinarPage,
                    type: webinarType,
                    app_id: appId,
                }, additionalData);

                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: data,
                    success: function(response) {
                        console.log(response);
                        try {
                            const decoded = JSON.parse(response);
                            if (decoded.success) {
                                console.log(decoded.message);
                            } else {
                                alert(decoded.message);
                            }
                        } catch (err) {
                            console.error('JSON Parsing Error:', err);
                            alert('Something went wrong');
                        }
                    }
                });
            }

            function beforeLeavePage() {
                const formData = new FormData();
                formData.append('action', 'wp2leads_itm_webinarignition_page_unloading');
                formData.append('page', webinarPage);
                formData.append('type', webinarType);
                formData.append('app_id', appId);

                if (leadId) {
                    formData.append('lid', leadId);
                }

                if (status) {
                    formData.append('status', status);
                }

                if (window.navigator.sendBeacon) {
                    window.navigator.sendBeacon(ajaxurl, formData);
                } else {
                    const xhr = new window.XMLHttpRequest();
                    xhr.open('POST', ajaxurl, false);
                    xhr.send(formData);
                }
            }

            function periodicalWorker() {
                const workerData = {
                    action: 'wp2leads_itm_webinarignition_page_periodical',
                    lid: leadId,
                    status: status,
                    start: Date.now(),
                    current: Date.now()
                };

                sendAjaxRequest('wp2leads_itm_webinarignition_page_periodical', workerData);
                setTimeout(periodicalWorker, 5000);
            }

            $(document).ready(function() {
                console.log('JS injected');
                console.log('Page:', webinarPage);
                console.log('Type:', webinarType);
                if (status) console.log('Status:', status);

                const pageLoadData = {
                    lid: leadId,
                    status: status
                };

                sendAjaxRequest('wp2leads_itm_webinarignition_page_loading', pageLoadData);

                if (!leadId || webinarPage !== 'webinar' || status === 'countdown') return;

                setTimeout(periodicalWorker, 30000);
            });

            window.addEventListener('beforeunload', beforeLeavePage);
            window.addEventListener('unload', beforeLeavePage);

        })(jQuery);
    </script>
    <?php

    return ob_get_clean();
}

function wp2leads_itm_webinarignition_get_webinar_status($leadID, $webinar_data) {
    if ($webinar_data->webinar_date == "AUTO") {
        global $wpdb;
        $table_db_name = $wpdb->prefix . "webinarignition_leads_evergreen";
        $is_lead_protected = !empty($webinar_data->protected_lead_id) && 'protected' === $webinar_data->protected_lead_id;
        $query = "(SELECT * FROM $table_db_name WHERE id = '$leadID' )";

        if ($is_lead_protected) {
            $query = "(SELECT * FROM $table_db_name WHERE hash_ID = '$leadID' )";
        }
        $leadinfo = $wpdb->get_row($query, OBJECT);
        // Set Timezone
        date_default_timezone_set($leadinfo->lead_timezone);
        // Get Dates
        $todaysdate = strtotime("now");
        $live = strtotime($leadinfo->date_picked_and_live);
        $replay = strtotime($leadinfo->date_after_live);

        $webinarDuration = empty($webinar_data->auto_video_length) ? 60 : (int) $webinar_data->auto_video_length;
        $webinarEndTime = strtotime( " +{$webinarDuration} minutes", $live );
        $webinarStarted = $todaysdate >= $live;
        $webinarIsOver = $todaysdate >= $webinarEndTime;

        if (!$webinarStarted) {
            return 'countdown';
        } else if ($webinarIsOver) {
            return 'replay';
        } else {
            return 'online';
        }
    } else {
        if ($webinar_data->webinar_switch == "" || $webinar_data->webinar_switch == "countdown") {
            return 'countdown';
        } else if ($webinar_data->webinar_switch == "live") {
            return 'online';
        } else if ($webinar_data->webinar_switch == "replay") {
            return 'replay';
        } else if ($webinar_data->webinar_switch == "closed") {
            return 'closed';
        }
    }
}

function wp2leads_itm_webinarignition_check_webinar_to_map() {
    $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
    if ($is_ajax) return;
    $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());

    if (empty($webinar_to_map)) return;

    else {
        $all_webinars = wp2leads_itm_webinarignition_get_all_webinars();

        foreach ($webinar_to_map as $i => $v) {
            $map = MapsModel::get($v);

            if (empty($map)) {
                unset($webinar_to_map[$i]);
            } else {
                $webinar_exists = false;

                foreach ($all_webinars as $webinar) {
                    if ((int)$webinar['ID'] === (int)$i) {
                        $webinar_exists = true;
                    }
                }

                if (!$webinar_exists) {
                    global $wpdb;
                    $wpdb->delete( $wpdb->prefix . 'wp2l_maps', array( 'id' => $v ) );
                    unset($webinar_to_map[$i]);
                }
            }
        }

        // delete_option('wp2leads_itm_webinarignition_webinar_to_map');
        update_option('wp2leads_itm_webinarignition_webinar_to_map', $webinar_to_map);
    }
}

add_action('init', 'wp2leads_itm_webinarignition_check_webinar_to_map');

function wp2leads_itm_webinarignition_set_online_transient($app_id, $lid, $type) {
    global $wpdb;

    $option_name = 'wp2leads_itm_webinarignition_online__' . $type . '__' . $app_id . '__' . $lid;

    wp2leads_itm_webinarignition_delete_online_transient($app_id, $lid, $type);
    $now = time();

    $wpdb->query( $wpdb->prepare( "INSERT INTO $wpdb->options (option_name, option_value, autoload) VALUES ( %s, %s, %s)", $option_name, $now, 'no' ) );
}

function wp2leads_itm_webinarignition_delete_online_transient($app_id, $lid, $type) {
    global $wpdb;
    $option_name = 'wp2leads_itm_webinarignition_online__' . $type . '__' . $app_id . '__' . $lid;
    $wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name = '{$option_name}'" );
}
