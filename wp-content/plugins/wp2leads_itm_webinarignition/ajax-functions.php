<?php
add_action('webinarignition_lead_created', 'wp2leads_itm_webinarignition_lead_created', 10, 2);

function wp2leads_itm_webinarignition_lead_created ($leadId, $table_db_name) {
    if (!class_exists('Wp2leadsItmWebinarignitionModel')) include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';

    $post_input     = filter_input_array(INPUT_POST);

    $app_id = null;
    if (!empty($post_input['id']) || !empty($post_input["campaignID"])) {
        $app_id = empty($post_input['id']) ? trim(sanitize_text_field($post_input["campaignID"])) : trim(sanitize_text_field($post_input['id']));
    }
    $email = !empty($post_input['email']) ? trim(sanitize_text_field($post_input['email'])) : null;

    if (empty($app_id) || empty($email)) return;
    $webinar_data = Wp2leadsItmWebinarignitionManager::get_webinar_data($app_id);
    if (empty($webinar_data)) return;

    $webinar_type = webinarignition_is_auto($webinar_data) ? 'evergreen' : 'live';
    $wp2l_table = 'evergreen' === $webinar_type ? 'leads_evergreen' : 'leads';
    $wi_table = 'evergreen' === $webinar_type ? 'wi_leads_evergreen' : 'wi_leads';

    $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $app_id, 'email' => $email));
    if (empty($lead)) return;
    $wi_lead = Wp2leadsItmWebinarignitionModel::get_lead($wi_table, array('ID' => $leadId));
    if (empty($wi_lead)) return;

    $lead_w_id = $lead["w_id"];
    $is_lead_protected = !empty($webinar_data->protected_lead_id) && 'protected' === $webinar_data->protected_lead_id;
    $leadIdUrl = $is_lead_protected ? $wi_lead['hash_ID'] : $leadId;
    if (empty($leadIdUrl)) {
        $leadIdUrl = $leadId;
    }

    $data = array();

    if (empty($lead["w_permalink"])) {
        $data['w_permalink'] = $webinar_data->webinar_permalink . '?live&lid=' . $leadIdUrl;
    }
    if ('Pending' === $lead["status"]) $data['status'] = 'Registered';
    if (empty($lead_w_id) || (int)$lead_w_id !== (int)$leadId) $data['w_id'] = $leadId;

    if (!empty($data)) {
        $data['ID'] = $lead['ID'];
        Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);
        if ('evergreen' === $webinar_type) {
            do_action('wp2leads_itm_webinarignition_leads_evergreen_updated', $leadId, $app_id);
        } else {
            do_action('wp2leads_itm_webinarignition_leads_updated', $leadId, $app_id);
        }
    }
}

add_action('wp_ajax_nopriv_webinarignition_add_lead_auto', 'wp2leads_itm_webinarignition_add_lead_callback', 5);
add_action('wp_ajax_webinarignition_add_lead_auto', 'wp2leads_itm_webinarignition_add_lead_callback', 5);
add_action('wp_ajax_nopriv_webinarignition_add_lead', 'wp2leads_itm_webinarignition_add_lead_callback', 5);
add_action('wp_ajax_webinarignition_add_lead', 'wp2leads_itm_webinarignition_add_lead_callback', 5);
// function wp2leads_itm_webinarignition_add_lead_auto_callback() {
function wp2leads_itm_webinarignition_add_lead_callback() {
    if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
        include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
    }

    $post_input     = filter_input_array(INPUT_POST);
    $post_input['id']   = empty($post_input['id']) ? $post_input["campaignID"] : $post_input['id'];

    $results = Wp2leadsItmWebinarignitionManager::get_webinar_data($post_input['id']);
    if (empty($results)) return;

    $webinar_type = webinarignition_is_auto($results) ? 'evergreen' : 'live';
    $wp2l_table = 'evergreen' === $webinar_type ? 'leads_evergreen' : 'leads';

    $data = wp2leads_itm_webinarignition_prepare_form_data($post_input, $results);
    $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $data['app_id'], 'email' => $data['email']));
    $data['status'] = 'Pending';
    if (!empty($lead['ID'])) {
        $data['ID'] = $lead['ID'];
    }

    $fmt = 'Y-m-d H:i:s';
    $data['registered'] = wp2leads_itm_webinarignition_convertTimeToLocal(date($fmt));
    $data['w_permalink'] = '';
    $data['joined'] = '';
    $data['exited'] = '';
    $data['online'] = '';
    $data['cta_seen'] = '';
    $data['half_viewed'] = '';
    $data['replay'] = '';

    if ('evergreen' === $webinar_type) {
        $webinarLength      = $results->auto_video_length;
        $lead_timezone = !empty($post_input['timezone']) ? new DateTimeZone($post_input['timezone']) : '';

        if ($post_input['date'] == "instant_access") {
            $current_time = new DateTime( 'now', $lead_timezone );
            $todaysDate   = $current_time->format( "Y-m-d" );
            $todaysTime   = $current_time->format( "H:i" );
            $time = date( 'H:i', strtotime( $todaysTime."+0 hours" ) );
            $post_input['date'] = $todaysDate;
            $post_input['time'] = $time;
        }

        $dpl = $post_input['date'] . " " . $post_input['time'];
        $data['w_start'] = date($fmt, strtotime($dpl));
        $data['w_complete'] = date($fmt, strtotime($dpl . " +$webinarLength minutes"));
        $data['w_length'] = $webinarLength;
    } else {
        $webinar_date_array = explode('-', $results->webinar_date);
        $webinar_date = $webinar_date_array[2] . '-' . $webinar_date_array[0] . '-' . $webinar_date_array[1];
        $dpl = $webinar_date. " " . $results->webinar_start_time;
        $data['w_start'] = date($fmt, strtotime($dpl));
    }

    $create = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);
    if (empty($create)) return;
    wp2leads_itm_webinarignition_set_cookie($data['app_id'], array('wp2leads_lid' => $create));

    if (!empty($lead['w_id'])) {
        $action = 'evergreen' === $webinar_type ? 'wp2leads_itm_webinarignition_leads_evergreen_updated' : 'wp2leads_itm_webinarignition_leads_updated';
        do_action($action, $lead['w_id'], $data['app_id']);
    } else {
        $action = 'evergreen' === $webinar_type ? 'wp2leads_itm_webinarignition_leads_evergreen_created' : 'wp2leads_itm_webinarignition_leads_created';
        do_action($action, $create, $data['app_id']);
    }
}

// ADD NEW LEAD
add_action('wp_ajax_nopriv_webinarignition_add_lead_auto_reg', 'wp2leads_itm_webinarignition_add_lead_auto_reg_callback', 5);
add_action('wp_ajax_webinarignition_add_lead_auto_reg', 'wp2leads_itm_webinarignition_add_lead_auto_reg_callback', 5);
function wp2leads_itm_webinarignition_add_lead_auto_reg_callback()
{
    if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
        include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
    }

    $post_input     = filter_input_array(INPUT_POST);
    $results = Wp2leadsItmWebinarignitionManager::get_webinar_data($post_input['id']);
    if (empty($results)) return;
    $data = wp2leads_itm_webinarignition_prepare_form_data($post_input, $results);

    $lead = Wp2leadsItmWebinarignitionModel::get_lead('leads', array('app_id' => $data['app_id'], 'email' => $data['email']));

    if (!empty($lead['ID'])) {
        $data['ID'] = $lead['ID'];
        $data['status'] = !empty($lead['status']) ? $lead['status'] : 'Pending';
    } else {
        $data['status'] = 'Pending';
    }
    $webinar_date_array = explode('-', $results->webinar_date);
    $webinar_date = $webinar_date_array[2] . '-' . $webinar_date_array[0] . '-' . $webinar_date_array[1];
    $dpl = $webinar_date. " " . $results->webinar_start_time;
    $fmt = 'Y-m-d H:i:s';
    $data['registered'] = wp2leads_itm_webinarignition_convertTimeToLocal(date($fmt));
    $data['w_start'] = date($fmt, strtotime($dpl));

    $create = Wp2leadsItmWebinarignitionModel::create_lead('leads', $data);
    if (empty($create)) return;
    wp2leads_itm_webinarignition_set_cookie($data['app_id'], array('wp2leads_lid' => $create));

    if (!empty($lead['w_id'])) {
        do_action('wp2leads_itm_webinarignition_leads_updated', $lead['w_id'], $data['app_id']);
    } else {
        do_action('wp2leads_itm_webinarignition_leads_created', $create, $data['app_id']);
    }
}

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_page_unloading', 'wp2leads_itm_webinarignition_page_unloading');
add_action('wp_ajax_wp2leads_itm_webinarignition_page_unloading', 'wp2leads_itm_webinarignition_page_unloading');
function wp2leads_itm_webinarignition_page_unloading() {
    $post_input     = filter_input_array(INPUT_POST);

    if (
        empty($post_input['action']) ||
        sanitize_text_field($post_input['action']) !== 'wp2leads_itm_webinarignition_page_unloading' ||
        empty($post_input['page']) ||
        empty($post_input['app_id'])
    ) {
        return;
    }

    $webinar_id = $post_input['app_id'];
    $results = Wp2leadsItmWebinarignitionManager::get_webinar_data($webinar_id);
    if (empty($results)) return;

    if (!empty($post_input['lid'])) {
        $leadId = $post_input['lid'];
    } elseif (!empty($post_input['email'])) {
        $getLiveIDByEmail = webinarignition_live_get_lead_by_email($webinar_id, $post_input['email']);
        $leadId = $getLiveIDByEmail->ID;
    } elseif( !empty($_COOKIE['we-trk-' . $webinar_id]) ) {
        $leadId = $_COOKIE['we-trk-' . $webinar_id];
    }

    if (empty($leadId)) {
        return;
    }

    $webinar_type = webinarignition_is_auto($results) ? 'evergreen' : 'live';
    $wp2l_table = 'leads';
    $wi_table = 'wi_leads';

    if ('evergreen' === $webinar_type) {
        $wp2l_table = 'leads_evergreen';
        $wi_table = 'wi_leads_evergreen';
    }

    if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
        include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
    }

    $wi_lead = Wp2leadsItmWebinarignitionModel::get_lead($wi_table, array('ID' => $leadId));

    if (empty($wi_lead)) {
        return;
    }

    $lead_updated = false;
    $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $webinar_id, 'email' => $wi_lead['email']));

    if (empty($lead)) {
        return;
    }

    $lead_status = $lead["status"];
    $lead_w_id = $lead["w_id"];
    $data = array();

    if (empty($lead_w_id) || (int)$lead_w_id !== (int)$leadId) {
        $lead_updated = true;
        $data['w_id'] = $leadId;
    }
    if ('Pending' === $lead_status) {
        $lead_updated = true;
        $data['status'] = 'Registered';
    }

    if (!empty($data)) {
        $lead_updated = true;
        $data['ID'] = $lead['ID'];
    }

    if ($lead_updated) {
        $update = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);
    }

    $fmt = 'Y-m-d H:i:s';

    if ($post_input["page"] === 'webinar') {
        $webinar_status = $post_input["status"];

        if ('online' === $webinar_status || 'replay' === $webinar_status) {
            $data['ID'] = $lead['ID'];

            if ('Completed' !== $lead_status) {
                $data['exited'] = wp2leads_itm_webinarignition_convertTimeToLocal(date($fmt));
                $data['status'] = 'Exited';

                wp2leads_itm_webinarignition_delete_online_transient($webinar_id, $wi_lead['ID'], $webinar_type);
            }

            wp2leads_itm_webinarignition_delete_online_transient($webinar_id, $lead['w_id'], $webinar_type);

            $update = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);

            if ('evergreen' === $webinar_type) {
                do_action('wp2leads_itm_webinarignition_leads_evergreen_updated', $lead['w_id'], $webinar_id);
            } else {
                do_action('wp2leads_itm_webinarignition_leads_updated', $lead['w_id'], $webinar_id);
            }

            $leads = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $webinar_id), false);
            $online_count = 0;
            $total_count = 0;

            foreach ($leads as $lead) {
                if (!empty($lead['w_id'])) {
                    $total_count++;

                    if ($lead['status'] === 'Online') {
                        $online_count++;
                    }
                }
            }

            if (0 !== $total_count) {
                $percent = ($online_count / $total_count) * 100;
            }

            if (2 > $percent) {
                wp2leads_itm_webinarignition_switch_updated($webinar_id, 'closed', 'live', $results);
            }
        }
    }

    return;
}

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_page_periodical', 'wp2leads_itm_webinarignition_page_periodical');
add_action('wp_ajax_wp2leads_itm_webinarignition_page_periodical', 'wp2leads_itm_webinarignition_page_periodical');

function wp2leads_itm_webinarignition_page_periodical() {
    $post_input     = filter_input_array(INPUT_POST);

    if (empty($post_input["start"] || empty($post_input["current"]) || empty($post_input["status"]) || empty($post_input["lid"]) || empty($post_input["app_id"]) || empty($post_input["type"]) || empty($post_input["page"]))) {
        $response = array('success' => 1, 'error' => 0, 'message' => 'Test periodical');
        echo json_encode($response);
        wp_die();
    }

    $start = round((int)$post_input["start"] / 60000, 0);
    $start_s = round((int)$post_input["start"] / 1000, 0);
    $current = round((int)$post_input["current"] / 60000, 0);
    $current_s = round((int)$post_input["current"] / 1000, 0);
    $total = round((int)$current - (int)$start, 0);
    $total_s = round((int)$current_s - (int)$start_s, 0);
    $total = (int)$total;
    $total_s = (int)$total_s;
    $lid = $post_input["lid"];
    $app_id = $post_input["app_id"];
    $type = $post_input["type"];
    $status = $post_input["status"];
    $page = $post_input["page"];
    $results = Wp2leadsItmWebinarignitionManager::get_webinar_data($app_id);

    if (empty($results)) return;

    if ($page === 'webinar' && ($status === 'online' || $status === 'replay') && $total > 0) {
        $updated = false;
        $wp2l_table = 'leads';

        if ('evergreen' === $type) {
            $wp2l_table = 'leads_evergreen';
        }

        if (!class_exists('Wp2leadsItmWebinarignitionModel')) include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';

        $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $app_id, 'w_id' => $lid));

        if (empty($lead) || $lead['status'] !== 'Online') {
            $response = array('success' => 1, 'error' => 0, 'message' => 'Test periodical');
            echo json_encode($response);
            wp_die();
        }

        if ('evergreen' !== $type) {
            if (!empty($lead['online'])) {
                $total = $total + $lead['online'];
            };
        }

        $data = array('ID'=> $lead['ID'],'online' => $total);

        if (!empty($lead['w_length'])) {
            $w_length = (int)$lead['w_length'];

            if ($total >= $w_length) {
                $data['online'] = '';
                // $data['online'] = $w_length;
                $data['status'] = 'Completed';
                if ($data['status'] !== $lead['status']) {
                    $updated = true;
                }
                $data['exited'] = '';
                $data['half_viewed'] = '';
            } elseif ($total >= ($w_length / 2)) {
                $data['half_viewed'] = '50% Viewed';
                if ($data['half_viewed'] !== $lead['half_viewed']) {
                    $updated = true;
                }
            }
        }
        $auto_action = isset($results->auto_action) ? $results->auto_action : '';
        $auto_action_time = isset($results->auto_action_time) ? $results->auto_action_time : '';

        $auto_action_time_array = explode(':', $auto_action_time);
        $auto_action_time = 0;

        if (!empty($auto_action_time_array[0])) {
            $auto_action_time += $auto_action_time_array[0] * 60;
        }

        if (!empty($auto_action_time_array[1])) {
            $auto_action_time += $auto_action_time_array[1];
        }

        if (isset($auto_action) && $auto_action === 'time' && !empty($auto_action_time) && is_numeric($auto_action_time)) {
            $cta_time = (int)$auto_action_time;

            if ($total_s >= $cta_time) {
                $data['cta_seen'] = 'CTA Seen';
            }
        }

        if (!property_exists($results, 'air_toggle') || $results->air_toggle == "" || $results->air_toggle == "off") {

        } else {
            $data['cta_seen'] = 'CTA Seen';
        }

        $update = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);

        if ($updated) {
            if ('evergreen' === $type) {
                do_action('wp2leads_itm_webinarignition_leads_evergreen_updated', $lead['w_id'], $app_id);
            } else {
                do_action('wp2leads_itm_webinarignition_leads_updated', $lead['w_id'], $app_id);
            }
        }

        if (isset($data['status']) && 'Completed' === $data['status']) {
            wp2leads_itm_webinarignition_delete_online_transient($app_id, $lead['w_id'], $type);
        } else {
            wp2leads_itm_webinarignition_set_online_transient($app_id, $lead['w_id'], $type);
        }

        global $wpdb;
        $transient_name_like = 'wp2leads_itm_webinarignition_online__' . $type . '__'.$app_id.'__%';
        $now = time() - WP2LITM_WEBINARIGNITION_DELAY;
        $sql = "SELECT * FROM {$wpdb->options} WHERE option_name LIKE '{$transient_name_like}' AND option_value <= " . $now;
        $transients = $wpdb->get_results( $sql, ARRAY_A );

        if (!empty($transients)) {
            $leads_to_transfer = array();

            foreach ($transients as $transient) {
                $option_name = $transient['option_name'];
                $option_name_array = explode('__', $option_name);
                $last_updated = (int)$transient["option_value"];

                if ($now > $last_updated) {
                    $lid = $option_name_array[3];
                    $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $app_id, 'w_id' => $lid, 'status' => 'Online'));

                    if (!$lead) {
                        wp2leads_itm_webinarignition_delete_online_transient($app_id, $lid, $type);
                    } else {
                        $leads_to_transfer[] = $lid;

                        $joined = $lead['joined'];
                        $exited = wp2leads_itm_webinarignition_convertTimeToLocal(date('Y-m-d H:i:s', $last_updated));

                        $joined_timestamp = (int)strtotime($joined);
                        $exited_timestamp = (int)strtotime($exited);

                        $time_spent = ceil(($exited_timestamp - $joined_timestamp) / 60);
                        error_log($lid . ': joined ' . $joined . ' ' . $joined_timestamp . ': exited ' . $exited . ' ' . $exited_timestamp . ': time_spent ' . $time_spent);

                        $data = array(
                            'ID' => $lead['ID'],
                            'exited' => $exited,
                            'status' => 'Exited',
                            'online' => $time_spent
                        );

                        if (!empty($lead['w_length'])) {
                            if ($time_spent >= ($lead['w_length'] / 2)) {
                                $data['half_viewed'] = '50% Viewed';
                            }
                        }

                        $update = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);
                    }
                }
            }

            $result = Wtsr_Background_Wi_Bunch_Transfer::bg_process($app_id, $leads_to_transfer);
        }
    }

    $response = array('success' => 1, 'error' => 0, 'message' => 'Test periodical');
    echo json_encode($response);
    wp_die();
}

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_page_loading', 'wp2leads_itm_webinarignition_page_loading');
add_action('wp_ajax_wp2leads_itm_webinarignition_page_loading', 'wp2leads_itm_webinarignition_page_loading');

function wp2leads_itm_webinarignition_page_loading() {
    $post_input     = filter_input_array(INPUT_POST);

    if (
        empty($post_input['action']) ||
        sanitize_text_field($post_input['action']) !== 'wp2leads_itm_webinarignition_page_loading' ||
        empty($post_input['page']) ||
        empty($post_input['app_id'])
    ) {
        $message = '';

        if (empty($post_input['page'])) {
            $message .= 'No $post_input[page].';
        }

        if (empty($post_input['app_id'])) {
            $message .= 'No $post_input[app_id].';
        }

        $response = array('success' => 1, 'error' => 0, 'message' => $message);
        echo json_encode($response);
        wp_die();
    }

    $webinar_id = $post_input['app_id'];
    $results = Wp2leadsItmWebinarignitionManager::get_webinar_data($webinar_id);
    if (empty($results)) return;

    $cookie = array();

    if (!empty($_COOKIE['wp2leads_itm_webinarignition'])) {
        $cookie = $_COOKIE['wp2leads_itm_webinarignition'];
        $cookie = stripslashes($cookie);
        $cookie = json_decode($cookie, true);
    }

    if (!empty($post_input['lid'])) {
        $leadId = $post_input['lid'];
    } elseif( !empty($_COOKIE['we-trk-' . $webinar_id]) ) {
        $leadId = $_COOKIE['we-trk-' . $webinar_id];
    } elseif (!empty($post_input['email'])) {
        $getLiveIDByEmail = webinarignition_live_get_lead_by_email($webinar_id, $post_input['email']);
        $leadId = $getLiveIDByEmail->ID;
    }

    if (empty($leadId)) {
        $response = array('success' => 1, 'error' => 0, 'message' => __('Success', 'wp2leads_itm_webinarignition'));
        echo json_encode($response);
        wp_die();
    }

    $webinar_type = webinarignition_is_auto($results) ? 'evergreen' : 'live';
    $wp2l_table = 'leads';
    $wi_table = 'wi_leads';

    if ('evergreen' === $webinar_type) {
        $wp2l_table = 'leads_evergreen';
        $wi_table = 'wi_leads_evergreen';
    }

    if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
        include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
    }

    $wi_lead = Wp2leadsItmWebinarignitionModel::get_lead($wi_table, array('ID' => $leadId));

    if (empty($wi_lead)) {
        $response = array('success' => 1, 'error' => 0, 'message' => __('Success', 'wp2leads_itm_webinarignition'));
        echo json_encode($response);
        wp_die();
    }

    $lead_updated = false;
    $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $webinar_id, 'email' => $wi_lead['email']));

    if (empty($lead)) {
        $response = array('success' => 1, 'error' => 0, 'message' => __('Success', 'wp2leads_itm_webinarignition'));
        echo json_encode($response);
        wp_die();
    }

    $lead_status = $lead["status"];
    $lead_w_id = $lead["w_id"];
    $data = array();

    if (empty($lead_w_id) || (int)$lead_w_id !== (int)$leadId) {
        $lead_updated = true;
        $data['w_id'] = $leadId;
    }

    if (empty($lead["w_permalink"])) {
        $data['w_permalink'] = $results->webinar_permalink . '?live&lid=' . $leadId;
    }
    if ('pending' === strtolower($lead_status)) {
        $lead_updated = true;
        $data['status'] = 'Registered';
    }

    if (!empty($data)) {
        $lead_updated = true;
        $data['ID'] = $lead['ID'];
    }

    if ($lead_updated) {
        $update = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);

        if ($post_input["page"] === 'confirmed') {
            if ('evergreen' === $webinar_type) {
                do_action('wp2leads_itm_webinarignition_leads_evergreen_updated', $leadId, $webinar_id);
            } else {
                do_action('wp2leads_itm_webinarignition_leads_updated', $leadId, $webinar_id);
            }
        }
    }

    $fmt = 'Y-m-d H:i:s';

    if ($post_input["page"] === 'webinar') {
        $webinar_status = $post_input["status"];

        if ('online' === $webinar_status || 'replay' === $webinar_status) {
            $data['ID'] = $lead['ID'];

            if ('online' === $webinar_status) {
                if ('evergreen' === $webinar_type) {
                    $joined = wp2leads_itm_webinarignition_convertTimeToLocal(date($fmt));
                } else {
                    $joined = !empty($lead['joined']) ? $lead['joined'] : wp2leads_itm_webinarignition_convertTimeToLocal(date($fmt));
                    if (!empty($lead['online'])) {
                        //$data['online'] = $lead['online'];
                    }
                }

                $data['status'] = 'Online';
                $data['replay'] = '';
                $data['joined'] = $joined;
                $data['exited'] = '';
                $data['cta_seen'] = '';
                $data['half_viewed'] = '';

                $update = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);
                wp2leads_itm_webinarignition_set_online_transient($webinar_id, $wi_lead['ID'], $webinar_type);
            } else {
                if ($lead_status !== 'Completed') {
                    $data['status'] = 'Online';
                    $data['replay'] = 'Replay';
                    $data['joined'] = wp2leads_itm_webinarignition_convertTimeToLocal(date($fmt));
                    $data['exited'] = '';
                    $data['cta_seen'] = '';
                    $data['half_viewed'] = '';

                    $update = Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);
                    wp2leads_itm_webinarignition_set_online_transient($webinar_id, $wi_lead['ID'], $webinar_type);
                }
            }

            if ('evergreen' === $webinar_type) {
                do_action('wp2leads_itm_webinarignition_leads_evergreen_updated', $leadId, $webinar_id);
            } else {
                do_action('wp2leads_itm_webinarignition_leads_updated', $leadId, $webinar_id);
            }
        }
    }

    $response = array('success' => 1, 'error' => 0, 'message' => __('Success', 'wp2leads_itm_webinarignition'));
    echo json_encode($response);
    wp_die();
}

function wp2leads_itm_webinarignition_prepare_form_data($input, $settings) {
    $data = array();

    if (!empty($input['id']) || !empty($input["campaignID"])) {
        $data['app_id']   = empty($input['id']) ? trim(sanitize_text_field($input["campaignID"])) : trim(sanitize_text_field($input['id']));
    }

    if (!empty($input['name']))             $data['name']                       = trim(sanitize_text_field($input['name']));
    if (!empty($input['email']))            $data['email']                      = trim(sanitize_text_field($input['email']));
    if (!empty($input['phone']))            $data['phone']                      = trim(sanitize_text_field($input['phone']));
    if (!empty($input['status']))           $data['status']                     = trim(sanitize_text_field($input['status']));

    $saved_data = array_values($data);

    if (!empty($input['firstName']))        $data['meta']['firstName']          = trim(sanitize_text_field($input['firstName']));
    if (!empty($input['lastName']))         $data['meta']['lastName']           = trim(sanitize_text_field($input['lastName']));
    if (!empty($input['ip']))               $data['meta']['ip']                 = trim(sanitize_text_field($input['ip']));
    if (!empty($input['source']))           $data['meta']['source']             = trim(sanitize_text_field($input['source']));
    if (isset($settings->webinarURLName2))  $data['meta']['webinarURLName2']    = trim(sanitize_text_field($settings->webinarURLName2));
    if (isset($settings->webinar_host))     $data['meta']['webinar_host']       = trim(sanitize_text_field($settings->webinar_host));

    $saved_data = array_merge($saved_data, array_values($data['meta']));

    if (!empty($input['wiRegForm'])) {
        foreach ($input['wiRegForm'] as $field => $value) {
            if (!in_array(trim(sanitize_text_field($value)), $saved_data)) {
                $data['meta'][$field] = sanitize_text_field($value);
            }
        }
    }

    return $data;
}

function wp2leads_itm_webinarignition_set_cookie($app_id, $data) {
    $cookie_data = array();

    if (!empty($_COOKIE['wp2leads_itm_webinarignition']) && is_array(json_decode(stripslashes($_COOKIE['wp2leads_itm_webinarignition']), true))) {
        $cookie_data = json_decode(stripslashes($_COOKIE['wp2leads_itm_webinarignition']), true);
    }

    $cookie_data[$app_id] = $data;

    setcookie('wp2leads_itm_webinarignition', json_encode($cookie_data), time() + (365 * 24 * 60 * 60), '/');
}

function wp2leads_itm_webinarignition_get_cookie($app_id) {
    if (!empty($_COOKIE['wp2leads_itm_webinarignition']) && is_array(json_decode(stripslashes($_COOKIE['wp2leads_itm_webinarignition']), true))) {
        $cookie_data = json_decode(stripslashes($_COOKIE['wp2leads_itm_webinarignition']), true);

        if (!empty($cookie_data[$app_id])) {
            return $cookie_data[$app_id];
        }
    }

    return false;
}

add_action('wp_ajax_webinarignition_edit', 'wp2leads_itm_webinarignition_edit_callback', 5);

function wp2leads_itm_webinarignition_edit_callback() {
    if (!current_user_can('edit_posts')) {return;}
    $post_input     = filter_input_array(INPUT_POST);
    $id             = $post_input['id'];
    $results = Wp2leadsItmWebinarignitionManager::get_webinar_data($id);
    if (empty($results)) return;

    if (isset($results->webinar_switch) && !empty($post_input["webinar_switch"])) {
        $old_webinar_switch = $results->webinar_switch;
        $new_webinar_switch = $post_input["webinar_switch"];

        if ($old_webinar_switch != $new_webinar_switch) {
            do_action('wp2leads_itm_webinarignition_switch_updated', $id, $new_webinar_switch, $old_webinar_switch, $results);
        }
    }
}

add_action('wp2leads_itm_webinarignition_switch_updated', 'wp2leads_itm_webinarignition_switch_updated', 5, 4);

function wp2leads_itm_webinarignition_switch_updated($app_id, $new_webinar_switch, $old_webinar_switch, $webinar_settings) {
    // closed replay live countdown
    $webinar_type = webinarignition_is_auto($webinar_settings) ? 'evergreen' : 'live';

    if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
        include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
    }

    if ('live' === $webinar_type) {
        global $wpdb;

        if ('live' === $old_webinar_switch && ('replay' === $new_webinar_switch || 'closed' === $new_webinar_switch)) {
            $webinar_date_array = explode('-', $webinar_settings->webinar_date);
            $webinar_date = $webinar_date_array[2] . '-' . $webinar_date_array[0] . '-' . $webinar_date_array[1];
            $dpl = $webinar_date. " " . $webinar_settings->webinar_start_time;
            $fmt = 'Y-m-d H:i:s';
            $w_complete = wp2leads_itm_webinarignition_convertTimeToLocal(date($fmt));
            $webinar_start_timestamp = strtotime($dpl);
            $webinar_end_timestamp = strtotime($w_complete);
            $webinar_length = (int) ceil(($webinar_end_timestamp - $webinar_start_timestamp) / 60);

            $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads',
                array( 'w_complete' => $w_complete, 'w_length' => $webinar_length ),
                array( 'app_id' => $app_id )
            );

            global $wpdb;
            $transient_name_like = 'wp2leads_itm_webinarignition_online__live__'.$app_id.'__%';
            $now = time() - WP2LITM_WEBINARIGNITION_DELAY;
            $sql = "SELECT * FROM {$wpdb->options} WHERE option_name LIKE '{$transient_name_like}' AND option_value <= " . $now;
            $transients = $wpdb->get_results( $sql, ARRAY_A );

            if (!empty($transients)) {
                foreach ($transients as $transient) {
                    $option_name = $transient['option_name'];
                    $option_name_array = explode('__', $option_name);
                    $last_updated = (int)$transient["option_value"];

                    if ($now > $last_updated) {
                        $lid = $option_name_array[3];
                        $lead = Wp2leadsItmWebinarignitionModel::get_lead($wpdb->prefix . 'wp2leads_itm_webinarignition_leads', array('app_id' => $app_id, 'w_id' => $lid, 'status' => 'Online'));

                        if (!$lead) {
                            // wp2leads_itm_webinarignition_delete_online_transient($app_id, $lid, $type);
                        } else {
                            $joined = $lead['joined'];
                            $exited = wp2leads_itm_webinarignition_convertTimeToLocal(date('Y-m-d H:i:s', $last_updated));

                            $joined_timestamp = (int)strtotime($joined);
                            $exited_timestamp = (int)strtotime($exited);

                            $time_spent = ceil(($exited_timestamp - $joined_timestamp) / 60);
                            error_log($lid . ': joined ' . $joined . ' ' . $joined_timestamp . ': exited ' . $exited . ' ' . $exited_timestamp . ': time_spent ' . $time_spent);

                            $data = array(
                                'ID' => $lead['ID'],
                                'exited' => $exited,
                                'status' => 'Exited',
                                'online' => $time_spent
                            );

                            if (!empty($lead['w_length'])) {
                                if ($time_spent >= ($lead['w_length'] / 2)) {
                                    $data['half_viewed'] = '50% Viewed';
                                }
                            }

                            $update = Wp2leadsItmWebinarignitionModel::create_lead($wpdb->prefix . 'wp2leads_itm_webinarignition_leads', $data);
                        }
                    }
                }
            }

            $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads',
                array( 'exited' => '', 'cta_seen' => '', 'half_viewed' => '', 'online' => '', 'status' => 'Completed' ),
                array( 'app_id' => $app_id, 'status' => 'Online' )
            );

            $leads = Wp2leadsItmWebinarignitionModel::get_lead($wpdb->prefix . 'wp2leads_itm_webinarignition_leads', array('app_id' => $app_id, 'status' => 'Exited'), false);

            if (!empty($leads)) {
                foreach ($leads as $lead) {
                    if (!empty($lead['w_length']) && !empty($lead['online'])) {
                        $half_seen = (int)$lead['online'] >= ((int)$lead['w_length'] / 2);

                        if ($half_seen) {
                            $data = array(
                                'ID' => $lead['ID'],
                                'half_viewed' => $lead['50% Viewed'],
                            );
                            $update = Wp2leadsItmWebinarignitionModel::create_lead($wpdb->prefix . 'wp2leads_itm_webinarignition_leads', $data);
                        }
                    }
                }
            }

            $result = Wtsr_Background_Wi_Bunch_Transfer::bg_process($app_id);
        }

//        if ('live' === $new_webinar_switch) {
//            $w_start = wp2leads_itm_webinarignition_convertTimeToLocal(date('Y-m-d H:i:s'));
//
//            $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads',
//                array( 'w_complete' => '', 'w_start' => $w_start,  ),
//                array( 'app_id' => $app_id )
//            );
//
//            $result = Wtsr_Background_Wi_Bunch_Transfer::bg_process($app_id);
//        }
    }
}
