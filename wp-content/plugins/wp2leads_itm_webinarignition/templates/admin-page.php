<?php
$start = time();
$license_level = Wp2leadsItmWebinarignitionManager::get_webinarignition_license_level();
$wp2leads_itm_webinarignition_settings = Wp2leadsItmWebinarignitionSettings::get_settings();
$connector = new Wp2leadsItmWebinarignitionKlickTippConnector();
$username = get_option('wp2l_klicktipp_username');
$password = get_option('wp2l_klicktipp_password');
$logged_in = $connector->login($username, $password);
$optin_processes = $connector->subscription_process_index();
$optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());
$optin = '';
$wi_email_notifications = [
    'email_signup' => __( "On Sign Up Email - First Email" , 'webinarignition' ),
    'email_notiff_1' => __( "Email Notification #1 - Day Before Webinar", 'webinarignition' ),
    'email_notiff_2' => __( "Email Notification #2 - 1 Hour Before Webinar", 'webinarignition' ),
    'email_notiff_3' => __( "Email Notification #3 - Live Webinar", 'webinarignition' ),
    'email_notiff_4' => __( "Email Notification #4 - 1 Hour After Webinar", 'webinarignition' ),
    'email_notiff_5' => __( "Email Notification #5 - 1 Day After Webinar", 'webinarignition' ),
];

if (!empty($optin_pages['optin'])) {
    $optin = $optin_pages['optin'];
}

$optin_processes_loaded = array();

if (!empty($optin_processes)) {
    foreach ($optin_processes as $id => $optin_process) {
        if (!empty($optin_process)) {
            $sp = json_decode(json_encode($connector->subscription_process_get($id)), true);
            $selected = (int)$optin === (int)$id;
            $optin_processes_loaded[$id] = $sp;
            $optin_processes_loaded[$id]['pending_url'] = !empty($sp['pendingurl']) ? $sp['pendingurl'] : __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');
            $optin_processes_loaded[$id]['thankyou_url'] = !empty($sp['thankyouurl']) ? $sp['thankyouurl'] : __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');

            if ($selected) {
                $pendingurl_label = __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');
                $thankyouurl_label = __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');

                if (!empty($sp['pendingurl'])) {
                    $pendingurl = !empty($sp['pendingurl']) ? $sp['pendingurl'] : __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');
                    $pendingurl_label = $sp['pendingurl'];
                }

                if (!empty($sp['thankyouurl'])) {
                    $thankyouurl = $sp['thankyouurl'];
                    $thankyouurl_label = $sp['thankyouurl'];
                }
            }
        } else {
            $optin_processes_loaded[$id] = '';
        }
    }
}

$all_settings_done = true;
ob_start();
?>

    <?php
    if (!empty($optin_pages['confirmation'])) {
        $confirmation_page_id = $optin_pages['confirmation'];
        $confirmation_page = get_post($confirmation_page_id);
    }

    if (!empty($optin_pages['thankyou'])) {
        $thankyou_page_id = $optin_pages['thankyou'];
        $thankyou_page = get_post($thankyou_page_id);
    }

    if (!empty($optin_pages['apikey'])) {
        $apikey = $optin_pages['apikey'];
    }
    ?>
    <div class="wptl-row">
        <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-4">
            <h4>
                1. <?php _e('Own confirmation page', 'wp2leads_itm_webinarignition') ?>
            </h4>
        </div>

        <div class="wptl-col-xs-12 wptl-col-md-7 wptl-col-lg-8">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <ol>
                        <li>
                            <?php echo __('Standard is the Klick-Tipp Standard confirmation-page. You can add any page with any content.', 'wp2leads_itm_webinarignition'); ?>
                        </li>
                    </ol>
                    <p>

                    </p>
                </div>
            </div>

            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <hr style="margin-top: 5px; margin-bottom: 5px;">
                </div>
            </div>
            <?php
            if (!empty($confirmation_page)) {
                $confirmation_page_url = get_permalink( $confirmation_page_id );
                ?>
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <?php echo __('Page Title', 'wp2leads_itm_webinarignition'); ?>:
                            <strong><?php echo $confirmation_page->post_title ?></strong>
                            <br>
                            <?php echo __('Page URL', 'wp2leads_itm_webinarignition'); ?>:
                            <strong id="wp2leads_itm_webinarignition_confirmation_page_url"><?php echo $confirmation_page_url; ?></strong>
                        </p>

                        <?php
                        if (!empty($pendingurl)) {
                            if ($confirmation_page_url !== $pendingurl) {
                                $all_settings_done = false;
                                ?>
                                <p>
                                    <strong class="warning-text">
                                        <?php echo __('Your Confirmation Page URL do not match with URL in selected Opt-in process', 'wp2leads_itm_webinarignition'); ?>
                                    </strong>
                                </p>
                                <?php
                            }
                        } else {
                            $all_settings_done = false;
                            ?>
                            <p>
                                <strong class="warning-text">
                                    <?php echo __('Selected Opt-in process do not have Confirmation page URL, add it in settings', 'wp2leads_itm_webinarignition'); ?>
                                </strong>
                            </p>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <a
                                    href="<?php echo get_admin_url( null, 'post.php?post='.$confirmation_page_id.'&action=edit' ) ?>"
                                    class="button button-small"
                                    target="_blank"
                            >
                                <?php echo __('Edit Page', 'wp2leads_itm_webinarignition'); ?>
                            </a>

                            <button
                                    type="button"
                                    class="button button-primary button-small copy_block_content"
                                    data-copy="wp2leads_itm_webinarignition_confirmation_page_url"
                                    data-confirm="<?php echo __('Confirmation page URL copied!', 'wp2leads_itm_webinarignition'); ?>"
                            >
                                <?php echo __('Copy Confirmation Page URL', 'wp2leads_itm_webinarignition'); ?>
                            </button>

                            <button
                                    id="wp2leads_itm_webinarignition_confirmation_page_title_delete"
                                    type="button"
                                    class="button button-small delete_optin_page"
                                    data-type="confirmation"
                                    data-confirm="<?php echo __('Are you sure you want to delete confirmation page?', 'wp2leads_itm_webinarignition'); ?>"
                            >
                                <?php echo __('Delete Page', 'wp2leads_itm_webinarignition'); ?>
                            </button>
                        </p>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <input
                                    class="form-input"
                                    type="text"
                                    id="wp2leads_itm_webinarignition_confirmation_page_title"
                                    placeholder="<?php echo __('Input Confirmation Page Title', 'wp2leads_itm_webinarignition'); ?>"
                            >
                        </p>

                        <?php
                        if (!empty($pendingurl)) {
                            $all_settings_done = false;
                            ?>
                            <p>
                                <strong class="warning-text">
                                    <?php echo __('You need to create Confirmation Page with URL matched in selected Opt-in process', 'wp2leads_itm_webinarignition'); ?>
                                </strong>
                            </p>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <button
                                    id="wp2leads_itm_webinarignition_confirmation_page_title_generate"
                                    type="button"
                                    class="button button-small button-primary generate_optin_page"
                                    data-type="confirmation"
                            >
                                <?php echo __('Generate Page', 'wp2leads_itm_webinarignition'); ?>
                            </button>
                        </p>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="wptl-row hr-row">
        <div class="wptl-col-xs-12">
            <hr>
        </div>
    </div>

    <div class="wptl-row">
        <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-4">
            <h4>
                2. <?php _e('WebinarIgnition thank you page (required)', 'wp2leads_itm_webinarignition') ?>
            </h4>
        </div>

        <div class="wptl-col-xs-12 wptl-col-md-7 wptl-col-lg-8">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <ol>
                        <li><?php echo __('This page will redirect the visitor to the WebinarIgnition generated thank you page.', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('The URL need to be setup in each Webinar Double-Opt-in-Prozess in "thank you page URL".', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('This page is used for all Webinars and can be altered only inside WebinarIgnition.', 'wp2leads_itm_webinarignition'); ?></li>
                    </ol>
                </div>
            </div>

            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <hr style="margin-top: 5px; margin-bottom: 5px;">
                </div>
            </div>
            <?php
            if (!empty($thankyou_page)) {
                $thankyou_page_url = get_permalink( $thankyou_page_id );
                ?>
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <?php echo __('Page Title', 'wp2leads_itm_webinarignition'); ?>:
                            <strong><?php echo $thankyou_page->post_title ?></strong>
                            <br>
                            <?php echo __('Page URL', 'wp2leads_itm_webinarignition'); ?>:
                            <strong id="wp2leads_itm_webinarignition_thankyou_page_url"><?php echo $thankyou_page_url; ?></strong>
                        </p>

                        <?php
                        if (!empty($thankyouurl)) {
                            if ($thankyou_page_url !== $thankyouurl) {
                                $all_settings_done = false;
                                ?>
                                <p>
                                    <strong class="warning-text">
                                        <?php echo __('Your Thankyou Page URL do not match with URL in selected Opt-in process', 'wp2leads_itm_webinarignition'); ?>
                                    </strong>
                                </p>
                                <?php
                            }
                        } else {
                            $all_settings_done = false;
                            ?>
                            <p>
                                <strong class="warning-text">
                                    <?php echo __('Selected Opt-in process do not have Thankyou page URL, add it in settings', 'wp2leads_itm_webinarignition'); ?>
                                </strong>
                            </p>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>

                            <button
                                    type="button"
                                    class="button button-primary button-small copy_block_content"
                                    data-copy="wp2leads_itm_webinarignition_thankyou_page_url"
                                    data-confirm="<?php echo __('Thankyou page URL copied!', 'wp2leads_itm_webinarignition'); ?>"
                            >
                                <?php echo __('Copy Thank You Page URL', 'wp2leads_itm_webinarignition'); ?>
                            </button>
                            <button
                                    id="wp2leads_itm_webinarignition_thankyou_page_title_delete"
                                    type="button"
                                    class="button button-small delete_optin_page"
                                    data-type="thankyou"
                                    data-confirm="<?php echo __('Are you sure you want to delete thankyou page?', 'wp2leads_itm_webinarignition'); ?>"
                            >
                                <?php echo __('Delete Page', 'wp2leads_itm_webinarignition'); ?>
                            </button>
                        </p>
                    </div>
                </div>
                <?php
            } else {
                $all_settings_done = false;
                ?>
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <input
                                    class="form-input"
                                    type="text"
                                    id="wp2leads_itm_webinarignition_thankyou_page_title"
                                    placeholder="<?php echo __('Input Page Title', 'wp2leads_itm_webinarignition'); ?>"
                            >
                        </p>

                        <?php
                        if (!empty($thankyouurl)) {
                            ?>
                            <p>
                                <strong class="warning-text">
                                    <?php echo __('You need to create Thankyou Page with URL matched in selected Opt-in process', 'wp2leads_itm_webinarignition'); ?>
                                </strong>
                            </p>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                        <p>
                            <button
                                    id="wp2leads_itm_webinarignition_confirmation_page_title_generate"
                                    type="button"
                                    class="button button-primary generate_optin_page"
                                    data-type="thankyou"
                            >
                                <?php echo __('Generate Page', 'wp2leads_itm_webinarignition'); ?>
                            </button>
                        </p>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="wptl-row hr-row">
        <div class="wptl-col-xs-12">
            <hr>
        </div>
    </div>

    <div class="wptl-row">
        <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-4">
            <h4>
                3. <?php _e('Create double opt-in process', 'wp2leads_itm_webinarignition') ?>
            </h4>
        </div>

        <div class="wptl-col-xs-12 wptl-col-md-7 wptl-col-lg-8">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <ol>
                        <li><?php _e('Login to your <a href="https://app.klicktipp.com/user" target="_blank">Klick-Tipp account</a>', 'wp2leads_itm_webinarignition') ?></li>
                        <li><?php _e('Go to <strong>"Automatisierung"</strong> -> <strong>"Neuer Double-Opt-in-Prozess"</strong>', 'wp2leads_itm_webinarignition') ?></li>
                        <li><?php _e('Input <strong>"Name"</strong> for new double opt-in process and click <strong>"Double-Opt-in-Prozess anlegen"</strong> button', 'wp2leads_itm_webinarignition') ?></li>

                        <li><?php echo __('If you want to use your own confirmation page tick <strong>"eigene Bestätigungsseite verwenden"</strong> checkbox on <strong>"Double-Opt-in-Prozess bearbeiten"</strong> page', 'wp2leads_itm_webinarignition'); ?></li>
                        <?php
                        if (empty($confirmation_page)) {
                            ?>
                            <li><?php echo __('On this page <strong>Input Confirmation Page Title</strong> in field below and click <strong>"Generate Page"</strong> button', 'wp2leads_itm_webinarignition'); ?></li>
                            <?php
                        }
                        ?>
                        <li><?php echo __('On this page click <strong>"Copy Confirmation Page URL"</strong> button', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('Go to <strong>"Double-Opt-in-Prozess bearbeiten"</strong> page and input copied URL into <strong>"URL der Bestätigungsseite"</strong> field', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('Make sure <strong>"E-Mail-Adresse an URL anfügen"</strong> checkbox is selected', 'wp2leads_itm_webinarignition'); ?></li>

                        <li><?php echo __(' In order to redirect user to registered Webinar after email confirmation, you need to add WebinarIgnition thank you page', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('Tick <strong>"eigene Dankeschönseite verwenden"</strong> checkbox on <strong>"Double-Opt-in-Prozess bearbeiten"</strong> page', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('On this page click <strong>"Copy Thank You Page URL"</strong> button', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('Go to <strong>"Double-Opt-in-Prozess bearbeiten"</strong> page and input copied URL into <strong>"URL der Dankeschönseite"</strong> field', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('Make sure <strong>"E-Mail-Adresse an URL anfügen"</strong> checkbox is selected', 'wp2leads_itm_webinarignition'); ?></li>
                        <li><?php echo __('Click button <strong>"Speichern"</strong> for save settings', 'wp2leads_itm_webinarignition'); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="wptl-row hr-row">
        <div class="wptl-col-xs-12">
            <hr>
        </div>
    </div>

<div class="wptl-row">
    <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-4">
        <h4>
            4. <?php _e('Important', 'wp2leads_itm_webinarignition') ?>: <?php echo __('Please choose your double-opt-in-process', 'wp2leads_itm_webinarignition'); ?>
        </h4>
    </div>

    <div class="wptl-col-xs-12 wptl-col-md-7 wptl-col-lg-8">
        <?php
        if (!empty($optin_processes_loaded)) {
            ?>
            <div class="wptl-row">
                <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                    <p>
                        <?php echo __('This double-opt-in-process will be the default for all webinars.', 'wp2leads_itm_webinarignition'); ?>
                        <br>
                        <?php echo __('You can add a Thank-you-page per Webinar by choosing a different Double-Opt-in-Prozess inside the "Map to API" tab.', 'wp2leads_itm_webinarignition'); ?>
                    </p>
                </div>
                <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">

                </div>
            </div>
            <?php
        }
        ?>

        <div class="wptl-row">
            <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                <?php
                if (!empty($optin_processes_loaded)) {
                    $selected_optin = false;
                    ?>
                    <p>
                        <select id="wp2leads_itm_webinarignition_optin_process" class="form-input">
                            <?php
                            foreach ($optin_processes_loaded as $id => $optin_process) {
                                if (empty($optin_process)) {
                                    ?>
                                    <option value="" <?php echo empty($optin) ? ' selected' : '' ?> data-pending-url="<?php echo __('Select Optin Process', 'wp2leads_itm_webinarignition'); ?>" data-thankyou-url="<?php echo __('Select Optin Process', 'wp2leads_itm_webinarignition'); ?>">
                                        <?php echo __('Select Optin Process', 'wp2leads_itm_webinarignition'); ?>
                                    </option>
                                    <?php
                                } else {
                                    $sp = $optin_process;
                                    $selected = (int)$optin === (int)$id;

                                    if ($selected) {
                                        $selected_optin = true;
                                        $pendingurl_label = __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');
                                        $thankyouurl_label = __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');

                                        if (!empty($sp['pendingurl'])) {
                                            $pendingurl = !empty($sp['pendingurl']) ? $sp['pendingurl'] : __('Klick Tipp standard link', 'wp2leads_itm_webinarignition');
                                            $pendingurl_label = $sp['pendingurl'];
                                        }

                                        if (!empty($sp['thankyouurl'])) {
                                            $thankyouurl = $sp['thankyouurl'];
                                            $thankyouurl_label = $sp['thankyouurl'];
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $id; ?>" <?php echo $selected ? ' selected' : '' ?> data-pending-url="<?php echo $optin_process['pending_url']; ?>" data-thankyou-url="<?php echo $optin_process['thankyou_url']; ?>">
                                        <?php echo $optin_process['name']; ?>
                                    </option>
                                    <?php
                                }
                            }

                            if (!$selected_optin) {
                                $all_settings_done = false;
                            }
                            ?>
                        </select>
                    </p>

                    <p>
                        <?php _e('Confirmation redirect URL: ', 'wp2leads_itm_webinarignition'); ?>
                        <strong id="pendingurl_label_holder">
                            <?php echo !empty($pendingurl_label) ? $pendingurl_label : __('Select Optin Process', 'wp2leads_itm_webinarignition'); ?>
                        </strong>
                        <br>
                        <?php _e('Thank You redirect URL: ', 'wp2leads_itm_webinarignition'); ?>
                        <strong id="thankyouurl_label_holder">
                            <?php echo !empty($thankyouurl_label) ? $thankyouurl_label : __('Select Optin Process', 'wp2leads_itm_webinarignition'); ?>
                        </strong>
                    </p>
                    <?php
                }
                ?>
            </div>
            <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                <p>
                    <button
                            id="wp2leads_itm_webinarignition_optin_process_save"
                            type="button"
                            class="button button-primary"
                            data-type="confirmation"
                    >
                        <?php echo __('Save Optin process', 'wp2leads_itm_webinarignition'); ?>
                    </button>
                </p>
            </div>
        </div>
    </div>
</div>
<?php
$optin_settings_group = ob_get_clean();
?>
<div class="wrap">
    <h1><?php _e('Webinarignition to KlickTipp', 'wp2leads_itm_webinarignition'); ?></h1>
    <?php settings_errors(); ?>
    <h4>
        Webinarignition <?php _e('version', 'wp2leads_itm_webinarignition'); ?>: <?php echo $license_level['license_title']; ?>
    </h4>

    <?php
    if ('ultimate_plus' !== $license_level['license_level']) {
        ?>
        <div class="notice notice-warning inline">
            <p>
                <?php
                echo sprintf(
                    __(
                        'To transfer in real time, automatically and not just the current user in the Map to API tab, please use %sULTIMATE Unlimited Plus%s Webinarignition version.',
                        'wp2leads_itm_webinarignition'
                    ),
                    '<a href="' . admin_url('admin.php?page=webinarignition-dashboard-pricing') . '" target="_blank">',
                    '</a>'
                );
                ?><br>
                <?php
                _e(
                    'You can still make initial preparations, such as creating maps, setting up URLs for double opt-in redirections, etc.',
                    'wp2leads_itm_webinarignition'
                )
                ?>
            </p>
        </div>
        <?php
    }
    ?>

    <div class="wptl-settings-group <?php echo $all_settings_done ? 'closed' : 'open' ?>">
        <div class="wptl-settings-group-header wptl-settings-group-header-accordeon">
            <h3><?php _e('Double opt-in settings', 'wp2leads_itm_webinarignition') ?></h3>

            <span class="dashicons dashicons-arrow-up-alt2"></span>
            <span class="dashicons dashicons-arrow-down-alt2"></span>
        </div>

        <div class="wptl-settings-group-body" <?php echo $all_settings_done ? ' style="display:none;"' : '' ?>>
            <?php echo $optin_settings_group; ?>
        </div>
    </div>

    <!-- Webinar to Map Settings -->
    <div class="wptl-settings-group">
        <div class="wptl-settings-group-header">
            <h3><?php _e('Connect your webinars to wp2leads maps', 'wp2leads_itm_webinarignition') ?></h3>
        </div>

        <div class="wptl-settings-group-body">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <?php
                    $all_webinars = wp2leads_itm_webinarignition_get_all_webinars(true, true);
                    $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());

                    if (!empty($all_webinars)) {
                        foreach ($all_webinars as $single_webinar) {
                            $webinar_type = webinarignition_is_auto($single_webinar['settings']) ? 'evergreen' : 'live';
                            $webinar_type_label = 'evergreen' === $webinar_type ? __('Evergreen', 'wp2leads_itm_webinarignition') : __('Live', 'wp2leads_itm_webinarignition');
                            $webinar_to_map_exists = !empty($webinar_to_map[$single_webinar['ID']]);

                            if ($webinar_to_map_exists) {
                                $map = MapsModel::get($webinar_to_map[$single_webinar['ID']]);
                                $map_api = unserialize($map->api);
                                $map_info = unserialize($map->info);
                            }

                            ?>
                            <div class="wptl-row">
                                <div class="wptl-col-xs-12 wptl-col-md-12 wptl-col-lg-5">
                                    <h4>
                                        <?php echo $single_webinar['appname'] ?>
                                        <small>(<?php echo $webinar_type_label ?>)</small>
                                    </h4>
                                </div>

                                <?php
                                if ($webinar_to_map_exists) {
                                    $correct_optin = (int)$map_api['default_optin'] === (int)$optin;
                                    ?>
                                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                                        <p>
                                            <?php echo __('Map Name', 'wp2leads_itm_webinarignition'); ?>:
                                            <strong><?php echo $map->name; ?></strong>
                                            <?php
                                            if (empty($map_info["initial_settings"])) {
                                                ?>
                                                <br>
                                                <strong class="warning-text">
                                                    <?php echo __('Please, make initial settings for this map.', 'wp2leads_itm_webinarignition'); ?>
                                                </strong>
                                                <?php
                                            } elseif (!$correct_optin) {
                                                ?>
                                                <br>
                                                <strong class="warning-text">
                                                    <?php echo __('Map for this webinar using different opt-in process.', 'wp2leads_itm_webinarignition'); ?>
                                                </strong>
                                                <?php
                                            }
                                            ?>

                                        </p>
                                    </div>

                                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-4">
                                        <p>
                                            <a
                                                    href="<?php echo get_admin_url( null, 'admin.php?page=wp2l-admin&tab=map_to_api&active_mapping='.$webinar_to_map[$single_webinar['ID']] ) ?>"
                                                    class="button button-small "
                                                    target="_blank"
                                            >
                                                <?php echo __('Edit Map', 'wp2leads_itm_webinarignition'); ?>
                                            </a>
                                            <button
                                                    id="wp2leads_itm_webinarignition_generate_webinar_map_<?php echo $single_webinar['ID']; ?>"
                                                    type="button button-small "
                                                    class="button delete_webinarignition_map"
                                                    data-id="<?php echo $single_webinar['ID']; ?>"
                                            >
                                                <?php echo __('Delete Map', 'wp2leads_itm_webinarignition'); ?>
                                            </button>
                                        </p>
                                    </div>
                                    <?php
                                    if (!empty($map_info["initial_settings"])) {
                                        $is_webinar_to_map_settings_done = false;
                                        $webinar_to_map_settings = get_option('wp2leads_itm_webinarignition_webinar_to_map_settings_' . $single_webinar['ID'], null);

                                        if (!empty($webinar_to_map_settings)) {
                                            $is_webinar_to_map_settings_done = true;
                                        } else {
                                            $webinar_to_map_settings = [
                                                'email_signup' => 'off',
                                                'email_notiff_1' => 'off',
                                                'email_notiff_2' => 'off',
                                                'email_notiff_3' => 'off',
                                                'email_notiff_4' => 'off',
                                                'email_notiff_5' => 'off',
                                            ];
                                        }
                                        ?>
                                        <div style="padding: 0 5px 5px 5px;<?php echo 'ultimate_plus' !== $license_level['license_level'] ? ' display: none;' : '' ?>">
                                            <div class="wptl-col-xs-12" style="margin-top: 15px;">
                                                <?php
                                                foreach ($wi_email_notifications as $key => $title) {
                                                    ?>
                                                    <p>
                                                        <label for="wp2leads_itm_webinarignition_<?php echo $key ?>_<?php echo $single_webinar['ID'] ?>" style="cursor: pointer;">
                                                            <input
                                                                id="wp2leads_itm_webinarignition_<?php echo $key ?>_<?php echo $single_webinar['ID'] ?>"
                                                                type="checkbox"
                                                                <?php echo $webinar_to_map_settings[$key] === 'on' ? 'checked="checked"' : ''; ?>
                                                            >
                                                            <?php echo __('I want to send', 'wp2leads_itm_webinarignition'); ?>
                                                            <strong>
                                                                <?php echo $title; ?>
                                                            </strong>
                                                            <?php echo __('with Webinarignition', 'wp2leads_itm_webinarignition'); ?>
                                                        </label>
                                                    </p>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="wptl-col-xs-12" style="margin-top: 15px;">
                                                <button
                                                    type="button"
                                                    class="button button-primary button-small save_webinar_to_map_settings"
                                                    data-id="<?php echo $single_webinar['ID']; ?>"
                                                >
                                                    <?php echo __('Save settings', 'wp2leads_itm_webinarignition'); ?>
                                                </button>
                                            </div>
                                            <div class="wptl-col-xs-12">
                                                <p style="margin-top: 15px;">
                                                    <?php
                                                    $webinar_url = get_admin_url( null, 'admin.php?page=webinarignition-dashboard&id=' . $single_webinar['ID'] . '#tab8' );
                                                    $webinar_link = "<strong><a href='$webinar_url' target='_blank'>";
                                                    $webinar_link .= __('Webinarignition Notifications setting', 'wp2leads_itm_webinarignition');
                                                    $webinar_link .= "</a></strong>";
                                                    ?><small>
                                                        <?php
                                                        echo sprintf( __( 'Visit %s and make settings for all enabled notofications', 'wp2leads_itm_webinarignition' ),  $webinar_link)
                                                        ?>
                                                    </small><?php
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                } else {
                                    ?>
                                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-3">
                                        <p>
                                            <input
                                                    class="form-input"
                                                    type="text"
                                                    id="wp2leads_itm_webinarignition_map_title_<?php echo $single_webinar['ID']; ?>"
                                                    placeholder="<?php echo __('Input Map Name', 'wp2leads_itm_webinarignition'); ?>"
                                            >
                                        </p>
                                    </div>

                                    <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-4">
                                        <p>
                                            <button
                                                    id="wp2leads_itm_webinarignition_generate_webinar_map_<?php echo $single_webinar['ID']; ?>"
                                                    type="button"
                                                    class="button button-small button-primary generate_webinarignition_map"
                                                    data-id="<?php echo $single_webinar['ID']; ?>"
                                                    data-type="<?php echo $webinar_type; ?>"
                                            >
                                                <?php echo __('Generate Map', 'wp2leads_itm_webinarignition'); ?>
                                            </button>

                                            <?php
                                            if (empty($single_webinar["leads"])) {
                                                ?>
                                                <select id="wp2leads_itm_webinarignition_dummy_user_<?php echo $single_webinar['ID']; ?>">
                                                    <option value="add" selected>
                                                        <?php echo __('Add dummy user', 'wp2leads_itm_webinarignition'); ?>
                                                    </option>

                                                    <option value="off">
                                                        <?php echo __('Do not add dummy user', 'wp2leads_itm_webinarignition'); ?>
                                                    </option>
                                                </select>
                                                <?php
                                            } else {
                                                ?>
                                                <input type="hidden" value="off" id="wp2leads_itm_webinarignition_dummy_user_<?php echo $single_webinar['ID']; ?>">
                                                <?php
                                            }
                                            ?>
                                        </p>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <hr style="border-top-color: #ccc; margin: 5px 0;">
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>

            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <p>
                        <?php echo __('To have webinar-, campaigns-, fields-, tags-, names sorted, the suggestion is to use the "Webinar Map Name" in every name as prefix.', 'wp2leads_itm_webinarignition'); ?>
                    </p>

                    <p>
                        <?php echo __('Example prefix', 'wp2leads_itm_webinarignition'); ?>:
                        <strong><?php echo __('SW WEBI SALE1 (SW = saleswonder.biz, WEBI = Webinar, SALES1 = Name of the Webinar)', 'wp2leads_itm_webinarignition'); ?></strong>
                    </p>

                    <p>
                        <?php echo __('Example prefix usage:', 'wp2leads_itm_webinarignition'); ?><strong><?php echo __('', 'wp2leads_itm_webinarignition'); ?></strong>
                    </p>

                    <ul>
                        <li>
                            <?php echo __('campaign name', 'wp2leads_itm_webinarignition'); ?><strong>:
                                <?php echo __('SW WEBI SALE1 VL-WebinarIgnition-001', 'wp2leads_itm_webinarignition'); ?></strong>,
                            <?php echo __('Replace', 'wp2leads_itm_webinarignition'); ?>
                            <strong><?php echo __('"Import"', 'wp2leads_itm_webinarignition'); ?></strong> <?php echo __('with', 'wp2leads_itm_webinarignition'); ?> <strong><?php echo __('"SW WEBI SALE1"', 'wp2leads_itm_webinarignition'); ?></strong>
                        </li>
                        <li>
                            <?php echo __('tags name', 'wp2leads_itm_webinarignition'); ?>: <strong><?php echo __('SW WEBI SALE1 CTA Seen, ...', 'wp2leads_itm_webinarignition'); ?></strong>
                        </li>
                        <li>
                            <?php echo __('fields name', 'wp2leads_itm_webinarignition'); ?>: <strong><?php echo __('SW WEBI SALE1 WI: Webinar Title', 'wp2leads_itm_webinarignition'); ?></strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="wptl-settings-group closed">
        <div class="wptl-settings-group-header wptl-settings-group-header-accordeon">
            <h3><?php _e('Documentation', 'wp2leads_itm_webinarignition') ?></h3>

            <span class="dashicons dashicons-arrow-up-alt2"></span>
            <span class="dashicons dashicons-arrow-down-alt2"></span>
        </div>

        <div class="wptl-settings-group-body" style="display:none;">
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <h4>
                        <?php echo __('Instant transfer module for WebinarIgnition plugin generating and sending following data to Klick Tipp:', 'wp2leads_itm_webinarignition') ?>
                    </h4>
                </div>
            </div>

            <div class="wptl-row">
                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <h4>
                        <?php echo __('12 Klick Tipp fields:', 'wp2leads_itm_webinarignition') ?>
                    </h4>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('<strong>"Email"</strong>, <strong>"First Name"</strong>, <strong>"WI: Webinar Title"</strong> (type: string), <strong>"WI: Webinar URL"</strong> (type: url), <strong>"WI: Webinar Start Date"</strong> (type: date & time), <strong>"WI: Webinar End Date"</strong> (type: date & time), <strong>"WI: Webinar Length"</strong> (type: number), <strong>"WI: User Status"</strong> (type: string), <strong>"WI: User Register Date"</strong> (type: date & time), <strong>"WI: User Joined Date"</strong> (type: date & time), <strong>"WI: User Exited Date"</strong> (type: date & time), <strong>"WI: User Time online"</strong> (type: number).', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <h4>
                        <?php echo __('6 status tags:', 'wp2leads_itm_webinarignition') ?>
                    </h4>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('<strong>"Registered"</strong>, <strong>"Online"</strong>, <strong>"Completed"</strong>, <strong>"Exited"</strong>, <strong>"50% Viewed"</strong>, <strong>"CTA Seen"</strong>', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>
            </div>

            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <h4>
                        <?php echo __('After you connect your webinar, made initial settings and enable instant transfer module, following statuses and field values will be send to Klick Tipp:', 'wp2leads_itm_webinarignition') ?>
                    </h4>
                </div>
            </div>

            <div class="wptl-row">
                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('After user submit registration form and was redirected to next page, following fields are filled:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('<strong>"Email"</strong>, <strong>"First Name"</strong>, <strong>"WI: Webinar URL"</strong>, <strong>"WI: Webinar Start Date"</strong>, <strong>"WI: Webinar End Date"</strong>, <strong>"WI: Webinar Length"</strong>, <strong>"WI: User Status"</strong> - <strong>"Registered"</strong>, <strong>"WI: User Register Date"</strong> and <strong>"Registered"</strong> tag', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('If webinar type is <strong>Live</strong>:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('fields <strong>"WI: Webinar End Date"</strong> and <strong>"WI: Webinar Length"</strong> will be calculated and send to Klick Tipp after webinar finished', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('When user visit webinar page and webinar status is <strong>"Online"</strong>:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('field <strong>"WI: User Joined Date"</strong> will be filled, field <strong>"WI: User Status"</strong> will change to <strong>"Online"</strong>, tag <strong>"Registered"</strong> will be detached and tag <strong>"Online"</strong> will be added', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('While user online:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('plugin calculating time for <strong>"WI: User Time online"</strong> field in minutes. This value will be sent to Klick Tipp only when user exit before webinar finished', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('If user is online more than 50% of webinar length:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('<strong>"50% Viewed"</strong> tag will be added for this user. This tag will not be sent to Klick Tipp until user exit before webinar end. For live webinar this value will be calculated after webinar finished, and plugins knows the lengths', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('If during webinar CTA section is shown:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('<strong>"CTA Seen"</strong> tag will be added for this user. This tag will only be sent to Klick Tipp when user exit before webinar end', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('If user leave before webinar finished:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('fields - <strong>"WI: User Exited Date"</strong> and <strong>"WI: User Time online"</strong> will be generated, <strong>"WI: User Status"</strong> field will change to <strong>"Exited"</strong>, tag <strong>"Online"</strong> will be detached and tag <strong>"Exited"</strong> will be generated. If tags <strong>"50% Viewed"</strong> and <strong>"CTA Seen"</strong> where generated they will be attached for this user. All data will be sent to Klick Tipp', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('After webinar finish and user is <strong>Online</strong>:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('<strong>"WI: User Status"</strong> field will change to <strong>"Completed"</strong>, tag <strong>"Online"</strong> will be detached and tag <strong>"Completed"</strong> will be generated. Data will be sent to Klick Tipp. <strong>Important</strong>: Field <strong>"WI: User Time online"</strong>, tags <strong>"50% Viewed"</strong> and <strong>"CTA Seen"</strong> will not be sent to Klick Tipp', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('Live webinar will be completed:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('when host change webinar status to <strong>Replay</strong> or <strong>Closed</strong> or if 98% users left webinar page. After this <strong>"WI: Webinar End Date"</strong> and <strong>"WI: Webinar Length"</strong> will be calculated. All fields and tags for users will be calculated and sent to Klick Tipp.', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
                    <p>
                        <?php echo __('On Replay:', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>

                <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
                    <p>
                        <?php echo __('If user visits webinar page with webinar status <strong>Replay</strong> and his status is not <strong>"Completed"</strong> all data will be generated and sent to Klick Tipp in same way as in the list above.', 'wp2leads_itm_webinarignition') ?>
                    </p>
                </div>
            </div>

            <?php
            if (false) {
                ?>
                <div class="wptl-row">
                    <div class="wptl-col-xs-12">
                        <p>
                            <?php echo __('Instant transfer module for WebinarIgnition plugin generating and sending following data to Klick Tipp:', 'wp2leads_itm_webinarignition') ?>
                        </p>

                        <ul style="list-style-type: disc;">
                            <li>
                                <?php echo __('6 status tags: <strong>"Registered"</strong>, <strong>"Online"</strong>, <strong>"Completed"</strong>, <strong>"Exited"</strong>, <strong>"50% Viewed"</strong>, <strong>"CTA Seen"</strong>', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('12 Klick Tipp fields: <strong>"Email"</strong>, <strong>"First Name"</strong>, <strong>"WI: Webinar Title"</strong> (type: string), <strong>"WI: Webinar URL"</strong> (type: url), <strong>"WI: Webinar Start Date"</strong> (type: date & time), <strong>"WI: Webinar End Date"</strong> (type: date & time), <strong>"WI: Webinar Length"</strong> (type: number), <strong>"WI: User Status"</strong> (type: string), <strong>"WI: User Register Date"</strong> (type: date & time), <strong>"WI: User Joined Date"</strong> (type: date & time), <strong>"WI: User Exited Date"</strong> (type: date & time), <strong>"WI: User Time online"</strong> (type: number).', 'wp2leads_itm_webinarignition') ?>
                            </li>
                        </ul>

                        <p>
                            <?php echo __('After you connect your webinar, made initial settings and enable instant transfer module, following statuses and field values will be send to Klick Tipp:', 'wp2leads_itm_webinarignition') ?>
                        </p>

                        <ul style="list-style-type: disc;">
                            <li>
                                <?php echo __('After user submit registration form and was redirected to next page, following fields are filled:</br><strong>"Email"</strong>, <strong>"First Name"</strong>, <strong>"WI: Webinar URL"</strong>, <strong>"WI: Webinar Start Date"</strong>, <strong>"WI: Webinar End Date"</strong>, <strong>"WI: Webinar Length"</strong>, <strong>"WI: User Status"</strong> - <strong>"Registered"</strong>, <strong>"WI: User Register Date"</strong> and <strong>"Registered"</strong> tag', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('If webinar type is <strong>Live</strong>:</br> fields <strong>"WI: Webinar End Date"</strong> and <strong>"WI: Webinar Length"</strong> will be calculated and send to Klick Tipp after webinar finished', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('When user visit webinar page and webinar status is <strong>"Online"</strong>:</br>field <strong>"WI: User Joined Date"</strong> will be filled, field <strong>"WI: User Status"</strong> will change to <strong>"Online"</strong>, tag <strong>"Registered"</strong> will be detached and tag <strong>"Online"</strong> will be added', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('While user online:</br>plugin calculating time for <strong>"WI: User Time online"</strong> field in minutes. This value will be sent to Klick Tipp only when user exit before webinar finished', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('If user is online more than 50% of webinar length:</br><strong>"50% Viewed"</strong> tag will be added for this user. This tag will not be sent to Klick Tipp until user exit before webinar end. For live webinar this value will be calculated after webinar finished, and plugins knows the lengths', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('If during webinar CTA section is shown:</br> <strong>"CTA Seen"</strong> tag will be added for this user. This tag will only be sent to Klick Tipp when user exit before webinar end', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('If user leave before webinar finished:</br> fields - <strong>"WI: User Exited Date"</strong> and <strong>"WI: User Time online"</strong> will be generated, <strong>"WI: User Status"</strong> field will change to <strong>"Exited"</strong>, tag <strong>"Online"</strong> will be detached and tag <strong>"Exited"</strong> will be generated. If tags <strong>"50% Viewed"</strong> and <strong>"CTA Seen"</strong> where generated they will be attached for this user. All data will be sent to Klick Tipp', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('After webinar finish and user is <strong>Online</strong>:</br><strong>"WI: User Status"</strong> field will change to <strong>"Completed"</strong>, tag <strong>"Online"</strong> will be detached and tag <strong>"Completed"</strong> will be generated. Data will be sent to Klick Tipp. <strong>Important</strong>: Field <strong>"WI: User Time online"</strong>, tags <strong>"50% Viewed"</strong> and <strong>"CTA Seen"</strong> will not be sent to Klick Tipp', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('Live webinar will be completed:</br> when host change webinar status to <strong>Replay</strong> or <strong>Closed</strong> or if 98% users left webinar page. After this <strong>"WI: Webinar End Date"</strong> and <strong>"WI: Webinar Length"</strong> will be calculated. All fields and tags for users will be calculated and sent to Klick Tipp.', 'wp2leads_itm_webinarignition') ?>
                            </li>

                            <li>
                                <?php echo __('On Replay:</br>If user visits webinar page with webinar status <strong>Replay</strong> and his status is not <strong>"Completed"</strong> all data will be generated and sent to Klick Tipp in same way as in the list above.', 'wp2leads_itm_webinarignition') ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
