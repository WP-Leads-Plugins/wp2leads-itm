<?php
class Wp2leadsItmWebinarignitionSettings {
    public static function get_settings() {
        return array_merge(self::get_default_settings(), get_option('wp2leads_itm_webinarignition_settings', array()));
    }

    public static function get_default_settings() {
        return array(
            'wi_email_notification' => 'off',
            'optin_redirect' => '',
        );
    }

    public static function generate_webinar_map() {
        if (empty($_POST["app_id"]) || empty($_POST["app_type"])) self::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')));
        if (empty($_POST["map_title"])) self::error_response(array('message' => __('Missing map title', 'wp2leads_itm_webinarignition')));

        $map = Wp2leadsItmWebinarignitionMap::download_map(sanitize_text_field($_POST["app_type"]));

        if (empty($map)) self::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')));

        $new_map_mapping = unserialize($map['mapping']);
        unset($new_map_mapping["comparisons"]);

        $new_map_mapping["comparisons"][] = array(
            'tableColumn' => 'webinarignition.ID',
            'conditions' => array(
                0 => array (
                    'operator' => 'like',
                    'string' => $_POST["app_id"],
                ),
            ),
        );

        $new_map_api = unserialize($map['api']);
        $new_map_info = unserialize($map['info']);
        $new_map_info["initial_settings"] = false;
        $optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());

        if (!empty($optin_pages["optin"])) {
            $new_map_api["default_optin"] = $optin_pages["optin"];
        }
        $new_map_info["domain"] = Wp2leads_License::get_current_site();
        // unset($new_map_info["publicMapId"]);
        unset($new_map_info["publicMapHash"]);
        unset($new_map_info["publicMapContent"]);
        unset($new_map_info["publicMapOwner"]);
        unset($new_map_info["publicMapStatus"]);
        unset($new_map_info["publicMapVersion"]);
        unset($new_map_info["map_export_id"]);

        global $wpdb;

        $result = $wpdb->insert(
            $wpdb->prefix . 'wp2l_maps',
            [
                'time' => wp2leads_itm_webinarignition_convertTimeToLocal(date('Y-m-d H:i:s')),
                'name' => sanitize_text_field($_POST["map_title"]),
                'mapping' => serialize($new_map_mapping),
                'api' => serialize($new_map_api),
                'info'  => serialize($new_map_info),
            ],
            ['%s', '%s', '%s', '%s', '%s']
        );

        if (empty($result))  self::error_response(array('message' => __('Can not generate a map', 'wp2leads_itm_webinarignition')));

        $map_id = $wpdb->insert_id;

        $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());

        $webinar_to_map[$_POST["app_id"]] = $map_id;

        // delete_option('wp2leads_itm_webinarignition_webinar_to_map');
        update_option('wp2leads_itm_webinarignition_webinar_to_map', $webinar_to_map);

        if (!empty($_POST["add_user"]) && $_POST["add_user"] === 'add') {
            self::add_dummy_user($_POST["app_type"], $_POST["app_id"]);
        }

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('Settings saved.', 'wp2leads_itm_webinarignition'),
            'url' => get_admin_url( null, 'admin.php?page=wp2l-admin&tab=map_to_api&active_mapping=' . $map_id ),
        );

        self::success_response($params);
    }

    public static function add_dummy_user($app_type, $app_id) {
        global $wpdb;
        $request = wp_remote_get('https://randomuser.me/api/?nat=us,gb');
        $response = json_decode(wp_remote_retrieve_body( $request ), true);
        $fake_info = $response['results'][0];

        $name = $fake_info["name"]["first"] . ' ' . $fake_info["name"]["last"];
        $email = $fake_info["email"];
        $email_array = explode('@', $email);
        $email = $email_array[0] . '@ds.ds';
        $phone = $fake_info["phone"];
        $table_name = 'evergreen' === $app_type ? $wpdb->prefix . 'wp2leads_itm_webinarignition_leads_evergreen' : $wpdb->prefix . 'wp2leads_itm_webinarignition_leads';
        $wi_table_name = 'evergreen' === $app_type ? $wpdb->prefix . 'webinarignition_leads_evergreen' : $wpdb->prefix . 'webinarignition_leads';

        $wpdb->insert( $wi_table_name,
            array(
                'app_id' => $app_id,
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
            )
        );

        $w_id = $wpdb->insert_id;

        $wpdb->insert( $table_name,
            array(
                'w_id' => $w_id,
                'app_id' => $app_id,
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
            )
        );

        return;
    }

    public static function delete_webinar_map() {
        if (empty($_POST["app_id"])) self::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')));
        $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());
        $app_id = sanitize_text_field($_POST["app_id"]);

        if (empty($webinar_to_map[$app_id])) self::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')));

        $map_id = $webinar_to_map[$app_id];
        $map = MapsModel::get($map_id);

        if (empty($map)) {
            unset($webinar_to_map[$app_id]);
        } else {
            global $wpdb;

            $wpdb->delete( $wpdb->prefix . 'wp2l_maps', array( 'id' => $map_id ) );
            unset($webinar_to_map[$app_id]);
        }

        // delete_option('wp2leads_itm_webinarignition_webinar_to_map');
        update_option('wp2leads_itm_webinarignition_webinar_to_map', $webinar_to_map);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('Map deleted.', 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function generate_optin_page() {
        if (empty($_POST["page_type"])) self::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')));
        if (empty($_POST["page_title"])) self::error_response(array('message' => __('Missing page title', 'wp2leads_itm_webinarignition')));

        $page_title = sanitize_text_field($_POST["page_title"]);
        $page_type = sanitize_text_field($_POST["page_type"]);

        $page_create = array(
            'post_title'   => $page_title,
            'post_status'   => 'publish',
            'post_type'     => 'page',
            'post_author'   => wp_get_current_user(),
            'post_content'  => ''
        );

        if ('confirmation' === $page_type) {
            $page_create['post_content'] = wp2leads_itm_webinarignition_get_confirmation_page_content();
        }

        $post_id = wp_insert_post(  wp_slash( $page_create ) );
        $optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());
        $optin_pages[$page_type] = $post_id;

        delete_option( 'wp2leads_itm_webinarignition_optin_pages' );
        update_option('wp2leads_itm_webinarignition_optin_pages', $optin_pages);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('Page created.', 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function delete_optin_page() {
        if (empty($_POST["page_type"])) self::error_response(array('message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')));
        $page_type = sanitize_text_field($_POST["page_type"]);
        $optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());

        if (!empty($optin_pages[$page_type])) {
            $optin_page_id = $optin_pages[$page_type];
            $optin_page = get_post($optin_page_id);

            if ($optin_page) {
                wp_delete_post( $optin_page_id, true );
            }

            unset($optin_pages[$page_type]);
        }

        delete_option( 'wp2leads_itm_webinarignition_optin_pages' );
        update_option('wp2leads_itm_webinarignition_optin_pages', $optin_pages);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('Page deleted.', 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function save_optin_process() {
        if (empty($_POST["optin"])) self::error_response(array('message' => __('Select Opt-in process', 'wp2leads_itm_webinarignition')));
        $optin = sanitize_text_field($_POST["optin"]);
        $optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());
        $optin_pages['optin'] = $optin;


        delete_option( 'wp2leads_itm_webinarignition_optin_pages' );
        update_option('wp2leads_itm_webinarignition_optin_pages', $optin_pages);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('Opt-in process saved.', 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function save_apikey() {
        if (empty($_POST["apikey"])) self::error_response(array('message' => __('Missing API Key', 'wp2leads_itm_webinarignition')));
        $apikey = sanitize_text_field($_POST["apikey"]);
        $optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());
        $optin_pages['apikey'] = $apikey;


        delete_option( 'wp2leads_itm_webinarignition_optin_pages' );
        update_option('wp2leads_itm_webinarignition_optin_pages', $optin_pages);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('API Key saved.', 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function delete_apikey() {
        $optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());

        if (!empty($optin_pages['apikey'])) {
            unset($optin_pages['apikey']);
        }

        delete_option( 'wp2leads_itm_webinarignition_optin_pages' );
        update_option('wp2leads_itm_webinarignition_optin_pages', $optin_pages);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('API Key deleted.', 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function save_settings() {
        if (empty($_POST['formData']) || !is_array($_POST['formData'])) {
            $params = array(
                'message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')
            );

            self::error_response($params);
        }

        $data = array();

        foreach ($_POST['formData'] as $form_data) {
            $data[sanitize_text_field($form_data['name'])] = $form_data['value'];
        }

        delete_option( 'wp2leads_itm_webinarignition_settings' );
        update_option('wp2leads_itm_webinarignition_settings', $data);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __('Settings saved.', 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function save_webinar_to_map_settings() {
        if (empty($_POST['app_id']) || empty($_POST['webinar_to_map_settings'])) {
            $params = array(
                'message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')
            );

            self::error_response($params);
        }


        $app_id = sanitize_text_field($_POST['app_id']);
        $webinar_data = Wp2leadsItmWebinarignitionManager::get_webinar_data($app_id);

        if (empty($webinar_data)) {
            $params = array(
                'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __("Settings for webinar with ID {$_POST['app_id']} saved.", 'wp2leads_itm_webinarignition'),
                'reload' => 1,
            );

            self::success_response($params);
        }

        $settings_allowed = [
            'email_signup' => ['on', 'off'],
            'email_notiff_1' => ['on', 'off'],
            'email_notiff_2' => ['on', 'off'],
            'email_notiff_3' => ['on', 'off'],
            'email_notiff_4' => ['on', 'off'],
            'email_notiff_5' => ['on', 'off'],
        ];

        $settings = [];

        foreach ($_POST['webinar_to_map_settings'] as $key => $setting) {
            $key = sanitize_text_field($key);
            $setting = sanitize_text_field($setting);

            if (empty($settings_allowed[$key])) continue;
            $settings[$key] = in_array($setting, $settings_allowed[$key]) ? $setting : 'off';

            if (isset($webinar_data->$key)) {
                $webinar_data->$key = $settings[$key];
            }
        }

        delete_option( 'wp2leads_itm_webinarignition_webinar_to_map_settings_' . $app_id );
        update_option('wp2leads_itm_webinarignition_webinar_to_map_settings_' . $app_id, $settings);
        update_option('webinarignition_campaign_' . $app_id, $webinar_data);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_webinarignition') . ': ' . __("Settings for webinar with ID {$_POST['app_id']} saved.", 'wp2leads_itm_webinarignition'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function error_response($params = array()) {
        $response = array('success' => 0, 'error' => 1);

        if (!empty($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }

    public static function success_response($params = array()) {
        $response = array('success' => 1, 'error' => 0);

        if (!empty($params) && is_array($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }
}

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_settings_save', array('Wp2leadsItmWebinarignitionSettings', 'save_settings'));
add_action('wp_ajax_wp2leads_itm_webinarignition_settings_save', array('Wp2leadsItmWebinarignitionSettings', 'save_settings'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_generate_webinar_map', array('Wp2leadsItmWebinarignitionSettings', 'generate_webinar_map'));
add_action('wp_ajax_wp2leads_itm_webinarignition_generate_webinar_map', array('Wp2leadsItmWebinarignitionSettings', 'generate_webinar_map'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_save_webinar_to_map_settings', array('Wp2leadsItmWebinarignitionSettings', 'save_webinar_to_map_settings'));
add_action('wp_ajax_wp2leads_itm_webinarignition_save_webinar_to_map_settings', array('Wp2leadsItmWebinarignitionSettings', 'save_webinar_to_map_settings'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_delete_webinar_map', array('Wp2leadsItmWebinarignitionSettings', 'delete_webinar_map'));
add_action('wp_ajax_wp2leads_itm_webinarignition_delete_webinar_map', array('Wp2leadsItmWebinarignitionSettings', 'delete_webinar_map'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_generate_optin_page', array('Wp2leadsItmWebinarignitionSettings', 'generate_optin_page'));
add_action('wp_ajax_wp2leads_itm_webinarignition_generate_optin_page', array('Wp2leadsItmWebinarignitionSettings', 'generate_optin_page'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_delete_optin_page', array('Wp2leadsItmWebinarignitionSettings', 'delete_optin_page'));
add_action('wp_ajax_wp2leads_itm_webinarignition_delete_optin_page', array('Wp2leadsItmWebinarignitionSettings', 'delete_optin_page'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_save_apikey', array('Wp2leadsItmWebinarignitionSettings', 'save_apikey'));
add_action('wp_ajax_wp2leads_itm_webinarignition_save_apikey', array('Wp2leadsItmWebinarignitionSettings', 'save_apikey'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_delete_apikey', array('Wp2leadsItmWebinarignitionSettings', 'delete_apikey'));
add_action('wp_ajax_wp2leads_itm_webinarignition_delete_apikey', array('Wp2leadsItmWebinarignitionSettings', 'delete_apikey'));

add_action('wp_ajax_nopriv_wp2leads_itm_webinarignition_save_optin_process', array('Wp2leadsItmWebinarignitionSettings', 'save_optin_process'));
add_action('wp_ajax_wp2leads_itm_webinarignition_save_optin_process', array('Wp2leadsItmWebinarignitionSettings', 'save_optin_process'));
