<?php
class Wp2leadsItmWebinarignitionMap {
    public static function download_map($type) {
        $maps_from_server = MapBuilderManager::get_available_maps_from_server();

        if ('evergreen' === $type) {
            return self::download_evergreen_map($maps_from_server);
        } else {
            return self::download_live_map($maps_from_server);
        }
    }

    private static function download_evergreen_map($maps_from_server) {
        if (version_compare( WP2LEADS_VERSION, '3.0.0', '<' )) {
            $map_name = 'Webinarignition Evergreen Master';
        } else {
            $map_name = 'Webinarignition Evergreen v3 Master';
        }

        $map_id = false;
        foreach ($maps_from_server as $map_from_server) {
            if (false !== stripos ($map_from_server['name'], $map_name)) {
                $map_id = $map_from_server['map_id'];

                break;
            }
        }

        if (empty($map_id)) {
            return false;
        }


        $license_info = Wp2leads_License::get_lecense_info();
        $license_email = $license_info['email'];
        $license_key = $license_info['key'];
        $site_url = Wp2leads_License::get_current_site();

        $parameters = array (
            'license_email' => $license_email,
            'license_key' => $license_key,
            'site_url' => $site_url,
            'event' => 'import',
            'maps'  =>  serialize(array($map_id))
        );

        $request = wp_remote_post(
            base64_decode(MapBuilderManager::get_server()),
            array(
                'body'    => $parameters,
            )
        );

        $response = json_decode(wp_remote_retrieve_body( $request ), true);

        if (200 !== $response['code']) {
            return false;
        }

        $map = $response['body']['maps'][0];

        // $map = MapsModel::get(WI_EVERGREEN_MAP);
        return $map;
    }

    private static function download_live_map($maps_from_server) {
        if (version_compare( WP2LEADS_VERSION, '3.0.0', '<' )) {
            $map_name = 'Webinarignition Live Master';
        } else {
            $map_name = 'Webinarignition Live v3 Master';
        }

        $map_id = false;
        foreach ($maps_from_server as $map_from_server) {
            if (false !== stripos ($map_from_server['name'], $map_name)) {
                $map_id = $map_from_server['map_id'];

                break;
            }
        }

        if (empty($map_id)) {
            return false;
        }


        $license_info = Wp2leads_License::get_lecense_info();
        $license_email = $license_info['email'];
        $license_key = $license_info['key'];
        $site_url = Wp2leads_License::get_current_site();

        $parameters = array (
            'license_email' => $license_email,
            'license_key' => $license_key,
            'site_url' => $site_url,
            'event' => 'import',
            'maps'  =>  serialize(array($map_id))
        );

        $request = wp_remote_post(
            base64_decode(MapBuilderManager::get_server()),
            array(
                'body'    => $parameters,
            )
        );

        $response = json_decode(wp_remote_retrieve_body( $request ), true);

        if (200 !== $response['code']) {
            return false;
        }

        $map = $response['body']['maps'][0];

        // $map = MapsModel::get(WI_EVERGREEN_MAP);
        return $map;
//        $map = MapsModel::get(WI_LIVE_MAP);
//        return $map;
    }
}