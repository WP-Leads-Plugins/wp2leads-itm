<?php

class Wp2leadsItmWebinarignitionNotices {
    public static function show_message($notice_id) {
        add_action('admin_notices', 'Wp2leadsItmWebinarignitionNotices::' . $notice_id);
    }

    public static function requirements_failed() {
        $message = sprintf( __( 'Please, install all required plugins.', 'wp2leads_itm_webinarignition' ) );
        $wp2leads_installed = function_exists('run_wp2leads') && class_exists('Wp2leads_Background_Module_Transfer');
        if (!$wp2leads_installed) {
            $message .= ' <br> - ' . sprintf(
                /* translators: 1: open anchor tag, 2: close anchor tag */
                __( 'WP2LEADS plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_webinarignition' ),
                '<a href="/wp-admin/plugin-install.php?s=WP2LEADS&tab=search&type=term">',
                '</a>'
            );
        }

        if (!wp2leads_itm_webinarignition_is_wp2leads_correct_version()) {
            $message .= ' <br> - ' . sprintf(
                /* translators: 1: minimal WP2Leads version allowed 2: open anchor tag, 3: close anchor tag */
                __( 'WP2LEADS plugin minimal version %1$s is required. Update plugin %2$shere%3$s.', 'wp2leads_itm_webinarignition' ),
                wp2leads_itm_webinarignition_get_min_wp2leads_version(),
                '<a href="/wp-admin/plugins.php?plugin_status=active" target="_blank">',
                '</a>'
            );
        }

        if (!wp2leads_itm_webinarignition_is_plugin_activated( 'webinarignition', 'webinarignition.php' )) {
            $message .= ' <br> - ' . sprintf(
                /* translators: 1: open anchor tag, 2: close anchor tag */
                __( 'Webinarignition plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_webinarignition' ),
                '<a href="https://webinarignition.com/" target="_blank">',
                '</a>'
            );
        }
        ?>
        <div class="notice notice-error">
            <p>
                <strong><?php echo wp2leads_itm_webinarignition_get_plugin_name(); ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }
}
