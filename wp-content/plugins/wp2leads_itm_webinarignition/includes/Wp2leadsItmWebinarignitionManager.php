<?php

class Wp2leadsItmWebinarignitionManager
{
    private static $ultimate_plus_names = ['ultimate_powerup_tier3a'];
    public static function init() {
        if (!class_exists('Wp2leadsItmWebinarignitionModel')) include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
        if (!class_exists('WebinarignitionManager')) {
            $managerFile = WEBINARIGNITION_PATH . 'inc/class.WebinarignitionManager.php';
            if (file_exists($managerFile)) {
                include_once $managerFile;
            }
        }
        if (!class_exists('WebinarignitionLicense')) {
            $licenseFile = WEBINARIGNITION_PATH . 'inc/class.WebinarignitionLicense.php';
            if (file_exists($licenseFile)) {
                include_once $licenseFile;
            }
        }
    }

    public static function get_webinar_data($id) {
        if (method_exists('WebinarignitionManager', 'get_webinar_data')) {
            return WebinarignitionManager::get_webinar_data( $id );
        } else if (method_exists('WebinarignitionManager', 'webinarignition_get_webinar_data')) {
            return WebinarignitionManager::webinarignition_get_webinar_data( $id );
        }
        return false;
    }

    public static function get_webinarignition_license_level() {
        $wi_version = WEBINARIGNITION_VERSION;
        $is_new_wi = version_compare($wi_version, '4', '>=');
        $license_level = 'free';
        $license_title = __('Free', 'wp2leads_itm_webinarignition');

        if ($is_new_wi) {
            if (!method_exists('WebinarignitionLicense', 'webinarignition_get_license_level')) {
                return ['license_level' => $license_level, 'license_title' => $license_title];
            }

            $license_level_obj = WebinarignitionLicense::webinarignition_get_license_level();
            if (!empty($license_level_obj->name)) $license_level = $license_level_obj->name;
            if (!empty($license_level_obj->title)) $license_title = $license_level_obj->title;
        } else {
            if (method_exists('WebinarignitionLicense', 'get_license_level')) {
                $license_level_obj = WebinarignitionLicense::get_license_level();
                if (!empty($license_level_obj->name)) $license_level = $license_level_obj->name;
                if (!empty($license_level_obj->title)) $license_title = $license_level_obj->title;
            }
        }

        $license_level = in_array($license_level, self::$ultimate_plus_names) ? 'ultimate_plus' : $license_level;

        return ['license_level' => $license_level, 'license_title' => $license_title];
    }

    public static function add_hooks() {
        add_filter('wp2leads_is_map_transfer_allowed', 'Wp2leadsItmWebinarignitionManager::is_map_transfer_allowed', 999, 3);
        add_filter('wp2leads_is_module_usable', 'Wp2leadsItmWebinarignitionManager::is_module_usable', 999, 2);
        add_filter('wp2leads_map_to_api_module_not_allowed_message', 'Wp2leadsItmWebinarignitionManager::map_to_api_module_not_allowed_message', 999, 3);
    }

    public static function is_map_transfer_allowed($allowed, $map_id, $map) {
        global $webinarignition_license_level;
        $decoded_mapping = unserialize($map->mapping);
        if (
            empty($decoded_mapping['transferModule']) ||
            (
                $decoded_mapping['transferModule'] !== 'wp2leads_webinarignition_evergreen' &&
                $decoded_mapping['transferModule'] !== 'wp2leads_webinarignition_live'
            )
        ) {
            return $allowed;
        }

        if (empty($webinarignition_license_level)) {
            $webinarignition_license_level = Wp2leadsItmWebinarignitionManager::get_webinarignition_license_level();
        }
        return 'ultimate_plus' === $webinarignition_license_level['license_level'];
    }

    public static function is_module_usable($usable, $module_id) {
        global $webinarignition_license_level;
        if (!in_array($module_id, ['wp2leads_webinarignition_evergreen', 'wp2leads_webinarignition_live'])) {
            return $usable;
        }

        if (empty($webinarignition_license_level)) {
            $webinarignition_license_level = Wp2leadsItmWebinarignitionManager::get_webinarignition_license_level();
        }
        return 'ultimate_plus' === $webinarignition_license_level['license_level'];
    }

    public static function map_to_api_module_not_allowed_message($warning_message, $activeMapId, $map) {
        $decoded_mapping = unserialize($map->mapping);
        if (
            empty($decoded_mapping['transferModule']) ||
            (
                $decoded_mapping['transferModule'] !== 'wp2leads_webinarignition_evergreen' &&
                $decoded_mapping['transferModule'] !== 'wp2leads_webinarignition_live'
            )
        ) {
            return $warning_message;
        }

        $admin_url = admin_url('admin.php?page=webinarignition-dashboard-pricing');

        ob_start();
        echo sprintf(
            /* translators: 1: open anchor tag, 2: close anchor tag */
            __(
                'To transfer in real time, automatically and not just the current user in the Map to API tab, please use %1$sULTIMATE Unlimited Plus%2$s Webinarignition version.',
                'wp2leads_itm_webinarignition'
            ),
            '<a href="' . $admin_url . '" target="_blank">',
            '</a>'
        );
        $warning_message = ob_get_clean();

        return $warning_message;
    }
}
