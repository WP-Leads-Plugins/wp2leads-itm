<?php
class Wp2leadsItmWebinarignitionRedirection {
    public static function redirection($original_template) {
        $license_level = Wp2leadsItmWebinarignitionManager::get_webinarignition_license_level();
        if ('ultimate_plus' !== $license_level['license_level']) return $original_template;

        $input_get     = filter_input_array(INPUT_GET);
        global $post;

        if (empty($post)) return $original_template;

        $post_id = $post->ID;
        $optin_pages = get_option('wp2leads_itm_webinarignition_optin_pages', array());

        $confirmation_page = !empty($optin_pages['confirmation']) && (int)$post_id === (int)$optin_pages['confirmation'];
        $thankyou_page = !empty($optin_pages['thankyou']) && (int)$post_id === (int)$optin_pages['thankyou'];

        if (!empty($confirmation_page) || !empty($thankyou_page)) {
            if (empty($input_get["email"])) return $original_template;

            if (!class_exists('Wp2leadsItmWebinarignitionModel')) include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';

            $lead = Wp2leadsItmWebinarignitionModel::get_app_by_email($input_get["email"]);
            if (empty($lead)) return $original_template;

            $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());
            if (empty($webinar_to_map[$lead['app_id']])) return $original_template;

            $map = MapsModel::get($webinar_to_map[$lead['app_id']]);
            $map_api = unserialize($map->api);
            $is_confirmed = Wp2leadsItmWebinarignitionKlickTipp::is_user_confirmed($input_get["email"], $map_api['default_optin']);
            $webinar_id = $lead["app_id"];
            $webinar_data = get_option('webinarignition_campaign_' . $webinar_id);

            $urlId = $lead['w_id'];
            $is_lead_protected = !empty($webinar_data->protected_lead_id) && 'protected' === $webinar_data->protected_lead_id;

            if ($is_lead_protected) {
                $webinar_type = webinarignition_is_auto($webinar_data) ? 'evergreen' : 'live';
                $wi_table = 'evergreen' === $webinar_type ? 'wi_leads_evergreen' : 'wi_leads';
                $wi_lead = Wp2leadsItmWebinarignitionModel::get_lead($wi_table, array('ID' => $lead['w_id']));
                $urlId = $wi_lead['hash_ID'];
            }

            $thank_you_page_url = $webinar_data->webinar_permalink . '?confirmed&lid=' . $urlId;

            if (!empty($webinar_data->skip_ty_page) && 'yes' === $webinar_data->skip_ty_page) {
                $thank_you_page_url = $webinar_data->webinar_permalink . '?live&lid=' . $urlId;
            }

            if (!empty($is_confirmed["confirmed"])) self::redirect($thank_you_page_url);
            if (!empty($thankyou_page)) self::selected_optin_redirect($webinar_id, $map_api['default_optin'], $lead['email']);

            return $original_template;
        }

        $values = get_post_custom($post_id);

        if ( empty( $values['webinarignitionx_meta_box_select'] ) || empty( $values['webinarignitionx_meta_box_select'][0] ) || ( $values['webinarignitionx_meta_box_select'][0] == '0') ) { return $original_template; }

        $webinarignitionxSelected = $values['webinarignitionx_meta_box_select'];
        $webinar_id = $webinarignitionxSelected[0];

        if ($webinar_id !== "0" && !empty($webinar_id)) {
            $webinar_id     = urlencode($webinar_id);
            $webinar_data = get_option('webinarignition_campaign_' . $webinar_id);
            $webinar_type = webinarignition_is_auto($webinar_data) ? 'evergreen' : 'live';
            $wp2l_table = 'evergreen' === $webinar_type ? 'leads_evergreen' : 'leads';
            $wi_table = 'evergreen' === $webinar_type ? 'wi_leads_evergreen' : 'wi_leads';

            if (isset($input_get['confirmed']) || isset($input_get['live'])) {
                if (!is_user_logged_in() && ($webinar_data->paid_status == "paid" && !isset($input_get[md5($webinar_data->paid_code)]))) {
                    return $original_template;
                }

                if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
                    include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
                }

                if (!empty($input_get['lid'])) {
                    $leadId = $input_get['lid'];
                    setcookie('we-trk-' . $webinar_id, $leadId, time() + (30 * 24 * 60 * 60));
                } elseif (!empty($input_get['email'])) {
                    $getLiveIDByEmail = webinarignition_live_get_lead_by_email($webinar_id, $input_get['email']);
                    $leadId = $getLiveIDByEmail->ID;
                    setcookie('we-trk-' . $webinar_id, $leadId, time() + (30 * 24 * 60 * 60));
                } elseif( !empty($_COOKIE['we-trk-' . $webinar_id]) ) {
                    $leadId = $_COOKIE['we-trk-' . $webinar_id];
                } elseif(isset($input_get['lid'])) {
                    $wp2leads_itm_lead = wp2leads_itm_webinarignition_get_cookie($webinar_id);
                    $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $webinar_id, 'ID' => $wp2leads_itm_lead["wp2leads_lid"]));

                    if (!empty($lead)) {
                        $wi_lead = Wp2leadsItmWebinarignitionModel::get_lead($wi_table, array('email' => $lead['email'], 'app_id' => $webinar_id));

                        if (!empty($wi_lead)) {
                            setcookie('we-trk-' . $webinar_id, $wi_lead['ID'], time() + (30 * 24 * 60 * 60));
                            $request_uri = get_site_url('/') . $_SERVER["REQUEST_URI"];
                            $request_uri_parsed = parse_url($request_uri);

                            if (!empty($request_uri_parsed["host"]) && !empty($request_uri_parsed["path"]) && !empty($request_uri_parsed["query"])) {
                                parse_str($request_uri_parsed["query"], $query_array);
                                $query_array['lid'] = $wi_lead['ID'];
                                $request_uri_parsed["query"] = http_build_query($query_array);

                                $redirect_url = '';

                                if (!empty($request_uri_parsed["scheme"])) {
                                    $redirect_url .= $request_uri_parsed["scheme"] . '://';
                                }

                                $redirect_url .= $request_uri_parsed["host"] . $request_uri_parsed["path"] . '?' . $request_uri_parsed["query"];

                                self::redirect($redirect_url);
                            }
                        }
                    }
                }

                if (empty($leadId)) return $original_template;

                if (is_numeric($leadId)) {
                    $wi_lead = Wp2leadsItmWebinarignitionModel::get_lead($wi_table, array('ID' => $leadId));
                }

                if (empty($wi_lead)) {
                    $wi_lead = Wp2leadsItmWebinarignitionModel::get_lead($wi_table, array('hash_ID' => $leadId));
                }

                if (empty($wi_lead)) return $original_template;

                $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $webinar_id, 'email' => $wi_lead['email']));

                if (empty($lead)) return $original_template;

                $data = array();

                if (empty($lead["w_id"]) || (int)$lead["w_id"] !== (int)$wi_lead['ID']) $data['w_id'] = $leadId;
                if ('Pending' === $lead["status"]) $data['status'] = 'Registered';
                if (empty($lead["w_permalink"])) $data['w_permalink'] = $webinar_data->webinar_permalink . '?live&lid=' . $leadId;

                if (!empty($data)) {
                    $data['ID'] = $lead['ID'];
                    Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);

                    if ('evergreen' === $webinar_type) {
                        do_action('wp2leads_itm_webinarignition_leads_evergreen_updated', $leadId, $webinar_id);
                    } else {
                        do_action('wp2leads_itm_webinarignition_leads_updated', $leadId, $webinar_id);
                    }
                }

                if (isset($input_get['live'])) return $original_template;

                $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());

                if (empty($webinar_to_map[$webinar_id])) return $original_template;
                $map = MapsModel::get($webinar_to_map[$webinar_id]);
                if (empty($map)) return $original_template;

                $map_api = unserialize($map->api);
                $module_key = 'evergreen' === $webinar_type ? 'wp2leads_webinarignition_evergreen' : 'wp2leads_webinarignition_live';
                $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

                if (empty($existed_modules_map[$module_key][$webinar_to_map[$webinar_id]])) return $original_template;

                $is_confirmed = Wp2leadsItmWebinarignitionKlickTipp::is_user_confirmed($wi_lead['email'], $map_api["default_optin"]);

                if (empty($is_confirmed) || empty($is_confirmed['confirmed'])) {
                    self::selected_optin_redirect($webinar_id, $map_api["default_optin"], $wi_lead['email']);
                } else {
                    Wp2leadsItmWebinarignitionModel::email_confirmed($webinar_id, $wi_lead['email']);
                }
            }
        }
        return $original_template;
    }

    public static function redirect($url) {
        wp_redirect($url);
        exit;
    }

    public static function selected_optin_redirect($webinar_id, $default_optin, $email) {
        $connector = new Wp2leadsItmWebinarignitionKlickTippConnector();
        $username = get_option('wp2l_klicktipp_username');
        $password = get_option('wp2l_klicktipp_password');
        $connector->login($username, $password);
        $selected_optin_redirect = $connector->subscription_process_redirect ($default_optin, $email);

        if (!empty($selected_optin_redirect)) {
            Wp2leadsItmWebinarignitionModel::email_to_confirm($webinar_id, $email);
            self::redirect($selected_optin_redirect);
        }
    }
}

add_action('template_include', array('Wp2leadsItmWebinarignitionRedirection', 'redirection'), 3);

?>
