<?php

class Wp2leadsItmWebinarignitionWp2leadsHooks
{
    public static function available_options_map_columns($columns, $mapping) {
        if (empty($mapping) || empty($mapping["transferModule"]) || 'wp2leads_webinarignition_evergreen' !== $mapping["transferModule"]) {
            return $columns;
        }

        global $wpdb;

        $sql = "
            SELECT lead_id, meta_key, meta_value
            FROM {$wpdb->prefix}webinarignition_lead_evergreenmeta
            WHERE meta_key = 'wiRegForm'
            ORDER BY lead_id
            DESC;
        ";

        $lead_metas = $wpdb->get_results($sql, ARRAY_A);

        if (empty($lead_metas)) {
            return $columns;
        }

        $virtual_columns = array();

        foreach ($lead_metas as $lead_meta) {
            $lead_meta_unserialise = maybe_unserialize( $lead_meta['meta_value'] );

            foreach ($lead_meta_unserialise as $field => $meta) {
                $virtual_columns[] = "v.registration_{$field}";
            }
        }

        $virtual_columns = array_unique($virtual_columns);

        foreach ($virtual_columns as $column) {
            if (!in_array($column, $columns)) $columns[] = $column;
        }

        return $columns;
    }
    public static function map_results_dynamic_before_comparison($results, $map) {
        if (empty($map) || empty($map["transferModule"]) || 'wp2leads_webinarignition_evergreen' !== $map["transferModule"]) {
            return $results;
        }

        $lead_ids = array();

        foreach ($results as $result) {
            if (empty($result->{'webinarignition_leads_evergreen.ID'})) {
                continue;
            }
            $lead_id = $result->{'webinarignition_leads_evergreen.ID'};
            $lead_ids[] = $lead_id;
        }

        if (empty($lead_ids)) return $results;

        global $wpdb;
        $sql = "
            SELECT lead_id, meta_key, meta_value
            FROM {$wpdb->prefix}webinarignition_lead_evergreenmeta
            WHERE meta_key = 'wiRegForm'
            ORDER BY lead_id
            DESC;
        ";

        $lead_metas = $wpdb->get_results($sql, ARRAY_A);

        if (empty($lead_metas)) return $results;

        $meta_by_lead_id = array();
        $virtual_columns = array();

        foreach ($lead_metas as $lead_meta) {
            $lead_meta_unserialise = maybe_unserialize( $lead_meta['meta_value'] );
            $meta_by_lead_id[$lead_meta['lead_id']] = $lead_meta_unserialise;

            foreach ($lead_meta_unserialise as $field => $meta) {
                $virtual_columns[] = "v.registration_{$field}";
            }
        }
        $virtual_columns = array_unique($virtual_columns);

        foreach ($results as $i => $result) {
            foreach ($virtual_columns as $column) {
                $results[$i]->{$column} = '';
            }

            $lead_id = $result->{'webinarignition_leads_evergreen.ID'};

            if (empty($meta_by_lead_id[$lead_id])) continue;

            $lead_meta = $meta_by_lead_id[$lead_id];

            foreach ($lead_meta as $field => $meta) {
                $results[$i]->{"v.registration_{$field}"} = $meta['value'];
            }
        }

        return $results;
    }
}
