<?php

class Wp2leadsItmWebinarignitionModel {
    private static $table_webinarignition_leads_name = 'wp2leads_itm_webinarignition_leads';
    private static $table_webinarignition_leads_meta_name = 'wp2leads_itm_webinarignition_leads_meta';
    private static $table_webinarignition_leads_evergreen_name = 'wp2leads_itm_webinarignition_leads_evergreen';
    private static $table_webinarignition_leads_evergreen_meta_name = 'wp2leads_itm_webinarignition_leads_evergreen_meta';

    private static function get_table_name($table) {
        global $wpdb;

        if ('leads' === $table) {
            return $wpdb->prefix . self::$table_webinarignition_leads_name;
        } elseif ('leads_evergreen' === $table) {
            return $wpdb->prefix . self::$table_webinarignition_leads_evergreen_name;
        } elseif ('leads_meta' === $table) {
            return $wpdb->prefix . self::$table_webinarignition_leads_meta_name;
        } elseif ('leads_evergreen_meta' === $table) {
            return $wpdb->prefix . self::$table_webinarignition_leads_evergreen_meta_name;
        } elseif ('wi_leads' === $table) {
            return $wpdb->prefix . 'webinarignition_leads';
        } elseif ('wi_leads_evergreen' === $table) {
            return $wpdb->prefix . 'webinarignition_leads_evergreen';
        }

        return false;
    }

    private static function get_schema($table) {
        global $wpdb;

        $table_name = self::get_table_name($table);

        if (!$table_name) {
            return false;
        }

        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';

        if ('leads' === $table) {
            return "CREATE TABLE {$table_name} (
  ID BIGINT UNSIGNED NOT NULL auto_increment,
  w_id BIGINT UNSIGNED NOT NULL,
  app_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
  name varchar(200) NOT NULL,
  email varchar(200) NOT NULL,
  phone varchar(200) NOT NULL,
  status varchar(100) NOT NULL,
  kt_status varchar(100) NOT NULL,
  registered varchar(50) NOT NULL,
  joined varchar(50) NOT NULL,
  exited varchar(50) NOT NULL,
  online INT UNSIGNED NOT NULL,
  cta_seen varchar(50) NOT NULL,
  half_viewed varchar(50) NOT NULL,
  replay varchar(50) NOT NULL,
  w_start varchar(50) NOT NULL,
  w_complete varchar(50) NOT NULL,
  w_length INT UNSIGNED NOT NULL,
  w_permalink text,
  PRIMARY KEY  (ID)
) $collate;";
        } elseif ('leads_evergreen' === $table) {
            return "CREATE TABLE {$table_name} (
  ID BIGINT UNSIGNED NOT NULL auto_increment,
  w_id BIGINT UNSIGNED NOT NULL,
  app_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
  name varchar(200) NOT NULL,
  email varchar(200) NOT NULL,
  phone varchar(200) NOT NULL,
  status varchar(100) NOT NULL,
  kt_status varchar(100) NOT NULL,
  registered varchar(50) NOT NULL,
  joined varchar(50) NOT NULL,
  exited varchar(50) NOT NULL,
  online INT UNSIGNED NOT NULL,
  cta_seen varchar(50) NOT NULL,
  half_viewed varchar(50) NOT NULL,
  replay varchar(50) NOT NULL,
  w_start varchar(50) NOT NULL,
  w_complete varchar(50) NOT NULL,
  w_length INT UNSIGNED NOT NULL,
  w_permalink text,
  PRIMARY KEY  (ID)
) $collate;";
        } elseif ('leads_meta' === $table) {
            return "CREATE TABLE {$table_name} (
id BIGINT NOT NULL AUTO_INCREMENT,
lead_id BIGINT NOT NULL,
meta_key text NOT NULL,
meta_value text NOT NULL,
PRIMARY KEY (id)
) $collate;";
        } elseif ('leads_evergreen_meta' === $table) {
            return "CREATE TABLE {$table_name} (
id BIGINT NOT NULL AUTO_INCREMENT,
lead_id BIGINT NOT NULL,
meta_key text NOT NULL,
meta_value text NOT NULL,
PRIMARY KEY (id)
) $collate;";
        }

        return false;
    }

    public static function create_table($table) {
        global $wpdb;

        $wpdb->hide_errors();

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $sql = self::get_schema($table);

        error_log($sql);

        if ($sql) {
            dbDelta( self::get_schema($table) );
        }
    }

    public static function create_lead($table, $data) {
        global $wpdb;

        $table_name = self::get_table_name($table);

        if (!$table_name) {
            return false;
        }

        $meta = [];

        if (!empty($data['meta'])) {
            $meta = $data['meta'];

            unset($data['meta']);
        }

        if (!empty($data['ID'])) {
            $lead_id = $data['ID'];

            unset($data['ID']);

            $data_array = self::prepare_data($data, $table);

            $wpdb->update( $table_name,
                $data_array['data'],
                array( 'ID' => $lead_id ),
                $data_array['format']
            );
        } else {
            $data_array = self::prepare_data($data, $table);

            $wpdb->insert( $table_name,
                $data_array['data'],
                $data_array['format']
            );

            $lead_id = $wpdb->insert_id;
        }

        if (!empty($meta)) {
            foreach ($meta as $meta_key => $meta_value) {
                self::create_lead_meta($table, $lead_id, $meta_key, $meta_value);
            }
        }

        return $lead_id;
    }

    public static function create_lead_meta($table, $lead_id, $key, $value, $update = true) {
        global $wpdb;

        if ('leads' === $table) {
            $table_name = $wpdb->prefix . self::$table_webinarignition_leads_meta_name;
        } elseif ('leads_evergreen' === $table) {
            $table_name = $wpdb->prefix . self::$table_webinarignition_leads_evergreen_meta_name;
        } else {
            return false;
        }

        if ($update) {
            $meta_exists = $wpdb->get_row("SELECT id FROM {$table_name} WHERE lead_id = '{$lead_id}' AND meta_key = '{$key}'", ARRAY_A);

            if (!empty($meta_exists['id'])) {
                $wpdb->update( $table_name,
                    array( 'lead_id' => $lead_id, 'meta_key' => $key, 'meta_value' => $value ),
                    array( 'id' => $meta_exists['id'] )
                );

                return;
            }
        }

        $wpdb->insert( $table_name,
            array( 'lead_id' => $lead_id, 'meta_key' => $key, 'meta_value' => $value )
        );
    }

    public static function get_lead($table, $query, $single = true) {
        if (empty($query)) return false;

        global $wpdb;
        $table_name = self::get_table_name($table);
        $sql = "SELECT * FROM {$table_name} WHERE 1=1 ";

        foreach ($query as $col => $value) {
            $sql .= " AND {$col} = '{$value}'";
        }

        $leads = $wpdb->get_results($sql, ARRAY_A);

        if (empty($leads)) return false;

        if ($single) {
            return $leads[0];
        }

        return $leads;
    }

    public static function get_fields($table) {
        if ('leads' === $table) {
            return array(
                'ID' => array(
                    'format' => '%s',
                ),
                'w_id' => array(
                    'format' => '%s',
                ),
                'app_id' => array(
                    'format' => '%s',
                ),
                'name' => array(
                    'format' => '%s',
                ),
                'email' => array(
                    'format' => '%s',
                ),
                'phone' => array(
                    'format' => '%s',
                ),
                'status' => array(
                    'format' => '%s',
                ),
                'kt_status' => array(
                    'format' => '%s',
                ),
                'registered' => array(
                    'format' => '%s',
                ),
                'joined' => array(
                    'format' => '%s',
                ),
                'exited' => array(
                    'format' => '%s',
                ),
                'replay' => array(
                    'format' => '%s',
                ),
                'online' => array(
                    'format' => '%s',
                ),
                'cta_seen' => array(
                    'format' => '%s',
                ),
                'half_viewed' => array(
                    'format' => '%s',
                ),
                'w_start' => array(
                    'format' => '%s',
                ),
                'w_complete' => array(
                    'format' => '%s',
                ),
                'w_length' => array(
                    'format' => '%s',
                ),
                'w_permalink' => array(
                    'format' => '%s',
                ),
            );
        } elseif ('leads_evergreen' === $table) {
            return array(
                'ID' => array(
                    'format' => '%s',
                ),
                'w_id' => array(
                    'format' => '%s',
                ),
                'app_id' => array(
                    'format' => '%s',
                ),
                'name' => array(
                    'format' => '%s',
                ),
                'email' => array(
                    'format' => '%s',
                ),
                'phone' => array(
                    'format' => '%s',
                ),
                'status' => array(
                    'format' => '%s',
                ),
                'kt_status' => array(
                    'format' => '%s',
                ),
                'registered' => array(
                    'format' => '%s',
                ),
                'joined' => array(
                    'format' => '%s',
                ),
                'exited' => array(
                    'format' => '%s',
                ),
                'replay' => array(
                    'format' => '%s',
                ),
                'online' => array(
                    'format' => '%s',
                ),
                'cta_seen' => array(
                    'format' => '%s',
                ),
                'half_viewed' => array(
                    'format' => '%s',
                ),
                'w_start' => array(
                    'format' => '%s',
                ),
                'w_complete' => array(
                    'format' => '%s',
                ),
                'w_length' => array(
                    'format' => '%s',
                ),
                'w_permalink' => array(
                    'format' => '%s',
                ),
            );
        }
    }

    private static function prepare_data($data, $table) {
        $data_array = array(
            'data' => array(),
            'format' => array(),
        );

        $fields = self::get_fields($table);

        foreach ($fields as $field => $settings) {
            if (isset($data[$field])) {
                $data_array['data'][$field] = $data[$field];
                $data_array['format'][] = $settings['format'];
            }
        }

        return $data_array;
    }

    /**
     * Reset kt_status
     *
     * @param $email
     */
    public static function email_reset($email) {
        global $wpdb;

        $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads',
            array( 'kt_status' => '' ),
            array( 'email' => $email )
        );

        $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads_evergreen',
            array( 'kt_status' => '' ),
            array( 'email' => $email )
        );
    }

    public static function email_confirmed($app_id, $email) {
        global $wpdb;

        self::email_reset($email);

        $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads',
            array( 'kt_status' => 'confirmed' ),
            array( 'email' => $email, 'app_id' => $app_id )
        );

        $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads_evergreen',
            array( 'kt_status' => 'confirmed' ),
            array( 'email' => $email, 'app_id' => $app_id )
        );
    }

    public static function email_to_confirm($app_id, $email) {
        global $wpdb;

        self::email_reset($email);

        $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads',
            array('kt_status' => 'pending'),
            array( 'email' => $email, 'app_id' => $app_id )
        );

        $wpdb->update( $wpdb->prefix . 'wp2leads_itm_webinarignition_leads_evergreen',
            array('kt_status' => 'pending'),
            array( 'email' => $email, 'app_id' => $app_id )
        );
    }

    public static function get_app_by_email($email) {
        $lead = self::get_lead('leads', array('email' => $email, 'kt_status' => 'pending'));

        if (!empty($lead)) {
            return $lead;
        }

        $lead = self::get_lead('leads', array('email' => $email, 'kt_status' => 'confirmed'));

        if (!empty($lead)) {
            return $lead;
        }

        $lead = self::get_lead('leads_evergreen', array('email' => $email, 'kt_status' => 'pending'));

        if (!empty($lead)) {
            return $lead;
        }

        $lead = self::get_lead('leads_evergreen', array('email' => $email, 'kt_status' => 'confirmed'));

        if (!empty($lead)) {
            return $lead;
        }

        return false;
    }
}
