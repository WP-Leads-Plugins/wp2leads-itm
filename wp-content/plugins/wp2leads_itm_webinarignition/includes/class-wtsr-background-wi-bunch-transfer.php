<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wtsr_Background_Wi_Bunch_Transfer {
    protected static $bg_process;

    public static function init() {
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wi-logger.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/abstract-class-wi-background.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/class-wi-background-bunch-transfer-process.php';

        self::$bg_process = new Wi_Background_Bunch_Transfer_Process();
    }

    public static function bg_process($app_id, $lead_ids = array()) {
        $i = 0;

        $webinar_to_map = get_option('wp2leads_itm_webinarignition_webinar_to_map', array());

        if (empty($webinar_to_map[$app_id])) {
            return false;
        }

        if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
            include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
        }

        $results = get_option('webinarignition_campaign_' . $app_id);

        $webinar_type = webinarignition_is_auto($results) ? 'evergreen' : 'live';

        $wp2l_table = 'leads';

        if ('evergreen' === $webinar_type) {
            $wp2l_table = 'leads_evergreen';
        }

        if (empty($lead_ids)) {
            $leads = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $app_id), false);

            if (empty($leads)) return false;

            foreach ($leads as $lead) {
                if (!empty($lead['w_id'])) {
                    self::$bg_process->push_to_queue( array (
                        'w_id' => $lead['w_id'],
                        'app_id' => $app_id
                    ) );

                    $i++;
                }
            }
        } else {
            foreach ($lead_ids as $lead_id) {
                $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $app_id, 'w_id' => $lead_id));

                if (!empty($lead['w_id'])) {
                    self::$bg_process->push_to_queue( array (
                        'w_id' => $lead_id,
                        'app_id' => $app_id
                    ) );

                    $i++;
                }
            }
        }


        self::$bg_process->save()->dispatch();

        return $i;
    }
}

add_action( 'init', array( 'Wtsr_Background_Wi_Bunch_Transfer', 'init' ) );