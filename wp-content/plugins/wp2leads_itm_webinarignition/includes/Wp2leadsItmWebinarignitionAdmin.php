<?php
class Wp2leadsItmWebinarignitionAdmin {
    public static function add_submenu_page() {
        add_submenu_page(
            'webinarignition-dashboard',
            __("Webinarignition to KlickTipp", 'wp2leads_itm_webinarignition'),
            __("Webinarignition to KlickTipp", 'wp2leads_itm_webinarignition'),
            'manage_options',
            'wp2leads_itm_webinarignition',
            'Wp2leadsItmWebinarignitionAdmin::display_submenu_page'
        );
    }

    public static function display_submenu_page() {
        include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'templates/admin-page.php';
    }

    public static function enqueue_scripts() {
        if (!empty($_GET['page']) && $_GET['page'] === 'wp2leads_itm_webinarignition') {
            wp_enqueue_style( 'wp2leads-itm-webinarignition', plugin_dir_url( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'assets/css/admin.css', array(), WP2LITM_WEBINARIGNITION_VERSION . time(), 'all' );
            wp_enqueue_script( 'wp2leads-itm-webinarignition', plugin_dir_url( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'assets/js/admin.js', array( 'jquery' ), WP2LITM_WEBINARIGNITION_VERSION . time(), true );
        }

        if (!empty($_GET['page']) && $_GET['page'] === 'webinarignition-dashboard' && !empty($_GET['id'])) {
            wp_enqueue_script( 'wp2leads-itm-webinarignition-wi', plugin_dir_url( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'assets/js/wi.js', array( 'jquery' ), WP2LITM_WEBINARIGNITION_VERSION . time(), true );
        }
    }
}

add_action('admin_menu', array ('Wp2leadsItmWebinarignitionAdmin', 'add_submenu_page'), 1000);
add_action( 'admin_enqueue_scripts', array( 'Wp2leadsItmWebinarignitionAdmin', 'enqueue_scripts' ), 1 );
