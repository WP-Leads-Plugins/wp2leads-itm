<?php
function wp2leads_itm_webinarignition_load_plugin_textdomain() {
    add_filter( 'plugin_locale', 'wp2leads_itm_webinarignition_check_de_locale');

    load_plugin_textdomain(
        'wp2leads_itm_webinarignition',
        false,
        dirname( plugin_basename( __FILE__ ) ) . '/languages/'
    );

    remove_filter( 'plugin_locale', 'wp2leads_itm_webinarignition_check_de_locale');
}

function wp2leads_itm_webinarignition_check_de_locale($domain) {
    $site_lang = get_user_locale();
    $de_lang_list = array(
        'de_CH_informal',
        'de_DE_formal',
        'de_AT',
        'de_CH',
        'de_DE'
    );

    if (in_array($site_lang, $de_lang_list)) return 'de_DE';
    return $domain;
}

function wp2leads_itm_webinarignition_get_plugin_name() {
    $data = get_plugin_data( WP2LITM_WEBINARIGNITION_PLUGIN_FILE );

    return $data['Name'];
}

function wp2leads_itm_webinarignition_get_min_wp2leads_version() {
    return '3.3.1';
}

function wp2leads_itm_webinarignition_requirement() {
    $wp2leads_installed = function_exists('run_wp2leads') && class_exists('Wp2leads_Background_Module_Transfer');
    if (!$wp2leads_installed) return false;
    if (!wp2leads_itm_webinarignition_is_wp2leads_correct_version()) return false;
    $webinarignition = wp2leads_itm_webinarignition_is_plugin_activated( 'webinarignition', 'webinarignition.php' );
    if (!$webinarignition) return false;

    return true;
}

function wp2leads_itm_webinarignition_is_wp2leads_correct_version() {
    $plugin_version = wp2leads_itm_webinarignition_get_plugin_version('wp2leads/wp2leads.php');

    return $plugin_version && version_compare($plugin_version, wp2leads_itm_webinarignition_get_min_wp2leads_version(), '>=');
}

function wp2leads_itm_webinarignition_get_plugin_version($plugin_slug) {
    // Include the necessary file if it's not already included
    if (!function_exists('get_plugin_data')) {
        require_once(ABSPATH . 'wp-admin/includes/plugin.php');
    }

    // Get the plugin data
    $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/' . $plugin_slug);

    // Return the version if it exists
    return isset($plugin_data['Version']) ? $plugin_data['Version'] : null;
}

function wp2leads_itm_webinarignition_is_plugin_activated( $plugin_folder, $plugin_file ) {
    if ( wp2leads_itm_webinarignition_is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
    else return wp2leads_itm_webinarignition_is_plugin_active_by_file( $plugin_file );
}

function wp2leads_itm_webinarignition_is_plugin_active_simple( $plugin ) {
    return (
        in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
        ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
    );
}

function wp2leads_itm_webinarignition_is_plugin_active_by_file( $plugin_file ) {
    foreach ( wp2leads_itm_webinarignition_get_active_plugins() as $active_plugin ) {
        $active_plugin = explode( '/', $active_plugin );
        if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
    }

    return false;
}

function wp2leads_itm_webinarignition_get_active_plugins() {
    $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
    if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );

    return $active_plugins;
}

function wp2leads_itm_webinarignition_init() {
    $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
    if (!$is_ajax) wp2leads_itm_webinarignition_check_version();
    if ($is_ajax && !empty($_POST['action']) && $_POST['action'] === 'heartbeat') return;
    Wp2leadsItmWebinarignitionManager::add_hooks();
}

function wp2leads_itm_webinarignition_load_dependencies() {
    require_once 'includes/Wp2leadsItmWebinarignitionManager.php';
    Wp2leadsItmWebinarignitionManager::init();
    require_once 'includes/Wp2leadsItmWebinarignitionSettings.php';
    require_once 'includes/Wp2leadsItmWebinarignitionController.php';
    require_once 'includes/Wp2leadsItmWebinarignitionMap.php';
    require_once 'includes/Wp2leadsItmWebinarignitionKlickTipp.php';
    require_once 'includes/Wp2leadsItmWebinarignitionRedirection.php';
    require_once 'includes/class-wtsr-background-wi-bunch-transfer.php';
    if (is_admin()) {
        require_once 'includes/Wp2leadsItmWebinarignitionAdmin.php';
    }
}

function wp2leads_itm_webinarignition_check_version() {
    $version = get_option( 'wp2leads_itm_webinarignition_version' );
    $dbversion = get_option( 'wp2leads_itm_webinarignition_db_version' );

    if (
        empty($version) || version_compare( $version, WP2LITM_WEBINARIGNITION_VERSION, '<' ) ||
        empty($dbversion) || version_compare( $dbversion, WP2LITM_WEBINARIGNITION_DB_VERSION, '<' )
    ) {
        wp2leads_itm_webinarignition_install();
        do_action( 'wp2leads_itm_webinarignition_updated' );
    }
}

function wp2leads_itm_webinarignition_install() {
    if (!class_exists('Wp2leadsItmWebinarignitionModel')) {
        include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';
    }
    Wp2leadsItmWebinarignitionModel::create_table('leads');
    Wp2leadsItmWebinarignitionModel::create_table('leads_evergreen');
    Wp2leadsItmWebinarignitionModel::create_table('leads_meta');
    Wp2leadsItmWebinarignitionModel::create_table('leads_evergreen_meta');
    wp2leads_itm_webinarignition_update_version();
}

function wp2leads_itm_webinarignition_update_version() {
    delete_option( 'wp2leads_itm_webinarignition_version' );
    add_option( 'wp2leads_itm_webinarignition_version', WP2LITM_WEBINARIGNITION_VERSION );

    delete_option( 'wp2leads_itm_webinarignition_db_version' );
    add_option( 'wp2leads_itm_webinarignition_db_version', WP2LITM_WEBINARIGNITION_DB_VERSION );
}

function wp2leads_itm_webinarignition_get_all_webinars($settings = false, $leads = false) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'webinarignition';

    $sql = "SELECT * FROM {$table_name}";

    $webinars = $wpdb->get_results($sql, ARRAY_A);

    if ($settings) {
        foreach ($webinars as $i => $webinar) {
            $sql = "SELECT * FROM {$wpdb->options} WHERE option_name = 'webinarignition_campaign_{$webinar['ID']}'";
            $w_settings = $wpdb->get_row($sql, ARRAY_A);

            $webinars[$i]['settings'] = !empty($w_settings['option_value']) ? unserialize($w_settings['option_value']) : '';
        }
    }

    if ($leads) {
        foreach ($webinars as $i => $webinar) {
            $sql = "SELECT * FROM {$wpdb->options} WHERE option_name = 'webinarignition_campaign_{$webinar['ID']}'";
            $w_settings = $wpdb->get_row($sql, ARRAY_A);

            $lead_table = webinarignition_is_auto(unserialize($w_settings['option_value'])) ? $wpdb->prefix . 'wp2leads_itm_webinarignition_leads_evergreen' : $wpdb->prefix . 'wp2leads_itm_webinarignition_leads';
            $sql = "SELECT * FROM {$lead_table} WHERE app_id = '{$webinar['ID']}'";
            $leads = $wpdb->get_results($sql, ARRAY_A);
            $webinars[$i]['leads'] = !empty($leads) ? $leads : array();
        }
    }

    return $webinars;
}

function wp2leads_itm_webinarignition_modules_init() {
    include_once 'transfer-modules/Wp2leads_Webinarignition_Evergreen.php';
    include_once 'transfer-modules/Wp2leads_Webinarignition_Live.php';
}

function wp2leads_itm_webinarignition_convertTimeToLocal($time) {
    $format = 'Y-m-d H:i:s';
    $time_zone = StatisticsManager::getTimeZone();

    $dt = new DateTime();
    $dt->setTimestamp(strtotime($time));
    $dt->setTimezone(new DateTimeZone($time_zone));
    return $dt->format($format);
}

function wp2leads_itm_webinarignition_check_leads_timestamp() {
    $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

    if ($is_ajax) return;

    if (!class_exists('Wp2leadsItmWebinarignitionModel')) include_once plugin_dir_path( WP2LITM_WEBINARIGNITION_PLUGIN_FILE ) . 'includes/Wp2leadsItmWebinarignitionModel.php';

    global $wpdb;
    $transient_name_like = 'wp2leads_itm_webinarignition_online__%';
    $now = time() - WP2LITM_WEBINARIGNITION_DELAY;
    $sql = "SELECT * FROM {$wpdb->options} WHERE option_name LIKE '{$transient_name_like}' AND option_value <= " . $now;
    $transients = $wpdb->get_results( $sql, ARRAY_A );

    if (!empty($transients)) {
        $leads_to_transfer = array();
        $webinars_array = array();

        foreach ($transients as $transient) {
            $option_name = $transient['option_name'];
            $option_name_array = explode('__', $option_name);
            $webinars_array[$option_name_array[2]][] = array(
                'app_id' => $option_name_array[2],
                'type' => $option_name_array[1],
                'lid' => $option_name_array[3],
                'last_updated' => (int)$transient["option_value"]
            );

        }

        if (!empty($webinars_array)) {
            foreach ($webinars_array as $app_id => $webinar_array) {
                $results = get_option('webinarignition_campaign_' . $app_id);
                $webinar_type = $webinar_array[0]['type'];
                $wp2l_table = 'leads';
                $wi_table = 'wi_leads';

                if ('evergreen' === $webinar_type) {
                    $wp2l_table = 'leads_evergreen';
                    $wi_table = 'wi_leads_evergreen';
                }

                $leads_to_transfer = array();

                foreach ($webinar_array as $lead_data) {
                    $lid = $lead_data["lid"];
                    $last_updated = (int)$lead_data["last_updated"];
                    $lead = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $app_id, 'w_id' => $lid, 'status' => 'Online'));

                    if (!$lead) {
                        wp2leads_itm_webinarignition_delete_online_transient($app_id, $lid, $webinar_type);
                    } else {
                        $leads_to_transfer[] = $lid;

                        $joined = $lead['joined'];
                        $exited = wp2leads_itm_webinarignition_convertTimeToLocal(date('Y-m-d H:i:s', $last_updated));

                        $joined_timestamp = (int)strtotime($joined);
                        $exited_timestamp = (int)strtotime($exited);

                        $time_spent = ceil(($exited_timestamp - $joined_timestamp) / 60);

                        $data = array(
                            'ID' => $lead['ID'],
                            'exited' => $exited,
                            'status' => 'Exited',
                            'online' => $time_spent
                        );

                        if (!empty($lead['w_length'])) {
                            if ($time_spent >= ($lead['w_length'] / 2)) {
                                $data['half_viewed'] = '50% Viewed';
                            }
                        }

                        Wp2leadsItmWebinarignitionModel::create_lead($wp2l_table, $data);
                    }
                }

                $leads = Wp2leadsItmWebinarignitionModel::get_lead($wp2l_table, array('app_id' => $app_id), false);
                $online_count = 0;
                $total_count = 0;

                foreach ($leads as $lead) {
                    if (!empty($lead['w_id'])) {
                        $total_count++;

                        if ($lead['status'] === 'Online') {
                            $online_count++;
                        }
                    }
                }

                if (0 !== $total_count) {
                    $percent = ($online_count / $total_count) * 100;
                }

                if (2 > $percent) {
                    wp2leads_itm_webinarignition_switch_updated($app_id, 'closed', 'live', $results);
                } else {
                    Wtsr_Background_Wi_Bunch_Transfer::bg_process($app_id, $leads_to_transfer);
                }
            }
        }
    }
}
