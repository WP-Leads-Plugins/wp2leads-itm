<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class WP2LITMFOOE_Transfer_Events {
    private static $key = 'wp2leads_itm_foo_events';
    private static $required_column = 'wp2leads_itm_fooevents_ticket.id';

    public static function get_label() {
        return __('FooEvents for WooCommerce: Order created / updated', 'wp2leads_itm_foo_events');
    }

    public static function get_description() {
        return __('This module will transfer user data once order will be created or order\'s data will be changed', 'wp2leads_itm_foo_events');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for orders created with FooEvents for WooCommerce plugin map.', 'wp2leads_itm_foo_events') ?></p>
        <p><?php _e('Once new FooEvents for WooCommerce order created or updated user data will be transfered to KlickTipp account.', 'wp2leads_itm_foo_events') ?></p>
        <p><?php _e('Requirement: <strong>wp2leads_itm_fooevents_ticket.id</strong> column within selected data.', 'wp2leads_itm_foo_events') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function transfer_init() {
        add_action('wp2leads_itm_foo_events_user_updated', 'WP2LITMFOOE_Transfer_Events::transfer', 30);
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_itm_foo_events($transfer_modules) {
    $transfer_modules['wp2leads_itm_foo_events'] = 'WP2LITMFOOE_Transfer_Events';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_itm_foo_events');