<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for Foo Events plugin
 * Description:
 * Version:         1.0.10
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_foo_events
 *
 * Requires at least: 5.0
 * Tested up to: 5.9
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_FOOEVENTS_VERSION', '1.0.10' );
define( 'WP2LITM_FOOEVENTS_DB_VERSION', '1.0.10' );
define( 'WP2LITM_FOOEVENTS_PLUGIN_FILE', __FILE__ );
define( 'WP2LITM_FOOEVENTS_PLUGIN_REL_FILE', dirname( plugin_basename( __FILE__ ) ) );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_foo_events.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_foo_events'
);

require_once('includes/WP2LITMFOOE_Functions.php');
require_once('includes/WP2LITMFOOE_Notices.php');

add_action( 'plugins_loaded', 'WP2LITMFOOE_Functions::load_plugin_textdomain' );

if (!WP2LITMFOOE_Functions::requirement()) {
    add_action('admin_notices', 'WP2LITMFOOE_Notices::requirements_failed');
}

require_once 'includes/WP2LITMFOOE_Import_Background.php';
require_once 'includes/WP2LITMFOOE_Import_Magic_Ticket_Background.php';
require_once 'includes/WP2LITMFOOE_Model.php';
require_once 'includes/WP2LITMFOOE_Manager.php';



if (WP2LITMFOOE_Manager::get_import_counter()) {
    add_action('admin_notices', 'WP2LITMFOOE_Notices::initial_import_is_running');
}

if (WP2LITMFOOE_Manager::get_import_counter('wp2leads_itm_foo_events_import_magic_ticket_counter')) {
    add_action('admin_notices', 'WP2LITMFOOE_Notices::initial_magic_ticket_import_is_running');
}

if (is_admin()) {
    require_once 'includes/WP2LITMFOOE_Admin.php';
}

add_action( 'woocommerce_checkout_update_order_meta', 'WP2LITMFOOE_Manager::woocommerce_events_process', 1000 );
add_action('woocommerce_order_status_changed', 'WP2LITMFOOE_Manager::order_status_changed', 1000, 4);
add_action('fooevents_create_ticket_admin', 'WP2LITMFOOE_Manager::fooevents_create_ticket', 1000, 1);
add_action('fooevents_create_ticket', 'WP2LITMFOOE_Manager::fooevents_create_ticket', 1000, 1);
add_action('fooevents_check_in_ticket', 'WP2LITMFOOE_Manager::fooevents_check_in_ticket', 1000, 1);
add_action( 'init', 'WP2LITMFOOE_Functions::init' );
require_once('transfer-modules/WP2LITMFOOE_Transfer_Events.php');
require_once('transfer-modules/WP2LITMFOOE_Transfer_Magic_Tickets.php');
