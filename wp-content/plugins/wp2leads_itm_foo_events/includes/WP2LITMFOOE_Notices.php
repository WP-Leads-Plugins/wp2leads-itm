<?php


class WP2LITMFOOE_Notices {
    public static $plugin_functions = 'WP2LITMFOOE_Functions';

    public static function show_message($notice_id) {
        add_action('admin_notices', 'WP2LITMFOOE_Notices::' . $notice_id);
    }

    public static function initial_import_is_running() {
        $plugin_functions = self::$plugin_functions;
        $import_counter = WP2LITMFOOE_Manager::get_import_counter();
        if (empty($import_counter)) {
            return;
        }
        $counter_data = maybe_unserialize($import_counter['option_value']);

        if (!is_array($counter_data) || empty($counter_data['total']) || empty($counter_data['ids'])) {
            return;
        }

        $total = $counter_data['total'];
        $done = count($counter_data['ids']);

        $message = sprintf(
            __( 'Initial orders import is running in background. Total: <strong>%s</strong>, left: <strong>%s</strong>', 'wp2leads_itm_foo_events' ),
            $total, $done
        );
        ?>
        <div class="notice notice-info">
            <p>
                <strong><?php echo $plugin_functions::get_plugin_name(); ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }

    public static function initial_magic_ticket_import_is_running() {
        $plugin_functions = self::$plugin_functions;
        $import_counter = WP2LITMFOOE_Manager::get_import_counter('wp2leads_itm_foo_events_import_magic_ticket_counter');
        if (empty($import_counter)) {
            return;
        }
        $counter_data = maybe_unserialize($import_counter['option_value']);

        if (!is_array($counter_data) || empty($counter_data['total']) || empty($counter_data['ids'])) {
            return;
        }

        $total = $counter_data['total'];
        $done = count($counter_data['ids']);

        $message = sprintf(
            __( 'Initial tickets import is running in background. Total: <strong>%s</strong>, left: <strong>%s</strong>', 'wp2leads_itm_foo_events' ),
            $total, $done
        );
        ?>
        <div class="notice notice-info">
            <p>
                <strong><?php echo $plugin_functions::get_plugin_name(); ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }

    public static function requirements_failed() {
        $plugin_functions = self::$plugin_functions;

        if ($plugin_functions::requirement()) {
            return '';
        }

        $message = sprintf( __( 'Please, install all required plugins.', 'wp2leads_itm_foo_events' ) );

        if (!$plugin_functions::is_plugin_activated( 'woocommerce', 'woocommerce.php' )) {
            $message .= ' <br>' . sprintf(
                    __( 'Woocommerce plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_foo_events' ),
                    '<a href="/wp-admin/plugin-install.php?s=Woocommerce&tab=search&type=term" target="_blank">',
                    '</a>'
                );
        }

        if (!$plugin_functions::is_plugin_activated( 'fooevents', 'fooevents.php' )) {
            $message .= ' <br>' . sprintf(
                    __( 'FooEvents for WooCommerce plugin is required. Install it %1$shere%2$s.', 'wp2leads_itm_foo_events' ),
                    '<a href="https://www.fooevents.com/" target="_blank">',
                    '</a>'
                );
        }

        if (!$plugin_functions::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) {
            $message .= '  <br>' . sprintf(
                    __( 'Please, install WP2LEADS plugin %1$shere%2$s.', 'wp2leads_itm_foo_events' ),
                    '<a href="/wp-admin/plugin-install.php?s=WP2LEADS&tab=search&type=term" target="_blank">',
                    '</a>'
                );
        }
        ?>
        <div class="notice notice-error">
            <p>
                <strong><?php echo $plugin_functions::get_plugin_name(); ?>:</strong>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }

}