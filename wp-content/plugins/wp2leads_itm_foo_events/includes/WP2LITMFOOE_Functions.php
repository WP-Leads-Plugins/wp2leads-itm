<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}


class WP2LITMFOOE_Functions {
    public static function init() {
        $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;

        if (!$is_ajax) {
            self::check_version();
            self::check_updates();
            self::initial_import();
        }
    }

    public static function check_version() {
        $version = get_option( 'wp2leads_itm_foo_events_version' );
        $dbversion = get_option( 'wp2leads_itm_foo_events_db_version' );

        if (
            empty($version) || version_compare( $version, WP2LITM_FOOEVENTS_VERSION, '<' ) ||
            empty($dbversion) || version_compare( $dbversion, WP2LITM_FOOEVENTS_DB_VERSION, '<' )
        ) {
            self::install();
            do_action( 'wp2leads_itm_foo_events_updated' );
        }

        update_option('wp2leads_itm_foo_events_version', WP2LITM_FOOEVENTS_VERSION);
        update_option('wp2leads_itm_foo_events_db_version', WP2LITM_FOOEVENTS_DB_VERSION);
    }

    public static function initial_import() {
        $initial_import = get_option( 'wp2leads_itm_foo_events_initial_import' );
        $initial_import_update = get_option( 'wp2leads_itm_foo_events_initial_import_update' );

        if (!$initial_import || !$initial_import_update) {
            update_option('wp2leads_itm_foo_events_initial_import', 1);
            update_option('wp2leads_itm_foo_events_initial_import_update', 1);
            WP2LITMFOOE_Manager::import_tickets(true);
        }

        $initial_magic_ticket_import = get_option( 'wp2leads_itm_foo_events_initial_magic_ticket_import' );

        if (!$initial_magic_ticket_import) {
            update_option('wp2leads_itm_foo_events_initial_magic_ticket_import', 1);
            WP2LITMFOOE_Manager::import_magic_tickets(true);
        }
    }

    public static function check_updates() {
        $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
        if ($is_ajax) return;
        $wp2leads_itm_foo_events_0_0_5_update = get_option('wp2leads_itm_foo_events_0_0_5_update');
        if (empty($wp2leads_itm_foo_events_0_0_5_update)) WP2LITMFOOE_Model::model_0_0_5_update();
        $wp2leads_itm_foo_events_1_0_7_update = get_option('wp2leads_itm_foo_events_1_0_7_update');
        if (empty($wp2leads_itm_foo_events_1_0_7_update)) WP2LITMFOOE_Model::model_1_0_7_update();
    }

    public static function install() {
        WP2LITMFOOE_Model::create_table();
        WP2LITMFOOE_Model::create_meta_table();
    }

    public static function load_plugin_textdomain() {
        add_filter( 'plugin_locale', 'WP2LITMFOOE_Functions::check_de_locale');

        load_plugin_textdomain(
            'wp2leads_itm_foo_events',
            false,
            WP2LITM_FOOEVENTS_PLUGIN_REL_FILE . '/languages/'
        );

        remove_filter( 'plugin_locale', 'WP2LITMFOOE_Functions::check_de_locale');
    }

    public static function check_de_locale($domain) {
        $site_lang = get_user_locale();
        $de_lang_list = array(
            'de_CH_informal',
            'de_DE_formal',
            'de_AT',
            'de_CH',
            'de_DE'
        );

        if (in_array($site_lang, $de_lang_list)) return 'de_DE';
        return $domain;
    }

    public static function requirement() {
        if (!self::is_plugin_activated( 'woocommerce', 'woocommerce.php' )) return false;
        if (!self::is_plugin_activated( 'fooevents', 'fooevents.php' )) return false;
        if (!self::is_plugin_activated( 'wp2leads', 'wp2leads.php' )) return false;

        return true;
    }

    public static function is_plugin_activated( $plugin_folder, $plugin_file ) {
        if ( self::is_plugin_active_simple( $plugin_folder . '/' . $plugin_file ) ) return true;
        else return self::is_plugin_active_by_file( $plugin_file );
    }

    public static function is_plugin_active_simple( $plugin ) {
        return (
            in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) ||
            ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
        );
    }

    public static function is_plugin_active_by_file( $plugin_file ) {
        foreach ( self::get_active_plugins() as $active_plugin ) {
            $active_plugin = explode( '/', $active_plugin );
            if ( isset( $active_plugin[1] ) && $plugin_file === $active_plugin[1] ) return true;
        }

        return false;
    }

    public static function get_active_plugins() {
        $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) );
        if ( is_multisite() ) {
            $active_plugins = array_merge( $active_plugins, array_keys( get_site_option( 'active_sitewide_plugins', array() ) ) );
        }

        return $active_plugins;
    }

    public static function get_plugin_name() {
        if( !function_exists('get_plugin_data') )require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        $data = get_plugin_data( WP2LITM_FOOEVENTS_PLUGIN_FILE );

        return $data['Name'];
    }

}