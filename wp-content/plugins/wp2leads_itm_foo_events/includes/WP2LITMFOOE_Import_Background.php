<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class WP2LITMFOOE_Import_Background {
    protected static $bg_process;

    public static function init() {
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/WP2LITMFOOE_Abstract_Import_Background.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/WP2LITMFOOE_Import_Background_Process.php';

        self::$bg_process = new WP2LITMFOOE_Import_Background_Process();
    }

    public static function bg_process($is_initial = false, $meta_ids = [], $force_update = false) {
        $i = 0;
        global $wpdb;

        if (empty($meta_ids)) {
            $sql = "SELECT meta_id FROM {$wpdb->postmeta} WHERE meta_key='WooCommerceEventsOrderTickets'";
            $result = $wpdb->get_results($sql, ARRAY_A);
            $meta_ids = [];

            if (!empty($result)) {
                foreach ($result as $item) {
                    $meta_id = $item['meta_id'];
                    $meta_ids[] = $meta_id;

                    $i++;
                }
            }
        }

        $meta_ids_chunk = array_chunk($meta_ids, 20);

        if (!empty($meta_ids_chunk)) {
            foreach ($meta_ids_chunk as $chunk) {
                self::$bg_process->push_to_queue( ["meta_id" => $chunk, "is_initial" => $is_initial, "force_update" => $force_update] );
            }

            if ($is_initial) {
                $counter = [
                    'total' => count($meta_ids),
                    'ids' => [],
                ];

                foreach ($meta_ids as $meta_id) {
                    $counter['ids']['item_' . $meta_id] = $meta_id;
                }

                WP2LITMFOOE_Manager::set_import_counter($counter);
            }
        }

        self::$bg_process->save()->dispatch();

        return $i;

    }

}

add_action( 'init', array( 'WP2LITMFOOE_Import_Background', 'init' ) );