<?php


class WP2LITMFOOE_Model {
    #region Model Info
    private static $table_name = 'wp2leads_itm_fooevents_ticket';
    private static $meta_table_name = 'wp2leads_itm_fooevents_ticket_meta';
    private static $ticket_table_name = 'wp2leads_itm_fooevents_magic_ticket';
    #endregion Model Info

    #region Create Model Tables
    private static function get_schema() {
        global $wpdb;
        $table = $wpdb->prefix . self::$table_name;
        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';

        return "CREATE TABLE {$table} (
id BIGINT NOT NULL AUTO_INCREMENT,
order_id BIGINT NOT NULL,
product_id BIGINT NOT NULL,
email varchar(200) NOT NULL,
first_name varchar(200) NOT NULL,
last_name varchar(200) NOT NULL,
type varchar(200) NOT NULL,
status varchar(200) NOT NULL,
price varchar(200) NOT NULL,
z1 smallint DEFAULT 1,
PRIMARY KEY (id)
) $collate;";
    }

    public static function create_table() {
        global $wpdb;
        $wpdb->hide_errors();
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $sql = self::get_schema();
        error_log($sql);

        if ($sql) {
            dbDelta( self::get_schema() );
        }
    }

    private static function get_meta_schema() {
        global $wpdb;
        $table = $wpdb->prefix . self::$meta_table_name;
        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';

        return "CREATE TABLE {$table} (
id BIGINT NOT NULL AUTO_INCREMENT,
ticket_id BIGINT NOT NULL,
meta_key text NOT NULL,
meta_value text NOT NULL,
PRIMARY KEY (id)
) $collate;";
    }

    public static function create_meta_table() {
        global $wpdb;
        $wpdb->hide_errors();
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $sql = self::get_schema();
        error_log($sql);

        if ($sql) {
            dbDelta( self::get_meta_schema() );
        }
    }

    private static function get_magic_ticket_schema() {
        global $wpdb;
        $table = $wpdb->prefix . self::$ticket_table_name;
        $collate = ( $wpdb->has_cap( 'collation' ) ) ? $wpdb->get_charset_collate() : '';

        return "CREATE TABLE {$table} (
id BIGINT NOT NULL AUTO_INCREMENT,
magic_ticket_id BIGINT NOT NULL,
order_id BIGINT NOT NULL,
product_id BIGINT NOT NULL,
price varchar(200) NOT NULL,
PRIMARY KEY (id)
) $collate;";
    }

    public static function create_magic_ticket_table() {
        global $wpdb;
        $wpdb->hide_errors();
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $sql = self::get_magic_ticket_schema();
        error_log($sql);

        if ($sql) {
            dbDelta( self::get_magic_ticket_schema() );
        }
    }
    #endregion Create Model Tables

    #region Data Manipulation
    private static function prepare_data($data, $table) {
        $data_array = array(
            'data' => array(),
            'format' => array(),
        );

        $fields = [
            'id' => array(
                'format' => '%s',
            ),
            'order_id' => array(
                'format' => '%s',
            ),
            'product_id' => array(
                'format' => '%s',
            ),
            'email' => array(
                'format' => '%s',
            ),
            'first_name' => array(
                'format' => '%s',
            ),
            'last_name' => array(
                'format' => '%s',
            ),
            'type' => array(
                'format' => '%s',
            ),
            'status' => array(
                'format' => '%s',
            ),
            'price' => array(
                'format' => '%s',
            ),
            'z1' => array(
                'format' => '%s',
            )
        ];

        foreach ($fields as $field => $settings) {
            if (isset($data[$field])) {
                $data_array['data'][$field] = $data[$field];
                $data_array['format'][] = $settings['format'];
            }
        }

        return $data_array;
    }

    public static function update($data) {
        global $wpdb;
        $table = $wpdb->prefix . self::$table_name;
        $meta = [];

        if (!empty($data['meta'])) {
            $meta = $data['meta'];

            unset($data['meta']);
        }

        if (!empty($data['id'])) {
            $id = $data['id'];
            unset($data['id']);

            $data_array = self::prepare_data($data, $table);

            $wpdb->update( $table,
                $data_array['data'],
                array( 'id' => $id ),
                $data_array['format']
            );
        } else {
            $data_array = self::prepare_data($data, $table);

            $wpdb->insert( $table,
                $data_array['data'],
                $data_array['format']
            );

            $id = $wpdb->insert_id;
        }

        if (!empty($meta)) {
            foreach ($meta as $meta_key => $meta_value) {
                self::update_meta($id, $meta_key, $meta_value);
            }
        }

        return $id;
    }

    public static function update_meta($ticket_id, $key, $value, $update = true) {
        global $wpdb;
        $table = $wpdb->prefix . self::$meta_table_name;

        if ($update) {
            $meta_exists = $wpdb->get_row("SELECT id FROM {$table} WHERE ticket_id = '{$ticket_id}' AND meta_key = '{$key}'", ARRAY_A);

            if (!empty($meta_exists['id'])) {
                $wpdb->update( $table,
                    array( 'ticket_id' => $ticket_id, 'meta_key' => $key, 'meta_value' => $value ),
                    array( 'id' => $meta_exists['id'] )
                );

                return;
            }
        }

        $wpdb->insert( $table,
            array( 'ticket_id' => $ticket_id, 'meta_key' => $key, 'meta_value' => $value )
        );
    }

    public static function get_one($id, $select = '*') {
        global $wpdb;
        $table = $wpdb->prefix . self::$table_name;

        $sql = "SELECT {$select} FROM {$table} WHERE id = {$id}";

        $result = $wpdb->get_row($sql, ARRAY_A);

        if (!empty($result['id'])) {
            $ticket_id = $result['id'];
            $meta = self::get_meta('meta_key, meta_value', "ticket_id = '{$ticket_id}'");

            if (!empty($meta)) {
                $result['meta'] = [];

                foreach ($meta as $item) {
                    $result['meta'][$item['meta_key']] = $item['meta_value'];
                }
            }
        }

        return $result;
    }

    public static function get($select = '*', $where = '', $is_single = true) {
        global $wpdb;
        $table = $wpdb->prefix . self::$table_name;

        $sql = "SELECT {$select} FROM {$table} WHERE 1=1";

        if (!empty($where)) {
            $sql .= " AND {$where}";
        }

        if ($is_single) {
            $result = $wpdb->get_row($sql, ARRAY_A);

            if (!empty($result['id'])) {
                $ticket_id = $result['id'];
                $meta = self::get_meta('meta_key, meta_value', "ticket_id = '{$ticket_id}'");

                if (!empty($meta)) {
                    $result['meta'] = [];

                    foreach ($meta as $item) {
                        $result['meta'][$item['meta_key']] = $item['meta_value'];
                    }
                }
            }
        } else {
            $result = $wpdb->get_results($sql, ARRAY_A);

            if (!empty($result)) {
                foreach ($result as $i => $res_item) {
                    if (!empty($res_item['id'])) {
                        $ticket_id = $res_item['id'];
                        $meta = self::get_meta('meta_key, meta_value', "ticket_id = '{$ticket_id}'");

                        if (!empty($meta)) {
                            $result[$i]['meta'] = [];

                            foreach ($meta as $item) {
                                $result[$i]['meta'][$item['meta_key']] = $item['meta_value'];
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public static function get_meta($select = '*', $where = '') {
        global $wpdb;
        $table = $wpdb->prefix . self::$meta_table_name;

        $sql = "SELECT {$select} FROM {$table} WHERE 1=1";

        if (!empty($where)) {
            $sql .= " AND {$where}";
        }

        return $wpdb->get_results($sql, ARRAY_A);
    }

    public static function delete($id) {
        global $wpdb;
        $table = $wpdb->prefix . self::$table_name;
        $wpdb->delete( $table, [ 'id' => $id ] );

        $table = $wpdb->prefix . self::$meta_table_name;
        $wpdb->delete( $table, [ 'ticket_id' => $id ] );
    }

    private static function mt_prepare_data($data, $table) {
        $data_array = array( 'data' => [], 'format' => []);

        $fields = [
            'id' => array(
                'format' => '%s',
            ),
            'magic_ticket_id' => array(
                'format' => '%s',
            ),
            'order_id' => array(
                'format' => '%s',
            ),
            'product_id' => array(
                'format' => '%s',
            ),
            'price' => array(
                'format' => '%s',
            )
        ];

        foreach ($fields as $field => $settings) {
            if (isset($data[$field])) {
                $data_array['data'][$field] = $data[$field];
                $data_array['format'][] = $settings['format'];
            }
        }

        return $data_array;
    }

    public static function mt_update($data) {
        global $wpdb;
        $table = $wpdb->prefix . self::$ticket_table_name;

        if (!empty($data['id'])) {
            $id = $data['id'];
            unset($data['id']);

            $data_array = self::mt_prepare_data($data, $table);

            $wpdb->update( $table,
                $data_array['data'],
                array( 'id' => $id ),
                $data_array['format']
            );
        } else {
            $data_array = self::mt_prepare_data($data, $table);

            $wpdb->insert( $table,
                $data_array['data'],
                $data_array['format']
            );

            $id = $wpdb->insert_id;
        }

        return $id;
    }

    #endregion Data Manipulation

    #region Model Updates
    public static function model_0_0_5_update() {
        global $wpdb;
        $table = $wpdb->prefix . self::$table_name;
        $row = $wpdb->get_results("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{$table}' AND column_name = 'z1' ");
        if (empty($row)) $wpdb->query("ALTER TABLE {$table} ADD COLUMN z1 smallint DEFAULT 1");
        update_option('wp2leads_itm_foo_events_0_0_5_update', 1);
    }

    public static function model_1_0_7_update() {
        WP2LITMFOOE_Model::create_magic_ticket_table();
        update_option('wp2leads_itm_foo_events_1_0_7_update', 1);
    }
    #endregion Model Updates
}