<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class WP2LITMFOOE_Import_Magic_Ticket_Background {
    protected static $bg_process;

    public static function init() {
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/WP2LITMFOOE_Abstract_Import_Background.php';
        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/background/WP2LITMFOOE_Import_Magic_Ticket_Background_Process.php';

        self::$bg_process = new WP2LITMFOOE_Import_Magic_Ticket_Background_Process();
    }

    public static function bg_process($is_initial = false, $ids = [], $force_update = false) {
        $i = 0;
        global $wpdb;

        if (empty($ids)) {
            $sql = "SELECT ID FROM {$wpdb->posts} WHERE post_type = 'event_magic_tickets' AND post_status = 'publish'";
            $result = $wpdb->get_results($sql, ARRAY_A);
            $ids = [];

            if (!empty($result)) {
                foreach ($result as $item) {
                    $id = $item['ID'];
                    $ids[] = $id;

                    $i++;
                }
            }
        }

        $ids_chunk = array_chunk($ids, 20);

        if (!empty($ids_chunk)) {
            foreach ($ids_chunk as $chunk) {
                self::$bg_process->push_to_queue( ["id" => $chunk, "is_initial" => $is_initial, "force_update" => $force_update] );
            }

            if ($is_initial) {
                $counter = [
                    'total' => count($ids),
                    'ids' => [],
                ];

                foreach ($ids as $id) {
                    $counter['ids']['item_' . $id] = $id;
                }

                WP2LITMFOOE_Manager::set_import_counter($counter, 'wp2leads_itm_foo_events_import_magic_ticket_counter');
            }
        }

        self::$bg_process->save()->dispatch();

        return $i;

    }

}

add_action( 'init', array( 'WP2LITMFOOE_Import_Magic_Ticket_Background', 'init' ) );