<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class WP2LITMFOOE_Import_Magic_Ticket_Background_Process extends WP2LITMFOOE_Abstract_Import_Background {

    /**
     * Initiate new background process.
     */
    public function __construct() {
        $this->action = 'wp2leads_itm_foo_events_import_magic_ticket';

        // This is needed to prevent timeouts due to threading. See https://core.trac.wordpress.org/ticket/36534.
        if (function_exists('putenv')) {
            @putenv( 'MAGICK_THREAD_LIMIT=1' );  // @codingStandardsIgnoreLine.
        }

        parent::__construct();
    }

    /**
     * Handle.
     */
    protected function handle() {
        $this->lock_process();

        do {
            $batch = $this->get_batch();
            $count = count($batch->data);

            ob_start();

            foreach ( $batch->data as $key => $value ) {
                $bg_task = $this->bg_task($value);

                $task = $this->task( $value );

                if ( false !== $task ) {
                    $batch->data[ $key ] = $task;
                } else {
                    unset( $batch->data[ $key ] );
                }

                if ( $this->batch_limit_exceeded() ) {
                    // Batch limits reached.
                    break;
                }
            }

            // Update or delete current batch.
            if ( ! empty( $batch->data ) ) {
                $this->update( $batch->key, $batch->data );
            } else {
                $this->delete( $batch->key );
            }
        } while ( ! $this->batch_limit_exceeded() && ! $this->is_queue_empty() );

        $this->unlock_process();

        // Start next batch or complete process.
        if ( ! $this->is_queue_empty() ) {
            $this->dispatch();
        } else {
            $this->complete();
        }
    }

    protected function bg_task($value) {
        $meta_id = $value['id'];
        $is_initial = !empty($value['is_initial']) ? $value['is_initial'] : false;
        $force_update = !empty($value['force_update']) ? $value['force_update'] : false;

        if (!is_array($meta_id)) {
            $meta_id = [$meta_id];
        }

        if (is_array($meta_id)) {
            foreach ($meta_id as $id) {
                WP2LITMFOOE_Manager::import_magic_ticket($id, $is_initial, $force_update);

                if ($is_initial) {
                    $counter = WP2LITMFOOE_Manager::get_import_counter('wp2leads_itm_foo_events_import_magic_ticket_counter');

                    if (!empty($counter['option_value'])) {
                        $counter_data = maybe_unserialize($counter['option_value']);

                        if (is_array($counter_data)) {
                            if (!empty($counter_data['ids']['item_' . $id])) {
                                unset($counter_data['ids']['item_' . $id]);
                            }

                            if (empty($counter_data['ids'])) {
                                WP2LITMFOOE_Manager::delete_import_counter('wp2leads_itm_foo_events_import_magic_ticket_counter');
                            } else {
                                WP2LITMFOOE_Manager::set_import_counter($counter_data, 'wp2leads_itm_foo_events_import_magic_ticket_counter');
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Task
     */
    protected function task( $item ) {
        return false;
    }

    /**
     * Limit each task ran per batch to 1 for image regen.
     */
    protected function batch_limit_exceeded() {
        return true;
    }

    /**
     * Save queue
     */
    public function save() {
        $unique  = md5( microtime() . rand() );
        $prepend = $this->identifier . '_batch_';
        $key = substr( $prepend . $unique, 0, 64 );

        if ( ! empty( $this->data ) ) {
            update_site_option( $key, $this->data );
        }

        $this->data = array();

        return $this;
    }

    protected function complete() {
        delete_option('wp2leads_itm_foo_events_initial_magic_tickets_import_running');
        WP2LITMFOOE_Manager::delete_import_counter('wp2leads_itm_foo_events_import_magic_ticket_counter');
        parent::complete();
    }
}
