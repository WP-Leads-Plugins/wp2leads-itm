<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

class WP2LITMFOOE_Manager {
    public static function import_tickets($initial = false) {
        WP2LITMFOOE_Import_Background::bg_process($initial);
    }

    public static function import_magic_tickets($initial = false) {
        WP2LITMFOOE_Import_Magic_Ticket_Background::bg_process($initial);
    }

    public static function is_exists($data) {
        $email = $data['email'];
        $order_id = $data['order_id'];
        $product_id = $data['product_id'];
        $type = $data['type'];
        $where = "email = '{$email}' AND order_id = '{$order_id}' AND product_id = '{$product_id}' AND type = '{$type}'";

        $existed = WP2LITMFOOE_Model::get('*', $where);

        return $existed;
    }

    public static function is_update($data, $compare_data) {
        $id = $compare_data['id'];
        if (isset($data['id'])) unset($data['id']);
        if (isset($compare_data['id'])) unset($compare_data['id']);

        $meta = !empty($data['meta']) ? $data['meta'] : [];
        if (isset($data['meta'])) unset($data['meta']);
        $compare_meta = !empty($compare_data['meta']) ? $compare_data['meta'] : [];
        if (isset($compare_data['meta'])) unset($compare_data['meta']);

        $result_compare = array_diff($data, $compare_data);
        $result_compare_meta = array_diff($meta, $compare_meta);

        return empty($result_compare) && empty($result_compare_meta) ? false : $id;
    }

    public static function get_events_data($events) {
        $data = [];

        foreach ($events as $tickets) {
            $i = 0;

            if (empty($tickets)) {
                continue;
            }

            $purchaser_data = [
                'order_id' => '',
                'product_id' => '',
                'email' => '',
                'type' => 'purchaser',
                'meta' => []
            ];

            $purchaser_attendees = [];

            foreach ($tickets as $ticket) {
                if (
                    !empty($ticket['WooCommerceEventsProductID'])
                    && !empty($ticket['WooCommerceEventsOrderID'])
                    && !empty($ticket['WooCommerceEventsAttendeeEmail'])
                    && !empty($ticket['WooCommerceEventsPurchaserEmail'])
                ) {
                    $order_id = $ticket['WooCommerceEventsOrderID'];
                    $product_id = $ticket['WooCommerceEventsProductID'];
                    $email = $ticket['WooCommerceEventsAttendeeEmail'];

                    if ($i === 0) {
                        $purchaser_email = $ticket['WooCommerceEventsPurchaserEmail'];
                        unset($ticket['WooCommerceEventsPurchaserEmail']);
                        $purchaser_data['email'] = $purchaser_email;
                    }

                    unset($ticket['WooCommerceEventsOrderID']);
                    unset($ticket['WooCommerceEventsProductID']);
                    unset($ticket['WooCommerceEventsAttendeeEmail']);

                    $first_name = '';
                    if (isset($ticket['WooCommerceEventsAttendeeName'])) {
                        $first_name = $ticket['WooCommerceEventsAttendeeName'];
                        unset($ticket['WooCommerceEventsAttendeeName']);
                    }
                    $last_name = '';
                    if (isset($ticket['WooCommerceEventsAttendeeLastName'])) {
                        $last_name = $ticket['WooCommerceEventsAttendeeLastName'];
                        unset($ticket['WooCommerceEventsAttendeeLastName']);
                    }

                    $full_name = '';
                    if (!empty($first_name)) {
                        $full_name .= $first_name;
                    }
                    if (!empty($last_name)) {
                        if (!empty($full_name)) $full_name .= ' ';
                        $full_name .= $last_name;
                    }

                    if (!empty($full_name)) {
                        $purchaser_attendees[] = $full_name;
                    }

                    $status = '';
                    if (isset($ticket['WooCommerceEventsStatus'])) {
                        $status = $ticket['WooCommerceEventsStatus'];
                        unset($ticket['WooCommerceEventsStatus']);
                    }
                    $price = '';
                    if (isset($ticket['WooCommerceEventsPrice'])) {
                        $price = floatval(str_replace(',', '.', wp_strip_all_tags($ticket['WooCommerceEventsPrice'])));
                        unset($ticket['WooCommerceEventsPrice']);
                    }

                    $attendee_data = [
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'email' => $email,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'type' => 'attendee',
                        'status' => $status,
                        'price' => $price,
                    ];

                    $meta = [];

                    if (isset($ticket['WooCommerceEventsAttendeeTelephone'])) {
                        $meta['foo_phone'] = $ticket['WooCommerceEventsAttendeeTelephone'];
                        unset($ticket['WooCommerceEventsAttendeeTelephone']);
                    }
                    if (isset($ticket['WooCommerceEventsAttendeeCompany'])) {
                        $meta['foo_company'] = $ticket['WooCommerceEventsAttendeeCompany'];
                        unset($ticket['WooCommerceEventsAttendeeCompany']);
                    }
                    if (isset($ticket['WooCommerceEventsAttendeeCompany'])) {
                        $meta['foo_company'] = $ticket['WooCommerceEventsAttendeeCompany'];
                        unset($ticket['WooCommerceEventsAttendeeCompany']);
                    }
                    $ticket_type = '';
                    if (isset($ticket['WooCommerceEventsTicketType'])) {
                        $ticket_type = $ticket['WooCommerceEventsTicketType'];
                        unset($ticket['WooCommerceEventsTicketType']);
                    }

                    $meta['foo_ticket_type'] = $ticket_type;
                    $meta['foo_event_role'] = 'attendee';

                    if (isset($ticket['WooCommerceEventsCustomAttendeeFields']) && is_array($ticket['WooCommerceEventsCustomAttendeeFields'])) {
                        if (is_array($ticket['WooCommerceEventsCustomAttendeeFields']) && count($ticket['WooCommerceEventsCustomAttendeeFields'])) {
                            foreach ($ticket['WooCommerceEventsCustomAttendeeFields'] as $key => $field) {
                                $meta['foo_' . $key] = $field;
                            }
                        }
                        unset($ticket['WooCommerceEventsCustomAttendeeFields']);
                    }

                    if ($email === $purchaser_email) {
                        $meta['foo_enabled_email'] = 'disabled_email';
                    } else {
                        $meta['foo_enabled_email'] = 'enabled_email';
                    }

                    $dates = self::get_event_dates($product_id);

                    if (!empty($dates)) {
                        $meta = array_merge($meta, $dates);
                    }

                    $attendee_data['meta'] = $meta;

                    if ($i === 0) {
                        $first_name = '';
                        if (isset($ticket['WooCommerceEventsPurchaserFirstName'])) {
                            $first_name = $ticket['WooCommerceEventsPurchaserFirstName'];
                            unset($ticket['WooCommerceEventsPurchaserFirstName']);
                        }
                        $last_name = '';
                        if (isset($ticket['WooCommerceEventsPurchaserLastName'])) {
                            $last_name = $ticket['WooCommerceEventsPurchaserLastName'];
                            unset($ticket['WooCommerceEventsPurchaserLastName']);
                        }

                        $purchaser_data['order_id'] = $order_id;
                        $purchaser_data['product_id'] = $product_id;
                        $purchaser_data['first_name'] = $first_name;
                        $purchaser_data['last_name'] = $last_name;
                        $purchaser_data['status'] = $status;
                        $purchaser_data['price'] = $price;

                        if (isset($ticket['WooCommerceEventsPurchaserPhone'])) {
                            $purchaser_data['meta']['foo_phone'] = $ticket['WooCommerceEventsPurchaserPhone'];
                            unset($ticket['WooCommerceEventsPurchaserPhone']);
                        }

                        $purchaser_data['meta']['foo_ticket_type'] = $ticket_type;
                        $purchaser_data['meta']['foo_enabled_email'] = 'enabled_email';
                        $purchaser_data['meta']['foo_event_role'] = 'purchaser';

                        if (!empty($dates)) {
                            $purchaser_data['meta'] = array_merge($purchaser_data['meta'], $dates);
                        }
                    } else {
                        unset($ticket['WooCommerceEventsPurchaserEmail']);

                        if (isset($ticket['WooCommerceEventsPurchaserFirstName'])) {
                            unset($ticket['WooCommerceEventsPurchaserFirstName']);
                        }

                        if (isset($ticket['WooCommerceEventsPurchaserLastName'])) {
                            unset($ticket['WooCommerceEventsPurchaserLastName']);
                        }

                        if (isset($ticket['WooCommerceEventsPurchaserPhone'])) {
                            unset($ticket['WooCommerceEventsPurchaserPhone']);
                        }
                    }

                    $data[] = $attendee_data;
                    $i++;
                }
            }

            if (!empty($purchaser_attendees)) {
                $purchaser_data['meta']['foo_purchaser_attendees'] = implode(',', $purchaser_attendees);
            }

            $data[] = $purchaser_data;
        }

        return $data;
    }

    public static function import_ticket($id, $initial = false, $force_update = false) {
        global $wpdb;
        $sql = "SELECT * FROM {$wpdb->postmeta} WHERE meta_id={$id}";
        $result = $wpdb->get_row($sql, ARRAY_A);

        if (empty($result) || empty($result['meta_value']) || 'WooCommerceEventsOrderTickets' !== $result['meta_key']) {
            return false;
        }

        $events = maybe_unserialize($result['meta_value']);

        if (!is_array($events)) {
            return false;
        }

        $data_array = self::get_events_data($events);
        $import_array = [];

        if (!empty($data_array)) {
            foreach ($data_array as $data) {
                $data['z1'] = '1';
                $is_exists = self::is_exists($data);
                $need_transfer = false;

                if (!$is_exists) {
                    $id = WP2LITMFOOE_Model::update($data);

                    $need_transfer = !$initial;
                } else {
                    $is_update = WP2LITMFOOE_Manager::is_update($data, $is_exists);

                    if ($is_update) {
                        $id = WP2LITMFOOE_Model::update($data);
                        $need_transfer = !$initial;
                    } else {
                        $id = $is_exists['id'];
                    }
                }

                $import_array[$id] = $data;

                if ($need_transfer || !empty($force_update)) {
                    do_action('wp2leads_itm_foo_events_user_updated', $id);
                }
            }
        }

        return $import_array;
    }

    public static function woocommerce_events_process($order_id) {
        global $wpdb;
        $sql = "SELECT meta_id FROM {$wpdb->postmeta} WHERE post_id = '{$order_id}' AND meta_key='WooCommerceEventsOrderTickets'";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (empty($result)) {
            return;
        }

        $ids = [];
        foreach ($result as $ticket) {
            if (!empty($ticket['meta_id'])) {
                $ids[] = $ticket['meta_id'];
            }
        }

        if (!empty($ids)) {
            WP2LITMFOOE_Import_Background::bg_process(false, $ids, true);
        }
    }

    public static function get_import_counter($name = 'wp2leads_itm_foo_events_import_counter') {
        global $wpdb;
        $sql = "SELECT * FROM {$wpdb->options} WHERE option_name = '{$name}'";

        return $wpdb->get_row($sql, ARRAY_A);
    }

    public static function delete_import_counter($name = 'wp2leads_itm_foo_events_import_counter') {
        global $wpdb;
        $wpdb->delete( $wpdb->options, [ 'option_name' => $name ] );
    }

    public static function set_import_counter($data, $name = 'wp2leads_itm_foo_events_import_counter') {
        global $wpdb;
        $counter = self::get_import_counter($name);

        if (empty($counter['option_id'])) {
            $wpdb->insert( $wpdb->options, [ 'option_name' => $name, 'option_value' => serialize($data), 'autoload' => 'no' ] );
            return;
        }

        $wpdb->update( $wpdb->options,
            [ 'option_value' => serialize($data) ],
            [ 'option_id' => $counter['option_id'] ]
        );
    }

    public static function order_status_changed($order_id, $from, $to, $order_data) {
        self::woocommerce_events_process($order_id);
        self::magic_ticket_order_updated($order_id);
    }

    public static function get_event_dates($product_id) {
        global $wpdb;
        $sql = "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = {$product_id}";
        $sql .= " AND(meta_key LIKE '%WooCommerceEventsDate%' OR meta_key LIKE '%WooCommerceEventsEnd%')";
        $results = $wpdb->get_results($sql, ARRAY_A);
        $dates = [];

        if (!empty($results)) {
            foreach ($results as $result) {
                $dates['foo_' . $result['meta_key']] = $result['meta_value'];
            }
        }

        return $dates;
    }

    public static function magic_ticket_order_updated($order_id) {
        global $wpdb;
        $table = $wpdb->prefix . 'wp2leads_itm_fooevents_magic_ticket';
        $sql = "SELECT magic_ticket_id FROM {$table} WHERE order_id = {$order_id}";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!empty($result)) {
            foreach ($result as $item) {
                $id = $item['magic_ticket_id'];
                do_action('wp2leads_itm_foo_events_magic_ticket_updated', $id);
            }
        }
    }

    public static function import_magic_ticket($id, $initial = false, $force_update = false) {
        global $wpdb;

        $table = $wpdb->prefix . 'wp2leads_itm_fooevents_magic_ticket';
        $sql = "SELECT * FROM {$table} WHERE magic_ticket_id = {$id}";
        $exists = $wpdb->get_row($sql, ARRAY_A);
        if (!empty($exists && !$force_update)) return false;

        $sql = "SELECT * FROM {$wpdb->posts} WHERE ID = {$id}";
        $result = $wpdb->get_row($sql, ARRAY_A);
        if (empty($result)) return false;

        $sql = "SELECT * FROM {$wpdb->postmeta} WHERE post_id = {$id}";
        $meta_result = $wpdb->get_results($sql, ARRAY_A);
        if (empty($meta_result)) return false;

        $WooCommerceEventsProductID = '';
        $WooCommerceEventsOrderID = '';
        $WooCommerceEventsPrice = '';

        foreach ($meta_result as $meta) {
            if ($meta['meta_key'] === 'WooCommerceEventsProductID') $WooCommerceEventsProductID = $meta['meta_value'];
            if ($meta['meta_key'] === 'WooCommerceEventsOrderID') $WooCommerceEventsOrderID = $meta['meta_value'];
            if ($meta['meta_key'] === 'WooCommerceEventsPrice') $WooCommerceEventsPrice = $meta['meta_value'];
        }

        if ( empty($WooCommerceEventsProductID) || empty($WooCommerceEventsOrderID) || empty($WooCommerceEventsPrice) ) return false;

        $price = floatval(str_replace(',', '.', wp_strip_all_tags($WooCommerceEventsPrice)));

        $data = [
            'magic_ticket_id' => $id,
            'order_id' => $WooCommerceEventsOrderID,
            'product_id' => $WooCommerceEventsProductID,
            'price' => $price,
        ];

        if ($exists['id']) $data['id'] = $exists['id'];
        $id2 = WP2LITMFOOE_Model::mt_update($data);
        $need_transfer = !$initial;
        if ($need_transfer || !empty($force_update)) do_action('wp2leads_itm_foo_events_magic_ticket_updated', $id);

        return $id2;
    }

    public static function fooevents_create_ticket($id) {
        self::import_magic_ticket($id, false, true);
    }

    public static function fooevents_check_in_ticket($args) {
        if ( count( $args ) !== 3 ) {
            return false;
        }

        $id = $args[0];
        self::import_magic_ticket($id, false, true);
    }
}