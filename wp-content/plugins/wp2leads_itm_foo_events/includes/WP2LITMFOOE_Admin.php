<?php


class WP2LITMFOOE_Admin {
    public static function add_submenu_page() {
        add_submenu_page(
            'wp2l-admin',
            __("Foo Events to KlickTipp", 'wp2leads_itm_foo_events'),
            __("Foo Events to KlickTipp", 'wp2leads_itm_foo_events'),
            'manage_options',
            'wp2leads_itm_foo_events',
            'WP2LITMFOOE_Admin::display_submenu_page'
        );
    }

    public static function display_submenu_page() {
        include_once plugin_dir_path( WP2LITM_FOOEVENTS_PLUGIN_FILE ) . 'templates/admin-page.php';
    }

}

// add_action('admin_menu', array ('WP2LITMFOOE_Admin', 'add_submenu_page'));