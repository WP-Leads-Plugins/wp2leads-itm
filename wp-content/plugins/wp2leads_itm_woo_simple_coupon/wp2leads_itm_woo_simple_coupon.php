<?php
/**
 * Plugin Name:     WP2LEADS ITM - Coupon & Easy User Workflow for WooCommerce with KlickTipp
 * Description:		Generate individual coupons in WooCommerce & transfer coupon codes/links to KlickTipp KT. Via the link used in KT emails, customers are registered/loged in, added coupon/products and are redirected to checkout/any page automatically.
 * Version:         2.0.5
 * Author:          Saleswonder.biz Team
 * Author URI:      https://saleswonder.biz/
 * Plugin URI:		https://wp2leads.tawk.help/de/article/gutschein-easy-user-workflow-für-woocommerce-mit-klicktipp
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_woo_simple_coupon
 *
 * Requires at least: 5.0
 * Tested up to: 5.9
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_WOO_SIMPLE_COUPON_VERSION', '2.0.5' );
define( 'WP2LITM_WOO_SIMPLE_COUPON_DB_VERSION', '1.0.3' );

if ( ! defined( 'WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE' ) ) {
    define( 'WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE', __FILE__ );
}

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_woo_simple_coupon.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_woo_simple_coupon'
);

require_once('functions.php');

add_action( 'plugins_loaded', 'wp2leads_itm_woo_simple_coupon_load_plugin_textdomain' );

if (!wp2leads_itm_woo_simple_coupon_requirement()) return;

require_once 'includes/Wp2leadsItmWooSimpleCouponUrlBuilder.php';
require_once 'includes/Wp2leadsItmWooSimpleCouponModel.php';
require_once 'includes/Wp2leadsItmWooSimpleCouponAjax.php';
require_once 'includes/Wp2leadsItmWooSimpleCouponKlickTippManager.php';

if (is_admin()) {
    require_once 'includes/Wp2leadsItmWooSimpleCouponAdmin.php';
}

add_action( 'woocommerce_checkout_order_processed', 'Wp2leadsItmWooSimpleCouponModel::attach_coupon_to_order', 10, 3 );
add_action( 'rest_api_init', 'Wp2leadsItmWooSimpleCouponUrlBuilder::rest_api_init' );
add_action( 'wp_loaded', 'Wp2leadsItmWooSimpleCouponUrlBuilder::maybe_apply_coupon' );
add_action( 'init', 'Wp2leadsItmWooSimpleCouponUrlBuilder::maybe_redirect_to_coupon_to_kt', 100 );
add_action('woocommerce_checkout_order_processed', 'Wp2leadsItmWooSimpleCouponUrlBuilder::maybe_unset_coupon', 10, 3);
add_action('woocommerce_removed_coupon', 'Wp2leadsItmWooSimpleCouponUrlBuilder::maybe_unset_coupon_on_remove', 10);
add_filter( 'manage_edit-shop_coupon_columns', 'Wp2leadsItmWooSimpleCouponUrlBuilder::add_coupon_columns', 10, 1 );
add_action( 'manage_shop_coupon_posts_custom_column' , 'Wp2leadsItmWooSimpleCouponUrlBuilder::show_coupon_columns', 10, 2 );
// $this->loader->add_action( 'init', $plugin_public, 'process_kt_invite' );

wp2leads_itm_woo_simple_coupon_modules_init();
