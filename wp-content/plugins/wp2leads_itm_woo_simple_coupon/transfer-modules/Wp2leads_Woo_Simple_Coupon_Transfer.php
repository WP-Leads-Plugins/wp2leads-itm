<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_Woo_Simple_Coupon_Transfer {
    private static $key = 'wp2leads_woo_simple_coupon';
    private static $required_column = 'posts.ID';

    public static function transfer_init() {
        add_action( 'wp2leads_itm_woo_simple_coupon_attached', 'Wp2leads_Woo_Simple_Coupon_Transfer::coupon_attached', 10 );
    }

    public static function get_label() {
        return __('Woocommerce Simple coupons to Klick-Tipp', 'wp2leads_itm_woo_simple_coupon');
    }

    public static function get_description() {
        return __('This module will transfer attached simple coupons to Klick-Tipp', 'wp2leads_itm_woo_simple_coupon');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Woocommerce LeadValue maps.', 'wp2leads_itm_woo_simple_coupon') ?></p>
        <p><?php _e('Once order will be created or order\'s data changed user orders total amount will be transfered to Klick-Tipp account and update LeadValue.', 'wp2leads_itm_woo_simple_coupon') ?></p>
        <p><?php _e('Requirement: <strong>posts.ID</strong> column withing selected data.', 'wp2leads_itm_woo_simple_coupon') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function coupon_attached($order_id) {
        $order = wc_get_order($order_id);
        if (!$order) return false;

        $email = $order->get_billing_email();
        if (!$email) return false;

        self::transfer($order_id);
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_woo_simple_coupon($transfer_modules) {
    $transfer_modules['wp2leads_woo_simple_coupon'] = 'Wp2leads_Woo_Simple_Coupon_Transfer';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_woo_simple_coupon');