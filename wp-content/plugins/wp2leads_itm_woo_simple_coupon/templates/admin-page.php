<?php
/**
 * @var $subpages
 * @var $admin_page
 * @var $active_tab
 */
?>
<div class="wrap">
    <h1><?php _e('Coupon & Easy User Workflow for WooCommerce with KlickTipp', 'wp2leads_itm_woo_simple_coupon'); ?></h1>
    <?php settings_errors(); ?>

    <?php include_once 'admin-tabs.php' ?>
    <?php include_once 'subpages/'.$active_tab.'.php' ?>
</div>
