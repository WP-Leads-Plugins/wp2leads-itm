<?php
/**
 * @var $subpages
 * @var $admin_page
 * @var $active_tab
 */
?>

<h2 id="wp2leads_superadmin-nav-tab" class="nav-tab-wrapper">
    <?php
    foreach ($subpages as $sp_slug => $subpage) {
        ?>
        <a href="?page=<?php echo $admin_page ?>&tab=<?php echo $sp_slug ?>"
           class="nav-tab <?php echo $active_tab == $sp_slug ? 'nav-tab-active' : ''; ?>"
        ><?php echo $subpage['title'] ?></a>
        <?php
    }
    ?>
</h2>