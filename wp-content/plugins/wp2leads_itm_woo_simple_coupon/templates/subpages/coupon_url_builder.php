<?php
/**
 *
 */
$coupon_urls_ids = [];

$coupon_urls = get_posts([
    'numberposts' => -1,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'post_type'       => 'wp2leads_coupon_url',
    'suppress_filters' => true,
]);

if (!empty($coupon_urls)) {
    $coupon_urls_by_id = [];

    foreach ($coupon_urls as $coupon_url) {
        $coupon_urls_by_id[$coupon_url->ID] = [
            'title' => $coupon_url->post_title,
            'content' => $coupon_url->post_content,
        ];
    }

    $coupon_urls_ids = array_keys($coupon_urls_by_id);
}

$actions = ['list', 'new', 'edit'];
$action = 'list';

if (!empty($_GET['action']) && in_array($_GET['action'], $actions)) {
    if ('edit' === sanitize_text_field($_GET['action']) && !empty($_GET['ID'])) {
        if (!in_array($_GET['ID'], $coupon_urls_ids)) {
            $action = 'list';
        } else {
            $action = sanitize_text_field($_GET['action']);
        }
    } else {
        $action = sanitize_text_field($_GET['action']);
    }
}

include_once "coupon_url_builder-{$action}.php";
?>