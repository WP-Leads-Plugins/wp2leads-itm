<?php
/**
 * @var $title
 * @var $content
 * @var $website_url
 * @var $url_base
 * @var $expiry_period
 * @var $expiry_period_unit
 * @var $discount_type
 * @var $coupon_amount
 * @var $usage_limit
 * @var $limit_usage_to_x_items
 * @var $usage_limit_per_user
 * @var $minimum_amount
 * @var $maximum_amount
 * @var $customer_email
 * @var $product_to_cart_ids
 * @var $product_ids
 * @var $exclude_product_ids
 * @var $product_categories
 * @var $exclude_product_categories
 * @var $response_type
 * @var $user_link_to
 * @var $user_link_to_product_page
 * @var $user_link_to_product_cat
 * @var $user_link_to_post_page
 * @var $is_kt_invite
 * @var $kt_field
 * @var $kt_field_link
 */

$wc_products = wc_get_products(array(
    'status'  => array( 'publish' ),
    'limit'   => "-1",
    'orderby' => array(
        'title' => 'ASC',
    ),
));

$categories   = get_terms( 'product_cat', 'orderby=name&hide_empty=0' );
$kt_fields = Wp2leadsItmWooSimpleCouponKlickTippManager::get_kt_fields('custom');
$posts = get_posts([
    'numberposts' => -1,
    'orderby'     => 'title',
    'order'       => 'ASC',
    'post_type'       => ['page', 'post'],
    'post_status'       => ['publish'],
]);

global $wpdb;

$query = "
    SELECT 
        p.ID, 
        p.post_title,
        p.post_title,
        p.post_status,
        pm.meta_value AS sku
    FROM {$wpdb->prefix}posts AS p
    INNER JOIN {$wpdb->prefix}postmeta AS pm ON p.ID = pm.post_id AND pm.meta_key = '_sku'
    INNER JOIN {$wpdb->prefix}term_relationships AS term_relationships ON p.ID = term_relationships.object_id
    INNER JOIN {$wpdb->prefix}term_taxonomy AS term_taxonomy ON term_relationships.term_taxonomy_id = term_taxonomy.term_taxonomy_id
    INNER JOIN {$wpdb->prefix}terms AS terms ON term_taxonomy.term_id = terms.term_id
    WHERE p.post_status = 'publish'
      
    AND (
        p.post_type = 'product_variation'
        OR (
            p.post_type = 'product' AND term_taxonomy.taxonomy = 'product_type' AND terms.slug != 'variable'
        )
    )
    ORDER BY p.post_title, p.post_type, p.post_parent
";

$products = $wpdb->get_results($query, ARRAY_A);

if (!empty($product_ids)) {
    $product_not_exclude = array_unique(array_merge($product_to_cart_ids, $product_ids));
} else {
    $product_not_exclude = array_unique($product_to_cart_ids);
}
?>
<div class="wptl-settings-group" style="margin-top: 15px;">
    <div class="wptl-settings-group-header">
        <h3><?php echo __( 'Build New Coupon URL', 'wp2leads_itm_woo_simple_coupon' ); ?></h3>
    </div>

    <div class="wptl-settings-group-body">
        <div class="wptl-row">
            <div class="wptl-col-xs-12">
                <p>
                    <h4><?php echo __('Coupon URL Title', 'wp2leads_itm_woo_simple_coupon'); ?></h4>
                </p>
                <p>
                    <input
                            class="form-input"
                            type="text"
                            name="title"
                            id="coupon_title"
                            placeholder="<?php echo __('Coupon URL Title', 'wp2leads_itm_woo_simple_coupon'); ?>"
                            value="<?php echo $title; ?>"
                    >
                </p>

                <p>
                    <label><?php echo __('Generated Coupon URL', 'wp2leads_itm_woo_simple_coupon'); ?></label><br>

                    <?php echo __('Supports only <strong>GET</strong> HTTP method', 'wp2leads_itm_woo_simple_coupon'); ?>
                </p>

                <p class="copy_field_content_row">
                    <input
                            class="form-input copy_field"
                            type="text"
                            id="coupon_url_complete"
                            readonly
                            placeholder="<?php echo __('Coupon URL', 'wp2leads_itm_woo_simple_coupon'); ?>"
                            value="<?php echo $url_base . $content; ?>"
                    >

                    <button class="button button-small copy_field_content" type="button">
                        <?php echo __('Copy', 'wp2leads_itm_woo_simple_coupon'); ?>
                    </button>
                </p>

                <p>
                    <strong>
                        <?php echo __('For your security never share this link with your customers.', 'wp2leads_itm_woo_simple_coupon'); ?>
                    </strong><br>

                    <?php echo __('This link generates coupon, return coupon code or coupon link in response and fill Coupon Code or Coupon URL fields in KlickTipp.', 'wp2leads_itm_woo_simple_coupon'); ?><br>
                    <?php echo __('F.e.', 'wp2leads_itm_woo_simple_coupon'); ?>
                    <?php echo __('Link could be used in KlickTipp campagne outbound link before sent Coupon Code or Coupon link to user!', 'wp2leads_itm_woo_simple_coupon'); ?><br>
                    <?php echo __('The coupon code and coupon link Fields content should be added to the email to the user, the coupon was created for.', 'wp2leads_itm_woo_simple_coupon'); ?>
                </p>

                    <input
                            class="form-input"
                            type="hidden"
                            id="coupon_url_base"
                            value="<?php echo $url_base; ?>"
                    >
                    <input
                            class="form-input"
                            type="hidden"
                            name="content"
                            id="coupon_content"
                            value="<?php echo $content; ?>"
                    >
            </div>
        </div>
    </div>
</div>

<div class="wptl-settings-group">
    <div class="wptl-settings-group-header">
        <h4 style="margin: 0;"><?php echo __( 'Coupon settings', 'wp2leads_itm_woo_simple_coupon' ); ?></h4>
    </div>

    <div class="wptl-settings-group-body">
        <div class="wptl-row">
            <div class="wptl-col-xs-12 wptl-col-md-8">
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-6">
                        <p>
                            <label for="discount_type"><?php echo __( 'Discount Type', 'wp2leads_itm_woo_simple_coupon' ); ?></label>
                        </p>

                        <p>
                            <select class="form-input" name="discount_type" id="discount_type">
                                <option value="percent"<?php echo $discount_type === 'percent' ? ' selected="selected"' : ''; ?>><?php echo __( 'Percentage discount', 'wp2leads_itm_woo_simple_coupon' ); ?></option>
                                <option value="fixed_cart"<?php echo $discount_type === 'fixed_cart' ? ' selected="selected"' : ''; ?>><?php echo __( 'Fixed cart discount', 'wp2leads_itm_woo_simple_coupon' ); ?></option>
                            </select>
                        </p>

                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-6">
                        <p>
                            <label for="coupon_amount"><?php echo __( 'Coupon amount', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                            <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-coupon_amount"></span>
                        </p>

                        <div id="tippy-content-coupon_amount" class="tippy-content-container">
                            <p>
                                <?php echo __( 'Value of the coupon.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </p>
                        </div>

                        <p>
                            <input
                                    class="form-input"
                                    type="number"
                                    name="coupon_amount"
                                    id="coupon_amount"
                                    min="1"
                                    placeholder="0"
                                    value="<?php echo $coupon_amount; ?>"
                            >
                        </p>
                    </div>
                </div>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-4">
                <div class="wptl-row">
                    <div class="wptl-col-xs-12">
                        <p>
                            <label for="expiry_period"><?php echo __( 'Expiration period', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                            <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-expiry_period"></span>
                        </p>

                        <div id="tippy-content-expiry_period" class="tippy-content-container">
                            <p>
                                <?php echo __( 'Set up days / hours / minutes amount after which coupon will be expired. Left empty or set 0 to disable coupon expiration.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </p>
                        </div>

                        <div class="wptl-row">
                            <div class="wptl-col-xs-12 wptl-col-md-6">
                                <p style="margin-top: 0;">
                                    <input
                                            class="form-input"
                                            type="number"
                                            min="0"
                                            id="expiry_period"
                                            name="expiry_period"
                                            value="<?php echo $expiry_period; ?>"
                                    >
                                </p>
                            </div>
                            <div class="wptl-col-xs-12 wptl-col-md-6">

                                <p style="margin-top: 0;">
                                    <?php
                                    if (false) {
                                        ?>
                                        <?php echo __( 'days', 'wp2leads_itm_woo_simple_coupon' ); ?>
                                        <input name="expiry_period_unit" id="expiry_period_unit" type="hidden" value="days">

                                        <?php
                                    }

                                    if (true) {
                                        ?>
                                        <select class="form-input" name="expiry_period_unit" id="expiry_period_unit">
                                            <option value="minutes"<?php echo $expiry_period_unit === 'minutes' ? ' selected="selected"' : ''; ?>><?php echo __( 'minutes', 'wp2leads_itm_woo_simple_coupon' ); ?></option>
                                            <option value="hours"<?php echo $expiry_period_unit === 'hours' ? ' selected="selected"' : ''; ?>><?php echo __( 'hours', 'wp2leads_itm_woo_simple_coupon' ); ?></option>
                                            <option value="days"<?php echo $expiry_period_unit === 'days' ? ' selected="selected"' : ''; ?>><?php echo __( 'days', 'wp2leads_itm_woo_simple_coupon' ); ?></option>
                                        </select>
                                        <?php
                                    }
                                    ?>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wptl-col-xs-12">
                <p>
                    <label for="free_shipping"><?php echo __( 'Allow free shipping', 'wp2leads_itm_woo_simple_coupon' ); ?></label>
                </p>

                <p>
                    <input
                            type="checkbox"
                            id="free_shipping"
                            name="free_shipping"
                            value="1"
                        <?php echo !empty($individual_use) ? ' checked="checked"' : '';?>
                    >
                    <span class="description">
                                <?php echo __( 'Check this box if the coupon grants free shipping. A free shipping method must be enabled in your shipping zone and be set to require "a valid free shipping coupon".', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </span>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="wptl-settings-group">
    <div class="wptl-settings-group-header">
        <h4 style="margin: 0;"><?php echo __( 'Usage restriction', 'wp2leads_itm_woo_simple_coupon' ); ?></h4>
    </div>

    <div class="wptl-settings-group-body">
        <div class="wptl-row">
            <div class="wptl-col-xs-12 wptl-col-md-6">
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-6">
                        <p>
                            <label for="minimum_amount"><?php echo __( 'Minimum spend', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                            <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-minimum_amount"></span>
                        </p>

                        <div id="tippy-content-minimum_amount" class="tippy-content-container">
                            <p>
                                <?php echo __( 'This field allows you to set the minimum spend (subtotal) allowed to use the coupon.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </p>
                        </div>

                        <p>
                            <input
                                    class="form-input"
                                    type="number"
                                    name="minimum_amount"
                                    id="minimum_amount"
                                    min="0"
                                    placeholder="<?php echo __( 'No minimum', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                                    value="<?php echo $minimum_amount; ?>"
                            >
                        </p>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-6">
                        <p>
                            <label for="maximum_amount"><?php echo __( 'Maximum spend', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                            <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-maximum_amount"></span>
                        </p>

                        <div id="tippy-content-maximum_amount" class="tippy-content-container">
                            <p>
                                <?php echo __( 'This field allows you to set the maximum spend (subtotal) allowed to use the coupon.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </p>
                        </div>

                        <p>
                            <input
                                    class="form-input"
                                    type="number"
                                    name="maximum_amount"
                                    id="maximum_amount"
                                    min="0"
                                    placeholder="<?php echo __( 'No maximum', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                                    value="<?php echo $maximum_amount; ?>"
                            >
                        </p>
                    </div>
                </div>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-6">
                <p>
                    <label for="customer_email"><?php echo __( 'Allowed email', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-customer_email"></span>
                </p>

                <div id="tippy-content-customer_email" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Allowed billing email to check against when an order is placed. You should set only one email.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <input
                            class="form-input"
                            type="text"
                            name="customer_email"
                            id="customer_email"
                            min="0"
                            placeholder="<?php echo __( 'Input allowed emails', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                            value="<?php echo $customer_email; ?>"
                    >
                </p>
            </div>
        </div>

        <div class="wptl-row">
            <div class="wptl-col-xs-12 wptl-col-md-6">
                <p>
                    <label for="individual_use"><?php echo __( 'Individual use only', 'wp2leads_itm_woo_simple_coupon' ); ?></label>
                </p>

                <p>
                    <input
                            type="checkbox"
                            id="individual_use"
                            name="individual_use"
                            value="1"
                        <?php echo !empty($individual_use) ? ' checked="checked"' : '';?>
                    >
                    <span class="description">
                                <?php echo __( 'Check this box if the coupon cannot be used in conjunction with other coupons.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </span>
                </p>
            </div>
            <div class="wptl-col-xs-12 wptl-col-md-6">
                <p>
                    <label for="exclude_sale_items"><?php echo __( 'Exclude sale items', 'wp2leads_itm_woo_simple_coupon' ); ?></label>
                </p>

                <p>
                    <input
                            type="checkbox"
                            id="exclude_sale_items"
                            name="exclude_sale_items"
                            value="1"
                        <?php echo !empty($exclude_sale_items) ? ' checked="checked"' : '';?>
                    >
                    <span class="description">
                                <?php echo __( 'Check this box if the coupon should not apply to items on sale. Per-item coupons will only work if the item is not on sale. Per-cart coupons will only work if there are items in the cart that are not on sale.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </span>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="wptl-settings-group">
    <div class="wptl-settings-group-header">
        <h4 style="margin: 0;"><?php echo __( 'Usage limits', 'wp2leads_itm_woo_simple_coupon' ); ?></h4>
    </div>

    <div class="wptl-settings-group-body">
        <div class="wptl-row">
            <div class="wptl-col-xs-12 wptl-col-md-4">
                <p>
                    <label for="usage_limit"><?php echo __( 'Usage limit per coupon', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-usage_limit"></span>
                </p>

                <div id="tippy-content-usage_limit" class="tippy-content-container">
                    <p>
                        <?php echo __( 'How many times this coupon can be used before it is void.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <input
                            class="form-input"
                            type="number"
                            name="usage_limit"
                            id="usage_limit"
                            min="0"
                            placeholder="<?php echo __( 'Usage limit per coupon', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                            value="<?php echo $usage_limit; ?>"
                    >
                </p>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-4">
                <p>
                    <label for="limit_usage_to_x_items"><?php echo __( 'Limit usage to X items', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-limit_usage_to_x_items"></span>
                </p>

                <div id="tippy-content-limit_usage_to_x_items" class="tippy-content-container">
                    <p>
                        <?php echo __( 'The maximum number of individual items this coupon can apply to when using product discounts. Leave blank to apply to all qualifying items in cart.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <input
                            class="form-input"
                            type="number"
                            name="limit_usage_to_x_items"
                            id="limit_usage_to_x_items"
                            min="0"
                            placeholder="<?php echo __( 'Limit usage to X items', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                            value="<?php echo $limit_usage_to_x_items; ?>"
                    >
                </p>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-4">
                <p>
                    <label for="usage_limit_per_user"><?php echo __( 'Usage limit per user', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-usage_limit_per_user"></span>
                </p>

                <div id="tippy-content-usage_limit_per_user" class="tippy-content-container">
                    <p>
                        <?php echo __( 'How many times this coupon can be used by an individual user. Uses billing email for guests, and user ID for logged in users.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <input
                            class="form-input"
                            type="number"
                            name="usage_limit_per_user"
                            id="usage_limit_per_user"
                            min="0"
                            placeholder="<?php echo __( 'Usage limit per user', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                            value="<?php echo $usage_limit_per_user; ?>"
                    >
                </p>
            </div>
        </div>
    </div>
</div>

<div class="wptl-settings-group">
    <div class="wptl-settings-group-header">
        <h4 style="margin: 0;"><?php echo __( 'Additional parameters', 'wp2leads_itm_woo_simple_coupon' ); ?></h4>
    </div>

    <div class="wptl-settings-group-body">
        <div class="wptl-row">
            <div class="wptl-col-xs-12 wptl-col-md-6">
                <p>
                    <label for="product_to_cart_ids"><?php echo __( 'Products to Add to Cart', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-product_to_cart_ids"></span>
                </p>

                <div id="tippy-content-product_to_cart_ids" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Add these products to the customers cart when they visit the coupon URL.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select id="product_to_cart_ids" class="form-input-select2__multiple" multiple="multiple"
                        data-placeholder="<?php esc_attr_e( 'Search for a product...', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                    >
                        <option></option>
                        <?php
                        foreach ($products as $product) {
                            $sku_label = !empty($product['sku']) ? ' (' . $product['sku'] . ')' : '';
                            $selected = in_array($product['ID'], $product_to_cart_ids) ? 'selected="selected"' : '';
                            ?>
                            <option value="<?php echo $product['ID'] ?>" <?php echo $selected; ?>>
                                <?php echo $product['post_title'] . $sku_label ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </p>

                <p>
                    <label for="product_ids"><?php echo __( 'Include products', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-product_ids"></span>
                </p>

                <div id="tippy-content-product_ids" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Products that the coupon will be applied to, or that need to be in the cart in order for the discount to be applied.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select id="product_ids" class="form-input-select2__multiple" multiple="multiple"
                            data-placeholder="<?php esc_attr_e( 'Search for a product...', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                    >
                        <option></option>
                        <?php
                        foreach ($products as $product) {
                            $sku_label = !empty($product['sku']) ? ' (' . $product['sku'] . ')' : '';
                            $selected = in_array($product['ID'], $product_ids) ? 'selected="selected"' : '';
                            ?>
                            <option value="<?php echo $product['ID'] ?>" <?php echo $selected; ?>>
                                <?php echo $product['post_title'] . $sku_label ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </p>

<!--                <p>-->
<!--                    <select-->
<!--                            id="product_ids"-->
<!--                            class="form-input-select2__product_search"-->
<!--                            multiple="multiple"-->
<!--                            data-exclude="--><?php //echo wc_esc_json( wp_json_encode( $exclude_product_ids ) ); ?><!--"-->
<!--                            data-exclude_type="variable"-->
<!--                            data-security="--><?php //echo wp_create_nonce( 'search-products' ); ?><!--"-->
<!--                            data-placeholder="--><?php //esc_attr_e( 'Search for a product...', 'wp2leads_itm_woo_simple_coupon' ); ?><!--"-->
<!--                            data-action="woocommerce_json_search_products_and_variations">-->
<!---->
<!--                        --><?php
//                        foreach( $product_ids as $product_id ) {
//                            if ( $product = wc_get_product( $product_id ) ) {
//                                if ( ! $product->is_type( 'variable' ) ) {
//                                    ?>
<!--                                    <option value="--><?php //echo esc_attr( $product_id ); ?><!--" selected="selected">-->
<!--                                        --><?php //echo $product->get_formatted_name(); ?>
<!--                                    </option>-->
<!--                                    --><?php
//                                }
//                            }
//                        } ?>
<!--                    </select>-->
<!--                </p>-->

                <p>
                    <label for="exclude_product_ids"><?php echo __( 'Exclude products', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-exclude_product_ids"></span>
                </p>

                <div id="tippy-content-exclude_product_ids" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Products that the coupon will not be applied to, or that cannot be in the cart in order for the discount to be applied.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select id="exclude_product_ids" class="form-input-select2__multiple" multiple="multiple"
                            data-placeholder="<?php esc_attr_e( 'Search for a product...', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                    >
                        <option></option>
                        <?php
                        foreach ($products as $product) {
                            $sku_label = !empty($product['sku']) ? ' (' . $product['sku'] . ')' : '';
                            $selected = in_array($product['ID'], $exclude_product_ids) ? 'selected="selected"' : '';
                            ?>
                            <option value="<?php echo $product['ID'] ?>" <?php echo $selected; ?>>
                                <?php echo $product['post_title'] . $sku_label ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </p>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-6">
                <p>
                    <label for="product_categories"><?php echo __( 'Include product categories', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-product_categories"></span>
                </p>

                <div id="tippy-content-product_categories" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Product categories that the coupon will be applied to, or that need to be in the cart in order for the discount to be applied.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select id="product_categories" class="form-input-select2__product_categories" multiple="multiple">
                        <?php
                        if ( $categories ) {
                            foreach ( $categories as $cat ) {
                                $selected = in_array($cat->term_id, $product_categories) ? ' selected="selected"' : '';
                                echo '<option value="' . esc_attr( $cat->term_id ) . '"'.$selected.'>' . esc_html( $cat->name ) . '</option>';
                            }
                        }
                        ?>
                    </select>
                </p>

                <p>
                    <label for="exclude_product_categories"><?php echo __( 'Exclude categories', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-exclude_product_categories"></span>
                </p>

                <div id="tippy-content-exclude_product_categories" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Product categories that the coupon will not be applied to, or that cannot be in the cart in order for the discount to be applied.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select id="exclude_product_categories" class="form-input-select2__exclude_product_categories" multiple="multiple">
                        <?php
                        if ( $categories ) {
                            foreach ( $categories as $cat ) {
                                $selected = in_array($cat->term_id, $exclude_product_categories) ? ' selected="selected"' : '';
                                echo '<option value="' . esc_attr( $cat->term_id ) . '"'.$selected.'>' . esc_html( $cat->name ) . '</option>';
                            }
                        }
                        ?>
                    </select>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="wptl-settings-group">
    <div class="wptl-settings-group-header">
        <h4 style="margin: 0;"><?php echo __( 'Other settings', 'wp2leads_itm_woo_simple_coupon' ); ?></h4>
    </div>

    <div class="wptl-settings-group-body">
        <div class="wptl-row">
            <div class="wptl-col-xs-12 wptl-col-md-4">
                <p>
                    <label for="response_type"><?php echo __( 'Response type', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-response_type"></span>
                </p>

                <div id="tippy-content-response_type" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Select which response will be for coupon URL with settings after coupon will be generated.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select class="form-input" name="response_type" id="response_type">
                        <option value="coupon_code"<?php echo 'coupon_code' === $response_type ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Coupon code', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>

                        <option value="coupon_url"<?php echo 'coupon_url' === $response_type ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Coupon link', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>
                    </select>
                </p>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-4">
                <p>
                    <label for="user_link_to"><?php echo __( 'Customer coupon link to', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-user_link_to"></span>
                </p>

                <div id="tippy-content-user_link_to" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Select page, post or product page where customer link should point to.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select class="form-input" name="user_link_to" id="user_link_to">
                        <option value="home_page"<?php echo empty($user_link_to) || 'home_page' === $user_link_to ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Home page', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>

                        <option value="shop_page"<?php echo 'shop_page' === $user_link_to ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Shop page', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>

                        <option value="product_page"<?php echo 'product_page' === $user_link_to ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Product page', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>

                        <option value="product_cat"<?php echo 'product_cat' === $user_link_to ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Product category', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>

                        <option value="post_page"<?php echo 'post_page' === $user_link_to ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Page / Post', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>
                    </select>
                </p>

                <p
                    id="user_link_to_product_page_container"
                    class="user_link_to_value_container"
                    <?php echo 'product_page' === $user_link_to ? '' : 'style="display:none;"'; ?>
                >
                    <?php
                    if (!empty($wc_products)) {
                        ?>
                        <select id="user_link_to_product_page" name="user_link_to_product_page" class="form-input-select2">
                            <?php
                            foreach ($wc_products as $wc_product) {
                                $selected = (int)$wc_product->get_ID() === (int)$user_link_to_product_page ? ' selected="selected"' : '';
                                echo '<option value="' . esc_attr( $wc_product->get_ID() ) . '"'.$selected.'>' . esc_html( $wc_product->get_title() ) . '</option>';
                            }
                            ?>
                        </select>
                        <?php
                    } else {
                        ?>
                        <?php echo __( 'Create at least one product', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        <?php
                    }
                    ?>
                </p>

                <p
                    id="user_link_to_product_cat_container"
                    class="user_link_to_value_container"
                    <?php echo 'product_cat' === $user_link_to ? '' : 'style="display:none;"'; ?>
                >
                    <?php
                    if (!empty($categories)) {
                        ?>
                        <select id="user_link_to_product_cat" name="user_link_to_product_cat" class="form-input-select2">
                            <?php
                            if ( $categories ) {
                                foreach ( $categories as $cat ) {
                                    $selected = (int)$cat->term_id === (int)$user_link_to_product_cat ? ' selected="selected"' : '';
                                    echo '<option value="' . esc_attr( $cat->term_id ) . '"'.$selected.'>' . esc_html( $cat->name ) . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <?php
                    } else {
                        ?>
                        <?php echo __( 'Create at least one category', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        <?php
                    }
                    ?>
                </p>

                <p
                    id="user_link_to_post_page_container"
                    class="user_link_to_value_container"
                    <?php echo 'post_page' === $user_link_to ? '' : 'style="display:none;"'; ?>
                >
                    <?php
                    if (!empty($posts)) {
                        ?>
                        <select id="user_link_to_post_page" name="user_link_to_post_page" class="form-input-select2">
                            <?php
                            if ( $posts ) {
                                foreach ( $posts as $post_item ) {
                                    $selected = (int)$post_item->ID === (int)$user_link_to_post_page ? ' selected="selected"' : '';
                                    echo '<option value="' . esc_attr( $post_item->ID ) . '"'.$selected.'>' . esc_html( $post_item->post_title ) . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <?php
                    } else {
                        ?>
                        <?php echo __( 'Create at least page / post', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        <?php
                    }
                    ?>
                </p>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-4">
                <p>
                    <label for="is_kt_invite"><?php echo __( 'KT Invite', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-is_kt_invite"></span>
                </p>

                <div id="tippy-content-is_kt_invite" class="tippy-content-container">
                    <p>
                        <?php echo __( 'If you have WP2LEADS version 3.1.0 or newer you can register and autologin customer using KlickTipp data select Enable for KT Invite. You need to set up "Allowed email" field or if you are going to use KlickTipp outbound link email will be added by default.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>

                <p>
                    <select class="form-input" name="is_kt_invite" id="is_kt_invite">
                        <option value="disabled"<?php echo 'disabled' === $is_kt_invite ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Disabled', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>

                        <option value="enabled"<?php echo 'enabled' === $is_kt_invite ? ' selected="selected"' : ''; ?>>
                            <?php echo __( 'Enabled', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        </option>
                    </select>
                </p>

                <?php
                if (is_wp_error($kt_fields)) {
                    echo sprintf(
                        __( 'This feature will work only if WP2LEADS has a working connection to KlickTipp. <br>Set up connection %shere%s.', 'wp2leads_itm_woo_simple_coupon' ),
                        '<a href="/wp-admin/admin.php?page=wp2l-admin&tab=settings" target="_blank">',
                        '</a>'
                    );
                }
                ?>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-6">
                <p>
                    <label for="kt_field"><?php echo __( 'KT Field for coupon code', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-kt_field"></span>
                </p>

                <div id="tippy-content-kt_field" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Select KlickTipp field for transferring customer coupon code', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>
                <p>
                    <?php
                    if (is_wp_error($kt_fields)) {
                        ?>
                        <input type="hidden" name="kt_field" value="<?php echo $kt_field ?>">
                        <?php
                        echo sprintf(
                            __( 'You can select fields only if WP2LEADS has a working connection to KlickTipp. <br>Set up connection %shere%s.', 'wp2leads_itm_woo_simple_coupon' ),
                            '<a href="/wp-admin/admin.php?page=wp2l-admin&tab=settings" target="_blank">',
                            '</a>'
                        );
                    } else if (empty($kt_fields)) {
                        ?>
                        <?php echo __( 'If you want to transfer customer coupon code to KlickTipp create at least one field in KlickTipp dashboard.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        <input type="hidden" name="kt_field" value="<?php echo $kt_field ?>">
                        <?php
                    } else {
                        ?>
                        <select name="kt_field" id="kt_field" class="form-input-select2">
                            <option></option>
                            <?php
                            foreach ($kt_fields as $fid => $ftitle) {
                                ?>
                                <option value="<?php echo $fid; ?>"<?php echo $fid === $kt_field ? ' selected="selected"' : ''; ?>>
                                    <?php echo $ftitle; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                </p>
            </div>

            <div class="wptl-col-xs-12 wptl-col-md-6">
                <p class="tippy-container">
                    <label for="kt_field"><?php echo __( 'KT Field for coupon link', 'wp2leads_itm_woo_simple_coupon' ); ?></label>

                    <span class="tippy-button dashicons dashicons-editor-help" data-tippy-template="tippy-content-kt_field_link"></span>
                </p>

                <div id="tippy-content-kt_field_link" class="tippy-content-container">
                    <p>
                        <?php echo __( 'Select KlickTipp field for transferring customer coupon link', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </p>
                </div>
                <p>
                    <?php
                    if (is_wp_error($kt_fields)) {
                        ?>
                        <input type="hidden" name="kt_field_link" value="<?php echo $kt_field_link ?>">
                        <?php
                        echo sprintf(
                            __( 'You can select fields only if WP2LEADS has a working connection to KlickTipp. <br>Set up connection %shere%s.', 'wp2leads_itm_woo_simple_coupon' ),
                            '<a href="/wp-admin/admin.php?page=wp2l-admin&tab=settings" target="_blank">',
                            '</a>'
                        );
                    } else if (empty($kt_fields)) {
                        ?>
                        <?php echo __( 'If you want to transfer customer coupon link to KlickTipp create at least one field in KlickTipp dashboard.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                        <input type="hidden" name="kt_field_link" value="<?php echo $kt_field_link ?>">
                        <?php

                        // var_dump($kt_fields);
                    } else {
                        ?>
                        <select name="kt_field_link" id="kt_field_link" class="form-input-select2">
                            <option></option>
                            <?php
                            foreach ($kt_fields as $fid => $ftitle) {
                                ?>
                                <option value="<?php echo $fid; ?>"<?php echo $fid === $kt_field_link ? ' selected="selected"' : ''; ?>>
                                    <?php echo $ftitle; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                </p>
                <?php
                // var_dump(Wp2leadsItmWooSimpleCouponKlickTippManager::get_subscriber_by_email('i.synthetica+202112061202@gmail.com'));
                ?>
            </div>
        </div>
    </div>
</div>
