<?php
$coupon_url = Wp2leadsItmWooSimpleCouponUrlBuilder::get(sanitize_text_field($_GET['ID']));
extract($coupon_url);
?>

<form id="update_coupon_url_form" class="coupon_url_form">
    <div class="wptl-row">
        <div class="wptl-col-xs-12 wptl-col-md-8 wptl-col-lg-9">
            <input type="hidden" name="id" id="id" value="<?php echo $_GET['ID']; ?>">
            <?php include_once "coupon_url_builder-fields.php"; ?>
        </div>

        <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-3">
            <div class="wptl-settings-group" style="margin-top: 15px;">
                <div class="wptl-settings-group-header">
                    <h3><?php echo __( 'Actions', 'wp2leads_itm_woo_simple_coupon' ); ?></h3>
                </div>

                <div class="wptl-settings-group-body">
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12">
                            <p>
                                <button id="update_coupon_url_btn" class="button button-primary" type="button">
                                    <?php echo __( 'Update', 'wp2leads_itm_woo_simple_coupon' ); ?>
                                </button>

                                <a href="?page=wp2leads_itm_woo_simple_coupon" id="cancel_coupon_url_btn" class="button button-cancel">
                                    <?php echo __( 'Cancel', 'wp2leads_itm_woo_simple_coupon' ); ?>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>