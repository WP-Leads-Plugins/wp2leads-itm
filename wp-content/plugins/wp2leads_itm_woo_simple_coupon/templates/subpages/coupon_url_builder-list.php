<?php
/**
 * @var $coupon_urls_by_id
 */

$url_base = rtrim(get_site_url('/'), '/') . '/wp-json/witmwoosc/v1/coupon';
$inactive_expiration = Wp2leadsItmWooSimpleCouponUrlBuilder::get_inactive_expiration();
?>

<div class="wptl-settings-group" style="margin-top: 15px;">
    <div class="wptl-settings-group-header">
        <h3 style="display: inline-block;margin-right: 5px;margin-top: 5px;"><?php echo __( 'URLs', 'wp2leads_itm_woo_simple_coupon' ); ?></h3>
            <a href="?page=wp2leads_itm_woo_simple_coupon&tab=coupon_url_builder&action=new"
               class="button button-small"
            ><?php echo __( 'Add New', 'wp2leads_itm_woo_simple_coupon' ); ?></a>
    </div>

    <div class="wptl-settings-group-body">
        <?php
        if (!empty($coupon_urls_by_id)) {
            foreach ($coupon_urls_by_id as $id => $item) {
                ?>
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-2">
                        <p>
                            <label><?php echo $item['title']; ?></label><br>
                            <?php echo __('Supports only <strong>GET</strong> HTTP method', 'wp2leads_itm_woo_simple_coupon'); ?>
                        </p>
                    </div>
                    <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-8">
                        <p>
                        <p class="copy_field_content_row">
                            <input
                                    class="form-input copy_field"
                                    type="text"
                                    id="coupon_url_complete"
                                    readonly
                                    value="<?php echo $url_base . $item['content']; ?>"
                            >

                            <button class="button button-small copy_field_content" type="button">
                                <?php echo __('Copy', 'wp2leads_itm_woo_simple_coupon'); ?>
                            </button>
                        </p>
                        </p>
                    </div>
                    <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-2">
                        <p>
                            <a href="?page=wp2leads_itm_woo_simple_coupon&tab=coupon_url_builder&action=edit&ID=<?php echo $id; ?>" id="cancel_coupon_url_btn" class="button button-primary">
                                <?php echo __( 'Edit', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </a>

                            <button data-id="<?php echo $id; ?>" class="button delete_coupon_url_btn" type="button"
                                    data-confirm="<?php echo __( 'Are you sure you want to delete this item?', 'wp2leads_itm_woo_simple_coupon' ); ?>"
                            >
                                <?php echo __( 'Delete', 'wp2leads_itm_woo_simple_coupon' ); ?>
                            </button>
                        </p>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="wptl-row">
                <div class="wptl-col-xs-12">
                    <h3><?php echo __( 'Start with creating new coupon generation URL', 'wp2leads_itm_woo_simple_coupon' ); ?></h3>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <div class="wptl-settings-group-body">
        <div class="wptl-row">
            <div class="wptl-col-xs-12">
                <h4>
                    <?php echo __( 'Manage generated coupons', 'wp2leads_itm_woo_simple_coupon' ); ?>
                </h4>

                <p>
                    <label for="">
                        <?php echo __( 'Outdated period (days)', 'wp2leads_itm_woo_simple_coupon' ); ?>:
                    </label>

                    <input id="update_generated_coupons_expiration" class="form-input" style="width: 60px;display: inline-block;" type="number" min="0" value="<?php echo $inactive_expiration; ?>">

                    <button id="update_generated_coupons_expiration_btn" class="button button-small" style="margin-right: 20px;">
                        <?php echo __( 'Update outdated period', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </button>

                    <button id="delete_expired_generated_coupons" class="button button-small">
                        <?php echo __( 'Delete outdated', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </button>

                    <button id="delete_all_generated_coupons" class="button button-small">
                        <?php echo __( 'Delete all', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </button>
                </p>

                <p>
                    <?php echo __( 'In this section you can clean up your coupons deleting outdated or even all automatically generated coupons.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    <br>
                    <?php echo __( "Outdated coupons - coupons that <strong>was not applied</strong>:", 'wp2leads_itm_woo_simple_coupon' ); ?>
                </p>
                <ol>
                    <li>
                        <?php echo __( 'if expiration period set up - until the date it should expire at', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </li>

                    <li>
                        <?php echo __( 'if expiration period not set up - until date it was generated plus days set up in "Outdated period (days)" field above', 'wp2leads_itm_woo_simple_coupon' ); ?>
                    </li>
                </ol>

                <p>
                    <?php echo __( 'All coupons that <strong>was applied</strong> at least once need to be managed in Woocommerce coupon page manually. Autogenerated coupons will have date under "Auto generated" column.', 'wp2leads_itm_woo_simple_coupon' ); ?>
                </p>
                <?php
                if (false) {
                    ?>
                    <p>
                        <?php
                        $now_ts = current_time('U');
                        // Wp2leadsItmWooSimpleCouponModel::delete_coupons_by_id(['503','504','505','506','507']);
                        $coupons_id = Wp2leadsItmWooSimpleCouponUrlBuilder::get_generated_coupons_id('expired');
                        $coupons_id = Wp2leadsItmWooSimpleCouponUrlBuilder::get_generated_coupons_id('active');
                        $coupons_id = Wp2leadsItmWooSimpleCouponUrlBuilder::get_generated_coupons_id();

                        ?>
                    <p>
                        Now: <?php echo date('d-m-Y H:i:s', $now_ts); ?>
                    </p>
                    <?php
                    foreach ($coupons_id as $id => $data) {
                        ?>
                        <h4><?php echo $id; ?></h4>
                        <p>
                            Date generated: <?php echo date('d-m-Y H:i:s', $data['date_generated']); ?>
                            <?php
                            if (!empty($data['date_expires'])) {
                                ?>
                                <br>
                                Now: <?php echo date('d-m-Y H:i:s', $now_ts); ?>
                                <br>
                                Date expires: <?php echo date('d-m-Y H:i:s', $data['date_expires']); ?>
                                <?php
                            }

                            if (!empty($data['expired'])) {
                                ?>
                                <br>
                                <strong style="color: red;">Expired</strong>
                                <?php
                            } else {
                                ?>
                                <br>
                                <strong style="color: green;">Active</strong>
                                <?php
                            }
                            ?>
                        </p>
                        <?php
                    }
                    ?>
                    </p>
                    <?php
                }
                ?>

            </div>
        </div>
        <hr>
    </div>
</div>
