<?php
$coupons = Wp2leadsItmWooSimpleCouponModel::get_coupons();
$coupons_teemplate = array(
    'coupon_1' => array(
        'row_title' => __('First coupon', 'wp2leads_itm_woo_simple_coupon'),
        'row_position' => 1,
    ),
    'coupon_2' => array(
        'row_title' => __('Second coupon', 'wp2leads_itm_woo_simple_coupon'),
        'row_position' => 2,
    ),
    'coupon_3' => array(
        'row_title' => __('Third coupon', 'wp2leads_itm_woo_simple_coupon'),
        'row_position' => 3,
    ),
);

$orders_attached = Wp2leadsItmWooSimpleCouponModel::get_attached_orders();
$map_name = !empty($_GET['map_name']) ? str_replace('_', ' ', sanitize_text_field($_GET['map_name'])) : '';

if (!empty($map_name)) {
    global $wpdb;
    $table = $wpdb->prefix . 'wp2l_maps';
    $sql = "SELECT * FROM {$table} WHERE name LIKE '%{$map_name}%' ORDER BY id DESC";
    $result = $wpdb->get_results($sql, ARRAY_A);

    if (!empty($result)) {
        $map_id = $result[0]['id'];
    } else {
        $map_id = get_option('wp2leads_itm_woo_simple_coupon_last_map');
    }
} else {
    $map_id = get_option('wp2leads_itm_woo_simple_coupon_last_map');
}

if (!empty($map_id)) {
    $map = MapsModel::get($map_id);
//    $mapping = unserialize($map->mapping);
//
//    var_dump($mapping);

    if (empty($map)) {
        $map_id = '';
    } else {
        delete_option('wp2leads_itm_woo_simple_coupon_last_map');
        add_option('wp2leads_itm_woo_simple_coupon_last_map', $map_id);
    }
}
?>

<div class="wptl-settings-group" style="margin-top: 15px;">
    <div class="wptl-settings-group-header">
        <h3><?php _e("Map to KlickTipp Settings" , 'wp2leads_itm_woo_simple_coupon') ?></h3>
    </div>

    <div class="wptl-settings-group-body">
        <?php
        foreach ($coupons_teemplate as $i => $coupon_template) {
            ?>
            <div class="wptl-row">
                <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-2">
                    <h4>
                        <?php echo $coupon_template['row_title'] ?>
                    </h4>
                </div>

                <?php
                if (!empty($coupons[$i])) {
                    $coupon = $coupons[$i];
                    ?>
                    <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-8">
                        <p>
                            <?php _e('Coupon code', 'wp2leads_itm_woo_simple_coupon') ?>: <strong><?php echo $coupon['coupon_code'] ?></strong> -
                            <?php _e('Coupon amount', 'wp2leads_itm_woo_simple_coupon') ?>: <strong><?php echo $coupon['coupon_amount'] ?></strong>
                        </p>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-3 wptl-col-lg-2">
                        <p>
                            <button
                                id="wp2leads_itm_woo_simple_coupon_delete_<?php echo $coupon_template['row_position'] ?>"
                                type="button"
                                data-coupon="<?php echo $coupon['ID'] ?>"
                                class="button button-primary wp2leads_itm_woo_simple_coupon_delete"
                            >
                                <?php echo __('Delete coupon', 'wp2leads_itm_woo_simple_coupon'); ?>
                            </button>
                        </p>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-8">
                        <div class="wptl-row">
                            <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                                <p>
                                    <input
                                        class="form-input"
                                        type="text"
                                        id="wp2leads_itm_woo_simple_coupon_generate_<?php echo $coupon_template['row_position'] ?>_code"
                                        placeholder="<?php echo __('Input Coupon Code', 'wp2leads_itm_woo_simple_coupon'); ?>"
                                    >
                                </p>
                            </div>

                            <div class="wptl-col-xs-12 wptl-col-md-6 wptl-col-lg-6">
                                <p>
                                    <input
                                        class="form-input"
                                        type="text"
                                        id="wp2leads_itm_woo_simple_coupon_generate_<?php echo $coupon_template['row_position'] ?>_amount"
                                        placeholder="<?php echo __('Input Coupon Amount, %', 'wp2leads_itm_woo_simple_coupon'); ?>"
                                    >
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-3 wptl-col-lg-2">
                        <p>
                            <button
                                id="wp2leads_itm_woo_simple_coupon_generate_<?php echo $coupon_template['row_position'] ?>"
                                type="button"
                                data-position="<?php echo $coupon_template['row_position'] ?>"
                                class="button button-primary wp2leads_itm_woo_simple_coupon_generate"
                            >
                                <?php echo __('Generate coupon', 'wp2leads_itm_woo_simple_coupon'); ?>
                            </button>
                        </p>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<?php
if (!empty($coupons)) {
    if (empty($orders_attached)) {
        ?>
        <div class="wptl-settings-group">
            <div class="wptl-settings-group-header">
                <h3>
                    <?php _e("Let's start" , 'wp2leads_itm_woo_simple_coupon') ?>
                </h3>
            </div>

            <div class="wptl-settings-group-body">
                <div class="wptl-row">
                    <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-2">
                        <h4>
                            <?php _e("Generate dummy order" , 'wp2leads_itm_woo_simple_coupon') ?>
                        </h4>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-8">
                        <p>
                            <?php _e("In order to set up your map you need to generate dummy order. Click \"Generate order\" button." , 'wp2leads_itm_woo_simple_coupon') ?>
                        </p>
                    </div>

                    <div class="wptl-col-xs-12 wptl-col-md-3 wptl-col-lg-2">
                        <p>
                            <button
                                id="wp2leads_itm_woo_simple_coupon_generate_order"
                                type="button"
                                class="button button-primary"
                            >
                                <?php echo __('Generate order', 'wp2leads_itm_woo_simple_coupon'); ?>
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="wptl-settings-group">
            <div class="wptl-settings-group-header">
                <h3>
                    <?php _e("You are ready to go" , 'wp2leads_itm_woo_simple_coupon') ?>
                </h3>
            </div>

            <div class="wptl-settings-group-body">

                <?php
                if (!empty($map_id)) {
                    ?>
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-2">
                            <h4>
                                <?php _e("Map installed" , 'wp2leads_itm_woo_simple_coupon') ?>
                            </h4>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-8">
                            <p>
                                <?php _e("You are ready to use Woo Simple Coupon map." , 'wp2leads_itm_woo_simple_coupon') ?>
                                <br>
                                <?php _e("Please, visit Map to API page and edit map if needed." , 'wp2leads_itm_woo_simple_coupon') ?>
                            </p>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-3 wptl-col-lg-2">
                            <p>
                                <a target="_blank" class="button button-primary" href="?page=wp2l-admin&tab=map_to_api&active_mapping=<?php echo $map_id ?>">
                                    <?php echo __('Edit map', 'wp2leads_itm_woo_simple_coupon'); ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="wptl-row">
                        <div class="wptl-col-xs-12 wptl-col-md-4 wptl-col-lg-2">
                            <h4>
                                <?php _e("Install map" , 'wp2leads_itm_woo_simple_coupon') ?>
                            </h4>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-5 wptl-col-lg-8">
                            <p>
                                <?php _e("Please, visit catalogue page and install Woo Simple Coupon Item." , 'wp2leads_itm_woo_simple_coupon') ?>
                            </p>
                        </div>

                        <div class="wptl-col-xs-12 wptl-col-md-3 wptl-col-lg-2">
                            <p>
                                <a target="_blank" class="button button-primary" href="?page=wp2l-admin">
                                    <?php echo __('Install map', 'wp2leads_itm_woo_simple_coupon'); ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
}
?>