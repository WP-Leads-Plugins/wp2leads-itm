<?php
/**
 * Class Wp2leadsItmWooSimpleCouponRest
 * @url
 */

class Wp2leadsItmWooSimpleCouponRest extends WP_REST_Controller  {
    protected $namespace;

    private $version = '1';

    public function __construct() {
        $this->namespace = 'witmwoosc/v' . $this->version;
        $this->rest_base = 'events';
    }

    public function register_routes() {
        $args = $this->get_coupon_fields();
        $args['coupon_code'] = [
            'type'     => 'string',
        ];
        register_rest_route( $this->namespace, '/coupon', [
            [
                'methods'             => [WP_REST_Server::READABLE],
                'callback'            => [ $this, 'generate' ],
                'permission_callback' => [ $this, 'permissions_check' ],
                'args' => $args,
            ]
        ]);
    }

    public function sanitize_customer_email($value) {
        $emails = explode(',', $value);

        foreach ($emails as $i => $email) {
            $email = trim($email);
            $email = str_replace(' ', '+', $email);
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);

            if (empty($email)) {
                unset($emails[$i]);
            } else {
                $emails[$i] = $email;
            }
        }

        return $emails;
    }

    public function get_coupon_fields() {
        return [
            'discount_type' => [
                'required' => true,
                'type'     => 'string',
                'enum' => ['percent', 'fixed_cart'],
            ],
            'coupon_amount' => [
                'required' => true,
                'type'     => 'number',
                'minimum' => 1,
            ],
            'customer_email' => [
                'type'     => 'string',
                'sanitize_callback' => [$this, 'sanitize_customer_email']
            ],
            'email' => [
                'type'     => 'string',
                'sanitize_callback' => [$this, 'sanitize_customer_email']
            ],
            'maximum_amount' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'minimum_amount' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'product_categories' => [
                'type'     => 'string',
            ],
            'exclude_product_categories' => [
                'type'     => 'string',
            ],
            'product_to_cart_ids' => [
                'type'     => 'string',
            ],
            'product_ids' => [
                'type'     => 'string',
            ],
            'exclude_product_ids' => [
                'type'     => 'string',
            ],
            'expiry_period' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'expiry_period_unit' => [
                'type'     => 'string',
                'default' => 'days',
                'enum' => ['minutes', 'hours', 'days'],
            ],
            'limit_usage_to_x_items' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'usage_limit_per_user' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'usage_limit' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'free_shipping' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'individual_use' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'exclude_sale_items' => [
                'type'     => 'number',
                'minimum' => 1,
            ],
            'response_type' => [
                'type'     => 'string',
                'default' => 'coupon_code',
                'enum' => ['coupon_url', 'coupon_code'],
            ],
            'user_link_to' => [
                'type'     => 'string',
                'enum' => ['shop_page', 'product_page', 'product_cat', 'post_page'],
            ],
            'user_link_to_id' => [
                'type'     => 'string',
            ],
            'is_kt_invite' => [
                'type'     => 'string',
            ],
            'kt_field' => [
                'type'     => 'string',
            ],
            'kt_field_link' => [
                'type'     => 'string',
            ],
        ];
    }

    public function generate($request) {
        $coupon_code = Wp2leadsItmWooSimpleCouponModel::generate_coupon_code();
        $params = $request->get_params();
        $coupon_amount = $params['coupon_amount'];
        $args = ['discount_type' => $params['discount_type']];
        unset($params['coupon_amount']);
        unset($params['discount_type']);
        $fields = $this->get_coupon_fields();
        $response_type = 'coupon_code';

        if (!empty($params['response_type'])) {
            $response_type = sanitize_text_field($params['response_type']);
            unset($params['response_type']);
        }

        if (!empty($params['email'])) {
            $params['customer_email'] = $params['email'];
            unset($params['email']);
        }

        $url_args = [];

        if (!empty($params['is_kt_invite'])) {
            $url_args['kt_invite'] = $params['is_kt_invite'];
            unset($params['is_kt_invite']);
        }

        if (!empty($params['kt_field'])) {
            $url_args['kt_field'] = sanitize_text_field($params['kt_field']);
            unset($params['kt_field']);
        }

        if (!empty($params['kt_field_link'])) {
            $url_args['kt_field_link'] = sanitize_text_field($params['kt_field_link']);
            unset($params['kt_field_link']);
        }

        if (!empty($params['user_link_to'])) {
            $user_link_to = sanitize_text_field($params['user_link_to']);

            if ('shop_page' === $user_link_to) {
                $url_args['user_link_to'] = $user_link_to;
            } else {
                if (!empty($params['user_link_to_id'])) {
                    $user_link_to_id = sanitize_text_field($params['user_link_to_id']);
                    $url_args['user_link_to_id'] = $user_link_to_id;
                    unset($params['user_link_to_id']);
                    $url_args['user_link_to'] = $user_link_to;
                }
            }

            unset($params['user_link_to']);
        }

        if (!empty($params['customer_email'])) {
            $url_args['email'] = $params['customer_email'];
        }

        $date_expires = [];

        foreach ($fields as $field => $data) {
            if (!empty($params[$field])) {
                if ($field === 'product_categories' || $field === 'exclude_product_categories') {
                    $_value = explode(',', $params[$field]);
                    $value = [];

                    if (count($_value)) {
                        foreach ($_value as $i => $cat) {
                            $value[$i] = sanitize_text_field($cat);
                        }

                        $args[$field] = $value;
                    }
                } elseif ($field === 'expiry_period' || $field === 'expiry_period_unit') {
                    $date_expires[$field] = $params[$field];
                } else {
                    $args[$field] = sanitize_text_field($params[$field]);
                }
            }
        }

        $now_ts = current_time('U');

        if (!empty($date_expires['expiry_period_unit']) && !empty($date_expires['expiry_period'])) {
            $period = $date_expires['expiry_period'] * 60;

            if ($date_expires['expiry_period_unit'] === 'days') {
                $period = $period * 60 * 24;
            } elseif ($date_expires['expiry_period_unit'] === 'hours') {
                $period = $period * 60;
            }

            $args['date_expires'] = $now_ts + $period;
            $args['expiry_period_unit'] = sanitize_text_field($date_expires['expiry_period_unit']);
            $args['expiry_period'] = sanitize_text_field($date_expires['expiry_period']);
        }

        $args['wp2leads_itm_woo_simple_coupon_link_generated'] = $now_ts;

        if (!empty($params['customer_email'])) {
            $args['customer_email'] = $params['customer_email'];
        }

        $coupon_id = Wp2leadsItmWooSimpleCouponModel::generate_coupon($coupon_code, $coupon_amount, '', $args);
        $coupon = new WC_Coupon( $coupon_id );
        $coupon_code = $coupon->get_code();

        $coupon_url = Wp2leadsItmWooSimpleCouponModel::generate_coupon_url($coupon_code, $url_args);

        if ($response_type === 'coupon_code') {
            return $coupon_code;
        }

        return $coupon_url[0];
    }

    public function permissions_check($request) {
        return true;
    }
}