<?php
/**
 * Class Wp2leadsItmWooSimpleCouponUrlBuilder
 */

class Wp2leadsItmWooSimpleCouponUrlBuilder {
    public static function init() {
        self::cpt();
    }

    public static function cpt() {
        $labels = array(
            'name'                  => _x( 'CouponURLs', 'Post Type General Name', 'wp2leads_itm_woo_simple_coupon' ),
            'singular_name'         => _x( 'CouponURL', 'Post Type Singular Name', 'wp2leads_itm_woo_simple_coupon' ),
            'menu_name'             => __( 'CouponURLs', 'wp2leads_itm_woo_simple_coupon' ),
            'name_admin_bar'        => __( 'CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'archives'              => __( 'CouponURL Archives', 'wp2leads_itm_woo_simple_coupon' ),
            'attributes'            => __( 'CouponURL Attributes', 'wp2leads_itm_woo_simple_coupon' ),
            'parent_item_colon'     => __( 'Parent CouponURL:', 'wp2leads_itm_woo_simple_coupon' ),
            'all_items'             => __( 'All CouponURLs', 'wp2leads_itm_woo_simple_coupon' ),
            'add_new_item'          => __( 'Add New CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'add_new'               => __( 'Add New', 'wp2leads_itm_woo_simple_coupon' ),
            'new_item'              => __( 'New CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'edit_item'             => __( 'Edit CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'update_item'           => __( 'Update CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'view_item'             => __( 'View CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'view_items'            => __( 'View CouponURLs', 'wp2leads_itm_woo_simple_coupon' ),
            'search_items'          => __( 'Search CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'not_found'             => __( 'Not found', 'wp2leads_itm_woo_simple_coupon' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'wp2leads_itm_woo_simple_coupon' ),
            'featured_image'        => __( 'Featured Image', 'wp2leads_itm_woo_simple_coupon' ),
            'set_featured_image'    => __( 'Set featured image', 'wp2leads_itm_woo_simple_coupon' ),
            'remove_featured_image' => __( 'Remove featured image', 'wp2leads_itm_woo_simple_coupon' ),
            'use_featured_image'    => __( 'Use as featured image', 'wp2leads_itm_woo_simple_coupon' ),
            'insert_into_item'      => __( 'Insert into CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'uploaded_to_this_item' => __( 'Uploaded to this CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'items_list'            => __( 'CouponURLs list', 'wp2leads_itm_woo_simple_coupon' ),
            'items_list_navigation' => __( 'CouponURLs list navigation', 'wp2leads_itm_woo_simple_coupon' ),
            'filter_items_list'     => __( 'Filter CouponURLs list', 'wp2leads_itm_woo_simple_coupon' ),
        );
        $args = array(
            'label'                 => __( 'CouponURL', 'wp2leads_itm_woo_simple_coupon' ),
            'description'           => __( 'Generate CouponURLs', 'wp2leads_itm_woo_simple_coupon' ),
            'labels'                => $labels,
            'supports'              => array( 'title' ),
            'hierarchical'          => false,
            'public'                => false,
            'show_ui'               => true,
            'show_in_menu'          => false,
            'menu_position'         => 5,
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => false,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => false,
            'capability_type'       => 'page',
            'show_in_rest'          => false,
        );
        register_post_type( 'wp2leads_coupon_url', $args );
    }

    public static function get($id = null) {
        $default = self::get_default();

        if (!empty($id)) {
            $settings = get_post_meta( $id, 'wp2leads_coupon_url_config', true );

            if (!empty($settings) && is_array($settings)) {
                $default = array_merge($default, $settings);
            }
        }

        // if ( empty($default['expiry_period_unit']) || 'days' !== $default['expiry_period_unit']) $default['expiry_period_unit'] = 'days';

        $url_base = rtrim(get_site_url('/'), '/') . '/wp-json/witmwoosc/v1/coupon';
        $default['url_base'] = $url_base;
        return $default;
    }

    public static function get_default() {
        return [
            'title' => '',
            'content' => '?discount_type=percent&coupon_amount=10&customer_email=%Subscriber:EmailAddress%&response_type=coupon_code',
            'expiry_period' => '',
            'expiry_period_unit' => 'days',
            'discount_type' => 'percent',
            'coupon_amount' => '10',
            'usage_limit' => '',
            'limit_usage_to_x_items' => '',
            'usage_limit_per_user' => '',
            'minimum_amount' => '',
            'maximum_amount' => '',
            'customer_email' => '__email__',
            'product_to_cart_ids' => [],
            'product_ids' => [],
            'exclude_product_ids' => [],
            'product_categories' => [],
            'exclude_product_categories' => [],
            'response_type' => 'coupon_code',
            'user_link_to' => '',
            'user_link_to_product_page' => '',
            'user_link_to_product_cat' => '',
            'user_link_to_post_page' => '',
            'kt_invite' => 'enabled',
            'kt_field' => '',
            'kt_field_link' => '',
        ];
    }

    public static function get_inactive_expiration() {
        return (int) get_option( 'wp2leads_itm_woo_simple_coupon_link_expiration', '3' );
    }

    public static function rest_api_init() {
        if (!class_exists('Wp2leadsItmWooSimpleCouponRest.php')) {
            require_once 'Wp2leadsItmWooSimpleCouponRest.php';
        }
        $controller = new Wp2leadsItmWooSimpleCouponRest();
        $controller->register_routes();
    }

    public static function maybe_redirect_to_coupon_to_kt() {
        if (
            !is_admin()
            || empty($_GET['map_name'])
            || empty($_GET['page'])
            || sanitize_text_field($_GET['page']) !== 'wp2leads_itm_woo_simple_coupon'
            || (!empty($_GET['tab']) && sanitize_text_field($_GET['tab']) === 'coupons_to_kt')
        ) return;

        $current_uri = home_url( add_query_arg( NULL, NULL ) );
        $parsed_url = parse_url($current_uri);

        $new_url = '';

        if (!empty($parsed_url["scheme"])) $new_url .= $parsed_url["scheme"] . '://';
        if (!empty($parsed_url["host"])) $new_url .= $parsed_url["host"];
        if (!empty($parsed_url["path"])) $new_url .= $parsed_url["path"];

        if (!empty($parsed_url["query"])) {
            parse_str($parsed_url["query"], $query_array);

            if (empty($query_array['tab'])) $query_array['tab'] = 'coupons_to_kt';

            $query_string = http_build_query($query_array);
            $new_url .= '?' . $query_string;
        }

        wp_redirect($new_url);
        exit;
    }

    public static function apply_coupon($coupon_code) {
        if ( ! WC()->cart->has_discount( $coupon_code ) ) {
            $applied = WC()->cart->add_discount( $coupon_code );

            if ( ! $applied ) {
                if ( ! WC()->session->has_session() ) {
                    WC()->session->set_customer_session_cookie( true );
                }
            } else {
                if ( ! WC()->session->has_session() ) {
                    WC()->session->set_customer_session_cookie( true );
                }
            }
        }
    }

    public static function maybe_unset_coupon($order_id, $posted_data, $order) {
        $coupon_code = WC()->session->get( 'kt_coupon_code' );
        if (empty(($coupon_code))) return;
        WC()->session->__unset( 'kt_coupon_code' );
    }

    public static function maybe_unset_coupon_on_remove($coupon_code_removed) {
        $coupon_code = WC()->session->get( 'kt_coupon_code' );
        if (empty(($coupon_code)) || $coupon_code_removed !== $coupon_code) return;
        WC()->session->__unset( 'kt_coupon_code' );
    }

    public static function maybe_apply_coupon() {
        if (is_admin()) return;
        if (!function_exists('WC') || empty(WC()->session)) return;
        $coupon_code = WC()->session->get( 'kt_coupon_code' );

        if ( $coupon_code && empty($_GET['kt_coupon_code']) ) {
            $coupon = new WC_Coupon( $coupon_code );
            $discounts = new WC_Discounts( WC()->cart );
            $valid_response = $discounts->is_coupon_valid( $coupon );

            if (!is_wp_error($valid_response)) {
                self::apply_coupon($coupon_code);
            }

            return;
        }

        if ( $is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return;
        }

        if ( ! isset( $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI'] ) ) {
            return;
        }

        if (empty($_GET['kt_coupon_code'])) {
            return;
        }

        $current_uri = home_url( add_query_arg( NULL, NULL ) );
        $parsed_url = parse_url($current_uri);

        $new_url = '';

        if (!empty($parsed_url["scheme"])) $new_url .= $parsed_url["scheme"] . '://';
        if (!empty($parsed_url["host"])) $new_url .= $parsed_url["host"];
        if (!empty($parsed_url["path"])) $new_url .= $parsed_url["path"];

        if (!empty($parsed_url["query"])) {
            parse_str($parsed_url["query"], $query_array);

            if (!empty($query_array['kt_coupon_code'])) {
                unset($query_array['kt_coupon_code']);
            }

            $query_array['kt_invite_nocache'] = 1;

            if (!empty($query_array)) {
                $query_string = http_build_query($query_array);
                $new_url .= '?' . $query_string;
            }
        }



        $coupon_code = sanitize_text_field($_GET['kt_coupon_code']);
        $coupon = new WC_Coupon( $coupon_code );
        $coupon_id = $coupon->get_id();
        $include_products = get_post_meta($coupon_id, 'product_to_cart_ids', true);

        if (!empty($include_products)) {
            $include_products_array = explode(',', $include_products);
            $in_cart_ids = [];

            foreach( WC()->cart->get_cart() as $cart_item ) {
                if (!empty($cart_item['variation_id'])) {
                    $in_cart_ids[] = $cart_item['variation_id'];
                }
            }

            foreach ($include_products_array as $product_id) {
                $in_cart_ids = [];

                foreach( WC()->cart->get_cart() as $cart_item ) {
                    if (!empty($cart_item['variation_id'])) {
                        $in_cart_ids[] = $cart_item['variation_id'];
                    }
                }

                $product_id = sanitize_text_field($product_id);
                $product = wc_get_product( absint( $product_id ) );
                if (! is_object( $product )) continue;

                if ( $product->is_type( array( 'variation' ) ) ) {
                    $variation_id = $product->get_id();
                    // Get variation data (attributes) for variable product
                    $attributes   = str_replace( 'attribute_', '', $product->get_variation_attributes() );
                    if( in_array($variation_id, $in_cart_ids) ) continue;
                    // Add to cart validation
                    if ( ! apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, 1, $variation_id, $attributes ) ) {
                        continue;
                    }

                    WC()->cart->add_to_cart( $product_id, 1, $variation_id, $attributes );

                    // Simple product
                } else {
                    if ( ! $product->is_type( 'variable' ) ) {
                        if ( ! apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, 1 ) ) continue;
                        $product_cart_id = WC()->cart->generate_cart_id( $product_id );
                        if( WC()->cart->find_product_in_cart( $product_cart_id ) ) continue;
                        WC()->cart->add_to_cart( $product_id );
                    } else {
                        $variations = $product->get_available_variations();
                        $variations_id = wp_list_pluck( $variations, 'variation_id' );

                        if (!empty($variations_id)) {
                            $product = wc_get_product( absint( $variations_id[0] ) );
                            $variation_id = $product->get_id();
                            $attributes   = str_replace( 'attribute_', '', $product->get_variation_attributes() );

                            if( in_array($variation_id, $in_cart_ids) ) continue;
                            // Add to cart validation
                            if ( ! apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, 1, $variation_id, $attributes ) ) {
                                continue;
                            }

                            WC()->cart->add_to_cart( $product_id, 1, $variation_id, $attributes );
                        }
                    }
                }
            }
        }

        $discounts = new WC_Discounts( WC()->cart );
        $valid_response = $discounts->is_coupon_valid( $coupon );

        if (is_wp_error($valid_response)) {
            WC()->session->set('kt_coupon_code', $coupon_code);
        } else {
            self::apply_coupon($coupon_code);
        }

        wp_redirect($new_url);
        exit;
    }

    public static function get_generated_coupons_id($type = 'all') {
        global $wpdb;

        $sql = "SELECT post_id, meta_key, meta_value FROM {$wpdb->postmeta} WHERE meta_key = 'wp2leads_itm_woo_simple_coupon_link_generated' ORDER BY post_id DESC";
        $results = $wpdb->get_results($sql, ARRAY_A);

        if (empty($results)) return false;

        $ids = [];

        foreach ($results as $result) {
            $ids[$result['post_id']]['date_generated'] = $result['meta_value'];
        }

        $ids_array = array_keys($ids);

        $in_string = "('";
        $in_string .= implode("','", $ids_array);
        $in_string .= "')";

        $sql = "SELECT post_id, meta_key, meta_value FROM {$wpdb->postmeta} WHERE meta_key = 'date_expires' AND post_id IN {$in_string}";
        $results = $wpdb->get_results($sql, ARRAY_A);

        if (!empty($results)) {
            foreach ($results as $result) {
                $ids[$result['post_id']]['date_expires'] = $result['meta_value'];
            }
        }

        $applied = [];

        // if ('expired' === trim($type)) {
            $sql = "SELECT post_id, meta_key, meta_value FROM {$wpdb->postmeta} WHERE meta_key = 'usage_count' AND post_id IN {$in_string}";
            $results = $wpdb->get_results($sql, ARRAY_A);

            foreach ($results as $result) {
                $applied[] = $result['post_id'];
            }
        // }

        $now_ts = current_time('U');
        $outdated_period = self::get_inactive_expiration();
        $outdated_period_sec = !empty($outdated_period) ? $outdated_period * 24 * 60 * 60 : false;

        foreach ($ids as $id => $data) {
            $is_expired = false;

            if (!empty($data['date_expires'])) {
                if ((int)$data['date_expires'] < $now_ts) {
                    $is_expired = true;
                }
            } elseif (!empty($outdated_period_sec)) {
                if (((int)$data['date_generated'] + $outdated_period_sec) < $now_ts) {
                    $is_expired = true;
                }

                $ids[$id]['date_expires'] = (int)$data['date_generated'] + $outdated_period_sec;
            }

            if (!empty($applied) && in_array($id, $applied)) {
                $is_expired = false;
            }

            $ids[$id]['expired'] = $is_expired;

            if ('expired' === trim($type) && !$is_expired) {
                unset($ids[$id]);
            } elseif ('active' === trim($type) && $is_expired) {
                unset($ids[$id]);
            }
        }

        return $ids;
    }

    public static function add_coupon_columns($columns) {
        $columns['coupon_link_generated'] = __( 'Auto generated', 'woocommerce' );

        return $columns;
    }

    public static function show_coupon_columns($column, $post_id) {
        if ( $column == 'coupon_link_generated' ) {
            // Author ID
            $generated_date = get_post_meta($post_id, 'wp2leads_itm_woo_simple_coupon_link_generated', true );

            // NOT empty
            if ( ! empty ( $generated_date ) ) {
                $value_formatted = date_i18n( wc_date_format(), $generated_date );
                echo $value_formatted;
            } else {
                echo '-';
            }
        }
    }
}

add_action( 'init', 'Wp2leadsItmWooSimpleCouponUrlBuilder::init', 0 );

