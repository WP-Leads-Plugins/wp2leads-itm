<?php


class Wp2leadsItmWooSimpleCouponAjax {
    public static function generate_coupon() {
        if (empty($_POST["coupon_position"])) self::error_response(array('message' => __('Cheating, hah!!!', 'wp2leads_itm_woo_simple_coupon')));
        if (empty($_POST["coupon_code"])) self::error_response(array('message' => __('Missing coupon code', 'wp2leads_itm_woo_simple_coupon')));
        if (empty($_POST["coupon_amount"])) self::error_response(array('message' => __('Missing coupon amount', 'wp2leads_itm_woo_simple_coupon')));

        $coupon = Wp2leadsItmWooSimpleCouponModel::generate_coupon(sanitize_text_field($_POST["coupon_code"]), sanitize_text_field($_POST["coupon_amount"]), sanitize_text_field($_POST["coupon_position"]) );

        $params = array(
            'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Coupon generated.', 'wp2leads_itm_woo_simple_coupon'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function delete_coupon() {
        if (empty($_POST["coupon_id"])) self::error_response(array('message' => __('Cheating, hah!!!', 'wp2leads_itm_woo_simple_coupon')));

        Wp2leadsItmWooSimpleCouponModel::delete_coupon(sanitize_text_field($_POST["coupon_id"]) );

        $params = array(
            'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Coupon deleted.', 'wp2leads_itm_woo_simple_coupon'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function generate_order() {
        try {
            $order = Wp2leadsItmWooSimpleCouponModel::generate_order();
        } catch (Exception $e) {
            $params = array(
                'message' => __('Error', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Please, try again later.', 'wp2leads_itm_woo_simple_coupon'),
            );

            self::error_response($params);
        }

        if (!$order || is_wp_error($order)) {
            $params = array(
                'message' => __('Error', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Please, try again later.', 'wp2leads_itm_woo_simple_coupon'),
            );

            self::error_response($params);
        }

        Wp2leadsItmWooSimpleCouponModel::attach_coupon_to_order($order->get_id(), array(), $order);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Order created.', 'wp2leads_itm_woo_simple_coupon'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function create_coupon_url() {
        if (empty($_POST['formData']) || !is_array($_POST['formData'])) {
            $params = array(
                'message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')
            );

            self::error_response($params);
        }

        $data = array();

        foreach ($_POST['formData'] as $form_data) {
            $data[sanitize_text_field($form_data['name'])] = $form_data['value'];
        }

        if (empty($data["content"]) || empty($data["discount_type"]) || empty($data["coupon_amount"])) {
            $params = array(
                'message' => __('Cheating, hah?', 'wp2leads_itm_webinarignition')
            );

            self::error_response($params);
        }

        if (empty($data["title"])) {
            $title = __('Coupon URL', 'wp2leads_itm_webinarignition') . ' - ' . current_time( 'mysql' );
            $data["title"] = $title;
        } else {
            $title = sanitize_text_field($data["title"]);
        }

        $content = sanitize_text_field($data["content"]);
        $new_content = "?discount_type={$data["discount_type"]}&coupon_amount={$data["coupon_amount"]}";
        if (!empty($data["expiry_period"]) && !empty($data["expiry_period_unit"])) $new_content .= "&expiry_period={$data["expiry_period"]}&expiry_period_unit={$data["expiry_period_unit"]}";
        if (!empty($data["free_shipping"])) $new_content .= "&free_shipping={$data["free_shipping"]}";
        if (!empty($data["minimum_amount"])) $new_content .= "&minimum_amount={$data["minimum_amount"]}";
        if (!empty($data["maximum_amount"])) $new_content .= "&maximum_amount={$data["maximum_amount"]}";

        if (!empty($data["customer_email"])) {
            $new_content .= "&customer_email={$data["customer_email"]}";
        }

        if (!empty($data["individual_use"])) $new_content .= "&individual_use={$data["individual_use"]}";
        if (!empty($data["exclude_sale_items"])) $new_content .= "&exclude_sale_items={$data["exclude_sale_items"]}";

        if (!empty($data["usage_limit"])) $new_content .= "&usage_limit={$data["usage_limit"]}";
        if (!empty($data["limit_usage_to_x_items"])) $new_content .= "&limit_usage_to_x_items={$data["limit_usage_to_x_items"]}";
        if (!empty($data["usage_limit_per_user"])) $new_content .= "&usage_limit_per_user={$data["usage_limit_per_user"]}";

        if (!empty($data["response_type"])) {
            $response_type = sanitize_text_field($data["response_type"]);
            $new_content .= "&response_type={$response_type}";
        } else {
            $new_content .= "&response_type=coupon_code";
        }

        if (!empty($data["user_link_to"]) && in_array(sanitize_text_field($data["user_link_to"]), ['shop_page', 'product_page', 'product_cat', 'post_page'])) {
            $user_link_to = sanitize_text_field($data["user_link_to"]);

            if ('shop_page' !== $user_link_to) {
                if (!empty(sanitize_text_field($data["user_link_to_" . $user_link_to]))) {
                    $new_content .= "&user_link_to={$user_link_to}";
                    $new_content .= "&user_link_to_id=" . sanitize_text_field($data["user_link_to_" . $user_link_to]);
                }
            } else {
                $new_content .= "&user_link_to={$user_link_to}";
            }
        }

        if (!empty($data["is_kt_invite"])) {
            $kt_invite = sanitize_text_field($data["is_kt_invite"]);

            if ('enabled' === $kt_invite) {
                $new_content .= "&is_kt_invite=1";
            }
        }

        if (!empty($data["kt_field"])) {
            $kt_field = sanitize_text_field($data["kt_field"]);
            $new_content .= "&kt_field={$kt_field}";
        }

        if (!empty($data["kt_field_link"])) {
            $kt_field_link = sanitize_text_field($data["kt_field_link"]);
            $new_content .= "&kt_field_link={$kt_field_link}";
        }

        if (!empty($data["product_to_cart_ids"])) {
            $product_ids = implode(',', $data["product_to_cart_ids"]);
            $new_content .= "&product_to_cart_ids={$product_ids}";
        }

        if (!empty($data["product_ids"])) {
            $product_ids = implode(',', $data["product_ids"]);
            $new_content .= "&product_ids={$product_ids}";
        }

        if (!empty($data["exclude_product_ids"])) {
            $product_ids = implode(',', $data["exclude_product_ids"]);
            $new_content .= "&exclude_product_ids={$product_ids}";
        }

        if (!empty($data["product_categories"])) {
            $product_categories = implode(',', $data["product_categories"]);
            $new_content .= "&product_categories={$product_categories}";
        }

        if (!empty($data["exclude_product_categories"])) {
            $product_categories = implode(',', $data["exclude_product_categories"]);
            $new_content .= "&exclude_product_categories={$product_categories}";
        }

        $data['content'] = $new_content;

        if (empty($data["id"])) {
            $post_data = array(
                'post_title'    => $title,
                'post_content'  => $new_content,
                'post_status'   => 'publish',
                'post_type'   => 'wp2leads_coupon_url',
            );

            $post_id = wp_insert_post( $post_data );
        } else {
            $post_id = sanitize_text_field($data["id"]);
            $post = get_post( $post_id );
            $post->post_content = $new_content;
            wp_update_post( $post );
        }

        update_post_meta( $post_id, 'wp2leads_coupon_url_config', $data );

        $params = array(
            'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('URL saved.', 'wp2leads_itm_woo_simple_coupon'),
            'url' => get_admin_url( null, 'admin.php?page=wp2leads_itm_woo_simple_coupon&tab=coupon_url_builder&action=edit&ID=' . $post_id ),
        );

        self::success_response($params);
    }

    public static function delete_coupon_url() {
        if (empty($_POST["id"])) self::error_response(array('message' => __('Cheating, hah!!!', 'wp2leads_itm_woo_simple_coupon')));

        $delete_result = wp_delete_post( sanitize_text_field($_POST["id"]), true );

        if (empty($delete_result)) {
            self::error_response(array('message' => __('Deletion failed, please try again later.', 'wp2leads_itm_woo_simple_coupon')));
        }

        $params = array(
            'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Coupon URL deleted.', 'wp2leads_itm_woo_simple_coupon'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function update_generated_coupons_expiration() {
        if (empty($_POST["expiration"])) self::error_response(array('message' => __('Cheating, hah!!!', 'wp2leads_itm_woo_simple_coupon')));
        $expiration = sanitize_text_field($_POST["expiration"]);
        if (!is_numeric($expiration)) self::error_response(array('message' => __('Cheating, hah!!!', 'wp2leads_itm_woo_simple_coupon')));

        update_option('wp2leads_itm_woo_simple_coupon_link_expiration', $expiration);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Outdated period updated.', 'wp2leads_itm_woo_simple_coupon'),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function delete_generated_coupons() {
        if (empty($_POST["type"])) self::error_response(array('message' => __('Cheating, hah!!!', 'wp2leads_itm_woo_simple_coupon')));
        $delete_type = sanitize_text_field($_POST["type"]);

        if (!in_array($delete_type, ['all', 'expired'])) self::error_response(array('message' => __('Cheating, hah!!!', 'wp2leads_itm_woo_simple_coupon')));

        $coupons_to_delete = Wp2leadsItmWooSimpleCouponUrlBuilder::get_generated_coupons_id($delete_type);

        if (empty($coupons_to_delete)) {
            $params = array(
                'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . __('Nothing to delete.', 'wp2leads_itm_woo_simple_coupon'),
            );

            self::success_response($params);
        }

        $coupons_to_delete_count = count($coupons_to_delete);

        $coupons_to_delete_ids = array_keys($coupons_to_delete);

        Wp2leadsItmWooSimpleCouponModel::delete_coupons_by_id($coupons_to_delete_ids);

        $params = array(
            'message' => __('Success', 'wp2leads_itm_woo_simple_coupon') . ': ' . sprintf(__('%s generated coupons deleted.', 'wp2leads_itm_woo_simple_coupon'), $coupons_to_delete_count),
            'reload' => 1,
        );

        self::success_response($params);
    }

    public static function error_response($params = array()) {
        $response = array('success' => 0, 'error' => 1);

        if (!empty($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }

    public static function success_response($params = array()) {
        $response = array('success' => 1, 'error' => 0);

        if (!empty($params) && is_array($params)) {
            $response = array_merge($response, $params);
        }

        echo json_encode($response);
        wp_die();
    }
}

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_generate', array('Wp2leadsItmWooSimpleCouponAjax', 'generate_coupon'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_generate', array('Wp2leadsItmWooSimpleCouponAjax', 'generate_coupon'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_delete', array('Wp2leadsItmWooSimpleCouponAjax', 'delete_coupon'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_delete', array('Wp2leadsItmWooSimpleCouponAjax', 'delete_coupon'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_generate_order', array('Wp2leadsItmWooSimpleCouponAjax', 'generate_order'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_generate_order', array('Wp2leadsItmWooSimpleCouponAjax', 'generate_order'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_create_coupon_url', array('Wp2leadsItmWooSimpleCouponAjax', 'create_coupon_url'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_create_coupon_url', array('Wp2leadsItmWooSimpleCouponAjax', 'create_coupon_url'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_update_coupon_url', array('Wp2leadsItmWooSimpleCouponAjax', 'create_coupon_url'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_update_coupon_url', array('Wp2leadsItmWooSimpleCouponAjax', 'create_coupon_url'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_delete_coupon_url', array('Wp2leadsItmWooSimpleCouponAjax', 'delete_coupon_url'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_delete_coupon_url', array('Wp2leadsItmWooSimpleCouponAjax', 'delete_coupon_url'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_delete_generated_coupons', array('Wp2leadsItmWooSimpleCouponAjax', 'delete_generated_coupons'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_delete_generated_coupons', array('Wp2leadsItmWooSimpleCouponAjax', 'delete_generated_coupons'));

add_action('wp_ajax_nopriv_wp2leads_itm_woo_simple_coupon_update_generated_coupons_expiration', array('Wp2leadsItmWooSimpleCouponAjax', 'update_generated_coupons_expiration'));
add_action('wp_ajax_wp2leads_itm_woo_simple_coupon_update_generated_coupons_expiration', array('Wp2leadsItmWooSimpleCouponAjax', 'update_generated_coupons_expiration'));