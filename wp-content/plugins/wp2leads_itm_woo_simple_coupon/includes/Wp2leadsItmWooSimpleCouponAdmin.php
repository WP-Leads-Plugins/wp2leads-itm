<?php
class Wp2leadsItmWooSimpleCouponAdmin {
    private static $submenues;

    public static function init() {

    }

    public static function get_subpages() {
        return [
            'coupon_url_builder' => array(
                'title' => __('Coupon & Workflow URL Builder', 'wp2leads_itm_woo_simple_coupon')
            ),
            'coupons_to_kt' => array(
                'title' => __('Simple Coupons (Old)', 'wp2leads_itm_woo_simple_coupon')
            ),
        ];
    }

    public static function add_submenu_page() {
        add_submenu_page(
            'wp2l-admin',
            __("Coupon & Easy User Workflow with KlickTipp", 'wp2leads_itm_woo_simple_coupon'),
            __("Coupon & Easy User Workflow with KlickTipp", 'wp2leads_itm_woo_simple_coupon'),
            'manage_options',
            'wp2leads_itm_woo_simple_coupon',
            'Wp2leadsItmWooSimpleCouponAdmin::display_submenu_page',
            10
        );
    }

    public static function display_submenu_page() {
        $admin_page = 'wp2leads_itm_woo_simple_coupon';
        $subpages = self::get_subpages();
        $subpages_slugs = array_keys($subpages);
        $active_tab = $subpages_slugs[0];

        if (!empty($_GET['tab']) && in_array($_GET['tab'], $subpages_slugs)) {
            $active_tab = sanitize_text_field($_GET['tab']);
        }

        include_once plugin_dir_path( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'templates/admin-page.php';
    }

    public static function enqueue_scripts() {
        if (!empty($_GET['page']) && $_GET['page'] === 'wp2leads_itm_woo_simple_coupon') {
            wp_enqueue_style( 'wp2leads-itm-select2', plugin_dir_url( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'assets/css/select2.min.css', array(), '4.0.13', 'all' );
            wp_enqueue_style( 'wp2leads-itm-woo-simple-coupon', plugin_dir_url( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'assets/css/admin.css', array(), WP2LITM_WOO_SIMPLE_COUPON_VERSION . time(), 'all' );
            wp_enqueue_script( 'wp2leads-itm-popper', plugin_dir_url( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'assets/js/popper.min.js', array( 'jquery' ), '2.11.0' . time(), true );
            wp_enqueue_script( 'wp2leads-itm-tippy', plugin_dir_url( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'assets/js/tippy.min.js', array( 'jquery' ), '6.3.7' . time(), true );
            wp_enqueue_script( 'wp2leads-itm-select2', plugin_dir_url( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'assets/js/select2.full.min.js', array( 'jquery' ), '4.0.13' . time(), true );
            wp_enqueue_script( 'wp2leads-itm-woo-simple-coupon', plugin_dir_url( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'assets/js/admin.js', array( 'jquery' ), WP2LITM_WOO_SIMPLE_COUPON_VERSION . time(), true );
            wp_localize_script(
                'wp2leads-itm-woo-simple-coupon',
                'wp2leads_itm_select2_params',
                array(
                    'select_one_item_placeholder' => __( 'Select one', 'wp2leads_itm_woo_simple_coupon' ),
                    'product_ids_placeholder' => __( 'Any product', 'wp2leads_itm_woo_simple_coupon' ),
                    'exclude_product_ids_placeholder' => __( 'Any product', 'wp2leads_itm_woo_simple_coupon' ),
                    'product_categories_placeholder' => __( 'Any category', 'wp2leads_itm_woo_simple_coupon' ),
                    'exclude_product_categories_placeholder' => __( 'No categories', 'wp2leads_itm_woo_simple_coupon' ),
                )
            );
        }
    }
}

add_action('admin_menu', array ('Wp2leadsItmWooSimpleCouponAdmin', 'add_submenu_page'));
add_action( 'admin_enqueue_scripts', array( 'Wp2leadsItmWooSimpleCouponAdmin', 'enqueue_scripts' ) );