<?php


class Wp2leadsItmWooSimpleCouponKlickTippManager {
    public static function get_kt_connector() {
        // get_option('wp2l_klicktipp_username'), get_option('wp2l_klicktipp_password')
        $username = get_option('wp2l_klicktipp_username');
        $password = get_option('wp2l_klicktipp_password');

        if (empty($username) || empty($password)) {
            return new WP_Error(
                'wp2leads_itm_woo_simple_coupon_kt_empty_credentials',
                __('Empty KT credentials', 'wp2leads_itm_woo_simple_coupon')
            );
        }

        if (!class_exists('Wp2LeadsItmWooSimpleCoupon_KlickTippConnector')) {
            include_once plugin_dir_path( WP2LITM_WOO_SIMPLE_COUPON_PLUGIN_FILE ) . 'includes/Wp2leadsItmWooSimpleCouponKlickTippConnector.php';
        }

        $connector = new Wp2leadsItmWooSimpleCouponKlickTippConnector();
        $login = $connector->login($username, $password);

        if (empty($login)) {
            return new WP_Error(
                'wp2leads_itm_woo_simple_coupon_kt_login_error',
                __('KT login error', 'wp2leads_itm_woo_simple_coupon')
            );
        }

        return $connector;
    }

    public static function get_default_kt_fields() {
        return array(
            'fieldFirstName',
            'fieldLastName',
            'fieldCompanyName',
            'fieldStreet1',
            'fieldStreet2',
            'fieldCity',
            'fieldState',
            'fieldZip',
            'fieldCountry',
            'fieldPrivatePhone',
            'fieldMobilePhone',
            'fieldPhone',
            'fieldFax',
            'fieldWebsite',
            'fieldBirthday',
            'fieldLeadValue',
        );
    }

    public static function get_kt_fields($return = 'all', $connector = null) {
        if (empty($connector)) {
            $connector = self::get_kt_connector();
        }

        if (is_wp_error($connector)) return $connector;

        $fields = $connector->field_index();

        if (false === $fields || null === $fields) {
            return new WP_Error(
                'wp2leads_itm_woo_simple_coupon_kt_http_request_error',
                __('KT Field index failed', 'wp2leads_itm_woo_simple_coupon')
            );
        }

        if ('all' === $return) {
            return $fields;
        }

        $default_kt_fields = self::get_default_kt_fields();

        $return_fields = [];

        foreach ($fields as $fid => $ftitle) {
            if ('default' === $return) {
                if (in_array($fid, $default_kt_fields)) {
                    $return_fields[$fid] = $ftitle;
                }
            } elseif ('custom' === $return) {
                if (!in_array($fid, $default_kt_fields)) {
                    $return_fields[$fid] = $ftitle;
                }

            } else {
                $return_fields[$fid] = $ftitle;
            }
        }

        return $return_fields;
    }

    public static function get_subscriber($id, $connector = null) {
        if (empty($connector)) {
            $connector = self::get_kt_connector();
        }

        if (is_wp_error($connector)) return $connector;

        $subscriber = $connector->subscriber_get($id);

        if (false === $subscriber || null === $subscriber) {
            return new WP_Error(
                'wp2leads_itm_woo_simple_coupon_kt_http_request_error',
                __('KT Subscriber get failed', 'wp2leads_itm_woo_simple_coupon')
            );
        }

        if (empty($subscriber)) return false;

        return $subscriber;
    }

    public static function get_subscriber_by_email($email, $connector = null) {
        if (empty($connector)) {
            $connector = self::get_kt_connector();
        }

        if (is_wp_error($connector)) return $connector;

        $subscriber_id = $connector->subscriber_search($email);

        if (false === $subscriber_id || null === $subscriber_id) {
            return new WP_Error(
                'wp2leads_itm_woo_simple_coupon_kt_http_request_error',
                __('KT Subscriber search failed', 'wp2leads_itm_woo_simple_coupon')
            );
        }

        if (empty($subscriber_id)) return false;

        $subscriber = self::get_subscriber($subscriber_id, $connector);

        return $subscriber;
    }

    public static function update_subscriber($id, $fields) {
        if (!is_array($fields)) return;

        if (empty($connector)) {
            $connector = self::get_kt_connector();
        }

        if (is_wp_error($connector)) return $connector;

        $update = $connector->subscriber_update($id, $fields);

        return $update;
    }
}