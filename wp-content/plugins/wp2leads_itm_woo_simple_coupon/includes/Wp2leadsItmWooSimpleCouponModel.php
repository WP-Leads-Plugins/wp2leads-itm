<?php

class Wp2leadsItmWooSimpleCouponModel {
    public static function generate_coupon_code() {
        $characters = apply_filters( 'woocommerce_coupon_code_generator_characters', 'ABCDEFGHJKMNPQRSTUVWXYZ23456789' );
        $char_length = apply_filters( 'woocommerce_coupon_code_generator_character_length', 8 );
        $prefix = apply_filters( 'woocommerce_coupon_code_generator_prefix', '' );
        $suffix = apply_filters( 'woocommerce_coupon_code_generator_suffix', '' );

        return $prefix . substr(str_shuffle($characters), 0, $char_length) . $suffix;
    }

    public static function generate_coupon($code, $amount, $position = '', $args = []) {
        $coupon = array(
            'post_title' => $code,
            'post_content' => '',
            'post_status' => 'publish',
            'post_author' => 1,
            'post_type' => 'shop_coupon',
        );

        $new_coupon_id = wp_insert_post( $coupon );

        $discount_type = 'percent';

        if (!empty($args['discount_type']) && in_array($args['discount_type'], ['percent', 'fixed_cart', 'fixed_product'])) {
            $discount_type = $args['discount_type'];
        }

        $coupon_data = array(
            'discount_type' => $discount_type,
            'coupon_amount' => $amount,
            'individual_use' => 'no',
            'free_shipping' => 'no',
            'exclude_sale_items' => 'no',
            'usage_limit' => '0',
            'usage_limit_per_user' => '0',
            'limit_usage_to_x_items' => '0',
            'usage_count' => '0',
        );

        if (!empty($position)) $coupon_data['wp2leads_itm_woo_simple_coupon'] = $position;

        if (!empty($args['individual_use'])) $coupon_data['individual_use'] = 'yes';
        if (!empty($args['exclude_sale_items'])) $coupon_data['exclude_sale_items'] = 'yes';
        if (!empty($args['free_shipping'])) $coupon_data['free_shipping'] = 'yes';

        if (!empty($args['customer_email'])) $coupon_data['customer_email'] = $args['customer_email'];
        if (!empty($args['usage_limit'])) $coupon_data['usage_limit'] = $args['usage_limit'];
        if (!empty($args['usage_limit_per_user'])) $coupon_data['usage_limit_per_user'] = $args['usage_limit_per_user'];
        if (!empty($args['limit_usage_to_x_items'])) $coupon_data['limit_usage_to_x_items'] = $args['limit_usage_to_x_items'];
        if (!empty($args['minimum_amount'])) $coupon_data['minimum_amount'] = $args['minimum_amount'];
        if (!empty($args['maximum_amount'])) $coupon_data['maximum_amount'] = $args['maximum_amount'];
        if (!empty($args['product_categories'])) $coupon_data['product_categories'] = $args['product_categories'];
        if (!empty($args['exclude_product_categories'])) $coupon_data['exclude_product_categories'] = $args['exclude_product_categories'];
        if (!empty($args['product_to_cart_ids'])) $coupon_data['product_to_cart_ids'] = $args['product_to_cart_ids'];
        if (!empty($args['product_ids'])) $coupon_data['product_ids'] = $args['product_ids'];
        if (!empty($args['exclude_product_ids'])) $coupon_data['exclude_product_ids'] = $args['exclude_product_ids'];
        if (!empty($args['date_expires'])) $coupon_data['date_expires'] = $args['date_expires'];

        if (!empty($args['expiry_period'])) $coupon_data['expiry_period'] = $args['expiry_period'];
        if (!empty($args['expiry_period_unit'])) $coupon_data['expiry_period_unit'] = $args['expiry_period_unit'];
        if (!empty($args['wp2leads_itm_woo_simple_coupon_link_generated'])) $coupon_data['wp2leads_itm_woo_simple_coupon_link_generated'] = $args['wp2leads_itm_woo_simple_coupon_link_generated'];

        // Write the $data values into postmeta table
        foreach ($coupon_data as $key => $value) {
            if (!empty($value)) {
                update_post_meta( $new_coupon_id, $key, $value );
            }
        }

        return $new_coupon_id;
    }

    public static function get_coupons() {
        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->postmeta} WHERE meta_key LIKE 'wp2leads_itm_woo_simple_coupon'";
        $result = $wpdb->get_results($sql, ARRAY_A);
        $coupons = array();

        if (!empty($result)) {
            foreach ($result as $coupon) {
                $coupon_post = get_post($coupon['post_id']);

                if (!empty($coupon_post)) {
                    $coupon_amount = get_post_meta($coupon_post->ID, 'coupon_amount', true);
                    $coupons['coupon_' . $coupon['meta_value']] = array(
                        // 'post' => $coupon_post,
                        'ID' => $coupon_post->ID,
                        'coupon_code' => $coupon_post->post_title,
                        'coupon_amount' => $coupon_amount,
                    );
                }

            }
        }

        return $coupons;
    }

    public static function delete_coupon($id) {
        wp_delete_post( $id, true );
    }

    public static function get_attached_orders() {
        global $wpdb;
        $sql = "SELECT * FROM {$wpdb->postmeta} WHERE meta_key LIKE 'order_simple_coupon_attached'";

        $result = $wpdb->get_results($sql, ARRAY_A);
        $orders = array();

        if (!empty($result)) {
            foreach ($result as $item) {
                $orders[$item['post_id']] = array(

                );
            }
        }

        return $orders;
    }

    public static function generate_order() {
        $products_array = self::get_dummy_simple_products();
        $order_max = count($products_array) - 1;
        $order_rand = rand(1, $order_max);

        $order_payments = array (
            'bacs',
            'cheque',
            'cod',
            'paypal',
        );
        $payment_max = count($order_payments) - 1;
        $payment_rand = rand(0, $payment_max);
        $payment_gateways = WC()->payment_gateways->payment_gateways();

        $order_address = array(
            'first_name' => __('John', 'wp2leads_itm_woo_simple_coupon'),
            'last_name'  => __('Smith', 'wp2leads_itm_woo_simple_coupon'),
            'company'    => __('John Smith LTD', 'wp2leads_itm_woo_simple_coupon'),
            'email'      => __('john.smith@ds.ds', 'wp2leads_itm_woo_simple_coupon'),
            'phone'      => __('+01234567891011', 'wp2leads_itm_woo_simple_coupon'),
            'address_1'  => __('Address st, 185', 'wp2leads_itm_woo_simple_coupon'),
            'address_2'  => '',
            'city'       => __('New York', 'wp2leads_itm_woo_simple_coupon'),
            'state'      => __('NY', 'wp2leads_itm_woo_simple_coupon'),
            'postcode'   => __('00100', 'wp2leads_itm_woo_simple_coupon'),
            'country'    => __('US', 'wp2leads_itm_woo_simple_coupon')
        );

        $order = wc_create_order();

        for ($i = 0; $i < $order_rand; $i++) {
            $products_array = array_values($products_array);
            $products_count = count($products_array);

            $rand = rand(0, $products_count - 1);

            $_product = $products_array[$rand];

            unset($products_array[$rand]);

            $order->add_product( wc_get_product( $_product->get_id() ), rand(1, 10) );
        }

        $order->set_address( $order_address, 'billing' );
        $order->set_address( $order_address, 'shipping' );
        $order->set_payment_method( $payment_gateways[$order_payments[$payment_rand]] );
        $order->calculate_totals();
        $order->set_created_via( 'checkout' );
        $order->update_status('on-hold', 'Lorem ipsum dolor sit amet, est at principes intellegat');

        return $order;
    }

    public static function get_dummy_simple_products() {
        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => -1
        );

        $posts_array = get_posts( $args );

        $products_array = array();

        foreach ($posts_array as $post_item) {
            $product_object = wc_get_product($post_item->ID);

            //if ('simple' === $product_object->get_type() || 'variable' === $product_object->get_type()) {
            if ('simple' === $product_object->get_type()) {
                $products_array[] = $product_object;
            }
        }

        return $products_array;
    }

    public static function attach_coupon_to_order($order_id, $order_data, $order) {
        $coupons = self::get_coupons();
        if (empty($coupons)) return;

        $order = wc_get_order($order_id);
        if (empty($order)) return;

        $email = $order->get_billing_email();
        if (empty($email)) return;

        foreach ($coupons as $i => $coupon) {
            $coupon_emails = get_post_meta($coupon['ID'], 'customer_email', true);

            if (!empty($coupon_emails)) {
                if (!in_array($email, $coupon_emails)) {
                    $coupon_emails[] = $email;
                }
            } else {
                $coupon_emails = array();
                $coupon_emails[] = $email;
            }

            update_post_meta( $coupon['ID'], 'customer_email', $coupon_emails );
            update_post_meta( $order_id, 'order_simple_' . $i, $coupon['coupon_code'] );
        }

        update_post_meta( $order_id, 'order_simple_coupon_attached', '1' );
        update_post_meta( $order_id, 'order_simple_coupon_attached_array', $coupons );

        do_action('wp2leads_itm_woo_simple_coupon_attached', $order_id);
    }

    public static function generate_coupon_url($coupon_code, $url_args) {
        $urls = [];
        $url = get_site_url('/');

        if (!empty($url_args['email'])) {
             foreach ($url_args['email'] as $email) {
                $subscriber = Wp2leadsItmWooSimpleCouponKlickTippManager::get_subscriber_by_email($email);

                if (is_wp_error($subscriber) || empty($subscriber) || empty($subscriber->id)) {
                    $subscriber = false;
                    if (!empty($url_args['kt_invite'])) unset($url_args['kt_invite']);
                }
                 $url = get_site_url('/');

                 if (!empty($url_args["user_link_to"])) {
                    if ('shop_page' === $url_args["user_link_to"]) {
                        $url = wc_get_page_permalink( 'shop' );
                    } elseif (!empty($url_args["user_link_to_id"])) {
                        $id = (int) sanitize_text_field($url_args["user_link_to_id"]);

                        if ('product_page' === $url_args["user_link_to"]) {
                            $product = wc_get_product( $id );

                            if (!empty($product)) {
                                $url = $product->get_permalink();
                            }
                        } elseif ('product_cat' === $url_args["user_link_to"]) {
                            $link = get_term_link( $id, 'product_cat' );

                            if (!empty($link) && !is_wp_error($link)) {
                                $url = $link;
                            }
                        } elseif ('post_page' === $url_args["user_link_to"]) {
                            $link = get_permalink( $id );

                            if (!empty($link) && !is_wp_error($link)) {
                                $url = $link;
                            }
                        }
                    }
                 }

                 if (strpos($url, '?') !== false) {
                     $url .= '&kt_coupon_code=' . $coupon_code;
                 } else {
                     $url .= '?kt_coupon_code=' . $coupon_code;
                 }

                if ($subscriber) {
                    if (!empty($url_args['kt_invite'])) $url .= '&kt_invite=' . $subscriber->id;
                    $fields_to_update = [];

                    if (!empty($url_args["kt_field"]) && property_exists($subscriber, $url_args["kt_field"]))  {
                        $fields_to_update[$url_args["kt_field"]] = $coupon_code;
                    }

                    if (!empty($url_args["kt_field_link"]) && property_exists($subscriber, $url_args["kt_field_link"])) {
                        $fields_to_update[$url_args["kt_field_link"]] = $url;
                    }

                    if (!empty($fields_to_update)) {
                        Wp2leadsItmWooSimpleCouponKlickTippManager::update_subscriber($subscriber->id, $fields_to_update);
                    }
                }

                 $urls[] = $url;
             }
        } else {
            $url = get_site_url('/');

            if (!empty($url_args["user_link_to"])) {
                if ('shop_page' === $url_args["user_link_to"]) {
                    $url = wc_get_page_permalink( 'shop' );
                } elseif (!empty($url_args["user_link_to_id"])) {
                    $id = (int) sanitize_text_field($url_args["user_link_to_id"]);

                    if ('product_page' === $url_args["user_link_to"]) {
                        $product = wc_get_product( $id );

                        if (!empty($product)) {
                            $url = $product->get_permalink();
                        }
                    } elseif ('product_cat' === $url_args["user_link_to"]) {
                        $link = get_term_link( $id, 'product_cat' );

                        if (!empty($link) && !is_wp_error($link)) {
                            $url = $link;
                        }
                    } elseif ('post_page' === $url_args["user_link_to"]) {
                        $link = get_permalink( $id );

                        if (!empty($link) && !is_wp_error($link)) {
                            $url = $link;
                        }
                    }
                }
            }

            if (strpos($url, '?') !== false) {
                $url .= '&kt_coupon_code=' . $coupon_code;
            } else {
                $url .= '?kt_coupon_code=' . $coupon_code;
            }

            $urls[] = $url;
        }

        return $urls;
    }

    public static function delete_coupons_by_id($ids = []) {
        if (empty($ids)) return;

        if (is_string($ids)) {
            $id = $ids;
            $ids = [];
            $ids[] = $id;
        }

        if (!is_array($ids)) return;

        global $wpdb;

        $in_string = "('";
        $in_string .= implode("','", $ids);
        $in_string .= "')";
        $sql_meta = "DELETE FROM {$wpdb->postmeta} WHERE post_id IN {$in_string}";
        $sql_coupon = "DELETE FROM {$wpdb->posts} WHERE ID IN {$in_string}";

        $wpdb->query($sql_meta);
        $wpdb->query($sql_coupon);

        return true;
    }
}