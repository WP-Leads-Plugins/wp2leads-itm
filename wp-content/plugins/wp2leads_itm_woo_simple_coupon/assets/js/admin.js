(function( $ ) {
    var select2_args = {
        width: '100%',
        minimumInputLength: 3,
        ajax: {
            url:         ajaxurl,
            dataType:    'json',
            delay:       250,
            data:        function( params ) {
                return {
                    term         : params.term,
                    action       : $( this ).data( 'action' ) || 'woocommerce_json_search_products_and_variations',
                    security     : $( this ).data( 'security' ),
                    exclude      : $( this ).data( 'exclude' ),
                    exclude_type : $( this ).data( 'exclude_type' ),
                    include      : $( this ).data( 'include' ),
                    limit        : $( this ).data( 'limit' ),
                    display_stock: $( this ).data( 'display_stock' )
                };
            },
            processResults: function( data ) {
                var terms = [];
                if ( data ) {
                    $.each( data, function( id, text ) {
                        terms.push( { id: id, text: text } );
                    });
                }
                return {
                    results: terms
                };
            },
            cache: true
        }
    };

    $(document.body).on('click', '.wp2leads_itm_woo_simple_coupon_generate', function() {
        var coupon_position = $(this).data('position');
        var coupon_code = $('#wp2leads_itm_woo_simple_coupon_generate_'+coupon_position+'_code').val();
        var coupon_amount = $('#wp2leads_itm_woo_simple_coupon_generate_'+coupon_position+'_amount').val();

        var data = {
            action: 'wp2leads_itm_woo_simple_coupon_generate',
            coupon_code: coupon_code,
            coupon_amount: coupon_amount,
            coupon_position: coupon_position,
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '.copy_field_content', function() {
        var control = $(this);
        var parent = control.parents('.copy_field_content_row');
        var toCopy = parent.find('.copy_field').val();

        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(toCopy).select();
        document.execCommand("copy");
        $temp.remove();

        control.hide();

        setTimeout(function() {
            control.show();
        }, 2000);
    });

    $(document.body).on('click', '.wp2leads_itm_woo_simple_coupon_delete', function() {
        var coupon_id = $(this).data('coupon');

        var data = {
            action: 'wp2leads_itm_woo_simple_coupon_delete',
            coupon_id: coupon_id,
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#wp2leads_itm_woo_simple_coupon_generate_order', function() {

        var data = {
            action: 'wp2leads_itm_woo_simple_coupon_generate_order',
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#update_generated_coupons_expiration_btn', function() {
        var expiration = $('#update_generated_coupons_expiration').val();

        ajaxRequest(
            {
                action: 'wp2leads_itm_woo_simple_coupon_update_generated_coupons_expiration',
                expiration: expiration
            },
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#delete_all_generated_coupons', function() {
        ajaxRequest(
            {
                action: 'wp2leads_itm_woo_simple_coupon_delete_generated_coupons',
                type: 'all'
            },
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#delete_expired_generated_coupons', function() {
        ajaxRequest(
            {
                action: 'wp2leads_itm_woo_simple_coupon_delete_generated_coupons',
                type: 'expired'
            },
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '#create_coupon_url_btn', function() {
        var formData = get_form_data();

        var data = {
            action: 'wp2leads_itm_woo_simple_coupon_create_coupon_url',
            formData: formData,
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document.body).on('click', '.delete_coupon_url_btn', function() {
        var btn = $(this);
        var id = btn.data('id');
        var confirm_message = btn.data('confirm');
        var confirmed = confirm(confirm_message);

        if (confirmed) {
            var data = {'action': 'wp2leads_itm_woo_simple_coupon_delete_coupon_url', id: id};
            ajaxRequest(data, function() {}, function() {});
        }
    });

    $(document.body).on('change', '.coupon_url_form select.form-input', function() {
        var control = $(this);
        var control_id = control.attr('id');

        if ('user_link_to' === control_id) {
            change_user_link_to_field();
        }

        update_coupon_url();
    });

    $(document.body).on('change keyup paste mouseup', '.coupon_url_form input[name="expiry_period"]', function() {
        var value = $(this).val();
        var expiry_period_unit = $('#expiry_period_unit');

        if (
            $.trim(value) === 0
            || $.trim(value) === '0'
            || $.trim(value) === ''
        ) {
            expiry_period_unit.attr("disabled", true);
        } else {
            expiry_period_unit.attr("disabled", false);
        }
    });

    $(document.body).on('change', 'select[name="discount_type"]', function() {
        var value = $(this).val();
        var limit_usage_to_x_items = $('#limit_usage_to_x_items');

        console.log(value);

        if (
            value === 'fixed_cart'
        ) {
            limit_usage_to_x_items.attr("disabled", true).addClass('disabled');
        } else {
            limit_usage_to_x_items.attr("disabled", false).removeClass('disabled');
        }
    });

    $(document.body).on('change keyup paste mouseup', '.coupon_url_form input[type="number"], .coupon_url_form input[type="text"]', function() {
        update_coupon_url();
    });

    function change_user_link_to_field() {
        var control = $('#user_link_to');
        var value = control.val();

        var containers = $('.user_link_to_value_container');

        if (containers.length) {
            containers.each(function() {
                $( this ).hide();
            });
        }

        var container = $('#user_link_to_'+value+'_container');

        if (container.length) {
            container.show();
        }
    }

    function update_product_search_items() {
        var product_to_cart_ids = $('#product_to_cart_ids').val();
        var product_ids = $('#product_ids').val();
        var exclude_product_ids = $('#exclude_product_ids').val();

        product_ids = product_ids.concat(product_to_cart_ids);

        var unique_product_ids = product_ids.filter(function(value, index, self) {
            return self.indexOf(value) === index;
        });

        $('#product_to_cart_ids').data('exclude', exclude_product_ids);
        $('#product_ids').data('exclude', exclude_product_ids);
        $('#exclude_product_ids').data('exclude', product_ids);

        var search_products = $('.form-input-select2__product_search');

        if (search_products.length) {
            search_products.each(function() {

                $( this ).select2(select2_args).on("change", update_coupon_url);
            });
        }
    }

    function update_coupon_url() {
        setTimeout(function() {
            update_product_search_items();
            var formData = get_form_data();

            var content = '';
            var expiry_period = false;
            var discount_type = '';
            var user_link_to = '';

            $.each(formData, function( index, field ) {
                var name = field.name;
                var value = field.value;

                if (
                    name === 'id'
                    || name === 'title'
                    || name === 'content'
                    || $.trim(value) === 0
                    || $.trim(value) === '0'
                    || $.trim(value) === ''
                    || (name === 'is_kt_invite' && 'enabled' !== $.trim(value))
                    || (name === 'user_link_to' && 'home_page' === $.trim(value))
                    || (name === 'user_link_to_product_page' && 'product_page' !== $.trim(user_link_to))
                    || (name === 'user_link_to_post_page' && 'post_page' !== $.trim(user_link_to))
                    || (name === 'user_link_to_product_cat' && 'product_cat' !== $.trim(user_link_to))
                ) {
                    return '';
                }

                if (name === 'kt_invite') value = '1';
                if (name === 'user_link_to') user_link_to = value;
                if (name === 'expiry_period') expiry_period = true;
                if (name === 'discount_type') discount_type = value;
                if (name === 'limit_usage_to_x_items' && discount_type === 'fixed_cart') return '';
                if (name === 'expiry_period_unit' && !expiry_period) return '';

                if (content === '') content += '?';
                else content += '&';

                if (
                    name === 'user_link_to_product_page'
                    || name === 'user_link_to_post_page'
                    || name === 'user_link_to_product_cat'
                ) {
                    name = 'user_link_to_id';
                }

                content += name + '=';

                if (
                    name === 'product_to_cart_ids'
                    || name === 'product_ids'
                    || name === 'exclude_product_ids'
                    || name === 'product_categories'
                    || name === 'exclude_product_categories'
                ) {
                    if (value.length) {
                        content += value.join(',');
                    }
                } else {
                    content += value;
                }
            });

            var coupon_url_base = $('#coupon_url_base').val();
            var url = coupon_url_base + content;

            $('#coupon_content').val(content);
            $('#coupon_url_complete').val(url);
        }, 100);
    }

    function get_form_data() {
        var formData = $('.coupon_url_form').serializeArray();

        var product_to_cart_ids = $('#product_to_cart_ids').val();
        var product_ids = $('#product_ids').val();
        var exclude_product_ids = $('#exclude_product_ids').val();
        var product_categories = $('#product_categories').val();
        var exclude_product_categories = $('#exclude_product_categories').val();

        if (product_to_cart_ids.length) formData.push({name: 'product_to_cart_ids', value: product_to_cart_ids});
        if (product_ids.length) formData.push({name: 'product_ids', value: product_ids});
        if (exclude_product_ids.length) formData.push({name: 'exclude_product_ids', value: exclude_product_ids});
        if (product_categories.length) formData.push({name: 'product_categories', value: product_categories});
        if (exclude_product_categories.length) formData.push({name: 'exclude_product_categories', value: exclude_product_categories});

        return formData;
    }

    $(document.body).on('click', '#update_coupon_url_btn', function() {
        var formData = get_form_data();

        var data = {
            action: 'wp2leads_itm_woo_simple_coupon_update_coupon_url',
            formData: formData,
        };

        ajaxRequest(
            data,
            function() {},
            function() {}
        );
    });

    $(document).ready(function () {
        var search_products = $('.form-input-select2__product_search');

        if (search_products.length) {
            search_products.each(function() {

                $( this ).select2(select2_args).on("change", update_coupon_url);
            });

        }
        $('.form-input-select2__multiple').select2({
            width: '100%',
            minimumResultsForSearch: 20,
        }).on("change", update_coupon_url);
        $('.form-input-select2').select2({
            width: '100%',
            placeholder: wp2leads_itm_select2_params.select_one_item_placeholder,
            allowClear: true
        }).on("change", update_coupon_url);
        $('.form-input-select2__product_ids').select2({
            width: '100%',
            placeholder: wp2leads_itm_select2_params.product_ids_placeholder
        }).on("change", update_coupon_url);
        $('.form-input-select2__exclude_product_ids').select2({
            width: '100%',
            placeholder: wp2leads_itm_select2_params.exclude_product_ids_placeholder
        }).on("change", update_coupon_url);
        $('.form-input-select2__product_categories').select2({
            width: '100%',
            placeholder: wp2leads_itm_select2_params.product_categories_placeholder
        }).on("change", update_coupon_url);
        $('.form-input-select2__exclude_product_categories').select2({
            width: '100%',
            placeholder: wp2leads_itm_select2_params.exclude_product_categories_placeholder
        }).on("change", update_coupon_url);

        tippy('.tippy-button', {
            content(reference) {
                var id = reference.getAttribute('data-tippy-template');
                var template = document.getElementById(id);
                return template.innerHTML;
            },
            allowHTML: true,
            theme: 'light-border',
            placement: 'bottom',
            interactive: true
        });
    });

    function periodical_worker() {
        var data = {
            action: 'wp2leads_itm_woo_leadvalue_calculation_progress',
        };

        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function(response) {
                var decoded;

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        $('#wp2leads_itm_woo_calculation_progress').text(decoded.progress_message);
                        $('.wptl-progress-bar span').css('width', decoded.progress_percent + '%')

                        if (decoded.progress_reload) {
                            setTimeout(function() {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                } else {

                }
            },
            complete: function() {
                // Schedule the next request when the current one's complete
                setTimeout(periodical_worker, 500);
            }
        });
    }

    function ajaxRequest(data, cb, cbError) {
        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function (response) {
                var decoded;

                console.log(response);

                try {
                    decoded = $.parseJSON(response);
                } catch(err) {
                    console.log(err);
                    decoded = false;
                }

                if (decoded) {
                    if (decoded.success) {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (decoded.url) {
                            window.location.replace(decoded.url);
                        } else if (decoded.reload) {
                            window.location.reload();
                        }

                        if (typeof cb === 'function') {
                            cb();
                        }
                    } else {
                        if (decoded.message) {
                            alert(decoded.message);
                        }

                        if (typeof cbError === 'function') {
                            cbError();
                        }
                    }
                } else {
                    alert('Something went wrong');
                }
            }
        });
    }
})( jQuery );
