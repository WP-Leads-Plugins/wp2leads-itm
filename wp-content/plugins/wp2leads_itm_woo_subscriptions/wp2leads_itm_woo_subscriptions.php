<?php
/**
 * Plugin Name:     Wp2Leads Instant transfer module for WooCommerce Subscriptions plugin
 * Description:
 * Version:         1.0.0
 * Author:          Tobias Conrad
 * Author URI:      https://saleswonder.biz/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     wp2leads_itm_woo_subscriptions
 *
 * Requires at least: 5.0
 * Tested up to: 5.4
 *
 * Domain Path:     /languages
 */

if ( ! defined( 'WPINC' ) ) die;

define( 'WP2LITM_WOO_SUBSCRIPTION_VERSION', '1.0.0' );

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://saleswonder.biz/wp2leads-itm/wp2leads_itm_woo_subscriptions.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'wp2leads_itm_woo_subscriptions'
);

require_once('functions.php');

add_action( 'plugins_loaded', 'wp2leads_itm_woo_subscriptions_load_plugin_textdomain' );

// If not met requirement do not run
if (!wp2leads_itm_woo_subscriptions_requirement()) return;

wp2leads_itm_woo_subscriptions_modules_init();