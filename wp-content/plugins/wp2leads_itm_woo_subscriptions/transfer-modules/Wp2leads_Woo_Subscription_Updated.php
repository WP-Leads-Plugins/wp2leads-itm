<?php


if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Wp2leads_Woo_Subscription_Updated {
    private static $key = 'wp2leads_woo_subscription_updated';
    private static $required_column = 'posts.ID';

    public static function get_label() {
        return __('Woocommerce Subscription', 'wp2leads_itm_woo_subscriptions');
    }

    public static function get_description() {
        return __('This module will transfer user data to KT once Woocommerce Subscription created or status changed', 'wp2leads_itm_woo_subscriptions');
    }

    public static function get_required_column() {
        return self::$required_column;
    }

    public static function get_instruction() {
        ob_start();
        ?>
        <p><?php _e('This module is created for Woocommerce Subscriptions.', 'wp2leads_itm_woo_subscriptions') ?></p>
        <p><?php _e('Once new Woocommerce Subscriptions created or existed updated user data will be transfered to KT account.', 'wp2leads_itm_woo_subscriptions') ?></p>
        <p><?php _e('Requirement: <strong>posts.ID</strong> column within selected data.', 'wp2leads_itm_woo_subscriptions') ?></p>
        <?php

        return ob_get_clean();
    }

    public static function transfer_init() {
        add_action('woocommerce_checkout_subscription_created', 'Wp2leads_Woo_Subscription_Updated::subscription_object_updated', 30);
        add_action('woocommerce_subscription_status_updated', 'Wp2leads_Woo_Subscription_Updated::subscription_object_updated', 30);
        add_action('woocommerce_scheduled_subscription_payment', 'Wp2leads_Woo_Subscription_Updated::subscription_id_updated', 30);
        add_action('woocommerce_scheduled_subscription_trial_end', 'Wp2leads_Woo_Subscription_Updated::subscription_id_updated', 30);
    }

    public static function subscription_object_updated($subscription) {
        $subscription_id = $subscription->get_id();

        if (!wp2leads_itm_woo_subscriptions_is_renewal($subscription_id)) {
            self::transfer($subscription_id);
        }
    }

    public static function subscription_id_updated($subscription_id) {
        if (!wp2leads_itm_woo_subscriptions_is_renewal($subscription_id)) {
            self::transfer($subscription_id);
        }
    }

    public static function transfer($id) {
        $existed_modules_map = Wp2leads_Transfer_Modules::get_modules_map();

        $condition = array(
            'tableColumn' => self::$required_column,
            'conditions' => array(
                0 => array(
                    'operator' => 'like',
                    'string' => (string) $id
                )
            )
        );

        if (!empty($existed_modules_map[self::$key])) {
            foreach ($existed_modules_map[self::$key] as $map_id => $status) {
                $result = Wp2leads_Background_Module_Transfer::module_transfer_bg($map_id, $condition);
            }
        }
    }
}

function wp2leads_woo_subscription_updated($transfer_modules) {
    $transfer_modules['wp2leads_woo_subscription_updated'] = 'Wp2leads_Woo_Subscription_Updated';

    return $transfer_modules;
}

add_filter('wp2leads_transfer_modules', 'wp2leads_woo_subscription_updated');